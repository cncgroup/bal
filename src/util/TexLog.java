package util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class TexLog {
	
	protected ArrayList<String> text;
	
	public TexLog() {
		text = new ArrayList<String>();
	}
	
	public void addLine(String line) {
		text.add(line);
	}
	
	public void addFigure(String path, String caption, String label, double widthratio) {
		text.add("\\begin{figure}[!htbp]");
		text.add("\\center");
		text.add("\\includegraphics[width="+widthratio+"\\textwidth]{"+path+"}");
		if (caption.length() > 0)
			text.add("\\caption{"+caption+"}");
		if (label.length() > 0)
			text.add("\\label{"+label+"}");
		text.add("\\end{figure}");	
	}
	
	public void addFigure(String[] path, String caption, String label, double widthratio) {
		text.add("\\begin{figure}[!htbp]");
		text.add("\\center");
		for (int i = 0; i < path.length; i++) {
			text.add("\\includegraphics[width="+widthratio+"\\textwidth]{"+path[i]+"}");
		}
		if (caption.length() > 0)
			text.add("\\caption{"+caption+"}");
		if (label.length() > 0)
			text.add("\\label{"+label+"}");
		text.add("\\end{figure}");	
	}

	public void addSubFigure(String path, String caption, String label, double widthratio) {
		text.add("\\begin{subfigure}[b]{0.45\\textwidth}");
		text.add("\\centering");
		text.add("\\includegraphics[width="+widthratio+"\\textwidth]{"+path+"}");
		if (caption.length() > 0)
			text.add("\\caption{"+caption+"}");
		if (label.length() > 0)
			text.add("\\label{"+label+"}");
		text.add("\\end{subfigure}");	
	}
	
	public void addItemize(String[] items) {
		text.add("\\begin{itemize}");
		for (int i = 0; i < items.length; i++) {
			text.add("\t\\item "+items[i]);
		}
		text.add("\\end{itemize}");
	}
	
	public void addTable(String[][] data, String caption, String label) {
		text.add("\\begin{table}[!htbp]");
		text.add("\\center");
		if (caption.length() > 0)
			text.add("\\caption{"+caption+"}");
		if (label.length() > 0)
			text.add("\\label{"+label+"}");
		String cols = "";
		for (int i = 0; i < data[0].length; i++) {
			cols += "|c";
		}
		cols += "|";
		text.add("\\begin{tabular}[t]{"+cols+"}");
		text.add("\\hline");
		for (int i = 0; i < data.length; i++) {
			String row = "";
			for (int j = 0; j < data[i].length; j++) {
				row += data[i][j] + " & "; 
			}
			row = row.substring(0,row.length() - 3);
			text.add(row+"\\\\");
			text.add("\\hline");
		}
		text.add("\\end{tabular}");
		text.add("\\end{table}");
		text.add("");
	}
	
	public void addResultsTable(String[] labels, String[][] data, String caption, String label) {
		text.add("\\begin{table}[!htbp]");
		text.add("\\center");
		if (caption.length() > 0)
			text.add("\\caption{"+caption+"}");
		if (label.length() > 0)
			text.add("\\label{"+label+"}");
		String cols = "";
		String labs = "";
		for (int i = 0; i < labels.length; i++) {
			cols += "|c";
			labs += labels[i] + " & ";
		}
		cols += "|";
		labs = labs.substring(0,labs.length() - 3);
		text.add("\\begin{tabular}[t]{"+cols+"}");
		text.add("\\hline");
		text.add(labs+"\\\\");
		text.add("\\hline");
		for (int i = 0; i < data.length; i++) {
			String row = "";
			for (int j = 0; j < data[i].length; j++) {
				row += data[i][j] + " & "; 
			}
			row = row.substring(0,row.length() - 3);
			text.add(row+"\\\\");
			text.add("\\hline");
		}
		text.add("\\end{tabular}");
		text.add("\\end{table}");
	}
	
	public void addResultsTable(String[] labels, double[][] data, String caption, String label) {
		text.add("\\begin{table}[!htbp]");
		text.add("\\center");
		if (caption.length() > 0)
			text.add("\\caption{"+caption+"}");
		if (label.length() > 0)
			text.add("\\label{"+label+"}");
		String cols = "";
		String labs = "";
		for (int i = 0; i < labels.length; i++) {
			cols += "|c";
			labs += labels[i] + " & ";
		}
		cols += "|";
		labs = labs.substring(0,labs.length() - 3);
		text.add("\\begin{tabular}[t]{"+cols+"}");
		text.add("\\hline");
		text.add(labs+"\\\\");
		text.add("\\hline");
		for (int i = 0; i < data.length; i++) {
			String row = "";
			for (int j = 0; j < data[i].length; j++) {
				row += data[i][j] + " & "; 
			}
			row = row.substring(0,row.length() - 3);
			text.add(row+"\\\\");
			text.add("\\hline");
		}
		text.add("\\end{tabular}");
		text.add("\\end{table}");
	}
	
	public void addPlotPaper(String[][] plotData,String labels[],String labX, String labY) {
		text.add("\\begin{tikzpicture}");
		text.add("\\begin{axis}[");
		text.add("legend pos = outer north east,");
		if (labX.length() > 0) 
			text.add("\txlabel={"+labX+"},");
		if (labY.length() > 0) 
			text.add("\tylabel={"+labY+"},");
		text.add("\theight=6cm,");
		text.add("\twidth=10cm,");
		text.add("\tgrid=major,");
		text.add("]");
		for (int i = 0; i < plotData.length; i++) {
			text.add("\\addplot plot[error bars/.cd, y dir=both, y explicit]");
			text.add("coordinates {");
			for (int j = 0; j < plotData[i].length; j++) {
				text.add(plotData[i][j]);
			}
			text.add("};");
			if (labels != null && labels[i].length() > 0)
				text.add("\\addlegendentry{"+labels[i]+"}");
		}
		text.add("\\end{axis}");
		text.add("\\end{tikzpicture}");
	}
			
	public void addPlot(String[][] plotData,String labels[],String labX, String labY) {
		text.add("\\begin{tikzpicture}");
		text.add("\\begin{axis}[");
		if (labX.length() > 0) 
			text.add("\txlabel={"+labX+"},");
		if (labY.length() > 0) 
			text.add("\tylabel={"+labY+"},");
		text.add("\theight=7cm,");
		text.add("\twidth=7cm,");
		text.add("\tgrid=major,");
		text.add("]");
		for (int i = 0; i < plotData.length; i++) {
			text.add("\\addplot coordinates {");
			for (int j = 0; j < plotData[i].length; j++) {
				if (plotData[i][j] != null)
					text.add(plotData[i][j]);
			}
			text.add("};");
			if (labels != null && labels[i].length() > 0)
				text.add("\\addlegendentry{"+labels[i]+"}");
		}
		text.add("\\end{axis}");
		text.add("\\end{tikzpicture}");
	}
	
	public void addPlot(String[][] plotData,String labels[], String[] plotFormats, String labX, String labY) {
		text.add("\\begin{tikzpicture}");
		text.add("\\begin{axis}[");
		if (labX.length() > 0) 
			text.add("\txlabel={"+labX+"},");
		if (labY.length() > 0) 
			text.add("\tylabel={"+labY+"},");
		text.add("\theight=7cm,");
		text.add("\twidth=7cm,");
		text.add("\tgrid=major,");
		text.add("]");
		for (int i = 0; i < plotData.length; i++) {
			text.add("\\addplot["+plotFormats[i]+"] coordinates {");
			for (int j = 0; j < plotData[i].length; j++) {
				text.add(plotData[i][j]);
			}
			text.add("};");
			if (labels != null && labels[i].length() > 0)
				text.add("\\addlegendentry{"+labels[i]+"}");
		}
		text.add("\\end{axis}");
		text.add("\\end{tikzpicture}");
	}
	
	public void addPlotSmall(String[][] plotData, String labX, String labY) {
		text.add("\\begin{tikzpicture}");
		text.add("\\begin{axis}[");
		if (labX.length() > 0) 
			text.add("\txlabel={"+labX+"},");
		if (labY.length() > 0) 
			text.add("\tylabel={"+labY+"},");
		text.add("\theight=5cm,");
		text.add("\twidth=5cm,");
		text.add("\tgrid=major,");
		text.add("]");
		for (int i = 0; i < plotData.length; i++) {
			text.add("\\addplot[mark=none] coordinates {");
			for (int j = 0; j < plotData[i].length; j++) {
				text.add(plotData[i][j]);
			}
			text.add("};");
		}
		text.add("\\end{axis}");
		text.add("\\end{tikzpicture}");
	}
	
	public void addPlotErrBars(String[][] plotData,String labels[],String labX, String labY) {
		text.add("\\begin{tikzpicture}");
		text.add("\\begin{axis}[");
		if (labX.length() > 0) 
			text.add("\txlabel={"+labX+"},");
		if (labY.length() > 0) 
			text.add("\tylabel={"+labY+"},");
		text.add("\theight=7cm,");
		text.add("\twidth=7cm,");
		text.add("\tgrid=major,");
		text.add("]");
		for (int i = 0; i < plotData.length; i++) {
			text.add("\\addplot plot[error bars/.cd, y dir=both, y explicit]");
			text.add("coordinates {");
			for (int j = 0; j < plotData[i].length; j++) {
				text.add(plotData[i][j]);
			}
			text.add("};");
			if (labels != null && labels[i].length() > 0)
				text.add("\\addlegendentry{"+labels[i]+"}");
		}
		text.add("\\end{axis}");
		text.add("\\end{tikzpicture}");
	}

	public void addPlot3D(String[][] plotData,String labels[],String labX, String labY) {
		text.add("\\begin{tikzpicture}");
		text.add("\\begin{axis}[");
		if (labX.length() > 0)
			text.add("\txlabel={"+labX+"},");
		if (labY.length() > 0)
			text.add("\tylabel={"+labY+"},");
		text.add("\theight=7cm,");
		text.add("\twidth=7cm,");
		text.add("\tgrid=major,");
		text.add("]");
		for (int i = 0; i < plotData.length; i++) {
			text.add("\\addplot coordinates {");
			for (int j = 0; j < plotData[i].length; j++) {
				if (plotData[i][j] != null)
					text.add(plotData[i][j]);
			}
			text.add("};");
			if (labels != null && labels[i].length() > 0)
				text.add("\\addlegendentry{"+labels[i]+"}");
		}
		text.add("\\end{axis}");
		text.add("\\end{tikzpicture}");
	}


	public void addLog(TexLog log2) {
		text.addAll(log2.text);
	}
	
	public void export(String filepath) throws IOException {
		ArrayList<String> temp = new ArrayList<String>();
		temp.add("\\documentclass[11pt]{article}");
		temp.add("\\usepackage[authoryear]{natbib}");
		temp.add("\\usepackage{multirow}");
		temp.add("\\usepackage{graphicx}");
		temp.add("\\usepackage[utf8]{inputenc}");
		temp.add("\\usepackage[T1]{fontenc}");
		temp.add("\\usepackage{pgfplots}");
		temp.add("\\usepackage{caption}");
		temp.add("\\usepackage{subcaption}");
		temp.add("\\usepackage[margin=2.5cm]{geometry}");
		text.add("");
		temp.add("\\title{Bidirectional Activation-based Learning Algorithm: Experiment Protocol}");
//		temp.add("\\author{Krist{\\'i}na Rebrov{\\'a}}");
		temp.add("\\author{Krist{\\'i}na Malinovsk{\\'a}}");
		temp.add("\\date{}");
		text.add("");
		temp.add("\\begin{document}");
		temp.add("\\maketitle");
		text.add("");
		temp.addAll(text);
		text.add("");
		temp.add("\\end{document}");
		
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(filepath)));
		for (int i = 0; i < temp.size(); i++) {
			if (temp.get(i) != null) {
				bw.write(temp.get(i));
				bw.newLine();
			}
		}
		bw.close();		
	}
	
	public void exportPartial(String filepath) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(filepath)));
		for (int i = 0; i < text.size(); i++) {
			bw.write(text.get(i));
			bw.newLine();
		}
		bw.close();		
	}
	
	public void echo() {
		for (int i = 0; i < text.size(); i++) {
			System.out.println(text.get(i));
		}		
	}
}
