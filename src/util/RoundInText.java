package util;

import java.io.*;
import java.util.ArrayList;

public class RoundInText {

	public static void main(String[] args) throws IOException {
		String path = "tmp";
		BufferedReader br = new BufferedReader(new FileReader(new File(path)));
		String line = "";
		ArrayList<String> text = new ArrayList<String>();
		while ((line = br.readLine()) != null) {
			String[] temp = line.split(" ");
			String newLine = "";
			for (int i = 0; i < temp.length; i++) {
				try {
					newLine += Util.round(Double.parseDouble(temp[i]),3) + " ";
				}
				catch(NumberFormatException e) {
					newLine += temp[i]+" ";
				}
			}
			text.add(newLine);
		}
		br.close();
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(path)));
		for (int i = 0; i < text.size(); i++) {
			bw.write(text.get(i));
			bw.newLine();
		}
		bw.close();
	}
	
}
