package util;

import java.util.ArrayList;
import java.util.Random;

import util.Quicksort;

public class Util {

	public static final double binTreshold = 0.5;

	public static double pow(double a, int x) {
		if (x == 0)
			return 1;
		else {
			double out = a;
			for (int i = 0; i < x; i++) {
				out *= a;
			}
			return out;
		}
	}

	public static int max(int a, int b) {
		return (a > b) ? a : b;
	}

	public static double randomGauss(double mean, double variance, Random r) {
		return mean + r.nextGaussian() * variance;
	}

///////////// STATISTICS //////////////////////////////////////////////////////

	public static double[] MeanAndStD(double[] input) {
		double[] result = new double[2];
		double temp = 0.0;
		for (int i = 0; i < input.length; i++) {
			temp += input[i];
		}
		temp = temp / input.length;
		// mean
		result[0] = temp;
		temp = 0.0;
		for (int i = 0; i < input.length; i++) {
			temp += (input[i] - result[0]) * (input[i] - result[0]);
		}
		temp = Math.sqrt(temp / input.length);
		// std
		result[1] = temp;
		return result;
	}

	public static double[] MeanAndStD(ArrayList<Double> input) {
		double[] result = new double[2];
		result[0] = 0.0;
		result[1] = 0.0;
		if (input.size() > 0) {
			double temp = 0.0;
			for (double val : input) {
				temp += val;
			}
			temp = temp / input.size();
			// mean
			result[0] = temp;
			temp = 0.0;
			for (double val : input) {
				temp += (val - result[0]) * (val - result[0]);
			}
			temp = Math.sqrt(temp / input.size());
			// std
			result[1] = temp;
		}
		return result;
	}

	public static double[] MeanAndStD(int[] input) {
		double[] result = new double[2];
		double temp = 0.0;
		for (int i = 0; i < input.length; i++) {
			temp += input[i];
		}
		temp = temp / input.length;
		// mean
		result[0] = temp;
		temp = 0.0;
		for (int i = 0; i < input.length; i++) {
			temp += (input[i] - result[0]) * (input[i] - result[0]);
		}
		temp = Math.sqrt(temp / input.length);
		// std
		result[1] = temp;
		return result;
	}

	public static int maxIndex(double[] input) {
		int m = 0;
		for (int i = 0; i < input.length; i++) {
			if (input[m] < input[i])
				m = i;
		}
		return m;
	}

	public static int minIndex(double[] input) {
		int m = 0;
		for (int i = 0; i < input.length; i++) {
			if (input[m] > input[i])
				m = i;
		}
		return m;
	}

	public static double median(double[] input) {
		double[] tmp = input.clone();
		Quicksort sort = new Quicksort();
		sort.sort(tmp);
		return tmp[tmp.length / 2];
	}

	public static double round(double value, int decimalPlace) {
		double power_of_ten = 1;
		while (decimalPlace-- > 0)
			power_of_ten *= 10.0;
		return Math.round(value * power_of_ten) / power_of_ten;
	}

	public static double max(double[] input) {
		double max = 0.0;
		for (Double x : input) {
			max = (x > max) ? x : max;
		}
		return max;
	}

	public static double max(double[][] input) {
		double max = 0.0;
		for (int i = 0; i < input.length; i++) {
			for (int j = 0; j < input[i].length; j++) {
				max = (input[i][j] > max) ? input[i][j] : max;
			}
		}
		return max;
	}

	public static int max(int[] input) {
		int max = 0;
		for (int x : input) {
			max = (x > max) ? x : max;
		}
		return max;
	}

	public static double max(ArrayList<Double> input) {
		double max = 0.0;
		for (Double x : input) {
			max = (x > max) ? x : max;
		}
		return max;
	}

	public static double min(ArrayList<Double> input) {
		double min = 10.0;
		for (Double x : input) {
			min = (x < min) ? x : min;
		}
		return min;
	}

///////////// VECTOR DISTANCE //////////////////////////////////////////////////////

	public static double distanceEuclid(double[] inp1, double[] inp2) {
		double out = 0.0;
		for (int i = 0; i < inp1.length; i++) {
			out += (inp1[i] - inp2[i]) * (inp1[i] - inp2[i]);
		}
		out = Math.sqrt(out);
		return out;
	}

	public static double distanceEuclid(double[][] inp1, double[][] inp2) {
		double out = 0.0;
		for (int i = 0; i < inp1.length; i++) {
			for (int j = 0; j < inp1[i].length; j++) {
				out += (inp1[i][j] - inp2[i][j]) * (inp1[i][j] - inp2[i][j]);
			}
		}
		out = Math.sqrt(out);
		return out;
	}

	public static double distanceGauss(double sigma, double numerator) {
		double g = 0;
		g = Math.exp(-(numerator / (2 * pow(sigma, 2))));
		return g;
	}

	public static double distanceMexicanHat(double sigma, double numerator) {
		double m = 0;
		m = (2 * Math.PI) / Math.sqrt(sigma)
				* (1 - 2 * Math.PI * numerator / pow(sigma, 2))
				* Math.exp(-(numerator / (pow(sigma, 2))));
		return m;
	}

///////////// VECTOR FUNCTIONS //////////////////////////////////////////////////////

	public static int[] permutedIndexes(int count) {
		int[] indexes = new int[count];
		for (int i = 0; i < count; i++) {
			indexes[i] = i;
		}
		for (int i = indexes.length - 1; i > 0; i--) {
			int x = indexes[i];
			int randIndex = (int) (Math.random() * (double) i);
			indexes[i] = indexes[randIndex];
			indexes[randIndex] = x;
		}
		return indexes;
	}

	public static int[] removeAtIndex(int[] inp, int i) {
		int[] out = new int[inp.length-1];
		int index = 0;
		for (int j = 0; j < inp.length; j++) {
			if (j != i) {
				out[index] = inp[j];
				index++;
			}
		}
		return out;
	}

	public static int[] duplicateAtIndex(int[] inp, int index) {
		int[] out = new int[inp.length+1];
		for (int i = 0; i < inp.length; i++) {
			if (i <= index)
				out[i] = inp[i];
			else
				out[i+1] = inp[i];
		}
		return out;
	}

	public static double[] add(double[] in1, double[] in2) {
		double[] out = new double[in1.length];
		for (int i = 0; i < out.length; i++) {
			out[i] = in1[i] + in2[i];
		}
		return out;
	}

	public static double[] copy(double[] in) {
		double[] out = new double[in.length];
		for (int i = 0; i < out.length; i++) {
			out[i] = in[i];
		}
		return out;
	}

	public static boolean identical(double[] first, double[] second) {
		boolean out = true;
		if (first.length != second.length) {
			return false;
		} else {
			for (int i = 0; i < second.length; i++) {
				if (first[i] != second[i])
					out = out & false;
			}
		}
		return out;
	}

	public static boolean identical(int[] first, int[] second) {
		boolean out = true;
		if (first.length != second.length) {
			return false;
		} else {
			for (int i = 0; i < second.length; i++) {
				if (first[i] != second[i])
					out = out & false;
			}
		}
		return out;
	}

	public static double[] scale(double[] sample) {
		double min = sample[0];
		double max = sample[0];
		double[] out = new double[sample.length];
		for (int i = 1; i < sample.length; i++) {
			min = (sample[i] < min) ? sample[i] : min;
			max = (sample[i] > max) ? sample[i] : max;
		}
		for (int i = 0; i < sample.length; i++) {
			if (max == min)
				out[i] = 0.5;
			else
				out[i] = (sample[i] - min) / (max - min);
		}
		return out;
	}

    public static double[] newBinCodeProb(int length, double probPosBit) {
        double[] out = new double[length];
        int ones = 0;
        for (int i = 0; i < length; i++) {
            if (Math.random()<probPosBit && ones < (length/2)) {
                out[i] = 1;
                ones++;
            } else
                out[i] = 0;
        }
        return out;
    }

    public static double[] newBinCodeMax(int length, int maxTrueBits) {
        double[] out = new double[length];
        int ones = 0;
        for (int i = 0; i < length; i++) {
            double prob = maxTrueBits/(length-i*1.0);
            if (Math.random() < prob && ones < maxTrueBits) {
                out[i] = 1;
                ones++;
            } else
                out[i] = 0;
        }
        return out;
    }


///////////// BINARIZATION /////////////////////////////////////////////////////////

	public static double[] binarize(double[] input) {
		double[] out = new double[input.length];
		for (int i = 0; i < input.length; i++) {
			if (input[i] >= binTreshold)
				out[i] = 1.0;
			else
				out[i] = 0.0;
		}
		return out;
	}

	public static String arrayToBinaryString(double[] input) {
		String out = "";
		for (int i = 0; i < input.length; i++) {
			if (input[i] >= binTreshold)
				out += "1";
			else
				out += "0";
		}
		return out;
	}

	public static String arrayToBinaryString(int[] input) {
		String out = "";
		for (int i = 0; i < input.length; i++) {
			if (input[i] >= binTreshold)
				out += "1";
			else
				out += "0";
		}
		return out;
	}

	public static double[] intArrayToDbl(int[] input) {
		double[] out = new double[input.length];
		for (int i = 0; i < out.length; i++) {
			out[i] = input[i];
		}
		return out;
	}

	public static int positiveBits(double[] input) {
		int out = 0;
		for (int i = 0; i < input.length; i++) {
			if (input[i] >= binTreshold)
				out++;
		}
		return out;
	}

	public static double[] binaryAndPositiveBits(double[] input1,
												 double[] input2) {
		double[] out = new double[input1.length];
		for (int i = 0; i < input1.length; i++) {
			if ((input1[i] >= binTreshold && input2[i] >= binTreshold))
				out[i] = 1.0;
			else
				out[i] = 0.0;
		}
		return out;
	}

	public static double binaryResemblance(double[] input) {
		double out = 0.0;
		for (int i = 0; i < input.length; i++) {
			if (input[i] >= binTreshold)
				out += Math.abs(1.0 - input[i]);
			else
				out += Math.abs(0.0 - input[i]);
		}
		out /= input.length;
		return out;
	}

///////////// ARRAY FUNCTIONS //////////////////////////////////////////////////////////////////

	public static double sum(ArrayList<Double> input) {
		double sum = 0.0;
		for (double val : input) {
			sum += val;
		}
		return sum;
	}

	public static String[][] subArray(String[][] input, int start, int end) {
		String[][] out = new String[end - start + 1][input[0].length];
		for (int i = start; i < (end + 1); i++) {
			out[i - start] = input[i];
		}
		return out;
	}

    public static double[][] subArray(double[][] input, int start, int end) {
        double[][] out = new double[end - start + 1][input[0].length];
        for (int i = start; i < (end + 1); i++) {
            out[i - start] = input[i];
        }
        return out;
    }

	public static double[] flatten(double[][] input) {
		double[] out = new double[input.length * input[0].length];
		for (int i = 0; i < input.length; i++) {
			for (int j = 0; j < input[i].length; j++) {
				out[input[0].length * i + j] = input[i][j];
			}
		}
		return out;
	}

    public static double[][] transpose(double[][] input) {
        double[][] out = new double[input[0].length][input.length];
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input[0].length; j++) {
                out[j][i] = input[i][j];
            }
        }
        return out;
    }

	public static double[][] flatten2(double[][][] input) {
		int length = 0;
		for (int i = 0; i < input.length; i++) {
			length += input[i].length;
		}
		double[][] out = new double[length][input[0][0].length];
		int it = 0;
		for (int i = 0; i < input.length; i++) {
			for (int j = 0; j < input[i].length; j++) {
				out[it] = input[i][j];
				it++;
			}
		}
		return out;
	}

	public static double[] merge(double[] in1, double[] in2) {
		double[] out = new double[in1.length + in2.length];
		int it = 0;
		for (int i = 0; i < in1.length; i++) {
			out[it] = in1[i];
			it++;
		}
		for (int i = 0; i < in2.length; i++) {
			out[it] = in2[i];
			it++;
		}
		return out;
	}

	public static double[][] merge(double[][] in1, double[][] in2) {
		double[][] out = new double[in1.length + in2.length][in1[0].length];
		int it = 0;
		for (int i = 0; i < in1.length; i++) {
			out[it] = in1[i];
			it++;
		}
		for (int i = 0; i < in2.length; i++) {
			out[it] = in2[i];
			it++;
		}
		return out;
	}

	public static double[][][] merge(double[][][] in1, double[][][] in2) {
		double[][][] out = new double[in1.length + in2.length][in1.length][in1[0][0].length];
		for (int i = 0; i < in1.length; i++) {
			out[i] = new double[in1[i].length][in1[i][0].length];
			out[i] = in1[i];
			out[in1.length + i] = new double[in2[i].length][in2[i][0].length];
			out[in1.length + i] = in2[i];
		}
		return out;
	}

	public static double[][] merge2(double[][] arr1, double[][] arr2) {
		double[][] out = new double[arr1.length][arr1[0].length	+ arr2[0].length];
		int x = 0;
		for (int i = 0; i < arr1.length; i++) {
			for (int j = 0; j < arr1[i].length; j++) {
				out[i][x + j] = arr1[i][j];
			}
		}
		x += arr1[0].length;
		for (int i = 0; i < arr2.length; i++) {
			for (int j = 0; j < arr2[i].length; j++) {
				out[i][x + j] = arr2[i][j];
			}
		}
		return out;
	}


//	public static void normalize(double[][] data, double nAvg, double nDev) {
//		double[] avgs = new double[data[0].length];
//		for (int i = 0; i < data.length; i++) {
//			for (int j = 0; j < avgs.length; j++) {
//				avgs[j] += data[i][j];
//			}
//		}
//		for (int i = 0; i < avgs.length; i++) {
//			avgs[i] = avgs[i] / data.length;
//		}
//		double[] avgDev = new double[data[0].length];
//		for (int i = 0; i < data.length; i++) {
//			for (int j = 0; j < avgs.length; j++) {
//				avgDev[j] += Math.abs(data[i][j] - avgs[j]);
//			}
//		}
//		for (int i = 0; i < avgDev.length; i++) {
//			avgDev[i] = avgDev[i] / data.length;
//		}
//		for (int i = 0; i < data.length; i++) {
//			for (int j = 0; j < avgs.length; j++) {
//				data[i][j] = (nAvg + (((data[i][j]) - avgs[j]) * nDev) / avgDev[j]);
//			}
//		}
//	}

	public static void scale(double[][] data) {
		for (double[] sample : data) {
			double min = sample[0];
			double max = sample[0];
			for (int i = 1; i < sample.length; i++) {
				min = (sample[i] < min) ? sample[i] : min;
				max = (sample[i] > max) ? sample[i] : max;
			}
			for (int i = 0; i < sample.length; i++) {
				if (max == min)
					sample[i] = 0.5;
				else
					sample[i] = (sample[i] - min) / (max - min);
			}
		}
	}

	public static void flip(double[][] input) {
		for (int i = 0; i < input.length; i++) {
			for (int j = 0; j < input[i].length; j++) {
				input[i][j] = 1.0 - input[i][j];
			}
		}
	}

	public static int[][] permutedIndexes2D(int count, int batchCount) {
		int[] indexes = new int[count];
		for (int i = 0; i < count; i++) {
			indexes[i] = i;
		}
		for (int i = indexes.length - 1; i > 0; i--) {
			int x = indexes[i];
			int randIndex = (int) (Math.random() * (double) i);
			indexes[i] = indexes[randIndex];
			indexes[randIndex] = x;
		}
		int[][] indexes2D = new int[batchCount][count];
		int sCount = count / batchCount;
		for (int b = 0; b < batchCount; b++) {
			for (int i = 0; i < sCount; i++) {
				indexes2D[b][i] = indexes[b * sCount + i];
				if (b * sCount + i + 1 >= indexes.length)
					break;
			}
		}
		return indexes2D;
	}

	public static double[][] rearrange(double[][] in, int[] indexer) {
	    double[][] out = new double[in.length][];
        for (int i = 0; i < indexer.length; i++) {
            out[i] = in[indexer[i]];
        }
        return out;
    }

///////////// STRING FUNCTIONS ////////////////////////////////////////////////////////////////////

	public static String arrayToStringRounded(double[] input, int decplac) {
		String out = "[";
		for (int i = 0; i < input.length; i++) {
			out += round(input[i], decplac) + ", ";
		}
		out = out.substring(0, out.length() - 2) + "]";
		return out;
	}

	public static double[] strToDouble(String input, String delimiter) {
		String[] temp = input.split(delimiter);
		double[] out = new double[temp.length];
		for (int i = 0; i < temp.length; i++) {
			out[i] = Double.parseDouble(temp[i]);
		}
		return out;
	}

	public static double[] strToDouble(String input) {
		double[] out = new double[input.length()];
		for (int i = 0; i < input.length(); i++) {
			out[i] = Double.parseDouble(input.charAt(i) + "");
		}
		return out;
	}

	public static String[] addStringFront(String[] input, String item) {
		String[] out = new String[input.length + 1];
		out[0] = item;
		for (int i = 0; i < input.length; i++) {
			out[i + 1] = input[i];
		}
		return out;
	}

	public static String[] addStringBack(String[] input, String item) {
		String[] out = new String[input.length + 1];
		for (int i = 0; i < input.length; i++) {
			out[i] = input[i];
		}
		out[input.length] = item;
		return out;
	}

	public static String[] merge(String[] a, String[] b) {
		String[] out = new String[a.length + b.length];
		for (int i = 0; i < a.length; i++) {
			out[i] = a[i];
		}
		for (int i = 0; i < b.length; i++) {
			out[i + a.length] = b[i];
		}
		return out;
	}

	public static String[] removeAtIndex(String[] inp, int i) {
		String[] out = new String[inp.length - 1];
		int index = 0;
		for (int j = 0; j < inp.length; j++) {
			if (j != i) {
				out[index] = inp[j];
				index++;
			}
		}
		return out;
	}

}
