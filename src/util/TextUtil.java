package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class TextUtil {

	public static int[] lineCountAndLength(String pathIn) {
		int[] out = new int[2];
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(new File(pathIn)));
			String line = br.readLine(); 
			out[1] = line.split(" ").length;
			out[0] = 1;
			while ((line = br.readLine()) != null &&  !"".equals(line)) {
				out[0]++;
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return out;
	}
	
	
}
