package util.visu.bal;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import base.Conf;
import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class DataAssocAll extends JFrame {
	private static final long serialVersionUID = 1L;
	public static final int sizeV = 14;
	public static final int sizeM = 8;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final String pathVisual = "input/msom/visual_"+sizeV+"_"+sizeV+".txt";
	public static final String pathMotor = "input/msom/motor_"+sizeM+"_"+sizeM+".txt";
	
	DataRoboticVis dataVis;
	DataRoboticMot dataMot;
	protected int sizeX;
	protected int sizeY;
	protected int pxs;
	protected int marg;
	protected int sizeVis;
	protected int sizeMot;
	
	public DataAssocAll(int pxs, int marg, DataRoboticVis dataV, DataRoboticMot dataM) {
		//initialize
		this.pxs = pxs;
		this.marg = marg;
		dataVis = dataV;
		dataMot = dataM;
		sizeVis = (int) Math.sqrt(dataVis.patternSize());
		sizeMot = (int) Math.sqrt(dataMot.patternSize());
		sizeX = Conf.movements * (Conf.perspectives+1) * (sizeVis+2)*pxs+3*marg;
		sizeY = dataMot.dataSeq()[0].size() * (sizeVis+3)*pxs+2*marg;
	}

	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, sizeX, sizeY);
		g.setColor(Color.black);
		Font font = new Font("Serif", Font.PLAIN, 9);
		g.setFont(font);
		int posX = marg;
		int posXrow = marg;
		int posY = marg;
		if (dataVis != null) {
			for (int mov = 0; mov < Conf.movements; mov++) {
				for (int sample = 0; sample < dataMot.dataSeq()[mov].size(); sample++) {
					posX = posXrow;
					String codeM = Util.arrayToBinaryString(dataMot.dataSeq()[mov].get(sample));
					for (int x = 0; x < sizeMot; x++) {
						for (int y = 0; y < sizeMot; y++) {
							g.drawRect(posX+pxs*x, posY+pxs*y, pxs, pxs);
							if (codeM.charAt(x * sizeMot + y) == '1') {
								g.setColor(new Color(Conf.colors2D[mov][0][0],Conf.colors2D[mov][0][1],Conf.colors2D[mov][0][2]));
								g.fillRect(posX+pxs*x, posY+pxs*y, pxs, pxs); 
								g.setColor(Color.black);
							}
						}
					}
					g.drawString("M "+(sample+1), posX, posY+(sizeMot+2)*pxs);
					posX += (sizeMot+2)*pxs;
					for (int persp = 0; persp < Conf.perspectives; persp++) {
						if (sample < dataVis.dataSeq()[mov][persp].size()) {
							String codeV = Util.arrayToBinaryString(dataVis.dataSeq()[mov][persp].get(sample));
							for (int x = 0; x < sizeVis; x++) {
								for (int y = 0; y < sizeVis; y++) {
									g.drawRect(posX+pxs*x, posY+pxs*y, pxs, pxs);
									if (codeV.charAt(x * sizeVis + y) == '1') {
										g.setColor(new Color(Conf.colors2D[mov][persp][0],Conf.colors2D[mov][persp][1],Conf.colors2D[mov][persp][2]));
										g.fillRect(posX+pxs*x, posY+pxs*y, pxs, pxs); 
										g.setColor(Color.black);
									}
								}
							}
							g.drawString("V "+(sample+1)+"-"+persp, posX, posY+(sizeVis+2)*pxs);
							posX += (sizeVis+2)*pxs;
						}
					}
					posY += (sizeVis+3)*pxs;
				}
				posY = marg;
				posXrow += 5*(sizeVis+2)*pxs+marg;
			}
		}
	}
	
	public void saveImage(String path) {
		BufferedImage image = new BufferedImage(sizeX,sizeY,BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = image.createGraphics();
		this.paint(graphics2D);
		try {
			File newfile = new File(path);
			if(!newfile.exists())
				newfile.createNewFile();
			ImageIO.write(image, "png", newfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void display() {
		this.setSize(sizeX,sizeY);
		this.setVisible(true);
	}

	public static void main(String[] arg) throws Exception {
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		DataAssocAll v = new DataAssocAll(3,10,dataVis,dataMot);
		v.display();
		v.saveImage(Conf.pathOutput+"data-kwta-associations-all.png");
	}
	
}