package util.visu.bal;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import base.Conf;

import util.visu.DataPattern;

import data.DataRoboticMot;
import data.DataRoboticVis;

public class DataPairsFPConti extends JFrame {
	private static final long serialVersionUID = 1L;
	public static final int sizeV = 14;
	public static final int sizeM = 8;
	public static final int kV = 16;
	public static final int kM = 16;
	public static final String pathVisual = "input/msom/visual_"+sizeV+"_"+sizeV+".txt";
	public static final String pathMotor = "input/msom/motor_"+sizeM+"_"+sizeM+".txt";
	public static final int pixelSize = 4;
	public static final int margin = 20;
	public static final int rowHeight = 8;
	
	private int sizeX;
	private int sizeY;
	private int pxs;
	private int marg;
	private int rowH;
	private DataPattern[] dataVis;
	private DataPattern[] dataMot;
	
	public DataPairsFPConti(int pxs, int marg, int rowH, DataRoboticVis dV, DataRoboticMot dM) {
		this.pxs = pxs;
		this.marg = marg;
		this.rowH = rowH;
		//data
		dataVis = new DataPattern[dV.sampleCountFP()];
		dataMot = new DataPattern[dM.sampleCount()];
		int c = 0;
		for (int m = 0; m < Conf.movements; m++) {
			for (double[] pat : dV.dataSeq()[m][0]) {
				dataVis[c] = new DataPattern(sizeV, kV, pat, Conf.colors[m]);
				c++;
			}
		}
		c = 0;
		for (int m = 0; m < Conf.movements; m++) {
			for (double[] pat : dM.dataSeq()[m]) {
				dataMot[c] = new DataPattern(sizeM, kM, pat, Conf.colors[m]);
				c++;
			}
		}
		int rowW = (dataVis.length % rowH == 0) ? dataVis.length/rowH : dataVis.length/rowH+1;
		sizeX = rowW*2*(sizeV+3)*pxs;
		sizeY = rowH*(sizeV+3)*pxs+3*marg;
	}

	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, sizeX, sizeY);
		g.setColor(Color.black);
		Font font = new Font("Serif", Font.PLAIN, 9);
		g.setFont(font);
		int posX = marg;
		int posXrow = marg;
		int posY = marg;
		int rowCounter = 1;
		for (int id = 0; id < dataVis.length; id++) {
			if (rowCounter > rowH) {
				posY = marg;
				posXrow += 2*(sizeV+2)*pxs+marg/2;
				rowCounter = 1;
				g.drawLine(posXrow-7, 0, posXrow-7, sizeY);
			} else if (rowCounter > 1) {
				posY += (sizeV+3)*pxs;
			}
			posX = posXrow;
			dataVis[id].paintMixed(g, posX, posY, pxs);
			posX += (sizeV+2)*pxs;
			dataMot[id].paintMixed(g, posX, posY, pxs);
			rowCounter++;	
		}
	}
	
	public void saveImage(String path) {
		BufferedImage image = new BufferedImage(sizeX,sizeY,BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = image.createGraphics();
		this.paint(graphics2D);
		try {
			File newfile = new File(path);
			if(!newfile.exists())
				newfile.createNewFile();
			ImageIO.write(image, "png", newfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void display() {
		this.setSize(sizeX,sizeY);
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
 		DataPairsFPConti vis = new DataPairsFPConti(pixelSize, margin, rowHeight, dataVis, dataMot);
 		vis.display();
	}
}