package util.visu.bal;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import base.Conf;
import base.NetUtil;
import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class DataPairsContiA extends JFrame {
	private static final long serialVersionUID = 1L;
	public static final int sizeV = 14;
	public static final int sizeM = 8;
	public static final int kV[] = {10,25,50};
	public static final int kM[] = {5,15,30};
	public static final String pathVisual = "input/msom/visual_"+sizeV+"_"+sizeV+".txt";
	public static final String pathMotor = "input/msom/motor_"+sizeM+"_"+sizeM+".txt";
	public static final int pixelSize = 3;
	public static final int margin = 10;
	public static final int rowHeight = 14;
	
	private int sizeX;
	private int sizeY;
	private int pxs;
	private int marg;
	private int rowH;
	private double[][] dataVis;
	private double[][] dataMot;
	private Color[] colors;
	
	public DataPairsContiA(int pxs, int marg, int rowH, double[][] dataVis, double[][] dataMot, int[] sequences) {
		this.pxs = pxs;
		this.marg = marg;
		this.rowH = rowH; 
		this.dataVis = dataVis;
		this.dataMot = dataMot;
		//data
		int rowW = (dataVis.length % rowH == 0) ? dataVis.length/rowH : dataVis.length/rowH+1;
		sizeX = rowW*2*(sizeV+3)*pxs;
		sizeY = rowH*(sizeV+3)*pxs+3*marg;
		colors = new Color[dataMot.length];
		int ind = 0;
		for (int i = 0; i < sequences.length; i++) {
			for (int j = 0; j < sequences[i]; j++) {
				colors[ind] = Conf.colors[i];
				ind++;
			}
		}
	}

	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, sizeX, sizeY);
		g.setColor(Color.black);
		Font font = new Font("Serif", Font.PLAIN, 9);
		g.setFont(font);
		int posX = marg;
		int posXrow = marg;
		int posY = marg;
		int rowCounter = 1;
		for (int sample = 0; sample < dataVis.length; sample++) {
			if (rowCounter > rowH) {
				posY = marg;
				posXrow += 2*(sizeV+2)*pxs+marg/2;
				rowCounter = 1;
				g.drawLine(posXrow-7, 0, posXrow-7, sizeY);
			} else if (rowCounter > 1) {
				posY += (sizeV+3)*pxs;
			}
			posX = posXrow;
			HashSet<Integer> kwta1 = NetUtil.kWTAMinIndices(dataVis[sample], kV[0]);
			HashSet<Integer> kwta2 = NetUtil.kWTAMinIndices(dataVis[sample], kV[1]);
			HashSet<Integer> kwta3 = NetUtil.kWTAMinIndices(dataVis[sample], kV[2]);
			for (int i = 0; i < sizeV; i++) {
				for (int j = 0; j < sizeV; j++) {
					double tval = dataVis[sample][i*sizeV+j];
					Color tcol = new Color(
						(int)((1.0-tval)*colors[sample].getRed()+tval*255),
						(int)((1.0-tval)*colors[sample].getGreen()+tval*255),
						(int)((1.0-tval)*colors[sample].getBlue()+tval*255)
					);
					if (kwta3.contains(i*sizeV+j)) {
						tcol = new Color(200,200,200);
					}
					if (kwta2.contains(i*sizeV+j)) {
						tcol = new Color(100,100,100);
					}
					if (kwta1.contains(i*sizeV+j)) {
						tcol = Color.black;
					}
					g.setColor(tcol);
					g.fillRect(posX+pxs*i, posY+pxs*j, pxs, pxs); 
					g.setColor(Color.black);
					g.drawRect(posX+pxs*i, posY+pxs*j, pxs, pxs);				
				}
			}
			g.drawString("vis"+(sample+1), posX, posY+(sizeV+2)*pxs);
			posX += (sizeV+2)*pxs;
			kwta1 = NetUtil.kWTAMinIndices(dataMot[sample], kM[0]);
			kwta2 = NetUtil.kWTAMinIndices(dataMot[sample], kM[1]);
			kwta3 = NetUtil.kWTAMinIndices(dataMot[sample], kM[2]);
			for (int i = 0; i < sizeM; i++) {
				for (int j = 0; j < sizeM; j++) {
					double tval = dataMot[sample][i*sizeM+j];
					Color tcol = new Color(
						(int)(tval*colors[sample].getRed()+(1-tval)*200),
						(int)(tval*colors[sample].getGreen()+(1-tval)*200),
						(int)(tval*colors[sample].getBlue()+(1-tval)*200)
					);
					if (kwta3.contains(i*sizeV+j)) {
						tcol = new Color(200,200,200);
					}
					if (kwta2.contains(i*sizeV+j)) {
						tcol = new Color(100,100,100);
					}
					if (kwta1.contains(i*sizeV+j)) {
						tcol = Color.black;
					}
					g.setColor(tcol);
					g.fillRect(posX+pxs*i, posY+pxs*j, pxs, pxs); 
					g.setColor(Color.black);
					g.drawRect(posX+pxs*i, posY+pxs*j, pxs, pxs);
				}
			}
			g.drawString("mot"+(sample+1), posX, posY+(sizeV+2)*pxs);
			rowCounter++;	
		}
	}
	
	public void saveImage(String path) {
		BufferedImage image = new BufferedImage(sizeX,sizeY,BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = image.createGraphics();
		this.paint(graphics2D);
		try {
			File newfile = new File(path);
			if(!newfile.exists())
				newfile.createNewFile();
			ImageIO.write(image, "png", newfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void display() {
		this.setSize(sizeX,sizeY);
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
//		double[][] dataV = dataVis.valuesFirstPersp();
		double[][] dataV = dataVis.values();
		dataMot.multiply();
		double[][] dataM = dataMot.values();
		Util.scale(dataV);
		Util.scale(dataM);
//		for (int i = 0; i < dataV.length; i++) {
//			System.out.println(Util.arrayToStringRounded(dataV[i], 3));
//		}
 		DataPairsContiA vis = new DataPairsContiA(pixelSize, margin, rowHeight, dataV, dataM, dataMot.sequences());
 		vis.display();
// 		vis.saveImage(Conf.pathOutput+"datapairs-gradient-FP.png");
 		vis.saveImage(Conf.pathOutput+"datapairs-gradient.png");
	}
}