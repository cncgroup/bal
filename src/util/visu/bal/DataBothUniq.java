package util.visu.bal;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import data.DataRoboticMot;
import data.DataRoboticVis;


public class DataBothUniq extends JFrame {
	private static final long serialVersionUID = 1L;
	public static final int[] visParams = {600, 860, 4, 22, 46, 10};
	
	private TreeMap<String,Integer> valuesVis = new TreeMap<String,Integer>();
	private TreeMap<String,Integer> valuesMot = new TreeMap<String,Integer>();
	private int sX;
	private int sY;
	private int pxs;
	private int marg;
	private int sVis;
	private int sMot;
	private int rowS;
	
	public DataBothUniq(int rowS, int pxs, int marg, double[][] dataVis, double[][] dataMot) {
		this.rowS = rowS;
		this.pxs = pxs;
		this.marg = marg;
		sVis = (int) Math.sqrt(dataVis[0].length);
		sMot = (int) Math.sqrt(dataMot[0].length);
		sX = rowS*(sVis+3)*pxs;
		sY = rowS*sVis*pxs+marg;
		//load data
		for (int i = 0; i < dataVis.length; i++) {
			String tmp = "";
			for (int j = 0; j < dataVis[i].length; j++) {
				tmp += (int) dataVis[i][j];
			}
			if (valuesVis.containsKey(tmp)) {
				valuesVis.put(tmp, valuesVis.get(tmp) + 1);
			} else {
				valuesVis.put(tmp, 1);
			}
		}
		for (int i = 0; i < dataMot.length; i++) {
			String tmp = "";
			for (int j = 0; j < dataMot[i].length; j++) {
				tmp += (int) dataMot[i][j];
			}
			if (valuesMot.containsKey(tmp)) {
				valuesMot.put(tmp, valuesMot.get(tmp) + 1);
			} else {
				valuesMot.put(tmp, 1);
			}
		}
	}
	
	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, sX, sY);
		g.setColor(Color.black);
		Font font = new Font("Serif", Font.PLAIN, 10);
		g.setFont(font);
		int posX = marg;
		int posY = marg;
		if (valuesVis != null && valuesMot != null) {
			int rowCounter = 1;
			int entryCounter = 1;
			g.drawString("Inputs: MSOM Visual",posX,posY-6);
			for (Map.Entry<String,Integer> entry : valuesVis.entrySet()) {
				String code = entry.getKey();
				for (int i = 0; i < sVis; i++) {
					for (int j = 0; j < sVis; j++) {
						g.drawRect(posX+pxs*i, posY+pxs*j, pxs, pxs);
						if (code.charAt(i * sVis + j) == '1')
							g.fillRect(posX+pxs*i, posY+pxs*j, pxs, pxs); 
					}
				}
				g.drawString(""+entry.getValue(), posX, posY+(sVis+2)*pxs);
				posX += (sVis+2)*pxs;
				if (rowCounter == rowS && entryCounter < valuesVis.size()) {
					posY += (sVis+3)*pxs;
					posX = marg;
					rowCounter = 1;
				} else {
					rowCounter++;
				}
				entryCounter++;
			}
			rowCounter = 1;
			entryCounter = 1;
			posY += (sVis+10)*pxs;
			posX = marg;
			g.drawString("Desired: MSOM Motor",posX,posY-8);
			for (Map.Entry<String,Integer> entry : valuesMot.entrySet()) {
				String code = entry.getKey();
				for (int i = 0; i < sMot; i++) {
					for (int j = 0; j < sMot; j++) {
						g.drawRect(posX+pxs*i, posY+pxs*j, pxs, pxs);
						if (code.charAt(i * sMot + j) == '1')
							g.fillRect(posX+pxs*i, posY+pxs*j, pxs, pxs); 
					}
				}
				g.drawString(""+entry.getValue(), posX, posY+(sMot+2)*pxs);
				posX += (sMot+2)*pxs;
				if (rowCounter == rowS && entryCounter < valuesMot.size()) {
					posY += (sMot+3)*pxs;
					posX = marg;
					rowCounter = 1;
				} else {
					rowCounter++;
				}
				entryCounter++;
			}
		}
	}
	
	public void saveImage(String path) {
		BufferedImage image = new BufferedImage(sX,sY,BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = image.createGraphics();
		this.paint(graphics2D);
		try {
			File newfile = new File(path);
			if(!newfile.exists())
				newfile.createNewFile();
			ImageIO.write(image, "png", newfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int getPatternsIn() {
		return valuesVis.size();
	}
	
	public int getPatternsOut() {
		return valuesMot.size();
	}
	
	public void display() {
		this.setSize(sX,sY);
		this.setVisible(true);
	}

	public static void main(String[] arg) throws Exception {
		int sizeVis = 14;
		int sizeMot = 12;
		int kVis = 16;
		int kMot = 8;
		String pathVisual = "input/msom/visual_"+sizeVis+"_"+sizeVis+".txt";
		String pathMotor = "input/msom/motor_"+sizeMot+"_"+sizeMot+".txt";
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		dataMot.multiply();
		DataBothUniq v = new DataBothUniq(15,3,10,dataVis.values(),dataMot.values());
		v.display();
		v.saveImage("output/uniquepatterns.png");
	}
	
}