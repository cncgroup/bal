package util.visu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import base.Conf;
import util.Util;

public class Histogram extends JFrame {
	private static final long serialVersionUID = 1L;

	private int[] values;
	private int width;
	private int height;
	private int mg;
	private int barWidth;
	private double barUnit;
	private int maxV;
	private Color color = new Color(170,200,255);
	private String[] labels;
	
	public Histogram(int[] values, int width, int height, int mg) {
		this.values = values;
		this.width = width;
		this.height = height;
		this.mg = mg;
		barWidth = (width-2*mg)/values.length;
		maxV = Util.max(values);
		barUnit = (height-2*mg)/(maxV*1.0);
	}
	
	public Histogram(int[] values, int width, int height) {
		this(values,width,height,Conf.marginHist);
	}
	
	public Histogram(int[] values, int width, int height, int mg, String[] labels) {
		this(values, width, height, mg);
		this.labels = labels;
	}	
	
	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, width, height);
		g.setColor(Color.black);
		Font font = new Font("Serif", Font.PLAIN, 9);
		g.setFont(font);
		g.drawLine(mg, mg, mg, height-mg-5);
		for (int i = 0; i < values.length; i++) {
			g.setColor(color);
			g.fillRect(mg+i*barWidth, (int)(mg+(maxV-values[i])*barUnit), barWidth, (int)(values[i]*barUnit));
			g.setColor(Color.black);
			g.drawRect(mg+i*barWidth, (int)(mg+(maxV-values[i])*barUnit), barWidth, (int)(values[i]*barUnit));
			if (labels != null && labels[i] != "") {
				g.drawString(labels[i], mg+i*barWidth, height-mg/2);
			}
		}
	}
	
	public void saveImage(String path) {
		BufferedImage image = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = image.createGraphics();
		this.paint(graphics2D);
		try {
			File newfile = new File(path);
			if(!newfile.exists())
				newfile.createNewFile();
			ImageIO.write(image, "png", newfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void display() {
		this.setSize(width,height);
		this.setVisible(true);
	}

	public static void main(String[] args) {
		int[] values = {3,5,1,7,8};
		String[] labels = new String[values.length];
		for (int i = 0; i < labels.length; i++) {
			labels[i] = ""+values[i];
		}
		Histogram test = new Histogram(values,600,400,30,labels);
		test.display();
	}
	
}
