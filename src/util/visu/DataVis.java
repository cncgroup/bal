package util.visu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class DataVis extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private HashSet<String> values = new HashSet<String>();
	
	private int sizeX;
	private int sizeY;
	private int pxs;
	private int margX;
	private int margY;
	private int matrixS;
	private int rowS;

	public DataVis(int[] visParams) {
		this.sizeX = visParams[0];
		this.sizeY = visParams[1];
		this.pxs = visParams[2];
		this.margX = visParams[3];
		this.margY = visParams[4];
		this.matrixS = visParams[5];
		this.rowS = visParams[6];
	}
	
	public DataVis(int[] visParams, double[][] data) {
		this(visParams);
		values = new HashSet<String>();
		for (int i = 0; i < data.length; i++) {
			String tmp = "";
			for (int j = 0; j < data[i].length; j++) {
				tmp += (int)data[i][j];
			}
			values.add(tmp);
		}
		int rowCount = values.size()/rowS;
		rowCount += (values.size()/rowS % rowS == 0)?0:1;
		sizeY = rowCount*(matrixS+3)*pxs+margY;
	}

	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, sizeX, sizeY);
		g.setColor(Color.black);
		Font font = new Font("Serif", Font.PLAIN, 9);
		g.setFont(font);
		int posX = margX;
		int posY = margY;
		if (values != null) {
			int rowCounter = 1;
			int entryCounter = 1;
			for (String code : values) {
				for (int i = 0; i < matrixS; i++) {
					for (int j = 0; j < matrixS; j++) {
						g.drawRect(posX+pxs*i, posY+pxs*j, pxs, pxs);
						if (code.charAt(i * matrixS + j) == '1') {
							g.fillRect(posX+pxs*i, posY+pxs*j, pxs, pxs); 
						}
					}
				}
				posX += (matrixS+2)*pxs;
				if (rowCounter == rowS && entryCounter < values.size()) {
					posY += (matrixS+3)*pxs;
					posX = margX;
					rowCounter = 1;
				} else {
					rowCounter++;
				}
				entryCounter++;
			}
		}
	}
	
	public void saveImage(String path) {
		BufferedImage image = new BufferedImage(sizeX,sizeY,BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = image.createGraphics();
		this.paint(graphics2D);
		try {
			File newfile = new File(path);
			if(!newfile.exists())
				newfile.createNewFile();
			ImageIO.write(image, "png", newfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int patternCount() {
		return values.size();
	}
	
	public int getsX() {
		return sizeX;
	}

	public void setsX(int sX) {
		this.sizeX = sX;
	}

	public int getsY() {
		return sizeY;
	}

	public void setsY(int sY) {
		this.sizeY = sY;
	}

	public int getPxs() {
		return pxs;
	}

	public void setPxs(int pxs) {
		this.pxs = pxs;
	}

	public int getmX() {
		return margX;
	}

	public void setmX(int mX) {
		this.margX = mX;
	}

	public int getmY() {
		return margY;
	}

	public void setmY(int mY) {
		this.margY = mY;
	}

	public int getMatrixS() {
		return matrixS;
	}

	public void setMatrixS(int matrixS) {
		this.matrixS = matrixS;
	}

	public int getRowS() {
		return rowS;
	}

	public void setRowS(int rowS) {
		this.rowS = rowS;
	}
	
	public HashSet<String> values() {
		return values;
	}

}