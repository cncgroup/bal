package util.visu;

import java.awt.Color;
import java.awt.Graphics;
import base.NetUtil;
import util.Util;

public class DataPattern {

	private int id;
	private int m = -1;
	private int p = -1;
	private int dim;
	private int k;
	private double[] values;
	private Color color;

	public DataPattern(int id, int dim, int k, double[] values, Color color) {
		this.id = id;
		this.dim = dim;
		this.k = k;
		this.values = values;
		this.color = color;
	}
	
	public DataPattern(int id, int dim, double[] values, Color color) {
		this(id, dim, -1, values, color);
	}
	
	public DataPattern(int id, int dim, int k, double[] values, Color color, int m) {
		this(id, dim, k, values, color);
		this.m = m;
	}

	public DataPattern(int id, int dim, int k, double[] values, Color color, int m, int p) {
		this(id, dim, k, values, color, m);
		this.p = p;
	}
//	VisConf.pxs

	public void paint(Graphics g, int x, int y, int pxs) {
		for (int i = 0; i < dim; i++) {
			for (int j = 0; j < dim; j++) {
				g.setColor(Color.black);
				g.drawRect(x+pxs*i, y+pxs*j, pxs, pxs);						
				if (values[i*dim+j] == 1.0) {
					g.setColor(color);					
					g.fillRect(x+pxs*i, y+pxs*j, pxs, pxs); 
				}		
			}
		}
	}
	
	public void paintConti(Graphics g, int x, int y, int pxs) {
		double[] patCont = Util.scale(values);
		for (int i = 0; i < dim; i++) {
			for (int j = 0; j < dim; j++) {
				double tval = patCont[i*dim+j];
				Color tcol = new Color(
					(int)((1.0-tval)*color.getRed()+tval*255),
					(int)((1.0-tval)*color.getGreen()+tval*255),
					(int)((1.0-tval)*color.getBlue()+tval*255)
				);
				g.setColor(tcol);
				g.fillRect(x+pxs*i, y+pxs*j, pxs, pxs); 
				g.setColor(Color.black);
				g.drawRect(x+pxs*i, y+pxs*j, pxs, pxs);				
			}
		}
	}
	
	public void paintkWTA(Graphics g, int x, int y, int pxs) {
		double[] patK = NetUtil.kWTAMin(values, k);
		for (int i = 0; i < dim; i++) {
			for (int j = 0; j < dim; j++) {
				g.setColor(Color.black);
				g.drawRect(x+pxs*i, y+pxs*j, pxs, pxs);	
				if (patK[i*dim+j] == 1.0) {
					g.setColor(color);					
					g.fillRect(x+pxs*i, y+pxs*j, pxs, pxs); 
				}		
			}
		}
	}
	
	public void paintBinary(Graphics g, int x, int y, int pxs) {
		for (int i = 0; i < dim; i++) {
			for (int j = 0; j < dim; j++) {
				g.setColor(Color.black);
				g.drawRect(x+pxs*i, y+pxs*j, pxs, pxs);		
				if (values[i*dim+j] == 1.0) {
					g.setColor(color);					
					g.fillRect(x+pxs*i, y+pxs*j, pxs, pxs); 
				}		
			}
		}
	}
	
	public void paintMixed(Graphics g, int x, int y, int pxs) {
		double[] patK = NetUtil.kWTAMin(values, k);
		double[] patCont = Util.scale(values);
		for (int i = 0; i < dim; i++) {
			for (int j = 0; j < dim; j++) {
				double tval = patCont[i*dim+j];
				Color tcol = new Color(
					(int)((1.0-tval)*color.getRed()+tval*255),
					(int)((1.0-tval)*color.getGreen()+tval*255),
					(int)((1.0-tval)*color.getBlue()+tval*255)
				);
				if (patK[i*dim+j] == 1.0) {
					tcol = Color.black;
				}
				g.setColor(tcol);
				g.fillRect(x+pxs*i, y+pxs*j, pxs, pxs); 
				g.setColor(Color.black);
				g.drawRect(x+pxs*i, y+pxs*j, pxs, pxs);				
			}
		}
	}
	
	public void paintDiff(Graphics g, double[] pat2, int x, int y, int pxs) {
		double[] pat = NetUtil.kWTAMin(values, k);
		for (int i = 0; i < dim; i++) {
			for (int j = 0; j < dim; j++) {
				g.setColor(Color.black);
				g.drawRect(x+pxs*i, y+pxs*j, pxs, pxs);						
				if (pat[i*dim+j] == 1.0) {
					if (pat[i*dim+j] == pat2[i*dim+j])
						g.setColor(Color.green);
					else if (pat2[i*dim+j] == 1.0) {
						g.setColor(Color.blue);
					} else {
						g.setColor(Color.red);
					}
					g.fillRect(x+pxs*i, y+pxs*j, pxs, pxs); 
				}		
			}
		}			
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int dim() {
		return dim;
	}

	public int getM() {
		return m;
	}

	public void setM(int m) {
		this.m = m;
	}

	public int getP() {
		return p;
	}

	public void setP(int p) {
		this.p = p;
	}

	public int getDim() {
		return dim;
	}

	public void setDim(int dim) {
		this.dim = dim;
	}

	public int getK() {
		return k;
	}

	public void setK(int k) {
		this.k = k;
	}

	public double[] getValues() {
		return values;
	}

	public void setValues(double[] values) {
		this.values = values;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
	
}
