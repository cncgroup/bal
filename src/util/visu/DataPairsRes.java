package util.visu;

import base.NetUtil;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class DataPairsRes extends DataVisFrame {
	private static final long serialVersionUID = 1L;

	private double[][][] patterns;

	public DataPairsRes(double[][][] patterns) {
		super((int)Math.sqrt(patterns[0][0].length),(int)Math.sqrt(patterns[2][0].length),count(patterns));
		this.patterns = patterns;
		setFrameSize();
	}

	public DataPairsRes(double[][][] patterns, int[] visParams) {
		super((int)Math.sqrt(patterns[0][0].length),(int)Math.sqrt(patterns[2][0].length),count(patterns),visParams[0],visParams[1],visParams[2],visParams[3]);
		this.patterns = patterns;
		setFrameSize();
	}

	public void setPatterns(double[][][] patterns) {
		this.patterns = patterns;
		this.repaint();
	}

	public static int count(double[][][] patterns) {
		int count = 0;
		for (int i = 0; i < patterns[0].length; i++) {
			count++;
		}
		return count;
	}
	
	public void paint(Graphics g) {
		int gap = 6;
		/*g.setColor(Color.white);
		g.fillRect(0, 0, width, height);*/
		g.setColor(Color.gray);
		Font font = new Font("Serif", Font.PLAIN, 9);
		g.setFont(font);
		int posX;
		int posXrow = margX;
		int posY = margY;
		int rowCounter = 1;
		for (int p = 0; p < patterns[0].length; p++) {
			if (rowCounter > rowh) {
				posY = margY;
				posXrow += (dimX+dimY+gap)*pxs;
				rowCounter = 1;
				g.setColor(Color.gray);
				g.drawLine(posXrow-(gap+1), 0, posXrow-(gap+1), height);
			} else if (rowCounter > 1) {
				posY += (dimMax+3)*pxs;
			}
			posX = posXrow;
			paintPattern(g, patterns[2][p], patterns[3][p], dimY, posX, posY);

			posX += (dimY+2)*pxs;
			paintPattern(g, patterns[0][p], patterns[1][p], dimX, posX, posY);

			rowCounter++;
		}
	}
	private int toRGBval(double n) {
		return  n <= 0.0 ? 0 :
				n >= 1.0 ? 255 :
				(int) (n*255.0);
	}

	public void paintPattern(Graphics g, double[] pattern0, double[] pattern1, int dim, int posX, int posY) {
		for (int i = 0; i < dim+1; i++) {
			for (int j = 0; j < dim; j++) {
				int x = i*dim+j;
				if (x >= pattern0.length) return;
				int pix = 0;
				g.setColor(Color.gray);
				g.drawRect(posX+pxs*j, posY+pxs*i, pxs, pxs);

				/*
				if (pattern0[x] >= NetUtil.binTreshold && pattern1[x] >= NetUtil.binTreshold)
					g.setColor(Color.green); // true positive
				else if (pattern0[x] >= NetUtil.binTreshold)
					g.setColor(Color.blue); // false negative
				else if (pattern1[x] >= NetUtil.binTreshold)
					g.setColor(Color.red); // false positive
				else {
					g.setColor(Color.white); // true negative
					pix = 1;
				}*/
//				g.setColor(new Color(
//						toRGBval(pattern1[x]-pattern0[x]),
//						toRGBval(pattern1[x]*pattern0[x]),
//						toRGBval(pattern0[x]-pattern1[x])));
				g.setColor(new Color(
						toRGBval(1.0-pattern0[x]),
						toRGBval(1.0-pattern1[x]-pattern0[x]+2*pattern1[x]*pattern0[x]),
						toRGBval(1.0-pattern1[x])));
//				if (       g.getColor().getRed() > 196
//						&& g.getColor().getGreen() > 196
//						&& g.getColor().getBlue() > 196)
//					pix = 1;

				g.fillRect(posX+pxs*j+pix,posY+pxs*i+pix,pxs-pix, pxs-pix);
			}
		}

	}
}