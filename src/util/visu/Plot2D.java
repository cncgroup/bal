package util.visu;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.traces.Trace2DSimple;

import java.awt.BasicStroke;

public class Plot2D extends Plot {
	private static final long serialVersionUID = 1L;

	protected ITrace2D traces[][];

	public Plot2D() {
		chart = new Chart2D();
	}

	public Plot2D(int trcount, String[] labels) {
		super();
		chart = new Chart2D();
		traces = new ITrace2D[trcount][2];
		traceColors = generateColors(trcount);
		for (int i = 0; i < traces.length; i++) {
			for (int j = 0; j < traces[i].length; j++) {
				traces[i][j] = new Trace2DSimple();
				traces[i][j].setColor(traceColors[i]);
				traces[i][j].setName("net"+i+":"+labels[j]);
				if (j == 1)
					traces[i][j].setStroke(new BasicStroke(
								1.0f,                      // Width
								BasicStroke.CAP_SQUARE,    // End cap
								BasicStroke.JOIN_MITER,    // Join style
								5.0f,                     // Miter limit
								new float[] {2.0f,2.0f}, // Dash pattern
								0.0f));
			    chart.addTrace(traces[i][j]);
			}
		}
		this.getContentPane().add(chart);
	}

	public void addPoints(int xval, double[] yval1, double[] yval2) {
		for (int i = 0; i < yval1.length; i++) {
			traces[i][0].addPoint(xval, yval1[i]);
			traces[i][1].addPoint(xval, yval2[i]);
		}
	}

	public void addPoint(int trid, int xval, double yval1, double yval2) {
		traces[trid][0].addPoint(xval, yval1);
		traces[trid][1].addPoint(xval, yval2);
	}

}
