package util.visu;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import base.Conf;

public class DataVisFrame extends JFrame {
	protected static final long serialVersionUID = 1L;
	
	protected int width;
	protected int height;
	protected int pxs;
	protected int rowh;
	protected int margX;
	protected int margY;

	protected int dimX;
	protected int dimY;
	protected int dimMax;
	protected int count;
	
	public DataVisFrame(int dimX, int dimY, int count, int pxs, int rowh, int margX, int margY) {
		this.pxs = pxs;
		this.rowh = rowh;
		this.margX = margX;
		this.margY = margY;
		this.dimX = dimX;
		this.dimY = dimY;
		this.count = count;
		setFrameSize();
		dimMax = (dimX > dimY) ? dimX : dimY;
	}

	public DataVisFrame(int dimX, int dimY, int count) {
		this(dimX, dimY, count, Conf.pixelSize, Conf.rowHeight, Conf.marginX, Conf.marginY);
	}
		
	protected void setFrameSize() {
		int roww = (count%rowh==0) ? count/rowh : count/rowh+1;
		width = roww*(dimX+dimY+6)*pxs+margX;
		height = rowh*(dimMax+3)*pxs+margY;
	}
	
	public void setRowH(int val) {
		this.rowh = val;
		setFrameSize();
	}

	public void setPxs(int pxs) {
		this.pxs = pxs;
		setFrameSize();		
	}

	public void saveImage(String path) {
		BufferedImage image = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = image.createGraphics();
		this.paint(graphics2D);
		try {
			File newfile = new File(path);
			if(!newfile.exists())
				newfile.createNewFile();
			ImageIO.write(image, "png", newfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void display() {
		this.setSize(width,height);
		this.setVisible(true);
	}
	
}
