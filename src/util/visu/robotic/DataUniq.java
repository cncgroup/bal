package util.visu.robotic;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import base.Conf;
import data.DataRoboticMot;

public class DataUniq extends JFrame {
	private static final long serialVersionUID = 1L;
	
	public static final String pathVisual = "input/msom/output_visual.txt";
	public static final String pathMotor = "input/msom/output_motor.txt";
	public static final String inPathPreproc = "output/robotic/data-in-binarized-k";
	public static final String outPathPreproc = "output/robotic/data-out-binarized-k";
	
	private HashMap<String,Integer> values = new HashMap<String,Integer>();
	
	private int sizeX;
	private int sizeY;
	private int pxs;
	private int margX;
	private int margY;
	private int matrixS;
	private int rowS;

	public DataUniq(int[] visParams) {
		this.sizeX = visParams[0];
		this.sizeY = visParams[1];
		this.pxs = visParams[2];
		this.margX = visParams[3];
		this.margY = visParams[4];
		this.matrixS = visParams[5];
		this.rowS = visParams[6];
	}
	
	public DataUniq(int[] visParams, double[][] data) {
		this(visParams);
		for (int i = 0; i < data.length; i++) {
			String tmp = "";
			for (int j = 0; j < data[i].length; j++) {
				tmp += (int) data[i][j];
			}
			if (values.containsKey(tmp)) {
				values.put(tmp, values.get(tmp) + 1);
			} else {
				values.put(tmp, 1);
			}
		}
		int rowCount = values.size()/rowS;
		rowCount += (values.size()/rowS % rowS == 0)?0:1;
		sizeY = (values.size() < rowS) ? (matrixS+3)*pxs+margY : rowCount*(matrixS+3)*pxs+margY;
	}
	
	public void savePreprocessed(String path) throws Exception {
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(path)));
		for (Map.Entry<String,Integer> entry : values.entrySet()) {
			bw.write(entry.getValue()+":"+entry.getKey());
			bw.newLine();
		}
		bw.close();	
	}

	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, sizeX, sizeY);
		g.setColor(Color.black);
		Font font = new Font("Serif", Font.PLAIN, 9);
		g.setFont(font);
		int posX = margX;
		int posY = margY;
		if (values != null) {
			int rowCounter = 1;
			int entryCounter = 1;
			for (Map.Entry<String,Integer> entry : values.entrySet()) {
				String code = entry.getKey();
				for (int i = 0; i < matrixS; i++) {
					for (int j = 0; j < matrixS; j++) {
						g.drawRect(posX+pxs*i, posY+pxs*j, pxs, pxs);
						if (code.charAt(i * matrixS + j) == '1')
							g.fillRect(posX+pxs*i, posY+pxs*j, pxs, pxs); 
					}
				}
				g.drawString(""+entry.getValue(), posX, posY+(matrixS+2)*pxs);
				posX += (matrixS+2)*pxs;
				if (rowCounter == rowS && entryCounter < values.size()) {
					posY += (matrixS+3)*pxs;
					posX = margX;
					rowCounter = 1;
				} else {
					rowCounter++;
				}
				entryCounter++;
			}
		}
	}
	
	public void saveImage(String path) {
		BufferedImage image = new BufferedImage(sizeX,sizeY,BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = image.createGraphics();
		this.paint(graphics2D);
		try {
			File newfile = new File(path);
			if(!newfile.exists())
				newfile.createNewFile();
			ImageIO.write(image, "png", newfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int patternCount() {
		return values.size();
	}
	
	public void display() {
		this.setSize(sizeX,sizeY);
		this.setVisible(true);
	}

	public static void main(String[] arg) throws Exception {
		int[] visParams = {500, 500, 4, 10, 30, 10, 10};
		int k = 10;
		DataRoboticMot data = new DataRoboticMot(Conf.dataSetsMotPaths[1]);
		System.out.println(data.sampleCount());
		data.kWinnerTakeAll(k);
		System.out.println(data.uniquePatternCount());
		DataUniq v = new DataUniq(visParams,data.values());
		v.display();

	}
	
}