package util.visu.robotic;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import base.Conf;
import data.DataRoboticVis;

public class DataVisMesh3 extends JFrame {
	private static final long serialVersionUID = 1L;
	protected Color[] colorValues;
	protected int sizeX;
	protected int sizeY;
	protected int pxs;
	protected int margX;
	protected int margY;
	protected int matrixS;
	
	public Color meshColors(Color c1, Color c2) {
		int r = (c1.getRed() + c2.getRed())/2;
		int g = (c1.getGreen() + c2.getGreen())/2;
		int b = (c1.getBlue() + c2.getBlue())/2;
		return new Color(r,g,b);
	}
	
	public Color meshColors(Color c1, int[] c2) {
		int r = (c1.getRed() + c2[0])/2;
		int g = (c1.getGreen() + c2[1])/2;
		int b = (c1.getBlue() + c2[2])/2;
		return new Color(r,g,b);
	}
	
	public DataVisMesh3(int[] visParams, DataRoboticVis data) {
		//initialize
		this.pxs = visParams[0];
		this.margX = visParams[1];
		this.margY = visParams[2];
		colorValues = new Color[data.patternSize()];
		//fill in data
		for (int i = 0; i < Conf.movements; i++) {
			for (int j = 0; j < Conf.perspectives; j++) {
				for (double[] sample: data.dataSeq()[i][j]) {
					for (int b = 0; b < sample.length; b++) {
						if (sample[b] == 1.0) {
							if (colorValues[b] == null) {
								colorValues[b] = new Color(Conf.colors2D[i][j][0],Conf.colors2D[i][j][1],Conf.colors2D[i][j][2]);
							} else {
								colorValues[b] = meshColors(colorValues[b],Conf.colors2D[i][j]);
							}
						}
					}
				}
			}
		}
		matrixS = (int)Math.sqrt(colorValues.length);
		sizeX = matrixS*(pxs+2)+margX;
		sizeY = matrixS*pxs+3*margX;
	}

	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, sizeX, sizeY);
		g.setColor(Color.black);
		Font font = new Font("Serif", Font.PLAIN, 9);
		g.setFont(font);
		int posX = margX;
		int posY = margY;
		if (colorValues != null) {
			for (int x = 0; x < matrixS; x++) {
				for (int y = 0; y < matrixS; y++) {
					if (colorValues[x*matrixS+y] != null) {
						g.setColor(colorValues[x*matrixS+y]);
						g.fillRect(posX+pxs*x, posY+pxs*y, pxs, pxs); 
						g.setColor(Color.black);
					}
					g.drawRect(posX+pxs*x, posY+pxs*y, pxs, pxs);
				}
			}
		}
	}
	
	public void display() {
		this.setSize(sizeX,sizeY);
		this.setVisible(true);
	}
	
	public void saveImage(String path) {
		BufferedImage image = new BufferedImage(sizeX,sizeY,BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = image.createGraphics();
		this.paint(graphics2D);
		try {
			File newfile = new File(path);
			if(!newfile.exists())
				newfile.createNewFile();
			ImageIO.write(image, "png", newfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] arg) throws Exception {
		int[] visParams = {12, 22, 42};
		int k = 12;
		DataRoboticVis data = new DataRoboticVis(Conf.dataSetsVisPaths[3]);
		data.kWinnerTakeAll(k);
		DataVisMesh3 v = new DataVisMesh3(visParams,data);
		v.display();
		v.saveImage("output/msom-visual-perspectives-mesh.png");
	}
	
}