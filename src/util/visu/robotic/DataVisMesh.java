package util.visu.robotic;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import util.Util;
import base.Conf;
import data.DataRoboticVis;

public class DataVisMesh extends JFrame {
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unchecked")
	protected HashMap<String,Integer>[][] values = new HashMap[Conf.movements][Conf.perspectives];
	protected int sizeX;
	protected int sizeY;
	protected int pxs;
	protected int margX;
	protected int margY;
	protected int matrixS;
	
	public DataVisMesh(int[] visParams, DataRoboticVis data) {
		//initialize
		this.sizeX = visParams[0];
		this.sizeY = visParams[1];
		this.pxs = visParams[2];
		this.margX = visParams[3];
		this.margY = visParams[4];
		this.matrixS = visParams[5];
		for (int i = 0; i < values.length; i++) {
			for (int j = 0; j < values[i].length; j++) {
				values[i][j] = new HashMap<String, Integer>();
			}
		}
		//fill in data
		for (int i = 0; i < data.dataSeq().length; i++) {
			for (int j = 0; j < data.dataSeq()[i].length; j++) {
				for (double[] sample: data.dataSeq()[i][j]) {
					String tmp = Util.arrayToBinaryString(sample);
					if (values[i][j].containsKey(tmp)) {
						values[i][j].put(tmp,values[i][j].get(tmp)+1);
					} else {
						values[i][j].put(tmp,1);
					}
				}
			}
		}
	}

	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, sizeX, sizeY);
		g.setColor(Color.black);
		Font font = new Font("Serif", Font.PLAIN, 9);
		g.setFont(font);
		int posX = margX;
		int posY = margY;
		if (values != null) {
			for (int i = 0; i < values.length; i++) {
				for (int j = 0; j < values[i].length; j++) {
					for (Map.Entry<String,Integer> entry : values[i][j].entrySet()) {
						String code = entry.getKey();
						for (int x = 0; x < matrixS; x++) {
							for (int y = 0; y < matrixS; y++) {
								if (code.charAt(x * matrixS + y) == '1') {
									g.setColor(new Color(Conf.colors2D[i][j][0],Conf.colors2D[i][j][1],Conf.colors2D[i][j][2]));
									g.fillRect(posX+pxs*x, posY+pxs*y, pxs, pxs); 
									g.setColor(Color.black);
								}
								g.drawRect(posX+pxs*x, posY+pxs*y, pxs, pxs);
							}
						}
					}
				}
				posX += (matrixS+2)*pxs;
			}
		}
	}
	
	public void saveImage(String path) {
		BufferedImage image = new BufferedImage(sizeX,sizeY,BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = image.createGraphics();
		this.paint(graphics2D);
		try {
			File newfile = new File(path);
			if(!newfile.exists())
				newfile.createNewFile();
			ImageIO.write(image, "png", newfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int getsX() {
		return sizeX;
	}

	public void setsX(int sX) {
		this.sizeX = sX;
	}

	public int getsY() {
		return sizeY;
	}

	public void setsY(int sY) {
		this.sizeY = sY;
	}

	public int getPxs() {
		return pxs;
	}

	public void setPxs(int pxs) {
		this.pxs = pxs;
	}

	public int getmX() {
		return margX;
	}

	public void setmX(int mX) {
		this.margX = mX;
	}

	public int getmY() {
		return margY;
	}

	public void setmY(int mY) {
		this.margY = mY;
	}

	public int getMatrixS() {
		return matrixS;
	}

	public void setMatrixS(int matrixS) {
		this.matrixS = matrixS;
	}

	public static void main(String[] arg) throws Exception {
		int[] visParams = {460, 180, 12, 22, 42, 10, 10};
		int k = 10;
		DataRoboticVis data = new DataRoboticVis(Conf.dataSetsVisPaths[0]);
//		DataSetRobotic data = new DataSetRobotic(pathMotor);
		data.kWinnerTakeAll(k);
		DataVisMesh v = new DataVisMesh(visParams,data);
		v.setSize(v.getsX(), v.getsY());
		v.setVisible(true);
		v.saveImage("output/msom-visual-perspectives.png");
	}
	
}