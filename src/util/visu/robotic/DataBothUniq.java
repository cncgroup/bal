package util.visu.robotic;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;


public class DataBothUniq extends JFrame {
	private static final long serialVersionUID = 1L;
	public static final int[] visParams = {600, 860, 4, 22, 46, 10, 10};
	
	private TreeMap<String,Integer> valuesIn = new TreeMap<String,Integer>();
	private TreeMap<String,Integer> valuesDes = new TreeMap<String,Integer>();
	private int sX;
	private int sY;
	private int pxs;
	private int mX;
	private int mY;
	private int matrixS;
	private int rowS;

	public DataBothUniq(int[] visParams) {
		this.sX = visParams[0];
		this.sY = visParams[1];
		this.pxs = visParams[2];
		this.mX = visParams[3];
		this.mY = visParams[4];
		this.matrixS = visParams[5];
		this.rowS = visParams[6];
	}
	
	public DataBothUniq(int[] visParams, double[][] dataIn, double[][] dataDes) {
		this(visParams);
		for (int i = 0; i < dataIn.length; i++) {
			String tmp = "";
			for (int j = 0; j < dataIn[i].length; j++) {
				tmp += (int) dataIn[i][j];
			}
			if (valuesIn.containsKey(tmp)) {
				valuesIn.put(tmp, valuesIn.get(tmp) + 1);
			} else {
				valuesIn.put(tmp, 1);
			}
		}
		for (int i = 0; i < dataDes.length; i++) {
			String tmp = "";
			for (int j = 0; j < dataDes[i].length; j++) {
				tmp += (int) dataDes[i][j];
			}
			if (valuesDes.containsKey(tmp)) {
				valuesDes.put(tmp, valuesDes.get(tmp) + 1);
			} else {
				valuesDes.put(tmp, 1);
			}
		}
	}
	
	public DataBothUniq(int[] visParams, String path) {
		this(visParams);
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(path)));
			String line;
			while ((line = br.readLine()) != null) {
				line = line.trim();
				String[] temp = line.split(":");
				valuesIn.put(temp[1],Integer.parseInt(temp[0]));
			}
			br.close();
			System.out.println("dataset "+path+" loaded");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, sX, sY);
		g.setColor(Color.black);
		Font font = new Font("Serif", Font.PLAIN, 10);
		g.setFont(font);
		int posX = mX;
		int posY = mY;
		if (valuesIn != null && valuesDes != null) {
			int rowCounter = 1;
			int entryCounter = 1;
			g.drawString("Inputs: MSOM Visual",posX,posY-6);
			for (Map.Entry<String,Integer> entry : valuesIn.entrySet()) {
				String code = entry.getKey();
				for (int i = 0; i < matrixS; i++) {
					for (int j = 0; j < matrixS; j++) {
						g.drawRect(posX+pxs*i, posY+pxs*j, pxs, pxs);
						if (code.charAt(i * matrixS + j) == '1')
							g.fillRect(posX+pxs*i, posY+pxs*j, pxs, pxs); 
					}
				}
				g.drawString(""+entry.getValue(), posX, posY+(matrixS+2)*pxs);
				posX += (matrixS+2)*pxs;
				if (rowCounter == rowS && entryCounter < valuesIn.size()) {
					posY += (matrixS+3)*pxs;
					posX = mX;
					rowCounter = 1;
				} else {
					rowCounter++;
				}
				entryCounter++;
			}
			rowCounter = 1;
			entryCounter = 1;
			posY += (matrixS+10)*pxs;
			posX = mX;
			g.drawString("Desired: MSOM Motor",posX,posY-8);
			for (Map.Entry<String,Integer> entry : valuesDes.entrySet()) {
				String code = entry.getKey();
				for (int i = 0; i < matrixS; i++) {
					for (int j = 0; j < matrixS; j++) {
						g.drawRect(posX+pxs*i, posY+pxs*j, pxs, pxs);
						if (code.charAt(i * matrixS + j) == '1')
							g.fillRect(posX+pxs*i, posY+pxs*j, pxs, pxs); 
					}
				}
				g.drawString(""+entry.getValue(), posX, posY+(matrixS+2)*pxs);
				posX += (matrixS+2)*pxs;
				if (rowCounter == rowS && entryCounter < valuesDes.size()) {
					posY += (matrixS+3)*pxs;
					posX = mX;
					rowCounter = 1;
				} else {
					rowCounter++;
				}
				entryCounter++;
			}
		}
	}
	
	public void saveImage(String path) {
		BufferedImage image = new BufferedImage(sX,sY,BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = image.createGraphics();
		this.paint(graphics2D);
		try {
			File newfile = new File(path);
			if(!newfile.exists())
				newfile.createNewFile();
			ImageIO.write(image, "png", newfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int getPatternsIn() {
		return valuesIn.size();
	}
	
	public int getPatternsOut() {
		return valuesDes.size();
	}
	
	public void display() {
		this.setSize(sX,sY);
		this.setVisible(true);
	}

	public static void main(String[] arg) throws Exception {
		int k = 16;
		DataRoboticVis dataV = new DataRoboticVis(Conf.dataSetsVisPaths[0]);
		DataRoboticMot dataM = new DataRoboticMot(Conf.dataSetsMotPaths[1]);
		dataM.multiply();
		dataM.kWinnerTakeAll(k);
		dataV.kWinnerTakeAll(k);
		DataBothUniq v = new DataBothUniq(visParams,dataM.values(),dataV.values());
		v.display();
//		v.saveImage("output/robotic/test.png");
	}
	
}