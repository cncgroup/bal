package util.visu.robotic;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import util.Util;

import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class DataPairs extends JFrame {
	private static final long serialVersionUID = 1L;

	DataRoboticVis dataVis;
	DataRoboticMot dataMot;
	protected int sizeX;
	protected int sizeY;
	protected int pxs;
	protected int margX;
	protected int margY;
	protected int matrixS;
	protected int rowH;
	
	public DataPairs(int[] visParams, DataRoboticVis dataV, DataRoboticMot dataM) {
		//initialize
		this.sizeX = visParams[0];
		this.sizeY = visParams[1];
		this.pxs = visParams[2];
		this.margX = visParams[3];
		this.margY = visParams[4];
		this.matrixS = visParams[5];
		this.rowH = visParams[6];
		dataVis = dataV;
		dataMot = dataM;
	}

	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, sizeX, sizeY);
		g.setColor(Color.black);
		Font font = new Font("Serif", Font.PLAIN, 9);
		g.setFont(font);
		int posX = margX;
		int posXrow = margX;
		int posY = margY;
		if (dataVis != null) {
			for (int mov = 0; mov < Conf.movements; mov++) {
				for (int sample = 0; sample < dataMot.dataSeq()[mov].size(); sample++) {
					posX = posXrow;
					String codeM = Util.arrayToBinaryString(dataMot.dataSeq()[mov].get(sample));
					for (int x = 0; x < matrixS; x++) {
						for (int y = 0; y < matrixS; y++) {
							g.drawRect(posX+pxs*x, posY+pxs*y, pxs, pxs);
							if (codeM.charAt(x * matrixS + y) == '1') {
								g.setColor(new Color(Conf.colors2D[mov][0][0],Conf.colors2D[mov][0][1],Conf.colors2D[mov][0][2]));
								g.fillRect(posX+pxs*x, posY+pxs*y, pxs, pxs); 
								g.setColor(Color.black);
							}
						}
					}
					g.drawString("M "+(sample+1), posX, posY+(matrixS+2)*pxs);
					posX += (matrixS+2)*pxs;
					for (int persp = 0; persp < Conf.perspectives; persp++) {
						if (sample < dataVis.dataSeq()[mov][persp].size()) {
							String codeV = Util.arrayToBinaryString(dataVis.dataSeq()[mov][persp].get(sample));
							for (int x = 0; x < matrixS; x++) {
								for (int y = 0; y < matrixS; y++) {
									g.drawRect(posX+pxs*x, posY+pxs*y, pxs, pxs);
									if (codeV.charAt(x * matrixS + y) == '1') {
										g.setColor(new Color(Conf.colors2D[mov][persp][0],Conf.colors2D[mov][persp][1],Conf.colors2D[mov][persp][2]));
										g.fillRect(posX+pxs*x, posY+pxs*y, pxs, pxs); 
										g.setColor(Color.black);
									}
								}
							}
							g.drawString("V "+(sample+1)+"-"+persp, posX, posY+(matrixS+2)*pxs);
							posX += (matrixS+2)*pxs;
						}
					}
					posY += (matrixS+3)*pxs;
				}
				posY = margY;
				posXrow += 5*(matrixS+2)*pxs+margX;
			}
		}
	}
	
	public void saveImage(String path) {
		BufferedImage image = new BufferedImage(sizeX,sizeY,BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = image.createGraphics();
		this.paint(graphics2D);
		try {
			File newfile = new File(path);
			if(!newfile.exists())
				newfile.createNewFile();
			ImageIO.write(image, "png", newfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int getsX() {
		return sizeX;
	}

	public void setsX(int sX) {
		this.sizeX = sX;
	}

	public int getsY() {
		return sizeY;
	}

	public void setsY(int sY) {
		this.sizeY = sY;
	}

	public int getPxs() {
		return pxs;
	}

	public void setPxs(int pxs) {
		this.pxs = pxs;
	}

	public int getmX() {
		return margX;
	}

	public void setmX(int mX) {
		this.margX = mX;
	}

	public int getmY() {
		return margY;
	}

	public void setmY(int mY) {
		this.margY = mY;
	}

	public int getMatrixS() {
		return matrixS;
	}

	public void setMatrixS(int matrixS) {
		this.matrixS = matrixS;
	}

	public static void main(String[] arg) throws Exception {
		int[] visParams = {765, 900, 4, 12, 12, 10, 10};
		int k = 10;
		DataRoboticVis dataV = new DataRoboticVis(Conf.dataSetsVisPaths[0]);
		DataRoboticMot dataM = new DataRoboticMot(Conf.dataSetsMotPaths[1]);
		dataV.kWinnerTakeAll(k);
		dataM.kWinnerTakeAll(k);
		DataPairs v = new DataPairs(visParams,dataV,dataM);
		v.setSize(v.getsX(), v.getsY());
		v.setVisible(true);
	}
	
}