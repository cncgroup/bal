package util.visu.robotic;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import base.Conf;
import data.DataRoboticMot;

public class DataMotMesh2 extends JFrame {
	private static final long serialVersionUID = 1L;
	protected Color[][] colorValues;
	protected int sizeX;
	protected int sizeY;
	protected int pxs;
	protected int margX;
	protected int margY;
	protected int matrixS;
	
	public Color meshColors(Color c1, Color c2) {
		int r = (c1.getRed() + c2.getRed())/2;
		int g = (c1.getGreen() + c2.getGreen())/2;
		int b = (c1.getBlue() + c2.getBlue())/2;
		return new Color(r,g,b);
	}
	
	public Color meshColors(Color c1, int[] c2) {
		int r = (c1.getRed() + c2[0])/2;
		int g = (c1.getGreen() + c2[1])/2;
		int b = (c1.getBlue() + c2[2])/2;
		return new Color(r,g,b);
	}
	
	public DataMotMesh2(int[] visParams, DataRoboticMot data) {
		//initialize
		this.pxs = visParams[0];
		this.margX = visParams[1];
		this.margY = visParams[2];
		colorValues = new Color[Conf.movements][data.patternSize()];
		//fill in data
		for (int i = 0; i < Conf.movements; i++) {
			for (double[] sample: data.dataSeq()[i]) {
				for (int b = 0; b < sample.length; b++) {
					if (sample[b] == 1.0) {
						if (colorValues[i][b] == null) {
							colorValues[i][b] = meshColors(new Color(Conf.colors2D[i][0][0],Conf.colors2D[i][0][1],Conf.colors2D[i][0][2]),new Color(255,255,255));
						} else {
							colorValues[i][b] = meshColors(colorValues[i][b],Conf.colors2D[i][0]);
						}
					}
				}
			}
		}
		matrixS = (int)Math.sqrt(colorValues[0].length);
		sizeX = 3*matrixS*(pxs+2)+2*margX;
		sizeY = matrixS*pxs+3*margX;
	}

	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, sizeX, sizeY);
		g.setColor(Color.black);
		Font font = new Font("Serif", Font.PLAIN, 9);
		g.setFont(font);
		int posX = margX;
		int posY = margY;
		if (colorValues != null) {
			for (int i = 0; i < Conf.movements; i++) {
				for (int x = 0; x < matrixS; x++) {
					for (int y = 0; y < matrixS; y++) {
						if (colorValues[i][x*matrixS+y] != null) {
							g.setColor(colorValues[i][x*matrixS+y]);
							g.fillRect(posX+pxs*x, posY+pxs*y, pxs, pxs); 
							g.setColor(Color.black);
						}
						g.drawRect(posX+pxs*x, posY+pxs*y, pxs, pxs);
					}
				}
				posX += (matrixS+2)*pxs;
			}
		}
	}
	
	public void display() {
		this.setSize(sizeX,sizeY);
		this.setVisible(true);
	}
	
	public void saveImage(String path) {
		BufferedImage image = new BufferedImage(sizeX,sizeY,BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = image.createGraphics();
		this.paint(graphics2D);
		try {
			File newfile = new File(path);
			if(!newfile.exists())
				newfile.createNewFile();
			ImageIO.write(image, "png", newfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] arg) throws Exception {
		int[] visParams = {12, 22, 42};
		int k = 10;
		DataRoboticMot data = new DataRoboticMot(Conf.dataSetsMotPaths[0]);
		data.kWinnerTakeAll(k);
		DataMotMesh2 v = new DataMotMesh2(visParams,data);
		v.display();
//		v.saveImage("output/msom-visual-perspectives.png");
	}
	
}