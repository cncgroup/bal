package util.visu.robotic;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import base.Conf;

import data.DataRoboticMot;
import data.DataRoboticVis;

public class DataPairsAssymFP extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private ArrayList<String> valuesIn = new ArrayList<String>();
	private ArrayList<String> valuesDes = new ArrayList<String>();
	
	private int sizeX;
	private int sizeY;
	private int pxs;
	private int margX;
	private int margY;
	private int matrixSin;
	private int matrixSdes;
	private int rowH;
	private Color[] colors;
	
	public DataPairsAssymFP(int[] visParams, double[][] dataIn, double[][] dataDes, int[] sequences) {
		this.pxs = visParams[0];
		this.margX = visParams[1];
		this.margY = visParams[2];
		this.rowH = visParams[3];
		//data
		for (int i = 0; i < dataIn.length; i++) {
			String tmp = "";
			for (int j = 0; j < dataIn[i].length; j++) {
				tmp += (int)dataIn[i][j];
			}
			valuesIn.add(tmp);
		}
		for (int i = 0; i < dataDes.length; i++) {
			String tmp = "";
			for (int j = 0; j < dataDes[i].length; j++) {
				tmp += (int)dataDes[i][j];
			}
			valuesDes.add(tmp);
		}
		matrixSin = (int)Math.sqrt(dataIn[0].length);
		matrixSdes = (int)Math.sqrt(dataDes[0].length);
		int rowW = (valuesIn.size()%rowH == 0) ? valuesIn.size()/rowH : valuesIn.size()/rowH+1;
		sizeX = rowW*2*(matrixSin+2)*pxs+4*margX;
		sizeY = rowH*(matrixSin+3)*pxs+3*margY;
		colors = new Color[valuesDes.size()];
		int ind = 0;
		for (int i = 0; i < sequences.length; i++) {
			for (int j = 0; j < sequences[i]; j++) {
				colors[ind] = Conf.colors[i];
				ind++;
			}
		}
	}

	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, sizeX, sizeY);
		g.setColor(Color.black);
		Font font = new Font("Serif", Font.PLAIN, 9);
		g.setFont(font);
		int posX = margX;
		int posXrow = margX;
		int posY = margY;
		int rowCounter = 1;
		int matrixS = (matrixSin > matrixSdes) ? matrixSin : matrixSdes;
		if (valuesIn != null) {
			for (int sample = 0; sample < valuesIn.size(); sample++) {
				if (rowCounter > rowH) {
					posY = margY;
					posXrow += 2*(matrixS+2)*pxs+margX/2;
					rowCounter = 1;
					g.drawLine(posXrow-7, 0, posXrow-7, sizeY);
				} else if (rowCounter > 1) {
					posY += (matrixS+3)*pxs;
				}
				posX = posXrow;
				String patIn = valuesIn.get(sample);
				for (int i = 0; i < matrixSin; i++) {
					for (int j = 0; j < matrixSin; j++) {
						g.drawRect(posX+pxs*i, posY+pxs*j, pxs, pxs);
						if (patIn.charAt(i * matrixSin + j) == '1') {
							g.setColor(colors[sample]);
							g.fillRect(posX+pxs*i, posY+pxs*j, pxs, pxs); 
							g.setColor(Color.black);
						}
					}
				}
//				g.drawString("in"+(sample+1), posX, posY+(matrixS+2)*pxs);
				posX += (matrixSin+2)*pxs;
				String patDes = valuesDes.get(sample);
				for (int i = 0; i < matrixSdes; i++) {
					for (int j = 0; j < matrixSdes; j++) {
						g.drawRect(posX+pxs*i, posY+pxs*j, pxs, pxs);
						if (patDes.charAt(i * matrixSdes + j) == '1') {
							g.setColor(colors[sample]);
							g.fillRect(posX+pxs*i, posY+pxs*j, pxs, pxs); 
							g.setColor(Color.black);
						}
					}
				}
//				g.drawString("des"+(sample+1), posX, posY+(matrixS+2)*pxs);
				rowCounter++;	
			}
		}
//		System.out.println("done painting");
	}
	
	public void saveImage(String path) {
		BufferedImage image = new BufferedImage(sizeX,sizeY,BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = image.createGraphics();
		this.paint(graphics2D);
		try {
			File newfile = new File(path);
			if(!newfile.exists())
				newfile.createNewFile();
			ImageIO.write(image, "png", newfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void display() {
		this.setSize(sizeX,sizeY);
		this.setVisible(true);
	}
	
	public static void main(String[] args) {
		int[] visParams = {5, 10, 30, 10, 6};
		int k = 8;
		DataRoboticVis dataVis = new DataRoboticVis(Conf.dataSetsVisPaths[0]);
		DataRoboticMot dataMot = new DataRoboticMot(Conf.dataSetsMotPaths[0]);
		dataVis.kWinnerTakeAll(k);
		dataMot.kWinnerTakeAll(k);
		double[][] dataM = dataMot.values();
		double[][] dataV = dataVis.valuesFirstPersp();
		System.out.println(dataM.length);
		System.out.println(dataV.length);
 		DataPairsAssymFP vis = new DataPairsAssymFP(visParams, dataV, dataM, dataMot.sequences());
 		vis.display();
	}
}