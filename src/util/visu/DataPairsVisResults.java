package util.visu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class DataPairsVisResults extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private ArrayList<String> clampF = new ArrayList<String>();
	private ArrayList<String> estimF = new ArrayList<String>();
	private ArrayList<String> clampB = new ArrayList<String>();
	private ArrayList<String> estimB = new ArrayList<String>();
	
	private int sizeX;
	private int sizeY;
	private int pxs;
	private int margX;
	private int margY;
	private int matrixS;
	private int rowH;
	
	public DataPairsVisResults(int[] visParams, double[][][] data) {
		this.pxs = visParams[0];
		this.margX = visParams[1];
		this.margY = visParams[2];
		this.matrixS = visParams[3];
		this.rowH = visParams[4];
		//data
		for (int i = 0; i < data[0].length; i++) {
			String tmp = "";
			for (int j = 0; j < data[0][i].length; j++) {
				tmp += (int)data[0][i][j];
			}
			clampF.add(tmp);
		}
		for (int i = 0; i < data[1].length; i++) {
			String tmp = "";
			for (int j = 0; j < data[1][i].length; j++) {
				tmp += (int)data[1][i][j];
			}
			estimF.add(tmp);
		}
		for (int i = 0; i < data[2].length; i++) {
			String tmp = "";
			for (int j = 0; j < data[2][i].length; j++) {
				tmp += (int)data[2][i][j];
			}
			clampB.add(tmp);
		}
		for (int i = 0; i < data[3].length; i++) {
			String tmp = "";
			for (int j = 0; j < data[3][i].length; j++) {
				tmp += (int)data[3][i][j];
			}
			estimB.add(tmp);
		}
		int rowW = (clampF.size()%rowH == 0) ? clampF.size()/rowH : clampF.size()/rowH+1;
		sizeX = rowW*2*(matrixS+2)*pxs+4*margX;
		sizeY = rowH*(matrixS+3)*pxs+3*margY;
	}

	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, sizeX, sizeY);
		g.setColor(Color.black);
		Font font = new Font("Serif", Font.PLAIN, 9);
		g.setFont(font);
		int posX = margX;
		int posXrow = margX;
		int posY = margY;
		int rowCounter = 1;
		if (clampF != null) {
			for (int sample = 0; sample < clampF.size(); sample++) {
				if (rowCounter > rowH) {
					posY = margY;
					posXrow += 2*(matrixS+2)*pxs+margX/2;
					rowCounter = 1;
					g.drawLine(posXrow-7, 0, posXrow-7, sizeY);
				} else if (rowCounter > 1) {
					posY += (matrixS+3)*pxs;
				}
				posX = posXrow;
				String clamped = clampB.get(sample);
				String estimated = estimB.get(sample);
				for (int i = 0; i < matrixS; i++) {
					for (int j = 0; j < matrixS; j++) {
						int x = i*matrixS+j;
						g.setColor(Color.black);
						g.drawRect(posX+pxs*i, posY+pxs*j, pxs, pxs);
						if (clamped.charAt(x) == '1' || estimated.charAt(x) == '1') {
							if (clamped.charAt(x) == estimated.charAt(x))
								g.setColor(Color.green);
							else if (clamped.charAt(x) == '1') {
								g.setColor(Color.blue);
							} else {
								g.setColor(Color.red);
							}
							g.fillRect(posX+pxs*i, posY+pxs*j, pxs, pxs); 
						}
					}
				}
				g.setColor(Color.black);
				g.drawString("Back"+(sample+1), posX, posY+(matrixS+2)*pxs);
				posX += (matrixS+2)*pxs;
				clamped = clampF.get(sample);
				estimated = estimF.get(sample);
				for (int i = 0; i < matrixS; i++) {
					for (int j = 0; j < matrixS; j++) {
						int x = i*matrixS+j;
						g.setColor(Color.black);
						g.drawRect(posX+pxs*i, posY+pxs*j, pxs, pxs);
						if (clamped.charAt(x) == '1' || estimated.charAt(x) == '1') {
							if (clamped.charAt(x) == estimated.charAt(x))
								g.setColor(Color.green);
							else if (clamped.charAt(x) == '1') {
								g.setColor(Color.blue);
							} else {
								g.setColor(Color.red);
							}
							g.fillRect(posX+pxs*i, posY+pxs*j, pxs, pxs); 
						}
					}
				}
				g.setColor(Color.black);
				g.drawString("Front"+(sample+1), posX, posY+(matrixS+2)*pxs);
				rowCounter++;	
			}
		}
//		System.out.println("done painting");
	}
	
	public void saveImage(String path) {
		BufferedImage image = new BufferedImage(sizeX,sizeY,BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = image.createGraphics();
		this.paint(graphics2D);
		try {
			File newfile = new File(path);
			if(!newfile.exists())
				newfile.createNewFile();
			ImageIO.write(image, "png", newfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void display() {
		this.setSize(sizeX,sizeY);
		this.setVisible(true);
	}

}