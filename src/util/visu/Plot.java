package util.visu;

import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.traces.Trace2DSimple;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class Plot extends JFrame {
	private static final long serialVersionUID = 1L;

	protected Color[] traceColors = {
			Color.blue,
			Color.red,
			new Color(51,102,0),
			Color.orange,
			Color.magenta,
			Color.green,
			Color.darkGray
	};

	protected Chart2D chart;
	protected ITrace2D traces[];

	public Plot() {
		chart = new Chart2D();
	}

    public Plot(String[] labels) {
        this(labels, false);
    }
	public Plot(String[] labels, boolean reverseTraceOrder) {
		int trcount = labels.length;
		chart = new Chart2D();
		traces = new ITrace2D[trcount];
		if (traceColors.length < trcount)
			traceColors = generateColors(trcount);
        if (reverseTraceOrder)
    		for (int i = traces.length-1; i>= 0; i--) {
	    		traces[i] = new Trace2DSimple();
		    	traces[i].setColor(traceColors[i]);
			    traces[i].setName(labels[i]);
    		    chart.addTrace(traces[i]);
	    	}
		else
    		for (int i = 0; i < traces.length; i++) {
                traces[i] = new Trace2DSimple();
                traces[i].setColor(traceColors[i]);
                traces[i].setName(labels[i]);
                chart.addTrace(traces[i]);
            }
		this.getContentPane().add(chart);
	}

	public Color[] generateColors(int n)
	{
		Color[] cols = new Color[n];
		for(int i = 0; i < n; i++)
		{
			cols[i] = Color.getHSBColor((float) i / (float) n, 0.85f, 1.0f);
		}
		return cols;
	}

	public void setName(String name) {
		this.setTitle(name);
	}

	public void addPoints(int trace, int xval, double[] yvals) {
		for (int i = 0; i < yvals.length; i++) {
			traces[trace].addPoint(xval, yvals[i]);
		}
	}

	public void addPoint(int trace, int xval, double yval) {
		traces[trace].addPoint(xval, yval);
	}

	public void addPoint(int trace, double xval, double yval) {
		traces[trace].addPoint(xval, yval);
	}

	public void display() {
		this.setSize(1000,500);
		this.setVisible(true);
	}

	public void display(int width, int height) {
		this.setSize(width,height);
		this.setVisible(true);
	}

	public void saveImage(String path) {
		try {
			File newfile = new File(path);
			if(!newfile.exists())
				newfile.createNewFile();
			ImageIO.write(chart.snapShot(), "png", newfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public BufferedImage snapShot() {
		return chart.snapShot();
	}

	public void setTraceColors(Color[] newColors) {
		traceColors = newColors;
	}
}
