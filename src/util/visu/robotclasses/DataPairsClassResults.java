package util.visu.robotclasses;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

import base.Conf;

public class DataPairsClassResults extends JFrame {
	private static final long serialVersionUID = 1L;
	public static final int sV = 14;
	public static final int sM = 12;
	public static final int kV = 16;
	public static final int kM = 8;
	public static final String pathVisual = "input/msom/visual_"+sV+"_"+sV+".txt";
	public static final String pathMotor = "input/msom/motor_"+sM+"_"+sM+".txt";
	public static final int[] visParams = {3, 10, 10, 17};
	private static final int M = Conf.movements;
	private static final int P = Conf.perspectives;
	public static final double[][] motorCodes = {{1,0,0},{0,1,0},{0,0,1}};
	public static final int[] seqSizes = Conf.seqSizes;
	
	private int sizeX;
	private int sizeY;
	private int pxs;
	private int margX;
	private int margY;
	private int matrixSin;
	private int matrixSdes;
	private int rowH;
	private Color[] colorsIn;
	private Color[] colorsDes;
	private ArrayList<String> valuesIn = new ArrayList<String>();
	private ArrayList<String> valuesDes = new ArrayList<String>();
	double[][] resultsM;
	
	public DataPairsClassResults(int[] visParams, double[][] dataV, double[][] dataM, double[][] resultsM) {
		this.pxs = visParams[0];
		this.margX = visParams[1];
		this.margY = visParams[2];
		this.rowH = visParams[3];
		this.resultsM = resultsM;
		matrixSin = (int)Math.sqrt(dataV[0].length);
		matrixSdes = (int)Math.sqrt(dataM[0].length);
		int rowW = (valuesIn.size()%rowH == 0) ? dataV.length/rowH : dataV.length/rowH+1;
		sizeX = rowW*2*(matrixSin+3)*pxs+margX/2;
		sizeY = rowH*(matrixSin+3)*pxs+3*margY;
		//data
		for (int i = 0; i < dataV.length; i++) {
			String tmp = "";
			for (int j = 0; j < dataV[i].length; j++) {
				tmp += (int)dataV[i][j];
			}
			valuesIn.add(tmp);
		}
		for (int i = 0; i < dataM.length; i++) {
			String tmp = "";
			for (int j = 0; j < dataM[i].length; j++) {
				tmp += (int)dataM[i][j];
			}
			valuesDes.add(tmp);
		}
		colorsIn = new Color[valuesIn.size()];
		colorsDes = new Color[valuesDes.size()];
		int ind = 0;
		for (int m = 0; m < M; m++) {
			for (int p = 0; p < P; p++) {
				for (int i = 0; i < seqSizes[m]; i++) {
					colorsIn[ind] = new Color(Conf.colors2D[m][p][0],Conf.colors2D[m][p][1],Conf.colors2D[m][p][2]);
					colorsDes[ind] = Conf.colors[m];
					ind++;
				}
			}
		}
	}

	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, sizeX, sizeY);
		g.setColor(Color.black);
		Font font = new Font("Serif", Font.PLAIN, 9);
		g.setFont(font);
		int posX = margX;
		int posXrow = margX;
		int posY = margY;
		int rowCounter = 1;
		int matrixS = (matrixSin > matrixSdes) ? matrixSin : matrixSdes;
		if (valuesIn != null) {
			for (int sample = 0; sample < valuesIn.size(); sample++) {
				if (rowCounter > rowH) {
					posY = margY;
					posXrow += 2*(matrixS)*pxs+margX;
					rowCounter = 1;
//					g.drawLine(posXrow-7, 0, posXrow-7, sizeY);
				} else if (rowCounter > 1) {
					posY += (matrixS+3)*pxs;
				}
				posX = posXrow;
				String patIn = valuesIn.get(sample);
				for (int i = 0; i < matrixSin; i++) {
					for (int j = 0; j < matrixSin; j++) {
						g.drawRect(posX+pxs*i, posY+pxs*j, pxs, pxs);
						if (patIn.charAt(i * matrixSin + j) == '1') {
							g.setColor(colorsIn[sample]);
							g.fillRect(posX+pxs*i, posY+pxs*j, pxs, pxs); 
							g.setColor(Color.black);
						}
					}
				}
//				g.drawString("in"+(sample+1), posX, posY+(matrixS+2)*pxs);
				posX += matrixSin*pxs;
				String patDes = valuesDes.get(sample);
				for (int i = 0; i < matrixSdes; i++) {
					for (int j = 0; j < matrixSdes; j++) {
						g.drawRect(posX+pxs*i, posY+pxs*j, pxs, pxs);
						if (patDes.charAt(i * matrixSdes + j) == '1') {
							g.setColor(colorsDes[sample]);
							g.fillRect(posX+pxs*i, posY+pxs*j, pxs, pxs); 
							g.setColor(Color.black);
						}
					}
				}
				int posX2 = posX + 2*pxs;
				int posY2 = posY + (matrixS-3)*pxs;
				for (int m = 0; m < motorCodes.length; m++) {
					g.drawRect(posX2+2*pxs*m, posY2+2*pxs, 2*pxs, 2*pxs);
					if (
							(m == 0 && sample < P*seqSizes[0]) || 
							(m == 1 && sample >= P*seqSizes[0] && sample < 2*P*seqSizes[1]) ||
							(m == 2 && sample >= 2*P*seqSizes[1] && sample < (2*P*seqSizes[2]+2*P*seqSizes[1]))
						) {
						g.fillRect(posX2+2*pxs*m, posY2+2*pxs, 2*pxs, 2*pxs);
					}
					
				}
//				g.drawString("des"+(sample+1), posX, posY+(matrixS+2)*pxs);
				rowCounter++;	
			}
		}
//		System.out.println("done painting");
	}
	
	public void saveImage(String path) {
		BufferedImage image = new BufferedImage(sizeX,sizeY,BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = image.createGraphics();
		this.paint(graphics2D);
		try {
			File newfile = new File(path);
			if(!newfile.exists())
				newfile.createNewFile();
			ImageIO.write(image, "png", newfile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void display() {
		this.setSize(sizeX,sizeY);
		this.setVisible(true);
	}
	
}