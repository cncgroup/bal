package util.visu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;

import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class DataPairsSel extends DataVisFrame {
	private static final long serialVersionUID = 1L;

	private ArrayList<DataPattern> dataX = new ArrayList<DataPattern>();
	private ArrayList<DataPattern> dataY = new ArrayList<DataPattern>();
	
	public static int count(double[][][] input) {
		int count = 0;
		for (int i = 0; i < input.length; i++) {
			count += input[i].length;
		}
		return count;
	}
	
	public DataPairsSel(double[][][] inputX, double[][][] inputY) {
		super((int)Math.sqrt(inputX[0][0].length),(int)Math.sqrt(inputY[0][0].length),count(inputX));
		int c = 0;
		for (int i = 0; i < inputX.length; i++) {
			for (int j = 0; j < inputX[i].length; j++) {
				dataX.add(new DataPattern(c, dimX, inputX[i][j], Conf.colors[i]));
				c++;
			}
		}
		c = 0;
		for (int i = 0; i < inputX.length; i++) {
			for (int j = 0; j < inputX[i].length; j++) {
				dataY.add(new DataPattern(c, dimY, inputY[i][j], Conf.colors[i]));
				c++;
			}
		}
		rowh = Conf.rowHSmallSet;
		setFrameSize();
	}
	
	public DataPairsSel(double[][] inputX, double[][] inputY) {
		super((int)Math.sqrt(inputX[0].length),(int)Math.sqrt(inputY[0].length),inputX.length);
		int c = 0;
		for (int i = 0; i < inputX.length; i++) {
			dataX.add(new DataPattern(c, dimX, inputX[i], Color.darkGray));
			c++;
		}
		c = 0;
		for (int i = 0; i < inputX.length; i++) {
			dataY.add(new DataPattern(c, dimY, inputY[i], Color.darkGray));
			c++;
		}
		rowh = Conf.rowHSmallSet;
		setFrameSize();
	}
	
	public DataPairsSel(double[][] inputX, double[][] inputY, int[] movm) {
		super((int)Math.sqrt(inputX[0].length),(int)Math.sqrt(inputY[0].length),inputX.length);
		int it = 0;
		for (int i = 0; i < inputX.length; i++) {
			Color col = Color.darkGray;
			if (it < movm[0])
				col = Conf.colors[0];
			else if (it >= movm[0] && it < (movm[0]+movm[1]))
				col = Conf.colors[1];
			else if (it >= (movm[0]+movm[1]))
				col = Conf.colors[2];
			dataX.add(new DataPattern(it, dimX, inputX[i], col));
			it++;
		}
		it = 0;
		for (int i = 0; i < inputX.length; i++) {
			Color col = Color.darkGray;
			if (it < movm[0])
				col = Conf.colors[0];
			else if (it >= movm[0] && it < (movm[0]+movm[1]))
				col = Conf.colors[1];
			else if (it >= (movm[0]+movm[1]))
				col = Conf.colors[2];			
			dataY.add(new DataPattern(it, dimY, inputY[i], col));
			it++;
		}
		rowh = Conf.rowHSmallSet;
		setFrameSize();
	}

	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, width, height);
		g.setColor(Color.black);
		Font font = new Font("Serif", Font.PLAIN, 9);
		g.setFont(font);
		int posX;
		int posXrow = margX;
		int posY = margY;
		int rowCounter = 1;
		for (int id = 0; id < dataX.size(); id++) {
			if (rowCounter > rowh) {
				posY = margY;
				posXrow += (dimX+dimY+4)*pxs+margX/2;
				rowCounter = 1;
				g.drawLine(posXrow-7, 0, posXrow-7, height);
			} else if (rowCounter > 1) {
				posY += (dimMax+3)*pxs;
			}
			posX = posXrow;
			dataX.get(id).paint(g, posX, posY, pxs);
			posX += (dimX+2)*pxs;
			dataY.get(id).paint(g, posX, posY, pxs);
			rowCounter++;	
		}
	}
	
	public static void main(String[] args) {
		int sizeV = 14;
		int sizeM = 8;
		int kVis = 16;
		int kMot = 16;		
		String pathVisual = "input/msom/visual_"+sizeV+"_"+sizeV+".txt";
		String pathMotor = "input/msom/motor_"+sizeM+"_"+sizeM+".txt";
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		double[][][] dataMSeq = dataMot.uniqPatSequences(); //unique patterns for mov types
		double[][][] dataVSeq = new double[Conf.movements][dataMSeq[0].length][dataVis.patternSize()];
		for (int i = 0; i < Conf.movements; i++) {
			dataVSeq[i] = new double[dataMSeq[i].length][dataVis.patternSize()];
			for (int j = 0; j < dataMSeq[i].length; j++) {
				int[] coord =  dataMot.findIndex(dataMSeq[i][j]);
				dataVSeq[i][j] = dataVis.dataSeq()[coord[0]][0].get(coord[1]);
			}
		}
 		DataPairsSel vis = new DataPairsSel(dataMSeq, dataVSeq);
 		vis.display();
	}
}