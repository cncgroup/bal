package util.visu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticMotNew;
import data.DataRoboticVis;
import data.DataRoboticVisNew;

public class DataPairsOneP extends DataVisFrame {
	private static final long serialVersionUID = 1L;

	private DataPattern[] dataVis;
	private DataPattern[] dataMot;
	
	public DataPairsOneP(DataRoboticVis dV, int kV, int persp, DataRoboticMot dM, int kM, int pxs, int margX, int margY, int rowH) {
		super((int)Math.sqrt(dV.patternSize()),(int)Math.sqrt(dM.patternSize()),dM.sampleCount(),pxs,rowH,margX,margY);
		dataVis = new DataPattern[dV.sampleCountFP()];
		dataMot = new DataPattern[dM.sampleCount()];
		int c = 0;
		for (int m = 0; m < Conf.movements; m++) {
			for (double[] pat : dV.dataSeq()[m][persp]) {
				dataVis[c] = new DataPattern(c, dimX, kV, pat, Conf.colors[m]);
				c++;
			}
		}
		c = 0;
		for (int m = 0; m < Conf.movements; m++) {
			for (double[] pat : dM.dataSeq()[m]) {
				dataMot[c] = new DataPattern(c, dimY, kM, pat, Conf.colors[m]);
				c++;
			}
		}
		setFrameSize();
	}	
	
	public DataPairsOneP(DataRoboticVis dV, int kV, int persp, DataRoboticMot dM, int kM) {
		this(dV, kV, persp, dM, kM, Conf.pxsAllP,Conf.rowHAllP,Conf.marginX,Conf.marginY);
	}

	public DataPairsOneP(DataRoboticVis dV, int kV, int persp, DataRoboticMot dM, int kM, int[] visParams) {
		this(dV, kV, persp, dM, kM, visParams[0], visParams[1], visParams[2], visParams[3]);
	}

	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, width, height);
		g.setColor(Color.black);
		Font font = new Font("Serif", Font.PLAIN, 9);
		g.setFont(font);
		int posX;
		int posXrow = margX;
		int posY = margY;
		int rowCounter = 1;
		for (int id = 0; id < dataVis.length; id++) {
			if (rowCounter > rowh) {
				posY = margY;
				posXrow += 2*dimX*pxs;
				rowCounter = 1;
				g.setColor(Color.black);
				g.drawLine(posXrow-7, 0, posXrow-7, height);
			} else if (rowCounter > 1) {
				posY += (dimX+3)*pxs;
			}
			posX = posXrow;
			dataVis[id].paintkWTA(g, posX, posY, pxs);
			posX += (dimX+2)*pxs;
			dataMot[id].paintkWTA(g, posX, posY, pxs);
			rowCounter++;	
		}
	}
	
	public static void main(String[] args) {
//		int sizeV = 14;
//		int sizeM = 8;
		int kVis = 20;
		int kMot = 20;
//		String pathVisual = "input/msom/visual_"+sizeV+"_"+sizeV+".txt";
//		String pathMotor = "input/msom/motor_"+sizeM+"_"+sizeM+".txt";
//		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
//		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		int[] visParams = {4, 10, 40, 8}; // pxs, margX, margY, rowHeight
		DataRoboticVisNew dataVis = new DataRoboticVisNew("input/msomnew/1513336034_stsp.act",",");
		DataRoboticMotNew dataMot = new DataRoboticMotNew("input/msomnew/1513336034_pmc.act", ",");
 		DataPairsOneP vis = new DataPairsOneP(dataVis, kVis, 0, dataMot, kMot, visParams);
 		vis.display();
	}
}