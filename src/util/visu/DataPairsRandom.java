package util.visu;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.io.IOException;

import base.Conf;
import data.DataBinary;

public class DataPairsRandom extends DataVisFrame {
	private static final long serialVersionUID = 1L;
	private DataPattern[] dataX;
	private DataPattern[] dataY;
	
	public DataPairsRandom(DataBinary data, int pxs, int margX, int margY, int rowH) {
		super(data.matrixSizeX(), data.matrixSizeY(), data.patternsX().length, pxs, margX, margY, rowH);
		//data
		dataX = new DataPattern[data.setSize()];
		dataY = new DataPattern[data.setSize()];
		for (int i = 0; i < data.setSize(); i++) {
			dataX[i] = new DataPattern(i, data.matrixSizeX(), data.dataX()[i], Color.black);
		}
		for (int i = 0; i < data.setSize(); i++) {
			dataY[i] = new DataPattern(i, data.matrixSizeY(), data.dataY()[i], Color.black);
		}
		setFrameSize();
	}
	
	public DataPairsRandom(DataBinary data) {
		this(data, Conf.pixelSize, Conf.marginX, Conf.marginY, Conf.rowHeight);
	}

	public void paint(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0, width, height);
		g.setColor(Color.black);
		Font font = new Font("Serif", Font.PLAIN, 9);
		g.setFont(font);
		int posX;
		int posXrow = margX;
		int posY = margY;
		int rowCounter = 1;
		for (int id = 0; id < dataX.length; id++) {
			if (rowCounter > rowh) {
				posY = margY;
				posXrow += 2*(dataX[0].dim()+2)*pxs+margX/2;
				rowCounter = 1;
				g.drawLine(posXrow-7, 0, posXrow-7, height);
			} else if (rowCounter > 1) {
				posY += (dataX[0].dim()+3)*pxs;
			}
			posX = posXrow;
			dataX[id].paintBinary(g, posX, posY, pxs);
			posX += (dataX[0].dim()+2)*pxs;
			dataY[id].paintBinary(g, posX, posY, pxs);
			rowCounter++;	
		}
	}
	
	public static void main(String[] args) throws IOException {
		DataBinary data = new DataBinary(Conf.pathRandInputs, Conf.pathRandDesired);
 		DataPairsRandom vis = new DataPairsRandom(data);
 		vis.display();
	}
}