package util.visu;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class DataPairsResRectangular extends JFrame {
	private static final long serialVersionUID = 1L;

    private int width;
    private int height;

    private static int GAP_X = 6;
    private static int GAP_Y = 2;

    private int rowh;
    private int margX;
    private int margY;

    private int dimXX;
    private int dimXY;
    private int pxsX;
    private int dimYX;
    private int dimYY;
    private int pxsY;
    private int rowInPixels;

	private double[][][] patterns;

	public DataPairsResRectangular(double[][][] patterns, int[] visParams) { //dimXX dimXY pxsX dimYX dimYY pxsY rowh margX margY
        this.dimXX = visParams[0];
        this.dimXY = visParams[1];
        this.pxsX = visParams[2];
        this.dimYX = visParams[3];
        this.dimYY = visParams[4];
        this.pxsY = visParams[5];
        this.rowh = visParams[6];
        this.margX = visParams[7];
        this.margY = visParams[8];
		this.patterns = patterns;
        rowInPixels = (dimXY*pxsX > dimYY*pxsY) ? (dimXY+GAP_Y)*pxsX : (dimYY+GAP_Y)*pxsY;
		setFrameSize();
	}

    protected void setFrameSize() {
        int count = patterns[0].length;
        int roww = (count % rowh == 0) ? count/rowh : count/rowh+1;
        width = roww * (dimXX+GAP_X)*pxsX + (dimYX+GAP_X)*pxsY + 2*margX;


        height = rowh * rowInPixels +margY;
    }

    public void setPatterns(double[][][] patterns) {
		this.patterns = patterns;
		this.repaint();
	}

	public void paint(Graphics g) {
		/*g.setColor(Color.white);
		g.fillRect(0, 0, width, height);*/
		g.setColor(Color.gray);
		Font font = new Font("Serif", Font.PLAIN, 9);
		g.setFont(font);
		int posX;
		int posXrow = margX;
		int posY = margY;
		int rowCounter = 1;
		for (int p = 0; p < patterns[0].length; p++) {
			if (rowCounter > rowh) {
				posY = margY;
				posXrow += (dimXX + GAP_X)*pxsX + (dimYX+1)*pxsY;
				rowCounter = 1;
				g.setColor(Color.gray);
			} else if (rowCounter > 1) {
				posY += rowInPixels+pxsX;
			}
			posX = posXrow;
			paintPattern(g, patterns[2][p], patterns[3][p], dimXX, dimXY, posX, posY, pxsX);
			posX += (dimXX+2)*pxsX;
			paintPattern(g, patterns[0][p], patterns[1][p], dimYX, dimYY, posX, posY, pxsY);
			rowCounter++;
		}
	}
	private int toRGBval(double n) {
		return  n <= 0.0 ? 0 :
				n >= 1.0 ? 255 :
				(int) (n*255.0);
	}

    public void paintPattern(Graphics g, double[] pattern0, double[] pattern1, int dimX, int dimY, int posX, int posY, int pxs) {
        for (int i = 0; i < dimY+1; i++) {
            for (int j = 0; j < dimX+1; j++) {
                int x = i*dimX+j;
                if (x >= pattern0.length) return;
                g.setColor(Color.gray);
                g.drawRect(posX+pxs*j, posY+pxs*i, pxs, pxs);
                g.setColor(new Color(
                        toRGBval(1.0-pattern0[x]),
                        toRGBval(1.0-pattern1[x]-pattern0[x]+2*pattern1[x]*pattern0[x]),
                        toRGBval(1.0-pattern1[x])));
                g.fillRect(posX+pxs*j,posY+pxs*i, pxs, pxs);
            }
        }

    }
    public void saveImage(String path) {
        BufferedImage image = new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = image.createGraphics();
        this.paint(graphics2D);
        try {
            File newfile = new File(path);
            if(!newfile.exists())
                newfile.createNewFile();
            ImageIO.write(image, "png", newfile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void display() {
        this.setSize(width,height);
        this.setVisible(true);
    }
}