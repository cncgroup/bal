package util;

public class Quicksort {
	private double[] values;
	private int size;

	public void sort(double[] values) {
		if (values == null || values.length == 0) {
			return;
		}
		this.values = values;
		size = values.length;
		quicksort(0, size - 1);
	}

	private void quicksort(int low, int high) {
		int i = low, j = high;
		double pivot = values[low + (high - low) / 2];

		while (i <= j) {
			while (values[i] < pivot) {
				i++;
			}
			while (values[j] > pivot) {
				j--;
			}
			if (i <= j) {
				exchange(i, j);
				i++;
				j--;
			}
		}
		if (low < j)
			quicksort(low, j);
		if (i < high)
			quicksort(i, high);
	}

	private void exchange(int i, int j) {
		double temp = values[i];
		values[i] = values[j];
		values[j] = temp;
	}
}