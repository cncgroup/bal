package data;

import base.Conf;
import base.NetUtil;
import util.Util;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class DataOmniglot {

	protected ArrayList<double[]> picsTrain;
	protected ArrayList<double[]> catsTrain;
	protected ArrayList<double[]> picsTest;
	protected ArrayList<double[]> catsTest;
	public long[] catCountTrain;
	public long[] catCountTest;
	protected int patternSize;
	protected int categoryCount;
	protected int n;

	@SuppressWarnings("unchecked")
	public DataOmniglot(){
		super();
	}

    private static byte[] toByteArrayAutoClosable(BufferedImage image, String type) throws IOException {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()){
            ImageIO.write(image, type, out);
            return out.toByteArray();
        }
    }

	public DataOmniglot(String path, String delimiter, long trainSize, long testSize) {
		// DATA TRAIN
	    int patternPrefix = 1;
		//seek the sample size
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(path)));
			String line = br.readLine();
			patternSize = line.split(delimiter).length - patternPrefix; // -1 because the first datum is category number, not part of picsTrain
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		//create data structure
		picsTrain = new ArrayList<double[]>();
		catsTrain = new ArrayList<double[]>();
		catCountTrain = new long[categoryCount];

		//read train data
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(path)));
			String line = "";
			double[] lastCat = new double[categoryCount];
			double[] lastPic = new double[patternSize];
			while ((line = br.readLine()) != null) {
				line = line.trim();
				String[] tmp = line.split(delimiter);

				// read category
				int c = Integer.parseInt(tmp[0]);
				Arrays.fill(lastCat, 0.0);
				lastCat[c] = 1.0;
				catCountTrain[c]++;

				// read picture
				for (int i = patternPrefix; i < tmp.length; i++) {
					double p = Integer.parseInt(tmp[i]);
					lastPic[i-patternPrefix] = p / 255.0;
				}

				// add
				catsTrain.add(lastCat.clone());
				picsTrain.add(lastPic.clone());
				if ((trainSize > 0) && (catsTrain.size() >= trainSize)) break;
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

        // DATA TEST
        patternPrefix = 1;
        //seek the sample size
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(path)));
            String line = br.readLine();
            patternSize = line.split(delimiter).length - patternPrefix; // -1 because the first datum is category number, not part of picsTrain
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //create data structure
        picsTest = new ArrayList<double[]>();
        catsTest = new ArrayList<double[]>();
        catCountTest = new long[categoryCount];
        //read data
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(path)));
            String line = "";
            double[] lastCat = new double[categoryCount];
            double[] lastPic = new double[patternSize];
            while ((line = br.readLine()) != null) {
                line = line.trim();
                String[] tmp = line.split(delimiter);

                // read category
                int c = Integer.parseInt(tmp[0]);
                Arrays.fill(lastCat, 0.0);
                lastCat[c] = 1.0;
                catCountTest[c]++;

                // read picture
                for (int i = patternPrefix; i < tmp.length; i++) {
                    double p = Integer.parseInt(tmp[i]);
                    lastPic[i-patternPrefix] = p / 255.0;
                }

                // add
                catsTest.add(lastCat.clone());
                picsTest.add(lastPic.clone());
                if ((testSize > 0) && (catsTest.size() >= testSize)) break;
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

    public DataOmniglot(String[] paths, String delimiter, double[][][] digitRepres, long trainSize, long testSize) {
        // DATA TRAIN
        int patternPrefix = 1;
        //seek the sample size
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(paths[0])));
            String line = br.readLine();
            patternSize = line.split(delimiter).length - patternPrefix; // -1 because the first datum is category number, not part of picsTrain
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //create data structure
        picsTrain = new ArrayList<double[]>();
        catsTrain = new ArrayList<double[]>();
        catCountTrain = new long[categoryCount];

        //read train data
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(paths[0])));
            String line = "";
            double[] lastPic = new double[patternSize];
            while ((line = br.readLine()) != null) {
                line = line.trim();
                String[] tmp = line.split(delimiter);

                // read category
                int c = Integer.parseInt(tmp[0]);
                // read picture
                for (int i = patternPrefix; i < tmp.length; i++) {
                    double p = Integer.parseInt(tmp[i]);
                    lastPic[i-patternPrefix] = p / 255.0;
                }
                // add
                catsTrain.add(Util.flatten(Util.transpose(digitRepres[c])));
                picsTrain.add(lastPic.clone());
                if ((trainSize > 0) && (catsTrain.size() >= trainSize)) break;
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // DATA TEST
        patternPrefix = 1;
        //seek the sample size
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(paths[1])));
            String line = br.readLine();
            patternSize = line.split(delimiter).length - patternPrefix; // -1 because the first datum is category number, not part of picsTrain
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //create data structure
        picsTest = new ArrayList<double[]>();
        catsTest = new ArrayList<double[]>();
        catCountTest = new long[categoryCount];
        //read data
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(paths[1])));
            String line = "";
            double[] lastPic = new double[patternSize];
            while ((line = br.readLine()) != null) {
                line = line.trim();
                String[] tmp = line.split(delimiter);

                // read category
                int c = Integer.parseInt(tmp[0]);
                // read picture
                for (int i = patternPrefix; i < tmp.length; i++) {
                    double p = Integer.parseInt(tmp[i]);
                    lastPic[i-patternPrefix] = p / 255.0;
                }
                // add
                catsTest.add(Util.flatten(Util.transpose(digitRepres[c])));
                picsTest.add(lastPic.clone());
                if ((testSize > 0) && (catsTest.size() >= testSize)) break;
            }
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	public double[][] valuesPics() {
		double[][] out = new double[picsTrain.size()][];
		for (int i = 0; i < picsTrain.size(); i++) {
			out[i] = picsTrain.get(i);
		}
		return out;
	}

	public double[][] valuesCats() {
		double[][] out = new double[catsTrain.size()][];
		for (int i = 0; i < catsTrain.size(); i++) {
			out[i] = catsTrain.get(i);
		}
		return out;
	}	

	public double[][] pic2d(int i) {
		return pic2d(i, 28); // HACK magic number
	}

	public double[][] pic2d(int i, int side) {
		double out[][] = new double[side][side];
		double[] picture = this.picsTrain.get(i);
		for (int j = 0; j < picture.length; j++)
			out[j/side][j%side] = picture[j];
		return out;
	}

	public char[][] pic2dASCII(int i, int side) {
		char[] ASCII = " ~:+?%&#@".toCharArray();
		char out[][] = new char[side][side];
		double[] picture = this.picsTrain.get(i);
		for (int j = 0; j < picture.length; j++)
			out[j/side][j%side] = ASCII[(int)(picture[j]*8)];
		return out;
	}

	public double[][][] valuesSamples() {
		double[][][] out = new double[catsTrain.size()][2][];
		for (int i = 0; i< catsTrain.size(); i++) {
			out[i][0] = picsTrain.get(i);
			out[i][1] = catsTrain.get(i);
		}
		return out;
	}

	public double[] getPics(int i) {
		return picsTrain.get(i);
	}

	public double[] getCats(int i) {
		return catsTrain.get(i);
	}

	public double[][] getSample(int i) {
		return new double[][] {
				picsTrain.get(i),
				catsTrain.get(i),
		};
	}

	public ArrayList<double[]> picSeq() {
		return picsTrain;
	}

	public int patternSize() {
		return patternSize;
	}

	public static void main(String[] args) {
//		DataOmniglot data = new DataOmniglot(Conf.dataSetsMNISTPaths[0]);
//		System.out.println(data.patternSize);
//		System.out.println(data.picsTrain.size());
//		System.out.println(data.catsTrain.size());
//		System.out.println(data.picsTest.size());
//		System.out.println(data.catsTest.size());
//
//		int index = 1;
//		System.out.println(index+": "+Arrays.toString(data.catsTrain.get(index)));
//		System.out.println(index+": "+Arrays.toString(data.catsTest.get(index)));
//		char[][] example = data.pic2dASCII(index, 28);
//		for(int i=0; i<example.length; i++)
//			System.out.println(example[i]);
//		System.out.println();
	}

}
