package data;

import base.Conf;
import base.NetUtil;
import util.Util;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.TreeMap;

public class DataRoboticVisNew extends DataRoboticVis {

	@SuppressWarnings("unchecked")
	public DataRoboticVisNew(String path, String delimiter, boolean distFunc){
		super();
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(path)));
			//parse the sample size
			String[] parsed = br.readLine().split(delimiter);
			patternSize = Integer.valueOf(parsed[0])*Integer.valueOf(parsed[1]);
			//create data structure
			data = new ArrayList[Conf.movements][Conf.perspectives];
			for (int x = 0; x < data.length; x++) {
				for (int y = 0; y < data[x].length; y++) {
					data[x][y] = new ArrayList<>();
				}
			}
			//read data
			String line = "";
			while ((line = br.readLine()) != null) {
				line = line.trim();
				String[] tmp = line.split(delimiter);
				int x = Integer.parseInt(tmp[1])-1;
				int y = Integer.parseInt(tmp[2]);
				double[] item = new double[patternSize];
				for (int i = 0; i < patternSize; i++) {
					item[i] = Double.parseDouble(tmp[i+3]);
				}
				data[x][y].add(item.clone());
			}

			br.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public DataRoboticVisNew(String path, String delimiter) {
		this(path, delimiter,false);
	}

	public DataRoboticVisNew(String path) {
		this(path, Conf.defaultDelimiter,false);
	}

}
