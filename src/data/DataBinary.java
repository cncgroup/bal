package data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class DataBinary {
	
	protected double[][] dataX;
	protected double[][] dataY;
		
	public DataBinary(String fileIn, String fileOut) throws IOException  {	
		BufferedReader br = new BufferedReader(new FileReader(new File(fileIn)));
		ArrayList<String> linesIn = new ArrayList<String>();
		String line;
		while ((line = br.readLine()) != null) {
			linesIn.add(line.trim());
		}
		br.close();
		BufferedReader br2 = new BufferedReader(new FileReader(new File(fileOut)));
		ArrayList<String> linesOut = new ArrayList<String>();
		while ((line = br2.readLine()) != null) {
			linesOut.add(line.trim());
		}
		br2.close();
		
		int samplec = (linesIn.size() < linesOut.size()) ? linesIn.size() : linesOut.size(); 
		int patSize = linesIn.get(0).length();
		dataX = new double[samplec][patSize];
		dataY = new double[samplec][patSize];
		
		int ind = 0;
		for (String tmpline : linesIn) {
			for (int j = 0; j < tmpline.length(); j++) {
				dataX[ind][j] = Double.parseDouble(""+tmpline.charAt(j));
			}
			ind++;
			if (ind == samplec)
				break;
		}
		ind = 0;
		for (String tmpline : linesOut) {
			for (int j = 0; j < tmpline.length(); j++) {
				dataY[ind][j] = Double.parseDouble(""+tmpline.charAt(j));
			}
			ind++;
			if (ind == samplec)
				break;
		}
	}

	public String patternX(int id) {
		String out = "";
		for (int j = 0; j < dataX[id].length; j++) {
			if (dataX[id][j] == 1.0) 
				out += "1";
			else 
				out += "0";
		}
		return out;
	}
	
	public String patternY(int id) {
		String out = "";
		for (int j = 0; j < dataY[id].length; j++) {
			if (dataY[id][j] == 1.0) 
				out += "1";
			else 
				out += "0";
		}
		return out;
	}
	
	public String[] patternsX() {
		String[] out = new String[dataX.length];
		for (int i = 0; i < dataX.length; i++) {
			out[i] = "";
			for (int j = 0; j < dataX[i].length; j++) {
				if (dataX[i][j] == 1.0) 
					out[i] += "1";
				else 
					out[i] += "0";
			}
		}
		return out;
	}
	
	public String[] patternsY() {
		String[] out = new String[dataY.length];
		for (int i = 0; i < dataY.length; i++) {
			out[i] = "";
			for (int j = 0; j < dataY[i].length; j++) {
				if (dataY[i][j] == 1.0) 
					out[i] += "1";
				else 
					out[i] += "0";
			}
		}
		return out;
	}
		
	public double[][] dataX() {
		return dataX;
	}
	
	public double[][] dataY() {
		return dataY;
	}
	
	public int patSizeX() {
		return dataX[0].length;
	}
	
	public int patSizeY() {
		return dataY[0].length;
	}
	
	public int matrixSizeX() {
		return (int)(Math.sqrt(dataX[0].length));
	}
	
	public int matrixSizeY() {
		return (int)(Math.sqrt(dataY[0].length));
	}
	
	public int setSize() {
		if (dataX.length == dataY.length) 
			return dataX.length;
		else {
			System.out.println("ERROR input length "+dataX.length+" != output length "+dataY.length);
			return -1;
		}
	}
	
}
