package data;

import base.Conf;
import base.NetUtil;
import util.Util;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.TreeMap;

public class DataRoboticMot {
	
	protected ArrayList<double[]>[] data;
	protected int patternSize;

	@SuppressWarnings("unchecked")
	public DataRoboticMot() {
		super();
	}

	public DataRoboticMot(String path, String delimiter, boolean distFunc) {
		//seek the sample size
		try {	
			BufferedReader br = new BufferedReader(new FileReader(new File(path)));
			String line = br.readLine();
			patternSize = line.split(delimiter).length;
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//create data structure
		data = new ArrayList[Conf.movements];
		for (int i = 0; i < data.length; i++) {
			data[i] = new ArrayList<double[]>();
		}
		//read data
		try {	
			BufferedReader br = new BufferedReader(new FileReader(new File(path)));
			String line = "";
			double[] lastVal = new double[patternSize];
			while ((line = br.readLine()) != null) {
				line = line.trim();
				String[] tmp = line.split(" ");
				if (line.indexOf("&") > -1) {
					int i = Integer.parseInt(tmp[1]);
					data[i].add(Util.copy(lastVal));
				} else {
					for (int i = 0; i < tmp.length; i++) {
						if (distFunc)
							Math.exp(-Conf.distExpConst*Double.parseDouble(tmp[i]));
						else 
							lastVal[i] = Double.parseDouble(tmp[i]);
					}
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public DataRoboticMot(String path, String delimiter) {
		this(path, delimiter,false);
	}

	public DataRoboticMot(String path) {
		this(path, Conf.defaultDelimiter,false);
	}
	
	public void kWinnerTakeAll(int k) {
		for (int i = 0; i < data.length; i++) {
			ArrayList<double[]> newData = new ArrayList<double[]>(); 
			for (double[] sample : data[i]) {		
				newData.add(NetUtil.kWTAMin(sample, k));
			}
			data[i] = newData;
		}
	}
	
	public void multiply(int c) {
		for (int m = 0; m < Conf.movements; m++) {
			ArrayList<double[]> tmp = new ArrayList<double[]>();
			for (double[] sample: data[m]) {
				for (int j = 0; j < c; j++) {
				 	tmp.add(sample);
				}
			}
			data[m] = tmp;
		}
	}
	
	public void multiply() {
		multiply(Conf.perspectives);
	}
	
	public double[][] valuesMergMovkWTA(int k) {
		int count = 0;
		for (int i = 0; i < Conf.movements; i++) {
			count += data[i].size();
		}
		double[][] out = new double[count][patternSize];
		double[][] meshed = new double[Conf.perspectives][patternSize];
		for (int i = 0; i < Conf.movements; i++) {
			double[] tmpmesh = new double[patternSize];
			for (double[] sample: data[i]) {
				tmpmesh = Util.add(tmpmesh, sample);
			}
			meshed[i] = NetUtil.kWTAMin(tmpmesh, k);
		}
		int iterator = 0;
		for (int i = 0; i < Conf.movements; i++) {
			for (int j = 0; j < data[i].size(); j++) {
				out[iterator] = meshed[i];
				iterator++;
			}
		}
		return out;
	}
	
	public double[][] values() {
		int count = 0;
		for (int i = 0; i < data.length; i++) {
			count += data[i].size();
		}
		double[][] out = new double[count][patternSize];
		int iterator = 0;
		for (int i = 0; i < data.length; i++) {
			for (double[] sample: data[i]) {
				out[iterator] = sample;
				iterator++;
			}
		}
		return out;
	}
	
	public double[][] valuesIndices(int[][] indices) {
		int count = 0;
		for (int i = 0; i < indices.length; i++) {
			count += indices[i].length;
		}
		double[][] out = new double[count][patternSize];
		int iterator = 0;
		for (int i = 0; i < Conf.movements; i++) {
			for (int j = 0; j < indices[i].length; j++) {
				out[iterator] = data[i].get(indices[i][j]);
				iterator++;
			}
		}
		return out;
	}
	
	public double[][][] valuesIndicesMov(int[][] indices) {
		double[][][] out = new double[indices.length][indices[0].length][patternSize];
		for (int i = 0; i < Conf.movements; i++) {
			out[i] = new double[indices[i].length][patternSize];
			for (int j = 0; j < indices[i].length; j++) {
				out[i][j] = data[i].get(indices[i][j]);
			}
		}
		return out;
	}
	
	public double[][] valuesIndicesMulti(int[][] indices) {
		int count = 0;
		for (int i = 0; i < indices.length; i++) {
			count += indices[i].length;
		}
		double[][] out = new double[count][patternSize];
		int iterator = 0;
		for (int i = 0; i < Conf.movements; i++) {
			for (int j = 0; j < indices[i].length; j++) {
				out[iterator] = data[i].get(indices[i][j]);
				iterator++;
			}
		}
		return out;
	}
	
	public double[][] valuesOnePat(int multiply) {
		double[][] out = new double[multiply*Conf.movements][patternSize];
		int it = 0;
		for (int j = 0; j < data.length; j++) {
			for (int i = 0; i < multiply; i++) {
				out[it] = data[j].get(0);
				it++;
			}			
		}
		return out;
	}
	
	public double[][] valuesOnePat() {
		double[][] out = new double[Conf.movements][patternSize];
		for (int i = 0; i < data.length; i++) {
			out[i] = data[i].get(0);
		}
		return out;
	}
	
	public ArrayList<double[]>[] dataSeq() {
		return data;
	}
	
	public int sampleCount() {
		int count = 0;
		for (int i = 0; i < data.length; i++) {
			count += data[i].size();
		}
		return count;
	}
	
	public int patternSize() {
		return patternSize;
	}
	
	public int[] sequences() {
		int[] out = new int[Conf.movements];
		for (int i = 0; i < data.length; i++) {
			out[i] = data[i].size();
		}
		return out;
	}
	
	public HashSet<double[]> valuesInSet() {
		HashSet<double[]> out = new HashSet<double[]>();
		for (int i = 0; i < data.length; i++) {
			for (double[] sample: data[i]) {
				out.add(sample);
			}
		}
		return out;
	}
	
	public double[][] dataPatternsOnly() {
		HashSet<String> patterns = dataPatternsStrSet();
		double[][] out = new double[patterns.size()][patternSize];
		int iterator = 0;
		for (String sample : patterns) {
			out[iterator] = Util.strToDouble(sample);
			iterator++;
		}
		return out;
	}
	
	public double[][][] uniqPatSequences() {
		@SuppressWarnings("unchecked")
		HashSet<String>[] patterns = new HashSet[Conf.movements];
		for (int i = 0; i < Conf.movements; i++) {
			patterns[i] = new HashSet<String>();
			for (double[] s : data[i]) {
				patterns[i].add(Util.arrayToBinaryString(s));
			}
		}
		int maxp = 0;
		for (int i = 0; i < patterns.length; i++) {
			maxp = (maxp < patterns[i].size()) ? patterns[i].size() : maxp;
		}
		double[][][] out = new double[3][maxp][patternSize];
		int it = 0;
		for (int i = 0; i < Conf.movements; i++) {
			out[i] = new double[patterns[i].size()][patternSize];
			for (String p : patterns[i]) {
				out[i][it] = Util.strToDouble(p);
				it++;
			}
			it = 0;
		}
		return out;
	}
	
	public int[][] uniqPatIndices() {
		@SuppressWarnings("unchecked")
		TreeMap<Integer,String>[] patterns = new TreeMap[Conf.movements];
		for (int i = 0; i < Conf.movements; i++) {
			patterns[i] = new TreeMap<Integer,String>();
			for (int j = 0; j < data[i].size(); j++) {
				if (!(patterns[i].containsValue(Util.arrayToBinaryString(data[i].get(j)))))
					patterns[i].put(j,Util.arrayToBinaryString(data[i].get(j)));
			}
		}
		int[][] out = new int[3][patterns[0].size()];
		for (int i = 0; i < Conf.movements; i++) {
			out[i] = new int[patterns[i].size()];
			int j = 0;
			for (Entry<Integer,String> val : patterns[i].entrySet()) {
				out[i][j] = val.getKey();
				j++;
			}
		}
		return out;
	}
	
	@SuppressWarnings("unchecked")
	public int[][] uniqPatIndices(int[][] fromIndices) {
		HashSet<Integer>[] allowedInd = new HashSet[3];
		for (int i = 0; i < Conf.movements; i++) {
			allowedInd[i] = new HashSet<Integer>();
			for (int j = 0; j < fromIndices[i].length; j++) {
				allowedInd[i].add(fromIndices[i][j]);
			}
		}
		TreeMap<Integer,String>[] patterns = new TreeMap[Conf.movements];
		for (int i = 0; i < Conf.movements; i++) {
			patterns[i] = new TreeMap<Integer,String>();
			for (int j = 0; j < data[i].size(); j++) {
				if (!(patterns[i].containsValue(Util.arrayToBinaryString(data[i].get(j)))))
					patterns[i].put(j,Util.arrayToBinaryString(data[i].get(j)));
			}
		}
		int[][] out = new int[3][patterns[0].size()];
		for (int i = 0; i < Conf.movements; i++) {
			out[i] = new int[patterns[i].size()];
			int j = 0;
			for (Entry<Integer,String> val : patterns[i].entrySet()) {
				out[i][j] = val.getKey();
				j++;
			}
		}
		return out;
	}

	public void softmax() {
		for (int i = 0; i < data.length; i++) {
			ArrayList<double[]> newData = new ArrayList<>();
			for (double[] sample : data[i]) {
				newData.add(NetUtil.softmax(sample));
			}
			data[i] = newData;
		}
	}

	public void winDistExp() {
		for (int i = 0; i < data.length; i++) {
			ArrayList<double[]> newData = new ArrayList<>();
			for (double[] sample : data[i]) {
				newData.add(NetUtil.winnerDist(sample));
			}
			data[i] = newData;
		}
	}
	
	public double[][] dataPatternsOnly(int m) {
		HashSet<String> patterns = dataPatternsStrSet(m);
		double[][] out = new double[patterns.size()][patternSize];
		int iterator = 0;
		for (String sample : patterns) {
			out[iterator] = Util.strToDouble(sample);
			iterator++;
		}
		return out;
	}
	
	public HashSet<String> dataPatternsStrSet() {
		HashSet<String> patterns = new HashSet<String>();
		for (int i = 0; i < Conf.movements; i++) {
			for (double[] s : data[i]) {
				patterns.add(Util.arrayToBinaryString(s));
			}
		}
		return patterns;
	}
	
	public HashSet<String> dataPatternsStrSet(int m) {
		HashSet<String> patterns = new HashSet<String>();
		for (double[] s : data[m]) {
			patterns.add(Util.arrayToBinaryString(s));
		}
		return patterns;
	}
	
	public int uniquePatternCount() {
		HashSet<String> patterns = dataPatternsStrSet();
		return patterns.size();
	}
	
	public int findIndex(int mov,int persp,double[] val) {
		for (int i = 0; i < data[mov].size(); i++) {
			if (val == data[mov].get(i))
				return i;
		}
		return 0;
	}
	
	public int[] findIndex(double[] val) {
		for (int i = 0; i < Conf.movements; i++) {
			for (int j = 0; j < data[i].size(); j++) {
				if (Arrays.equals(val,data[i].get(j))) {
					return new int[] {i,j};
				}
			}
		}
		return new int[] {0,0};
	}
	
	public void writeDataPatterns(String pathin) {
		HashSet<String> patterns = dataPatternsStrSet();
		try {
			BufferedWriter bwIn = new BufferedWriter(new FileWriter(new File(pathin)));
			for (String entry : patterns) {
				bwIn.append(entry);
				bwIn.newLine();
			}
			bwIn.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void echo() {
		for (int i = 0; i < data.length; i++) {
			for (double[] s : data[i]) {
				System.out.println(i+":"+Arrays.toString(s));
			}
		}
	}
	
	public void echoBin() {
		for (int i = 0; i < data.length; i++) {
			for (double[] s : data[i]) {
				System.out.println(i+":"+Util.arrayToBinaryString(s));
			}
		}
	}

	public static void main(String[] args) {
		DataRoboticMot test = new DataRoboticMot(Conf.dataSetsMotPaths[1]," ");
//		test.echo();
//		test.kWinnerTakeAll(10);
//		test.echoBin();
		for (int i = 0; i < Conf.movements; i++) {
			System.out.println(test.dataSeq()[i].size());
		}
		double[][] tmp = test.valuesMergMovkWTA(8);
		System.out.println(tmp.length);
	}

}
