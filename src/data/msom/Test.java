package data.msom;

import java.util.Arrays;

import data.DataRoboticMot;
import data.DataRoboticVis;
import base.Conf;

public class Test {
	public static final int sV = 14;
	public static final int sM = 12;
	public static final int kV = 16;
	public static final int kM = 8;
	public static final String pathVisual = "input/msom/visual_"+sV+"_"+sV+".txt";
	public static final String pathMotor = "input/msom/motor_"+sM+"_"+sM+".txt";
	public static final int[] visParams = {3, 10, 10, 16};
//	private static final int M = Conf.movements;
//	private static final int P = Conf.perspectives;
	public static final double[][] motorCodes = {{1,0,0},{0,1,0},{0,0,1}};
	public static final int[] seqSizes = Conf.seqSizes;
	
	public static void main(String[] args) {
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataMot.multiply();
		dataVis.kWinnerTakeAll(kV);
		dataMot.kWinnerTakeAll(kM);
		for (int i = 0; i < dataVis.sequences().length; i++) {
			System.out.println(Arrays.toString(dataVis.sequences()[i]));
		}
		System.out.println();
		for (int i = 0; i < dataMot.sequences().length; i++) {
			System.out.println(dataMot.sequences()[i]);
		}
	}
}
