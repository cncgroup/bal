package data.msom;

import java.io.IOException;
import java.util.HashSet;

import util.TexLog;
import util.Util;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class UniquePatAssocAllPerskWTA {
	
	public static final int maxEpcs = 1000;
	public static final int[] visParams = {5, 10, 30, 10, 10};
	public static final String pathVisual = Conf.dataSetsVisPaths[0];
	public static final String pathMotor = Conf.dataSetsMotPaths[1];
	
	public static void main(String[] args) throws IOException {
		
		int maxK = Conf.maxK;
		int minK = Conf.minK;
		String[][] resultTable = new String[maxK-minK+1][11]; //vis-0,mot-0,...,vis-3,mot-3
		
		for (int k = minK; k <= maxK; k++) {
			//load data set
			DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
			DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
			dataVis.kWinnerTakeAll(k);
			dataMot.kWinnerTakeAll(k);
			resultTable[k-minK][0] = ""+k;
			int patVisSum = 0;
			int patMotSum = 0;
			for (int persp = 0; persp < Conf.perspectives; persp++) {
				double[][] dataV = dataVis.dataPatternsOnly(persp); //selected perspective
				double[][] dataM = new double[dataV.length][dataV[0].length];
		 		for (int i = 0; i < dataV.length; i++) {
		 			int[] coord =  dataVis.findIndex(dataV[i]);
					dataM[i] = dataMot.dataSeq()[coord[0]].get(coord[2]);
				}
		 		//assigned motor patterns
				HashSet<String> motPat = new HashSet<String>();
				for (int i = 0; i < dataM.length; i++) {
					motPat.add(Util.arrayToBinaryString(dataM[i]));
				}
				resultTable[k-minK][2*persp+1] = ""+dataV.length;
				resultTable[k-minK][2*persp+2] = ""+motPat.size();
				patVisSum += dataV.length;
				patMotSum += motPat.size();
			}
			resultTable[k-minK][9] = ""+patVisSum;
			resultTable[k-minK][10] = ""+patMotSum;
		}
		
		for (int i = 0; i < resultTable.length; i++) {
			for (int j = 0; j < resultTable[i].length; j++) {
				System.out.print(resultTable[i][j]+"\t");
			}
			System.out.println();
		}
		
		String[] tabLabels = new String[11];
		tabLabels[0] = "k";
		for (int persp = 0; persp < Conf.perspectives; persp++) {
			tabLabels[2*persp+1] = "Vis-"+persp;
			tabLabels[2*persp+2] = "Mot-"+persp;
		}
		tabLabels[9] = "Vis-Sum";
		tabLabels[10] = "Mot-Sum";
		TexLog log = new TexLog();
		log.addResultsTable(tabLabels, resultTable, "Unique visual and motor patterns after kWTA from visual data viewpoint", "tab:unique-patterns-allpersp-kwta");
		log.echo();
	}
}
