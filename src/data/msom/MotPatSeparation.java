package data.msom;

import java.io.IOException;
import java.util.ArrayList;

import util.TexLog;
import util.visu.msom.DataMotMesh3;
import base.Conf;
import base.NetUtil;
import util.Util;
import data.DataRoboticMot;

public class MotPatSeparation {
	
	public static final int[] visParams = {10, 10, 10};
	public static final int minK = 4;
	public static final int maxK = 16;
	public static final int[] sizes = Conf.dataMotSizes;
	public static final String logName = "log-data-mot-pattern-distance.tex";
	
	public static void main(String[] args) throws IOException {
		
		TexLog figLog = new TexLog();
		double[][] patDist = new double[sizes.length][maxK-minK+1]; 
		int[][] uniquePat = new int[sizes.length][maxK-minK+1]; 
		
		int c = 0;
		for (int d = 0; d < sizes.length; d++) {
			for (int ki = 0; ki <= maxK-minK; ki++) {
				//init
				DataRoboticMot data = new DataRoboticMot("input/msom/motor_"+sizes[d]+"_"+sizes[d]+".txt");
				data.kWinnerTakeAll(minK+ki);
				//compute dist
				int operations = 0;
				ArrayList<double[]>[] dataSeq = data.dataSeq();
				for (int m = 0; m < Conf.movements; m++) {
					for (int m2 = 0; m2 < Conf.movements; m2++) {
						if (!(m == m2)) {
							for (double[] pattern1 : dataSeq[m]) {
								for (double[] pattern2 : dataSeq[m2]) {
									patDist[d][ki] += NetUtil.avgDistCenter(pattern1,pattern2);
									operations++;
								}
							}
						}
					}
				}
				patDist[d][ki] /= operations;
				uniquePat[d][ki] = data.uniquePatternCount();
				//Motorize
				String no = (c < 10) ? "0"+c : ""+c;
				no = (c < 100) ? "0"+c : ""+c;
				String figName = "dataMot"+no+"-"+sizes[d]+"x"+sizes[d]+"-k"+(minK+ki)+"-all"+Conf.figExt;
				DataMotMesh3 v = new DataMotMesh3(visParams,data);
				v.display();
				v.saveImage(Conf.pathOutput+Conf.pathFigures+"msommesh/"+figName);
				System.out.println("written: "+figName);
				//write fig log
				figLog.addFigure(
						Conf.pathFigures+"msommesh/"+figName, 
						"Motor robotic data "+sizes[d]+"x"+sizes[d]+" k="+(minK+ki), 
						"fig:robotic-data-mot-"+sizes[d]+"x"+sizes[d]+"-k"+(minK+ki), 
						0.6
				);
				c++;
			}
		}

		TexLog log = new TexLog();
		String[][] table = new String[maxK-minK+2][sizes.length+1]; 
		table[0][0] = "k/data";
		for (int d = 0; d < sizes.length; d++) {
			table[0][d+1] = sizes[d]+"x"+sizes[d];
			for (int ki = 0; ki <= maxK-minK; ki++) {
				table[ki+1][0] = ""+(minK+ki);
				table[ki+1][d+1] = Util.round(patDist[d][ki],3)+"";
			}
		}

		log.addTable(table, "Motor robotic data with selected k: pattern distances", "tab:data-robotic-mot-pattdist");
		String[][] table2 = new String[maxK-minK+2][sizes.length+1]; 
		table2[0][0] = "k/data";
		for (int d = 0; d < sizes.length; d++) {
			table2[0][d+1] = sizes[d]+"x"+sizes[d];
			for (int ki = 0; ki <= maxK-minK; ki++) {
				table2[ki+1][0] = ""+(minK+ki);
				table2[ki+1][d+1] = uniquePat[d][ki]+"";
			}
		}
		
		for (int i = 0; i < table.length; i++) {
			for (int j = 0; j < table[i].length; j++) {
				System.out.print(table[i][j]+"\t"+table2[i][j]+"\t");
			}
			System.out.println();
		}

		log.addTable(table2, "Motor robotic data with selected k: unique patterns", "tab:data-robotic-mot-patunique");
//		log.addLog(figLog);
		log.export(Conf.pathOutput+logName);
		System.out.println("written: "+logName);
	}
	
}
