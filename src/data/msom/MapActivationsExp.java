package data.msom;

import java.io.IOException;

import util.TexLog;
import util.visu.Histogram;
import base.Conf;
import base.NetUtil;
import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class MapActivationsExp {

	public static final String figName = "histogram-activations-expdist-";
	public static final double prec = 0.01;
	public static final double mi = 2.0;
	
	public static int[] histogram(double[][] data, double prec) {
		double max = Util.max(data);
		int[] hist = new int[(int) (max/prec)];
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[0].length; j++) {
				for (int a = 0; a < hist.length; a++) {
					if (data[i][j] >= a*prec && data[i][j] < (a+1)*prec) {
						hist[a]++;
					}
				}
			}
		}
		return hist;
	}
	
	public static void main(String[] args) {

		TexLog log = new TexLog();
		log.addLine("");
		log.addLine("\\section{MSOM activation distribution with exponential distance function}");
		log.addLine("Distance: $d' = e^-\\mu d$, where $\\mu = 2.0$");
		log.addLine("\\subsection{Visual maps - all sizes}");
		log.addLine("");
		for (int i = 0; i < Conf.dataVisSizes.length; i++) {
			int size = Conf.dataVisSizes[i];
			String path = "input/msom/visual_"+size+"_"+size+".txt";
			DataRoboticVis dataVis = new DataRoboticVis(path);
			int[] hist = MapActivationsExp.histogram(NetUtil.mapDistFunc(dataVis.values(),mi), prec);
			String[] actLabels = new String[hist.length];
			for (int j = 0; j < actLabels.length; j++) {
				actLabels[j] = (j%5==0) ? Util.round(j*prec,1)+"" : "";
			}
			Histogram plot = new Histogram(hist, 600, 400, 20, actLabels);
//			plot.display();
			String fig = Conf.pathFigures+figName+"visual-"+size+"x"+size+Conf.figExt;
			plot.saveImage(Conf.pathOutput+fig);
			log.addFigure(
					fig,
					"Distribution of activation values in MSOM visual data "+size+"x"+size,
					"msom-vis-"+size+"-act-distrib",
					.6);
			log.addLine("");
		}
		log.addLine("");
		log.addLine("\\subsection{Motor maps - all sizes}");
		log.addLine("");
		for (int i = 0; i < Conf.dataMotSizes.length; i++) {
			int size = Conf.dataMotSizes[i];	
			String path = "input/msom/motor_"+size+"_"+size+".txt";
			DataRoboticMot dataMot = new DataRoboticMot(path);
			int[] hist = MapActivationsExp.histogram(NetUtil.mapDistFunc(dataMot.values(),mi), prec);
			String[] actLabels = new String[hist.length];
			for (int j = 0; j < actLabels.length; j++) {
				actLabels[j] = (j%5==0) ? Util.round(j*prec,1)+"" : "";
			}
			Histogram plot = new Histogram(hist, 600, 400, 20, actLabels);
//			plot.display();
			String fig = Conf.pathFigures+figName+"motor-"+size+"x"+size+Conf.figExt;
			plot.saveImage(Conf.pathOutput+fig);
			log.addFigure(
					fig,
					"Distribution of activation values in MSOM motor data "+size+"x"+size,
					"msom-mot-"+size+"-act-distrib",
					.6);
			log.addLine("");
		}
		log.echo();
		try {
			log.export(Conf.pathOutput+"log-msom-activation-histogram-expdist.tex");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
