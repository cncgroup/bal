package data.msom;

import java.io.IOException;
import java.util.ArrayList;

import util.TexLog;
import base.Conf;
import base.NetUtil;
import util.Util;
import data.DataRoboticVis;

public class VisPatSeparation {
	
	public static final int[] visParams = {8, 10, 10};
	public static final int minK = 4;
	public static final int maxK = 20;
	public static final int[] sizes = {10,12,14,16,18,20};
	public static final String logName = "log-data-vis-pattern-distance.tex";
	
	public static void main(String[] args) throws IOException {
		
//		TexLog figLog = new TexLog();
		double[][] patDist = new double[sizes.length][maxK-minK+1]; 
		int[][] uniquePat = new int[sizes.length][maxK-minK+1]; 
		
//		int c = 0;
		for (int d = 0; d < sizes.length; d++) {
			for (int ki = 0; ki <= maxK-minK; ki++) {
				//init
				DataRoboticVis data = new DataRoboticVis("input/msom/visual_"+sizes[d]+"_"+sizes[d]+".txt");
				data.kWinnerTakeAll(minK+ki);
				//compute dist
				int operations = 0;
				ArrayList<double[]>[][] dataSeq = data.dataSeq();
				for (int m = 0; m < Conf.movements; m++) {
					for (int m2 = 0; m2 < Conf.movements; m2++) {
						for (int p = 0; p < Conf.perspectives; p++) {
							for (int p2 = 0; p2 < Conf.perspectives; p2++) {
								if (!(p == p2 && m == m2)) {
									for (double[] pattern1 : dataSeq[m][p]) {
										for (double[] pattern2 : dataSeq[m2][p2]) {
											patDist[d][ki] += NetUtil.avgDistCenter(pattern1,pattern2);
											operations++;
										}
									}
								}
							}
						}
					}
				}
				patDist[d][ki] /= operations;
				uniquePat[d][ki] = data.uniquePatternCount();
				//visualize
//				String no = (c < 10) ? "0"+c : ""+c;
//				no = (c < 100) ? "0"+c : ""+c;
//				String figName = "dataVis"+no+"-"+sizes[d]+"x"+sizes[d]+"-k"+(minK+ki)+"-allmaps"+Conf.figExt;
//				DataVisMesh4 v = new DataVisMesh4(visParams,data);
//				v.display();
//				v.saveImage(Conf.pathOutput+Conf.pathFigures+"msommesh/"+figName);
//				System.out.println("written: "+figName);
//				//write fig log
//				figLog.addFigure(
//						Conf.pathFigures+"msommesh/"+figName, 
//						"Visual robotic data "+sizes[d]+"x"+sizes[d]+" k="+(minK+ki), 
//						"fig:robotic-data-vis-"+sizes[d]+"x"+sizes[d]+"-k"+(minK+ki), 
//						0.6
//				);
//				c++;
			}
		}

		TexLog log = new TexLog();
		String[][] table = new String[maxK-minK+2][sizes.length+1]; 
		table[0][0] = "k/data";
		for (int d = 0; d < sizes.length; d++) {
			table[0][d+1] = sizes[d]+"x"+sizes[d];
			for (int ki = 0; ki <= maxK-minK; ki++) {
				table[ki+1][0] = ""+(minK+ki);
				table[ki+1][d+1] = Util.round(patDist[d][ki],3)+"";
			}
		}

		log.addTable(table, "Visual robotic data with selected k: pattern distances", "tab:data-robotic-vis-pattdist");
		String[][] table2 = new String[maxK-minK+2][sizes.length+1]; 
		table2[0][0] = "k/data";
		for (int d = 0; d < sizes.length; d++) {
			table2[0][d+1] = sizes[d]+"x"+sizes[d];
			for (int ki = 0; ki <= maxK-minK; ki++) {
				table2[ki+1][0] = ""+(minK+ki);
				table2[ki+1][d+1] = uniquePat[d][ki]+"";
			}
		}
		
		for (int i = 0; i < table.length; i++) {
			for (int j = 0; j < table[i].length; j++) {
				System.out.print(table[i][j]+"\t"+table2[i][j]+"\t");
			}
			System.out.println();
		}

		log.addTable(table2, "Visual robotic data with selected k: unique patterns", "tab:data-robotic-vis-patunique");
//		log.addLog(figLog);
		log.export(Conf.pathOutput+logName);
		System.out.println("written: "+logName);
	}
	
}
