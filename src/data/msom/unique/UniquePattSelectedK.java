package data.msom.unique;

import java.io.IOException;

import util.TexLog;
import base.Conf;
import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class UniquePattSelectedK {
	public static final int minK = 1;
	public static final int maxK = 40;
	public static final String[] tabLabelsVert = {"dataset","avg patterns","max patterns","min patterns","patterns-to-k ratio"};
	public static final String[] dataNames = Conf.dataSets;
	public static final String figName = "msom-patterns-kwta-selected";
	public static final String logName = "msom-patterns-kwta-selected.tex";
	
	public static void main(String[] args) throws IOException {
		
		double[][] patternStats = new double[dataNames.length][4]; //avg, max, min, patternsToK
		for (int i = 0; i < patternStats.length; i++) {
			patternStats[i][2] = 100;
		}
		String[][] plotData = new String[dataNames.length][maxK-minK+1];
				
		for (int i = 0; i < dataNames.length; i++) {
			System.out.println("processing "+dataNames[i]);
			for (int k = minK; k <= maxK; k++) {
				int pcount = 0;
				if (i < Conf.dataSetsVisPaths.length) {
					DataRoboticVis dataVis = new DataRoboticVis(Conf.dataSetsVisPaths[i]);
					dataVis.kWinnerTakeAll(k+1);
					pcount = dataVis.uniquePatternCount();
				} else {
					DataRoboticMot dataMot = new DataRoboticMot(Conf.dataSetsMotPaths[i-Conf.dataSetsVisPaths.length+1]);
					dataMot.kWinnerTakeAll(k+1);
					pcount = dataMot.uniquePatternCount();
				}
				patternStats[i][0] += pcount; //avg
				plotData[i][k-minK] = "("+k+","+pcount+")";
				patternStats[i][1] = (pcount > patternStats[i][1]) ? pcount : patternStats[i][1]; //max
				patternStats[i][2] = (pcount < patternStats[i][2]) ? pcount : patternStats[i][2]; //min
				patternStats[i][3] += (pcount / (k+1)); //pat/k
			}
		}

		System.out.println();
		String[][] outdata = new String[dataNames.length][tabLabelsVert.length];
		for (int i = 0; i < dataNames.length; i++) {
			outdata[i][0] = dataNames[i];
		}
		for (int i = 0; i < dataNames.length; i++) {
			outdata[i][1] = ""+Util.round(patternStats[i][0]/maxK, 3);
			outdata[i][2] = ""+(int) patternStats[i][1];
			outdata[i][3] = ""+(int) patternStats[i][2];
			outdata[i][4] = ""+Util.round(patternStats[i][3]/maxK, 3);
		}
		TexLog log = new TexLog();
		log.addLine("Unique patterns after kWTA with selected k");
		log.addLine("We selected k from range 4 to 40 and applied the previous test on all data sets.");
		log.addLine("");
		log.addResultsTable(tabLabelsVert, outdata, "Robotic msom data after kWTA: statistics", "tab:rdata-patterns-stats");
		log.addLine("");
		log.addLine("\\begin{figure}");
		log.addPlot(plotData, dataNames, "k", "unique patterns");
		log.addLine("\\label{fig:"+figName+"}");
		log.addLine("\\caption{Robotic msom data: patterns after kWTA}");
		log.addLine("\\end{figure}");
		log.addLine("");
//		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
}
