package data.msom;

import java.io.IOException;
import java.util.HashSet;

import util.TexLog;
import util.Util;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class UniquePatAssocAllMovkWTA {
	
	public static final int maxEpcs = 1000;
	public static final int k = 8;
	public static final int[] visParams = {5, 10, 30, 10, 10};
	public static final String pathVisual = Conf.dataSetsVisPaths[0];
	public static final String pathMotor = Conf.dataSetsMotPaths[1];
	
	public static void main(String[] args) throws IOException {
		
		int maxK = Conf.maxK;
		int minK = Conf.minK;
		String[][] resultTable = new String[maxK-minK+1][9]; //vis-0,mot-0,...,vis-3,mot-3
		
		for (int k = minK; k <= maxK; k++) {
			//load data set
			DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
			DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
			dataVis.kWinnerTakeAll(k);
			dataMot.kWinnerTakeAll(k);
			resultTable[k-minK][0] = ""+k;
			int patVisSum = 0;
			int patMotSum = 0;
			for (int mov = 0; mov < Conf.movements; mov++) {
				double[][] dataM = dataMot.dataPatternsOnly(mov); //selected movement
				double[][] dataV = new double[dataM.length][dataM[0].length];
				for (int i = 0; i < dataM.length; i++) {
//					for (int j = 0; j < Conf.perspectives; j++) {
			 			int[] coord =  dataMot.findIndex(dataM[i]);
						dataV[i] = dataVis.dataSeq()[coord[0]][0].get(coord[1]);
//					}
				}
		 		//assigned motor patterns
				HashSet<String> visPat = new HashSet<String>();
				for (int i = 0; i < dataV.length; i++) {
					visPat.add(Util.arrayToBinaryString(dataV[i]));
				}
				resultTable[k-minK][2*mov+1] = ""+dataM.length;
				resultTable[k-minK][2*mov+2] = ""+visPat.size();
				patMotSum += dataM.length;
				patVisSum += visPat.size();
			}
			resultTable[k-minK][7] = ""+patMotSum;
			resultTable[k-minK][8] = ""+patVisSum;
		}
		
		for (int i = 0; i < resultTable.length; i++) {
			for (int j = 0; j < resultTable[i].length; j++) {
				System.out.print(resultTable[i][j]+"\t");
			}
			System.out.println();
		}
		
		String[] tabLabels = new String[9];
		tabLabels[0] = "k";
		for (int persp = 0; persp < Conf.perspectives; persp++) {
			tabLabels[2*persp+1] = "Mot-"+persp;
			tabLabels[2*persp+2] = "Vis-"+persp;
		}
		tabLabels[7] = "Mot-Sum";
		tabLabels[8] = "Vis-Sum";
		TexLog log = new TexLog();
		log.addResultsTable(tabLabels, resultTable, "Unique motor and visual patterns after kWTA from motor data viewpoint", "tab:unique-patterns-allpersp-kwta");
		log.echo();
	}
}
