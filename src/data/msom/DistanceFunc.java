package data.msom;

import java.io.IOException;

import util.TexLog;
import util.visu.Histogram;
import base.Conf;
import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class DistanceFunc {
	
	public static final double maxMi = 5.0;
	public static final double minMi = 1.0;
	public static final double prec = 0.2;

	public static void main(String[] args) {
				
		int count = (int) (maxMi/prec);
		double[] mi = new double[count];
		for (int i = 0; i < mi.length; i++) {
			mi[i] = minMi + i*prec;
		}
		
		TexLog log = new TexLog();
		log.addLine("");
		log.addLine("\\subsection{Exploring distance function}");
		log.addLine("Plots display overal activity of neurons using different distance functions. First, only distance ($d$) is displayed," +
				"then exponential distance with different $\\mu$ according to $d = e^-\\mu d$");
		log.addLine("");
		
		for (int s = 0; s < Conf.dataVisSizes.length; s++) {
			int size = Conf.dataVisSizes[s];
			String path = "input/msom/visual_"+size+"_"+size+".txt";
			DataRoboticVis dataVis = new DataRoboticVis(path);
			double avgActivity[] = new double[mi.length+1];
			
			double[][] values = dataVis.values();
			for (int i = 0; i < values.length; i++) {
				for (int j = 0; j < values[0].length; j++) {
					avgActivity[0] += values[i][j];
					for (int a = 1; a < avgActivity.length; a++) {
						avgActivity[a] += Math.exp(-mi[a-1]*values[i][j]);
					}
				}
			}
			String[] labels = new String[avgActivity.length];
			for (int i = 0; i < labels.length; i++) {
				if (i == 0)
					labels[i] = "d";
				else 
					if ((i-1) % 5 == 0)
						labels[i] = "e^(-"+Util.round(mi[i-1],1)+"d)";
					else
						labels[i] = "";
			}
			int[] avgActTmp = new int[avgActivity.length];
			for (int i = 0; i < avgActivity.length; i++) {
				avgActTmp[i] = (int)avgActivity[i];
			}
			Histogram plot = new Histogram(avgActTmp,400,200,20,labels);
//			plot.display();
			String fig = Conf.pathFigures+"activity-distfunc-visual-"+size+"x"+size+Conf.figExt;
			plot.saveImage(Conf.pathOutput+fig);
			log.addFigure(
					fig,
					"Overal neural activity in visual MSOM "+size+"x"+size,
					"msom-vis-"+size+"-act-distfunc",
					.5);
			log.addLine("");
		}
		
		for (int s = 0; s < Conf.dataMotSizes.length; s++) {
			int size = Conf.dataVisSizes[s];
			String path = "input/msom/visual_"+size+"_"+size+".txt";
			DataRoboticMot dataMot = new DataRoboticMot(path);
			double avgActivity[] = new double[mi.length+1];
			
			double[][] values = dataMot.values();
			for (int i = 0; i < values.length; i++) {
				for (int j = 0; j < values[0].length; j++) {
					avgActivity[0] += values[i][j];
					for (int a = 1; a < avgActivity.length; a++) {
						avgActivity[a] += Math.exp(-mi[a-1]*values[i][j]);
					}
				}
			}
			String[] labels = new String[avgActivity.length];
			for (int i = 0; i < labels.length; i++) {
				if (i == 0)
					labels[i] = "d";
				else 
					if ((i-1) % 5 == 0)
						labels[i] = "e^(-"+Util.round(mi[i-1],1)+"d)";
					else
						labels[i] = "";
			}
			int[] avgActTmp = new int[avgActivity.length];
			for (int i = 0; i < avgActivity.length; i++) {
				avgActTmp[i] = (int)avgActivity[i];
			}
			Histogram plot = new Histogram(avgActTmp,400,200,20,labels);
//			plot.display();
			String fig = Conf.pathFigures+"activity-distfunc-motor-"+size+"x"+size+Conf.figExt;
			plot.saveImage(Conf.pathOutput+fig);
			log.addFigure(
					fig,
					"Overal neural activity in motor MSOM "+size+"x"+size,
					"msom-mot-"+size+"-act-distfunc",
					.5);
			log.addLine("");
		}
		log.echo();
		
		try {
			log.export(Conf.pathOutput+"log-msom-distance-func.tex");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
