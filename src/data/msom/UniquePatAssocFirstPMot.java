package data.msom;

import java.io.IOException;
import java.util.HashSet;

import util.Util;
import util.visu.DataPairsVis;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class UniquePatAssocFirstPMot {
	
	public static final int maxEpcs = 1000;
	public static final int k = 8;	
	public static final String pathVisual = Conf.dataSetsVisPaths[0];
	public static final String pathMotor = Conf.dataSetsMotPaths[1];
	public static final String figName = "generec-msom-endpoint-assoc.png";
	public static final String logName = "log-assoc-simple.tex";
	public static final int[] visParams = {5, 10, 30, 10, 10};
	
	public static void main(String[] args) throws IOException {
		
		//load data set
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(k);
		dataMot.kWinnerTakeAll(k);
		double[][] dataM = dataMot.dataPatternsOnly(0); //first movement
		double[][] dataV = new double[dataM.length][dataM[0].length];
 		for (int i = 0; i < dataM.length; i++) {
 			int[] coord =  dataMot.findIndex(dataM[i]);
 			dataV[i] = dataVis.dataSeq()[coord[0]][0].get(coord[1]);
		}
 		DataPairsVis vis = new DataPairsVis(visParams, dataM, dataV);
 		vis.display();
 		
 		//are connected motor patterns unique?
		HashSet<String> patterns = new HashSet<String>();
		for (int i = 0; i < dataV.length; i++) {
			patterns.add(Util.arrayToBinaryString(dataV[i]));
		}
		System.out.println("Mot-all:"+dataM.length);
		System.out.println("Vis-all:"+patterns.size());
		
		for (String pat : patterns) {
			System.out.println(pat);
		}
	}
}
