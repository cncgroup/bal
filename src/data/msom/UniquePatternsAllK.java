package data.msom;

import java.io.IOException;

import util.TexLog;
import util.Util;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class UniquePatternsAllK {
	
	public static final int maxK = 140;
	public static final String[] tabLabelsVert = {"dataset","avg patterns","max patterns","min patterns","patterns-to-k ratio"};
	public static final String[] dataNames = Conf.dataSets;
	public static final String figName = "msom-patterns-kwta-all";
	public static final String logName = "msom-patterns-kwta-all.tex";
	
	public static void main(String[] args) throws IOException {

		double[][] patternStats = new double[dataNames.length][4]; //avg, max, min, patternsToK
		for (int i = 0; i < patternStats.length; i++) {
			patternStats[i][2] = 100;
		}
		String[][] plotData = new String[dataNames.length][maxK];
				
		for (int i = 0; i < dataNames.length; i++) {
			System.out.println("processing "+dataNames[i]);
			for (int k = 0; k < maxK; k++) {
				int pcount = 0;
				if (i < 4) {
					DataRoboticVis dataVis = new DataRoboticVis(Conf.dataSetsVisPaths[i]);
					if (k < dataVis.patternSize()) {
						dataVis.kWinnerTakeAll(k+1);
						pcount = dataVis.uniquePatternCount();
						patternStats[i][0] += pcount; //avg
						plotData[i][k] = "("+k+","+pcount+")";
						patternStats[i][1] = (pcount > patternStats[i][1]) ? pcount : patternStats[i][1]; //max
						patternStats[i][2] = (pcount < patternStats[i][2]) ? pcount : patternStats[i][2]; //min
						patternStats[i][3] += (pcount / (k+1)); //pat/k
					}
				} else {
					DataRoboticMot dataMot = new DataRoboticMot(Conf.dataSetsMotPaths[i-4]);
					if (k < dataMot.patternSize()) {
						dataMot.kWinnerTakeAll(k+1);
						pcount = dataMot.uniquePatternCount();
						patternStats[i][0] += pcount; //avg
						plotData[i][k] = "("+k+","+pcount+")";
						patternStats[i][1] = (pcount > patternStats[i][1]) ? pcount : patternStats[i][1]; //max
						patternStats[i][2] = (pcount < patternStats[i][2]) ? pcount : patternStats[i][2]; //min
						patternStats[i][3] += (pcount / (k+1)); //pat/k
					}
				}
			}
		}

		System.out.println();
		String[][] outdata = new String[dataNames.length][tabLabelsVert.length];
		for (int i = 0; i < dataNames.length; i++) {
			outdata[i][0] = dataNames[i];
		}
		for (int i = 0; i < dataNames.length; i++) {
			outdata[i][1] = ""+Util.round(patternStats[i][0]/maxK, 3);
			outdata[i][2] = ""+(int) patternStats[i][1];
			outdata[i][3] = ""+(int) patternStats[i][2];
			outdata[i][4] = ""+Util.round(patternStats[i][3]/maxK, 3);
		}
		TexLog log = new TexLog();
		log.addLine("\\subsection{Unique patterns after kWTA with all possible k}");
		log.addLine("");
		log.addResultsTable(tabLabelsVert, outdata, "Robotic msom data after kWTA: statistics", "tab:rdata-patterns-stats");
		log.addLine("");
		log.addLine("\\begin{figure}");
		log.addPlot(plotData, dataNames, Conf.plotStyles1, "k", "unique patterns");
		log.addLine("\\label{fig:"+figName+"}");
		log.addLine("\\caption{Robotic msom data: patterns after kWTA}");
		log.addLine("\\end{figure}");
//		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
