package data;

import base.Conf;
import base.NetUtil;
import util.Util;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.TreeMap;

public class DataRoboticMotNew extends DataRoboticMot{

	@SuppressWarnings("unchecked")
	public DataRoboticMotNew(String path, String delimiter, boolean distFunc) {
		super();
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(path)));
			//parse the sample size
			String[] parsed = br.readLine().split(delimiter);
			patternSize = Integer.valueOf(parsed[0])*Integer.valueOf(parsed[1]);
			//create data structure
			data = new ArrayList[Conf.movements];
			for (int i = 0; i < data.length; i++) {
				data[i] = new ArrayList<double[]>();
			}
			String line = "";
			while ((line = br.readLine()) != null) {
				line = line.trim();
				String[] tmp = line.split(delimiter);
				int x = Integer.parseInt(tmp[1]) - 1;
				double[] item = new double[patternSize];
				for (int i = 0; i < patternSize; i++) {
					item[i] = Double.parseDouble(tmp[i+2]);
				}
				data[x].add(item.clone());
			}
			for (int i = 0; i < data.length; i++) {
				for (int j = 0; j < data[i].size(); j++) {
					for (int k = 0; k < data[i].get(j).length; k++) {
						System.out.print(data[i].get(j)[k]+" ");
					}
					System.out.println();
				}
				System.out.println();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public DataRoboticMotNew(String path, String delimiter) {
		this(path, delimiter,false);
	}

	public DataRoboticMotNew(String path) {
		this(path, Conf.defaultDelimiter,false);
	}

}
