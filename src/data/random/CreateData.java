package data.random;

import java.io.IOException;

import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class CreateData {
	public static final int sampleCount = 100;
//	public static final int sampleCount = 16;
	public static final int inps = 100;
	public static final int prefK = 10;
	public static final int minK = 4;
	public static final int maxK = 12;		
	public static final double pOnesIn = 0.1;
	public static final double pOnesOut = 0.1;
	
	public static final String pathVisual = Conf.dataSetsVisPaths[0];
	public static final String pathMotor = Conf.dataSetsMotPaths[1];
	public static final String pathExportMSOM = "input/msompatterns/8x8-";
	public static final String pathExportKWTA = "input/binary-kWTA/";
	public static final String pathExportKWTA2 = "input/binary-kWTA-hard/";
	public static final String pathExportRand = "input/binary-random/";
	
	public static void main(String[] args) throws IOException {
//		dataRandomkWTA(minK,maxK);
//		dataRandomMaxHalfTrue(pOnesIn, pOnesOut);
//		dataRoboticPatterns();
//		dataRandomkWTAHarder(minK,maxK,200);
//		dataRandomkWTA(minK,maxK,300);
//		for (int i = 0; i < 10; i++) {
//			dataRandomkWTA(prefK,prefK,50+10*i);
//		}
//		dataRandomkWTA(3,8);\
//		dataRandomMaxHalfTrue(pOnesIn, pOnesOut);
		Generator g = new Generator(pathExportKWTA+"k"+prefK+"-in", pathExportKWTA+"k"+prefK+"-des", inps);
		g.generate(105, 10);
		g.write();
//		Generator g = new Generator(
//				pathExportKWTA2+"data-impossible-k"+prefK+"-in",
//				pathExportKWTA2+"data-impossible-k"+prefK+"-des",
//				inps);
//		for (int i = 1; i < 10; i++) {
//			g.generateImpossible(16*i, prefK);
//			g.write();
//		}
	}
	
	public static void dataRoboticPatterns() throws IOException {
		for (int k = minK; k <= maxK; k++) {
			DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
			DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
			dataVis.kWinnerTakeAll(k);
			dataVis.writeDataPatterns(pathExportMSOM+"k"+k+"-in");
			dataMot.kWinnerTakeAll(k);
			dataMot.writeDataPatterns(pathExportMSOM+"k"+k+"-des");
			System.out.println("finished writing data patterns with k = "+k+", "+dataVis.sampleCount()+"/"+dataMot.sampleCount()+" sampleSize");
		}
	}
	
	public static void dataRandomkWTA(int minK, int maxK) {
		Generator[] gs = new Generator[maxK-minK+1];
		for (int k = minK; k <= maxK; k++) {
			int ind = k-minK;
			int size = k*Conf.kToSizeRatio;
			gs[ind] = new Generator(pathExportKWTA+"k"+k+"-in", pathExportKWTA+"k"+k+"-des", inps);
			gs[ind].generate(size, k);
			System.out.println("finished writing dataset with "+k+" T bits in "+inps+" bits long sample");
			gs[ind].write();
		}
	}
	
	public static void dataRandomkWTA(int minK,int maxK,int sampleSize,int sampleCount) {
		Generator[] gs = new Generator[maxK-minK+1];
		for (int k = minK; k <= maxK; k++) {
			int ind = k-minK;
			gs[ind] = new Generator(pathExportKWTA+"k"+k+"-in", pathExportKWTA+"k"+k+"-des", sampleSize);
			gs[ind].generate(sampleCount, k);
			System.out.println("finished writing dataset with "+k+" T bits in "+sampleSize+" bits long sample");
			gs[ind].write();
		}
	}
	
	public static void dataRandomkWTAHarder(int minK, int maxK, int dataSize) {
		Generator[] gs = new Generator[maxK-minK+1];
		for (int k = minK; k <= maxK; k++) {
			int ind = k-minK;
			int size = dataSize;
			gs[ind] = new Generator(pathExportKWTA2+"k"+k+"-in", pathExportKWTA2+"k"+k+"-des", inps);
			gs[ind].generateMultiply(size, k);
			System.out.println("finished writing dataset with "+k+" T bits in "+inps+" bits long sample");
			gs[ind].write();
		}
	}
	
	public static void dataRandomMaxHalfTrue(double pOnesIn, double pOnesOut) {
		Generator gen = new Generator(pathExportRand+"inp", pathExportRand+"des", inps);
//		gen.generate(sampleCount, pOnesIn, pOnesOut);
		gen.generateAssymInOut(sampleCount, pOnesIn, pOnesOut);
		gen.write();
		System.out.println("finished writing dataset");
	}
	
}
