package data.random;

import base.Conf;
import data.DataBinary;
import data.DataRoboticMot;
import data.DataRoboticVis;
import util.visu.DataPairsVis;

import java.io.IOException;

public class ViewDataPairsRandomHard {

	public static final int k = 10;
	public static final int[] visParams1 = {5, 0, 40, 10, 10}; // pxs, margX, margY, matrixS, rowHeight
	
	public static void main(String[] args) throws IOException {
		
		DataBinary data = new DataBinary(
				Conf.pathDataRandomHard+"data-impossible-k"+k+"-in100x100",
				Conf.pathDataRandomHard+"data-impossible-k"+k+"-des100x100");
		
		DataPairsVis vis = new DataPairsVis(visParams1,data.dataX(),data.dataY());
		vis.display();

	}
	
}
