package data.random;

import base.Conf;
import data.DataBinary;
import util.Util;
import util.visu.DataPairsRandom;

import java.io.IOException;

public class MapLike {

	public static final int[] visParams = {6, 5, 30, 4, 4};
	
	public static void main(String[] args) throws IOException {
		
//		int[] tmp = new int[16];
//		for (int i = 0; i < tmp.length; i++) {
//			System.out.println(Util.arrayToBinaryString(tmp));
//		}
		DataBinary data = new DataBinary("input/binary-mapstyle/k3-in-16x16", "input/binary-mapstyle/k3-des-16x16");
		for (int i = 0; i < data.dataY().length/4; i++) {
			System.out.println("des"+i+" "+Util.arrayToBinaryString(data.dataY()[4*i]));
			double[] tmp = new double[data.dataX().length];
			for (int x = 0; x < tmp.length; x++) {
				tmp[x] = 1;
			}
			for (int j = 0; j < 4; j++) {
				tmp = Util.binaryAndPositiveBits(data.dataX()[4*i+j],tmp);
				System.out.println("in"+4*i+j+": "+Util.arrayToBinaryString(data.dataX()[4*i+j])+"\t"+Util.positiveBits(tmp)+" positive bits in AND");
			}
			System.out.println();
		}
		DataPairsRandom vis = new DataPairsRandom(data);
		vis.display();
		vis.saveImage(Conf.pathOutput+"dataset-binary-random-maplike-4x4-k3"+Conf.figExt);
	}
	
}
