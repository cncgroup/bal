package data.random;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import util.Util;

public class Generator {

	private String pathin;
	private String pathdes;
	private int inps;
	private int[][] inputs;
	private int[][] desired;
	
	public Generator(String pathin, String pathout, int inps) {
		this.pathin = pathin;
		this.pathdes = pathout;
		this.inps = inps;
	}
	
	private int[] newBinCode(int length, double p) {
		int[] out = new int[length];
		int ones = 0;
		for (int i = 0; i < length; i++) {
			if (Math.random()<p && ones < (length/2)) {
				out[i] = 1;
				ones++;
			} else
				out[i] = 0;
		}
		return out;
	}

	private int[] newBinCode(int length, int maxTrueBits) {
		int[] out = new int[length];
		int ones = 0;
		for (int i = 0; i < length; i++) {
			double prob = maxTrueBits/(length-i*1.0);
			if (Math.random() < prob && ones < maxTrueBits) {
				out[i] = 1;
				ones++;
			} else
				out[i] = 0;
		}
		return out;
	}
	
	public void generateAssymInOut(int samplec, double pOnesIn, double pOnesOut) {
		inputs = new int[samplec][inps];
		for (int i = 0; i < samplec; i++) {
			inputs[i] = newBinCode(inps,pOnesIn);
			for (int j = 0; j < i; j++) {
				if (Util.identical(inputs[i], inputs[j]))
					inputs[i] = newBinCode(inps,pOnesIn);
			}
		}
		desired = new int[samplec][inps];
		for (int i = 0; i < samplec; i++) {
			desired[i] = newBinCode(inps,pOnesOut);
			for (int j = 0; j < i; j++) {
				if (Util.identical(desired[i], desired[j]))
					desired[i] = newBinCode(inps,pOnesOut);
			}
		}
	}
	
	public void generate(int samplec, int maxTrueBits) {
		inputs = new int[samplec][inps];
		for (int i = 0; i < samplec; i++) {
			inputs[i] = newBinCode(inps,maxTrueBits);
			for (int j = 0; j < i; j++) {
				if (Util.identical(inputs[i], inputs[j]))
					inputs[i] = newBinCode(inps,maxTrueBits);
			}
		}
		desired = new int[samplec][inps];
		for (int i = 0; i < samplec; i++) {
			desired[i] = newBinCode(inps,maxTrueBits);
			for (int j = 0; j < i; j++) {
				if (Util.identical(desired[i], desired[j]))
					desired[i] = newBinCode(inps,maxTrueBits);
			}
		}
	}

	public void generateImpossible(int samplec, int maxTrueBits) {
		inputs = new int[samplec][inps];
		for (int i = 0; i < samplec; i++) {
			inputs[i] = newBinCode(inps,maxTrueBits);
			for (int j = 0; j < i; j++) {
				if (Util.identical(inputs[i], inputs[j]))
					inputs[i] = newBinCode(inps,maxTrueBits);
			}
		}
		desired = new int[samplec][inps];
		int i = 0;
		while (i < samplec) {
			desired[i] = newBinCode(inps,maxTrueBits);
			for (int j = 0; j < i; j++) {
				if (Util.identical(desired[i], desired[j]))
					desired[i] = newBinCode(inps,maxTrueBits);
			}
			for (int m = 1; m < 4; m++) {
				desired[i+m] = desired[i];
			}
			i+=4;
		}
	}
	
	public void generateMultiply(int samplec,int maxTrueBits) {
		inputs = new int[samplec][inps];
		for (int i = 0; i < samplec/2; i++) {
			inputs[i] = newBinCode(inps,maxTrueBits);
		}
		for (int i = 0; i < samplec/2; i++) {
			inputs[samplec/2+i] = inputs[i];
		}
		desired = new int[samplec][inps];
		for (int i = 0; i < samplec/4; i++) {
			desired[i] = newBinCode(inps,maxTrueBits);
		}
		for (int i = 0; i < samplec/4; i++) {
			desired[samplec/4+i] = inputs[i];
			desired[2*samplec/4+i] = inputs[i];
			desired[3*samplec/4+i] = inputs[i];
		}
	}
	
	public void write() {
		int samplec = inputs.length; 
		BufferedWriter bwIn;
		try {
			bwIn = new BufferedWriter(new FileWriter(new File(pathin+inps+"x"+samplec)));
			for (int i = 0; i < samplec; i++) {
				String line = "";
				for (int j = 0; j < inps; j++) {
					line += Integer.toString((int)inputs[i][j]);
				}
				bwIn.append(line);
				if (i < (samplec-1)) 
					bwIn.newLine();
			}
			bwIn.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		BufferedWriter bwDes;
		try {
			bwDes = new BufferedWriter(new FileWriter(new File(pathdes+inps+"x"+samplec)));
			for (int i = 0; i < samplec; i++) {
				String line = "";
				for (int j = 0; j < inps; j++) {
					line += Integer.toString((int)desired[i][j]);
				}
				bwDes.append(line);
				if (i < (samplec-1)) 
					bwDes.newLine();
			}
			bwDes.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void echoData() {
		int samplec = inputs.length;
		System.out.println("input:");
		for (int i = 0; i < samplec; i++) {
			String line = "";
			for (int j = 0; j < inputs[i].length; j++) {
				line += Integer.toString((int)inputs[i][j]);
			}
			System.out.println(line);
		}
		System.out.println("desired:");
		for (int i = 0; i < samplec; i++) {
			String line = "";
			for (int j = 0; j < inputs[i].length; j++) {
				line += Integer.toString((int)desired[i][j]);
			}
			System.out.println(line);
		}
	}


}
