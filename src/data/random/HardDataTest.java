package data.random;

import data.DataBinary;
import util.Util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class HardDataTest {

	public static final int k = 11;
	public static final int samplec = 300;
	public static final String pathIn = "input/binary-kWTA/k"+k+"-in100x"+samplec;
	public static final String pathDes = "input/binary-kWTA/k"+k+"-des100x"+samplec;
	
	public static void main(String[] args) throws IOException {
		
		DataBinary data = new DataBinary(pathIn,pathDes);
		double[][] inp = data.dataX();
//		double[][] des = data.getDesired();
		HashMap<String,Integer> patterns = new HashMap<String,Integer>();
		for (int i = 0; i < inp.length; i++) {
			String tmp = Util.arrayToBinaryString(inp[i]);
			if (patterns.containsKey(tmp))
				patterns.put(tmp, patterns.get(tmp)+1);
			else
				patterns.put(tmp,1);
		}
		for (Map.Entry<String,Integer> entry : patterns.entrySet()) {
			System.out.println(entry.getKey()+" "+entry.getValue());
		}
		System.out.println(patterns.size());
	}
	
}
