package data.random;

import java.io.IOException;

import util.visu.DataVis;

import base.Conf;
import data.DataBinary;

public class ViewBinaryData {

	public static final int[] visParams = {500, 200, 4, 10, 10, 10, 10}; // sizeX, sizeY, pxs, margX, margY, matrixS, rowS
	
	public static void main(String[] args) throws IOException {
		
		DataBinary data = new DataBinary(Conf.pathRandInputs, Conf.pathRandDesired);
		
		DataVis vIn = new DataVis(visParams,data.dataX());
		vIn.setSize(vIn.getsX(), vIn.getsY());
		vIn.setVisible(true);
		
		DataVis vDes = new DataVis(visParams,data.dataY());
		vDes.setSize(vDes.getsX(), vDes.getsY());
		vDes.setVisible(true);
		
	}
	
}
