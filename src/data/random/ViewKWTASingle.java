package data.random;

import java.io.IOException;

import util.visu.DataVis;

import base.Conf;
import data.DataBinary;

public class ViewKWTASingle {
	
	public static final int k = 8;
	public static final int patternSize = 100;
	public static final int[] visParams = {500, 200, 4, 10, 10, 10, 10}; // sizeX, sizeY, pxs, margX, margY, matrixS, rowS
	
	public static void main(String[] args) throws IOException {
		
		DataBinary data = new DataBinary(
				Conf.pathDataKWTA+"k"+k+"-in"+patternSize+"x"+(k*Conf.kToSizeRatio), 
				Conf.pathDataKWTA+"k"+k+"-des"+patternSize+"x"+(k*Conf.kToSizeRatio));
		
		DataVis vIn = new DataVis(visParams,data.dataX());
		vIn.setSize(vIn.getsX(), vIn.getsY());
		vIn.setVisible(true);
		
		DataVis vDes = new DataVis(visParams,data.dataY());
		vDes.setSize(vDes.getsX(), vDes.getsY());
		vDes.setVisible(true);
		
	}
	
}
