package data.random;

import java.io.IOException;

import util.visu.DataPairsRandom;
import base.Conf;
import data.DataBinary;

public class ViewDataPairsSingle {

	public static final String pathVisual = Conf.dataSetsVisPaths[0];
	public static final String pathMotor = Conf.dataSetsMotPaths[1];
	public static final int k = 8;
	public static final int[] visParams1 = {5, 10, 10, 10, 8}; // pxs, margX, margY, matrixS, rowHeight
	public static final int[] visParams2 = {5, 10, 10, 10, 10}; // pxs, margX, margY, matrixS, rowHeight
	public static final int[] visParams3 = {4, 10, 10, 10, 16}; // pxs, margX, margY, matrixS, rowHeight
	
	public static void main(String[] args) throws IOException {
		
		DataBinary data = new DataBinary(
				Conf.pathDataKWTA+"k"+k+"-in"+100+"x"+(k*Conf.kToSizeRatio), 
				Conf.pathDataKWTA+"k"+k+"-des"+100+"x"+(k*Conf.kToSizeRatio));
		
		DataPairsRandom vis = new DataPairsRandom(data);
		vis.display();
		
//		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
//		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
//		dataVis.kWinnerTakeAll(k);
//		dataMot.kWinnerTakeAll(k);
//		
//		DataPairsFP visR = new DataPairsFP(dataVis,dataMot,k,k);
//		visR.display();
//		
//		dataMot.multiply();
//		DataPairsFP visR2 = new DataPairsRandom(visParams3,dataVis.values(),dataMot.values());
//		visR2.display();

	}
	
}
