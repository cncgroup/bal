package data;

import base.Conf;
import base.NetUtil;
import util.Util;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.TreeMap;

public class DataRoboticVis {
	
	protected ArrayList<double[]>[][] data;
	protected int patternSize;

	@SuppressWarnings("unchecked")
	public DataRoboticVis(){
		super();
	}

	public DataRoboticVis(String path, String delimiter, boolean distFunc){
		//seek the sample size
		try {	
			BufferedReader br = new BufferedReader(new FileReader(new File(path)));
			String line = br.readLine();
			patternSize = line.split(delimiter).length;
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//create data structure
		data = new ArrayList[Conf.movements][Conf.perspectives];
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				data[i][j] = new ArrayList<double[]>();
			}
		}
		//read data
		try {	
			BufferedReader br = new BufferedReader(new FileReader(new File(path)));
			String line = "";
			double[] lastVal = new double[patternSize];
			while ((line = br.readLine()) != null) {
				line = line.trim();
				String[] tmp = line.split(delimiter);
				if (line.indexOf("&") > -1) {
					int i = Integer.parseInt(tmp[1]);
					int j = Integer.parseInt(tmp[2]);
					data[i][j].add(lastVal.clone());
				} else {
					for (int i = 0; i < tmp.length; i++) {
						if (distFunc)
							lastVal[i] = Math.exp(-(Conf.distExpConst*Double.parseDouble(tmp[i])));
						else 
							lastVal[i] = Double.parseDouble(tmp[i]);
					}
				}
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public DataRoboticVis(String path, String delimiter) {
		this(path, delimiter,false);
	}

	public DataRoboticVis(String path) {
		this(path, Conf.defaultDelimiter,false);
	}
	
	public void applyDist() {
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				for (int x = 0; x < data[i][j].size(); x++) {
					double[] val = data[i][j].get(x);
					double[] newval = new double[val.length];
					for (int y = 0; y < newval.length; y++) {
						newval[y] = Math.exp(-Conf.distExpConst*val[y]);
					}
					data[i][j].add(x,newval);
				}
			}
		}
	}
	
	public void kWinnerTakeAll(int k) {
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				ArrayList<double[]> newData = new ArrayList<double[]>(); 
				for (double[] sample : data[i][j]) {		
					newData.add(NetUtil.kWTAMin(sample, k));
				}
				data[i][j] = newData;
			}
		}
	}

	public void softmax() {
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				ArrayList<double[]> newData = new ArrayList<>();
				for (double[] sample : data[i][j]) {
					newData.add(NetUtil.softmax(sample));
				}
				data[i][j] = newData;
			}
		}
	}

	public void winDistExp() {
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				ArrayList<double[]> newData = new ArrayList<>();
				for (double[] sample : data[i][j]) {
					newData.add(NetUtil.winnerDist(sample));
				}
				data[i][j] = newData;
			}
		}
	}

	public double[][] values() {
		int count = 0;
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				count += data[i][j].size();
			}
		}
		double[][] out = new double[count][patternSize];
		int iterator = 0;
		for (int i = 0; i < Conf.movements; i++) {
			for (int s = 0; s < data[i][0].size(); s++) {
				for (int j = 0; j < Conf.perspectives; j++) {
					out[iterator] = data[i][j].get(s);
					iterator++;
				}
			}
		}
		return out;
	}
	
	public double[][] valuesOnePat() {
		double[][] out = new double[Conf.movements*Conf.perspectives][patternSize];
		for (int i = 0; i < Conf.movements; i++) {
			for (int j = 0; j < Conf.perspectives; j++) {
				out[Conf.movements*i+j] = data[i][j].get(0);
			}
		}
		return out;
	}
	
	public double[][] valuesOnePatOnePersp(int persp) {
		double[][] out = new double[Conf.movements][patternSize];
		for (int i = 0; i < Conf.movements; i++) {
			out[i] = data[i][persp].get(0);
		}
		return out;
	}
	
	public double[][] valuesOnePatFirstPersp() {
		return valuesOnePatOnePersp(0);
	}
	
	public double[][] valuesPersp(int p) {
		int count = 0;
		for (int i = 0; i < data.length; i++) {
			count += data[i][p].size();
		}
		double[][] out = new double[count][patternSize];
		int iterator = 0;
		for (int i = 0; i < Conf.movements; i++) {
			for (double[] sample: data[i][p]) {
				out[iterator] = sample;
				iterator++;
			}
		}
		return out;
	}
	
	public double[][] valuesPerspIndices(int p, int[][] indices) {
		int count = 0;
		for (int i = 0; i < indices.length; i++) {
			count += indices[i].length;
		}
		double[][] out = new double[count][patternSize];
		int iterator = 0;
		for (int i = 0; i < Conf.movements; i++) {
			for (int j = 0; j < indices[i].length; j++) {
				out[iterator] = data[i][p].get(indices[i][j]);
				iterator++;
			}
		}
		return out;
	}

	public double[][] valuesAllPerspIndices(int[][] indices) {
		int count = 0;
		for (int i = 0; i < indices.length; i++) {
			count += indices[i].length;
		}
		double[][] out = new double[count*Conf.perspectives][patternSize];
		int iterator = 0;
		for (int p = 0; p < Conf.perspectives; p++) {
			for (int m = 0; m < Conf.movements; m++) {
				for (int j = 0; j < indices[m].length; j++) {
					out[iterator] = data[m][p].get(indices[m][j]);
					iterator++;
				}
			}
		}
		return out;
	}
	
	public double[][][] valuesPerspIndicesMov(int p, int[][] indices) {
		double[][][] out = new double[indices.length][indices[0].length][patternSize];
		for (int i = 0; i < Conf.movements; i++) {
			out[i] = new double[indices[i].length][patternSize];
			for (int j = 0; j < indices[i].length; j++) {
				out[i][j] = data[i][p].get(indices[i][j]);
			}
		}
		return out;
	}
	
	public double[][] valuesFirstPersp() {
		return valuesPersp(0);
	}
	
	public ArrayList<double[]>[][] dataSeq() {
		return data;
	}
	
	public int sampleCount() {
		int count = 0;
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				count += data[i][j].size();
			}
		}
		return count;
	}
	
	public int sampleCountFP() {
		int count = 0;
		for (int i = 0; i < data.length; i++) {
			count += data[i][0].size();
		}
		return count;
	}
	
	public int patternSize() {
		return patternSize;
	}
	
	public int[][] sequences() {
		int[][] out = new int[Conf.movements][Conf.perspectives];
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				out[i][j] = data[i][j].size();
			}
		}
		return out;
	}
	
	public double[][] dataPatternsOnly() {
		HashSet<String> patterns = dataPatternsStrSet();
		double[][] out = new double[patterns.size()][patternSize];
		int iterator = 0;
		for (String sample : patterns) {
			out[iterator] = Util.strToDouble(sample);
			iterator++;
		}
		return out;
	}
	
	public double[][] dataPatternsOnly(int perspective) {
		HashSet<String> patterns = dataPatternsStrSet(perspective);
		double[][] out = new double[patterns.size()][patternSize];
		int iterator = 0;
		for (String sample : patterns) {
			out[iterator] = Util.strToDouble(sample);
			iterator++;
		}
		return out;
	}
	
	public double[][][] uniqPatSequences(int persp) {
		@SuppressWarnings("unchecked")
		HashSet<String>[] patterns = new HashSet[Conf.movements];
		for (int i = 0; i < Conf.movements; i++) {
			patterns[i] = new HashSet<String>();
			for (double[] s : data[i][persp]) {
				patterns[i].add(Util.arrayToBinaryString(s));
			}
		}
		int maxp = 0;
		for (int i = 0; i < patterns.length; i++) {
			maxp = (maxp < patterns[i].size()) ? patterns[i].size() : maxp;
		}
		double[][][] out = new double[3][maxp][patternSize];
		int it = 0;
		for (int i = 0; i < Conf.movements; i++) {
			out[i] = new double[patterns[i].size()][patternSize];
			for (String p : patterns[i]) {
				out[i][it] = Util.strToDouble(p);
				it++;
			}
			it = 0;
		}
		return out;
	}
	
	public int findIndex(int mov,int persp,double[] val) {
		for (int i = 0; i < data[mov][persp].size(); i++) {
			if (val == data[mov][persp].get(i))
				return i;
		}
		return 0;
	}
	
	public int[] findIndex(double[] val) {
		for (int i = 0; i < Conf.movements; i++) {
			for (int j = 0; j < Conf.perspectives; j++) {
				for (int k = 0; k < data[i][j].size(); k++) {
					if (Arrays.equals(val,data[i][j].get(k))) {
						return new int[] {i,j,k};
					}
				}
			}
		}
		return new int[] {0,-1,-1};
	}
	
	@SuppressWarnings("unchecked")
	public int[][] uniqPatIndices(int[][] fromIndices) { //according to first perspective
		HashSet<Integer>[] allowedInd = new HashSet[3];
		for (int i = 0; i < Conf.movements; i++) {
			allowedInd[i] = new HashSet<Integer>();
			for (int j = 0; j < fromIndices[i].length; j++) {
				allowedInd[i].add(fromIndices[i][j]);
			}
		}
		TreeMap<Integer,String>[] patterns = new TreeMap[Conf.movements];
		for (int i = 0; i < Conf.movements; i++) {
			patterns[i] = new TreeMap<Integer,String>();
			for (int j = 0; j < data[i][0].size(); j++) {
				String pat = Util.arrayToBinaryString(data[i][0].get(j));
				if (allowedInd[i].contains(j) && !(patterns[i].containsValue(pat)))
					patterns[i].put(j,pat);
			}
		}
		int[][] out = new int[3][patterns[0].size()];
		for (int i = 0; i < Conf.movements; i++) {
			out[i] = new int[patterns[i].size()];
			int j = 0;
			for (Entry<Integer,String> val : patterns[i].entrySet()) {
				out[i][j] = val.getKey();
				j++;
			}
		}
		return out;
	}
	
	@SuppressWarnings("unchecked")
	public int[][] uniqPatIndices() { //according to first perspective
		TreeMap<Integer,String>[] patterns = new TreeMap[Conf.movements];
		for (int i = 0; i < Conf.movements; i++) {
			patterns[i] = new TreeMap<Integer,String>();
			for (int j = 0; j < data[i][0].size(); j++) {
				String pat = Util.arrayToBinaryString(data[i][0].get(j));
				if (!(patterns[i].containsValue(pat)))
					patterns[i].put(j,pat);
			}
		}
		int[][] out = new int[3][patterns[0].size()];
		for (int i = 0; i < Conf.movements; i++) {
			out[i] = new int[patterns[i].size()];
			int j = 0;
			for (Entry<Integer,String> val : patterns[i].entrySet()) {
				out[i][j] = val.getKey();
				j++;
			}
		}
		return out;
	}
	
	public HashSet<String> dataPatternsStrSet() {
		HashSet<String> patterns = new HashSet<String>();
		for (int i = 0; i < Conf.movements; i++) {
			for (int j = 0; j < Conf.perspectives; j++) {
				for (double[] s : data[i][j]) {
					patterns.add(Util.arrayToBinaryString(s));
				}
			}
		}
		return patterns;
	}
	
	public HashSet<String> dataPatternsStrSet(int perspective) {
		HashSet<String> patterns = new HashSet<String>();
		for (int i = 0; i < Conf.movements; i++) {
			for (double[] s : data[i][perspective]) {
				patterns.add(Util.arrayToBinaryString(s));
			}
		}
		return patterns;
	}
	
	public int uniquePatternCount() {
		HashSet<String> patterns = dataPatternsStrSet();
		return patterns.size();
	}
	
	public void writeDataPatterns(String pathin) {
		HashSet<String> patterns = dataPatternsStrSet();
		try {
			BufferedWriter bwIn = new BufferedWriter(new FileWriter(new File(pathin)));
			for (String entry : patterns) {
				bwIn.append(entry);
				bwIn.newLine();
			}
			bwIn.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void echo() {
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				for (double[] s : data[i][j]) {
					System.out.println(i+":"+j+":"+Arrays.toString(s));
				}
			}
		}
	}
	
	public void echoBin() {
		for (int i = 0; i < data.length; i++) {
			for (int j = 0; j < data[i].length; j++) {
				for (double[] s : data[i][j]) {
					System.out.println(i+":"+j+":"+Util.arrayToBinaryString(s));
				}
			}
		}
	}

	public static void main(String[] args) {
//		DataRoboticVisEnds test = new DataRoboticVisEnds(Conf.pathVisual,100);
//		for (int i = 0; i < Conf.movements; i++) {
//			for (int j = 0; j < Conf.perspectives; j++) {
//				System.out.println(test.dataSeq()[i][j].size());
//			}
//		}
//		int k = 8;
//		DataRoboticVis dataVis = new DataRoboticVis(Conf.pathVisual);
//		DataRoboticMot dataMot = new DataRoboticMot(Conf.pathMotor);
//		dataVis.kWinnerTakeAll(k);
//		dataMot.kWinnerTakeAll(k);
//		double[][] dataV = dataVis.dataPatternsOnly(0); //first perspective
//		double[][] dataM = new double[dataV.length][Conf.patternSize];
// 		for (int i = 0; i < dataM.length; i++) {
// 			int[] coord =  dataVis.findIndex(dataV[i]);
//// 			System.out.println(Arrays.toString(coord));
//			dataM[i] = dataMot.dataSeq()[coord[0]].get(coord[2]);
//		}
		DataRoboticVis dataVis = new DataRoboticVis(Conf.dataSetsVisPaths[2]);
//		for (int i = 0; i < Conf.movements; i++) {
//			for (int j = 0; j < Conf.perspectives; j++) {
//				System.out.println("m"+i+"p"+j+": "+dataVis.dataSeq()[i][j].size());
//			}
//		}
		System.out.println(dataVis.patternSize);
		System.out.println(dataVis.sampleCountFP());
		System.out.println(dataVis.sampleCount());
//		double[][] tmp = dataVis.values();
//		System.out.println(Util.arrayToStringRounded(tmp[0],3));
//		Util.scale(tmp);
//		System.out.println(Util.arrayToStringRounded(tmp[0],3));
//		Util.flip(tmp);
//		System.out.println(Util.arrayToStringRounded(tmp[0],3));
	}

}
