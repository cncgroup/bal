package base;

import util.Util;

import java.util.Arrays;
import java.util.Random;

public class BAL {

	protected int[] arch;
	protected double[] lr; //learning rate
	protected double[] beta; //clamping strength
	protected double[] gammaF; //echo strength
	protected double[] gammaB; //echo strength
	protected double[][][] weightsF; //Forward association
	protected double[][][] weightsB; //Backward association

	protected boolean actFunc = true; // whether to use activation function during prediction
	protected boolean actFuncDeriv = false; // whether to use activation function derivation during learning

	public BAL(int[] layerSzs, double[][][] weightsF, double[][][] weightsB, double lrnsp[], double[] clampstrength, double[] estimateStrengthF, double[] estimateStrengthB) {
		arch = layerSzs;
		this.weightsF = weightsF;
		this.weightsB = weightsB;
		this.lr = lrnsp;
		this.beta = clampstrength;
		this.gammaF = estimateStrengthF;
		this.gammaB = estimateStrengthB;
	}

	public BAL(int[] layerSzs, double[][][] weightsF, double[][][] weightsB, double lrnsp, double[] clampstrength, double[] estimateStrengthF, double[] estimateStrengthB) {
		this(layerSzs, weightsF, weightsB, BAL.paramSame(lrnsp,layerSzs.length-1), clampstrength, estimateStrengthF, estimateStrengthB);
	}

	public BAL(int[] layerSzs, double[][][] weightsF, double[][][] weightsB, double lrnsp, double[] clampstrength, double[] estimateStrength) {
		this(layerSzs, weightsF, weightsB, BAL.paramSame(lrnsp,layerSzs.length-1), clampstrength, estimateStrength, estimateStrength);
	}

	public BAL(int[] layerSzs, double[][][] weightsF, double[][][] weightsB, double lrnsp[], double clampstrength[], double estimateStrength[]) {
		this(layerSzs, weightsF, weightsB, lrnsp, clampstrength, estimateStrength, estimateStrength);
	}

	public BAL(int[] layerSzs, double[][][] weightsF, double[][][] weightsB, double lrnsp, double clampstrength, double estimateStrength) {
		this(layerSzs, weightsF, weightsB,
				paramSame(lrnsp, layerSzs.length-1),
				paramSymmetricSimple(clampstrength, Conf.clampTargetStrengthHidden, layerSzs.length),
				paramSame(estimateStrength, layerSzs.length));
	}

	public BAL(int[] layerSzs, double[][][] weightsF, double[][][] weightsB, double lrnsp, double clampstrength) {
		this(layerSzs, weightsF, weightsB, lrnsp, clampstrength, Conf.clampEchoStrength);
	}

	public BAL(int[] layerSzs, double wmin, double wmax, double[] lrnsp, double[] clampstrength) {
		this(layerSzs,
				NetUtil.initAllWeightsF(layerSzs, wmin, wmax, new Random(System.currentTimeMillis())),
				NetUtil.initAllWeightsB(layerSzs, wmin, wmax, new Random(System.currentTimeMillis())),
				lrnsp,
				clampstrength,
				paramSame(Conf.clampEchoStrength, layerSzs.length));
	}

	public BAL(int[] layerSzs, double wmin, double wmax, double[] lrnsp, double[] clampstrength, double[] estimateStrengthF, double[] estimateStrengthB) {
		this(layerSzs,
				NetUtil.initAllWeightsF(layerSzs, wmin, wmax, new Random(System.currentTimeMillis())),
				NetUtil.initAllWeightsB(layerSzs, wmin, wmax, new Random(System.currentTimeMillis())),
				lrnsp,
				clampstrength,
				estimateStrengthF, estimateStrengthB);
	}

	public BAL(int[] layerSzs, double wmin, double wmax, double lrnsp, double[] clampstrength, double[] estimateStrengthF, double[] estimateStrengthB) {
		this(layerSzs,
				NetUtil.initAllWeightsF(layerSzs, wmin, wmax, new Random(System.currentTimeMillis())),
				NetUtil.initAllWeightsB(layerSzs, wmin, wmax, new Random(System.currentTimeMillis())),
				paramSame(lrnsp, layerSzs.length-1),
				clampstrength,
				estimateStrengthF, estimateStrengthB);
	}

	public BAL(int[] layerSzs, double wmin, double wmax, double lrnsp, double[] clampstrength, double[] estimateStrength) {
		this(layerSzs,
				NetUtil.initAllWeightsF(layerSzs, wmin, wmax, new Random(System.currentTimeMillis())),
				NetUtil.initAllWeightsB(layerSzs, wmin, wmax, new Random(System.currentTimeMillis())),
				paramSame(lrnsp, layerSzs.length-1),
				clampstrength,
				estimateStrength, estimateStrength);
	}

	public BAL(int[] layerSzs, double wmin, double wmax, double lrnsp, double clampstrength) {
		this(layerSzs,
				NetUtil.initAllWeightsF(layerSzs, wmin, wmax, new Random(System.currentTimeMillis())),
				NetUtil.initAllWeightsB(layerSzs, wmin, wmax, new Random(System.currentTimeMillis())),
				lrnsp, clampstrength);
	}

	public BAL(int[] layerSzs, double wmin, double wmax, boolean gauss, double lrnsp, double clampstrength) {
		this(layerSzs,
				NetUtil.initAllWeightsF(layerSzs, wmin, wmax, new Random(System.currentTimeMillis()), gauss),
				NetUtil.initAllWeightsB(layerSzs, wmin, wmax, new Random(System.currentTimeMillis()), gauss),
				lrnsp, clampstrength);
	}

	public static double[] paramSame(double val, int layers) {
		double[] out = new double[layers];
		Arrays.fill(out, val);
		return out;
	}

	public static double[] paramSymmetricSimple(double val, double hid, int layers) {
		double[] out = new double[layers];
		Arrays.fill(out, hid);
		out[0] = val;
		out[out.length-1] = 1.0-val;
		return out;
	}

	public static double[] paramSymmetricFull(double[] val, int layers) {
		double[] out = new double[layers];
		int mid = ((layers+1)/2);
		for (int i=0; i<mid; i++) {
			out[i] = val[i];
			out[out.length-1-i] = 1.0-val[i];
		}
		return out;
	}

	protected double[] layerActivation(double[] inputAct, double[][] weights, boolean actFun) {
		double[] outputAct = new double[weights[0].length];
		double[] biasedIn = NetUtil.addBiasInput(inputAct,Conf.biasValue);
		for (int i = 0; i < inputAct.length+1; i++) {
			for (int j = 0; j < outputAct.length; j++) {
				outputAct[j] += weights[i][j] * biasedIn[i];
			}
		}
		if (actFun) {
			for (int i = 0; i < outputAct.length; i++) {
				outputAct[i] = NetUtil.actFun(outputAct[i]);
			}
		}
		return outputAct;
	}

	protected double[] layerActivation(double[] inputAct, double[][] weights) {
		return layerActivation(inputAct, weights, this.actFunc);
	}

	protected void adjustWeights(Activations netAct) {
		adjustWeights(netAct, this.actFuncDeriv);
	}

	protected void adjustWeights(Activations netAct, boolean actFuncDeriv) {
//		double beta = 1; // full clamping
		double[][] target = new double[arch.length][];
		double[][] estimateF = new double[arch.length][];
		double[][] estimateB = new double[arch.length][];
		for (int l = 0; l < arch.length; l++) {
			target[l] = new double[arch[l]];
			for (int i = 0; i < arch[l]; i++) {
				target[l][i] = beta[l]*netAct.actFPrediction()[l][i] + (1.0-beta[l])*netAct.actBPrediction()[l][i];
			}
			if (l > 0) {
				estimateF[l] = new double[arch[l]];
				for (int i = 0; i < arch[l]; i++) {
					estimateF[l][i] = (1.0-gammaF[l])*netAct.actFPrediction()[l][i] + gammaF[l]*netAct.actBEcho()[l][i];
				}
			}
			if (l < arch.length-1) {
				estimateB[l] = new double[arch[l]];
				for (int i = 0; i < arch[l]; i++) {
					estimateB[l][i] = (1.0-gammaB[l])*netAct.actBPrediction()[l][i] + gammaB[l]*netAct.actFEcho()[l][i];
				}
			}
		}
		for (int l = 0; l < arch.length-1; l++) {
			int k = l+1;
			// F: from l to k
			// B: from k to l
			for (int i = 0; i < arch[l]+1; i++) {
				for (int j = 0; j < arch[k]+1; j++) {
					if (j < arch[k])
						weightsF[l][i][j] += lr[l] * (i == arch[l] ? 1.0 : target[l][i]) * (target[k][j] - estimateF[k][j]) * (actFuncDeriv ? (estimateF[k][j]) * (1.0 - estimateF[k][j]) : 1.0);
					if (i < arch[l])
						weightsB[l][j][i] += lr[l] * (j == arch[k] ? 1.0 : target[k][j]) * (target[l][i] - estimateB[l][i]) * (actFuncDeriv ? (estimateB[l][i]) * (1.0 - estimateB[l][i]) : 1.0);
				}
			}
		}
	}

	/**
	 * return array of activation states: dim [pattern count]
	 */
	public Activations[] epoch(double[][] inputs, double[][] desired, boolean learn) {
		Activations[] out = new Activations[inputs.length];
		int[] indexer = Util.permutedIndexes(inputs.length);
		for (int n = 0; n < indexer.length; n++) {
			int s = indexer[n];
			//forward pass
			double[][] actFP = new double[arch.length][];
			double[][] actFE = new double[arch.length][];
			actFP[0] = inputs[s];
			for (int i = 1; i < arch.length; i++) {
				actFP[i] = layerActivation(actFP[i-1],weightsF[i-1], true);
				actFE[i-1] = layerActivation(actFP[i],weightsB[i-1], true);
			}
			//backward pass
			double[][] actBP = new double[arch.length][];
			double[][] actBE = new double[arch.length][];
			actBP[arch.length-1] = desired[s];
			for (int i = arch.length-1; i > 0; i--) {
				actBP[i-1] = layerActivation(actBP[i],weightsB[i-1], true);
				actBE[i] = layerActivation(actBP[i-1],weightsF[i-1], true);
			}
			Activations netActCurrent = new Activations(actFP, actFE, actBP, actBE);
			out[s] = netActCurrent;
			if (learn)
				adjustWeights(netActCurrent);
		}
		return out;
	}

	public double[][][] testEpochPatterns(double[][] inputs, double[][] desired) {
		double[][][] out = new double[4][inputs.length][inputs[0].length];	
		for (int s = 0; s < inputs.length; s++) {
			//forward pass
			double[][] actFP = new double[arch.length][];
			actFP[0] = inputs[s];
			for (int i = 1; i < arch.length; i++) {
				actFP[i] = layerActivation(actFP[i-1],weightsF[i-1], true);
			}
			//backward pass
			double[][] actBP = new double[arch.length][];
			actBP[arch.length-1] = desired[s];
			for (int i = arch.length-1; i > 0; i--) {
				actBP[i-1] = layerActivation(actBP[i],weightsB[i-1], true);
			}
			out[0][s] = desired[s];
			out[1][s] = actFP[arch.length-1];
			out[2][s] = inputs[s];
			out[3][s] = actBP[0];
		}
		return out;
	}

	public void printWeights() {
		for (int i = 0; i < weightsF.length; i++) {
			System.out.println(NetUtil.weightsToStr(weightsF[i]));
		}
		for (int i = 0; i < weightsB.length; i++) {
			System.out.println(NetUtil.weightsToStr(weightsF[i]));
		}
	}

	public int[] getArch() {
		return arch;
	}

	public void setActFunc(boolean actFunc) {
		this.actFunc = actFunc;
	}

	public void setActFuncDeriv(boolean actFuncDeriv) {
		this.actFuncDeriv = actFuncDeriv;
	}

	public static class Activations {
		//[direction: B/F][phase: -/+][arch.length][arch[n]]
		double[][] actForwardPrediction;
		double[][] actForwardEcho;
		double[][] actBackwardPrediction;
		double[][] actBackwardEcho;

		public Activations(double[][] actFP, double[][] actFE, double[][] actBP, double[][] actBE) {
			actForwardPrediction = actFP;
			actForwardEcho = actFE;
			actBackwardPrediction = actBP;
			actBackwardEcho = actBE;
		}

		public double[][] actFPrediction() {
			return actForwardPrediction;
		}

		public double[][] actFEcho() {
			return actForwardEcho;
		}

		public double[][] actBPrediction() {
			return actBackwardPrediction;
		}

		public double[][] actBEcho() {	return actBackwardEcho;	}

		public static double[][][] getPatterns(Activations[] act, double[][] inputs, double[][] desired) {
			double[][][] out = new double[4][inputs.length][];
			for (int s = 0; s < inputs.length; s++) {
				out[0][s] = desired[s];
				out[1][s] = act[s].actForwardPrediction[act[s].actForwardPrediction.length-1];
				out[2][s] = inputs[s];
				out[3][s] = act[s].actBackwardPrediction[0];
			}
			return out;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;

			Activations that = (Activations) o;

			if (!Arrays.deepEquals(actForwardPrediction, that.actForwardPrediction)) return false;
			if (!Arrays.deepEquals(actForwardEcho, that.actForwardEcho)) return false;
			if (!Arrays.deepEquals(actBackwardPrediction, that.actBackwardPrediction)) return false;
			return Arrays.deepEquals(actBackwardEcho, that.actBackwardEcho);
		}

		@Override
		public int hashCode() {
			int result = Arrays.deepHashCode(actForwardPrediction);
			result = 31 * result + Arrays.deepHashCode(actForwardEcho);
			result = 31 * result + Arrays.deepHashCode(actBackwardPrediction);
			result = 31 * result + Arrays.deepHashCode(actBackwardEcho);
			return result;
		}

	}
}