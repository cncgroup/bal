package base;

import java.awt.Color;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Conf {
	
	public static final NumberFormat formatter = new DecimalFormat("#0.0000");

	//standard network setup
	public static final double minMSE = 0.001;
	public static final double minPatSucc = 1.0;
	public static final double minBitSucc = 1.0;
	public static final double lRate = 0.2;
	public static final double wmin = -.1;
	public static final double wmax = .1;
	public static final double distExpConst = .5;
	public static final int inps = 100;
	public static final int hids = 80;
	public static final int outs = 100;

	//BAL 2
	public static final double clampStrength = 1.0;
	public static final double clampTargetStrengthHidden = .5;
	public static final double clampEchoStrength = 0.5;

	//BP params
	public static final double biasValue = 1.0;
//	public static final double biasValue = -1.0;
	public static final double bp_momentum = 0.1;
	public static final double bp_wdecay = 0.0000001;
	public static final double bp_wmin = -.2;
	public static final double bp_wmax = .2;

	//graph/table/error labels
	public static final String[] errorLabels = {"mseF","mseB","patSuccF","patSuccB","bitSuccF","bitSuccB"};
    public static final String[] errorLabelsBal = {"mseF","mseB","bitSuccF","bitSuccB"};
    public static final String[] errorLabelsBalCrossval = {"test.mseF","test.mseB","test.bitSuccF","test.bitSuccB",
                                                           "train.mseF","train.mseB","train.bitSuccF","train.bitSuccB"};
	public static final String[] errorLabelsF1 = {"mseF","mseB","F1scoreF","F1scoreB"};
	public static final String[] errorLabelsNew = {"mseF","mseB","patSuccF","patSuccB","bitSuccF","bitSuccB","patDistF","patDistB"};
	public static final String[] errorLabelsExt = {"mseF","mseB","patSuccF","patSuccB","bitSuccF","bitSuccB",
													"binarizationH-","binarizationH+","distH-H+","distWIH-WHI","distWOH-WHO"};
	public static final String[] errorLabelsBP = {"mse","pattern success","bit success"};
	public static final String[] errorLabelsBPF1 = {"mse","F1 score"};
	public static final String[] errorLabelsBP2 = {"mse","pattern success","bit success","epcs"};
	public static final String[] errorLabelsF1bidir = {"mseF","mseB","F1-F","F1-B", "F1-both"};
	public static final String[] mseLabels = {"mseF","mseB"};
	public static final String[] f1Labels = {"F1scoreF","F1scoreB"};
	public static final String[] patSuccLabels = {"patSuccF","patSuccB"};
	public static final String[] bitSuccLabels = {"bitSuccF","bitSuccB"};
	public static final String[][] plotLabels = {
		{"mseF","mseB"},
		{"patSuccF","patSuccB"},
		{"bitSuccF","bitSuccB"},
		{"pattDist","pattDist"}
	};
	public static final String[] epcLabels = {"epochs"};
	public static final String[] errorFigNames = {"mse","succ","errorBits","pattDist"};
	public static final String[] errorNames = {"mean squared error (per neuron)","pattern success","bit success","pattern distance"};

	public static final String[] netErrors = {"mseF","mseB","patSuccF","patSuccB","bitSuccF","bitSuccB"};
	public static final String[] testErrors = {"mseF","mseB","patSuccF","patSuccB","bitSuccF","bitSuccB","epcs"};
	public static final String[] testErrorsF1 = {"mseF","mseB","F1scoreF","F1scoreB","epcs"};
	public static final String[] testErrorsF1BP = {"mseF","F1scoreF","epcs"};
	public static final String[] testErrorsF1Bidir = {"mseF","mseB","F1-F","F1-B", "F1-both", "epcs"};

	//experimental param sets
//	public static final double[] learningRates = {0.005, 0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 0.8, 1.0};
	public static final double[] learningRates = {0.05, 0.1, 0.2, 0.5, 0.8, 1.0, 1.5, 2.0};
//	public static final int[] hiddenSizes = {72, 84, 96, 108, 120, 132, 144, 156, 168, 180, 192, 204};
	public static final int[] hiddenSizes = {60, 80, 100, 120, 140};
	
	//data setup
	public static final String pathRandInputs = "input/binary-random/inp100x100";
	public static final String pathRandDesired = "input/binary-random/des100x100";
	public static final String pathDataKWTA = "input/binary-kWTA/";
	public static final String pathDataRandomHard = "input/binary-kWTA-hard/";
	public static final String pathDataMap = "input/binary-maplike/";
	public static final int minK = 4;
	public static final int maxK = 12;
	public static final int kToSizeRatio = 5;
	//robotic data
	public static final int movements = 3;
	public static final int perspectives = 4;
	public static final String[] dataSets = {
		"motor 8x8",
		"motor 10x10",
		"motor 12x12",
		"motor 14x14",
		"motor 16x16",
		"motor 18x18",
		"motor 20x20",
	};
	public static final int[] dataVisSizes = {10,12,14,16,18,20};
	public static final int[] dataMotSizes = {8,10,12,14};
	public static final String[] dataSetsVisPaths = {
		"input/msom/visual_10_10.txt",
		"input/msom/visual_12_12.txt",
		"input/msom/visual_14_14.txt",
		"input/msom/visual_16_16.txt",
		"input/msom/visual_18_18.txt",
		"input/msom/visual_20_20.txt",
	};
	public static final String[] dataSetsMotPaths = {
			"input/msom/motor_8_8.txt",
			"input/msom/motor_10_10.txt",
			"input/msom/motor_12_12.txt",
			"input/msom/motor_14_14.txt",
	};
	public static final String[] dataSetsMNISTPaths = {
			"input/mnist/mnist_train.csv",
			"input/mnist/mnist_test.csv",
	};

	public static final String defaultDelimiter = " ";
	public static final String defaultMNISTDelimiter = ",";

	//output paths and patterns
	public static final String pathOutput = "output/";
	public static final String pathFigures = "figures/";
	public static final String figExt = ".png";
	
	//visualisation
	public static final int[] seqSizes = {19,17,19};
	public static final int[] visParams = {480, 500, 4, 10, 10, 10, 10}; //single figure for one binary dataset
	public static final Color[] colors = {new Color(204,0,0),new Color(76,153,0),new Color(0,0,153),Color.yellow};
	public static final Color[][] colorsMP = {
		{new Color(135,0,0),new Color(255,0,0),new Color(244,64,0),new Color(255,110,13)},
		{new Color(37,97,0),new Color(53,152,0),new Color(77,205,14),new Color(150,250,36)},
		{new Color(14,7,80),new Color(16,4,131),new Color(57,40,212),new Color(66,134,236)}
	};
	public static final int[][][] colors2D = {
			{{135,0,0},{255,0,0},{244,64,0},{255,110,13}},
			{{37,97,0},{53,152,0},{77,205,14},{150,250,36}},
			{{14,7,80},{16,4,131},{57,40,212},{66,134,236}}
	};
	public static final String[] plotStyles1 = {
		"color=blue,mark=*",
		"color=blue,mark=square*",
		"color=blue,mark=triangle*",
		"color=blue,mark=o",
		"color=red,mark=*",
		"color=red,mark=square*",
		"color=red,mark=triangle*",
	};

	public static final int pixelSize = 4;
	public static final int marginX = 10;
	public static final int marginY = 30;
	public static final int rowHeight = 10;
	public static final int rowHSmallSet = 4;
	public static final int pxsAllP = 3;
	public static final int margAllP = 5;
	public static final int rowHAllP = 15;
	public static final int marginHist = 10;

	public static String logPath(String simName) {
		return Conf.pathOutput+"log-"+simName+".tex";
	}


    //binary data
    public static final double[][] inputs424 = {{0,0,0,1},{0,0,1,0},{0,1,0,0},{1,0,0,0}};
    public static final double[][] inputsXOR = {{0,0},{0,1},{1,0},{1,1}};
    public static final double[][] outputsXOR = {{0},{1},{1},{0}};

    public static final double[][] inputsOR = {{0,0},{0,1},{1,0},{1,1}};
    public static final double[][] outputsOR = {{0},{1},{1},{1}};

    public static final double[][][] digitsBinary3x5 = {
        { //0
                {1,1,0},
                {1,0,1},
                {1,0,1},
                {1,0,1},
                {0,1,1},
        },
        { //1
                {0,1,0},
                {1,1,0},
                {0,1,0},
                {0,1,0},
                {0,1,0},
        },
        { //2
                {1,1,1},
                {0,0,1},
                {0,1,1},
                {1,0,0},
                {1,1,1},
        },
        { //3
                {1,1,1},
                {0,0,1},
                {0,1,0},
                {0,0,1},
                {1,1,1},
        },
        { //4
                {1,0,1},
                {1,0,1},
                {1,1,1},
                {0,0,1},
                {0,0,1},
        },
        { //5
                {1,1,1},
                {1,0,0},
                {1,1,0},
                {0,0,1},
                {1,1,1},
        },
        { //6
                {0,1,1},
                {1,0,0},
                {1,1,1},
                {1,0,1},
                {0,1,1},
        },
        { //7
                {1,1,1},
                {0,0,1},
                {0,1,0},
                {1,0,0},
                {1,0,0},
        },
        { //8
                {1,1,1},
                {1,0,1},
                {1,1,1},
                {1,0,1},
                {1,1,1},
        },
        { //9
                {0,1,1},
                {1,0,1},
                {1,1,1},
                {0,0,1},
                {1,1,0},
        }
    };

    public static final double[][][] lettersBinary7x7 = { //from Chartier et al. 2009
            { //A
                    {0,0,1,1,0,0,0},
                    {0,1,1,1,1,0,0},
                    {1,1,0,0,1,1,0},
                    {1,1,0,0,1,1,0},
                    {1,1,1,1,1,1,0},
                    {1,1,0,0,1,1,0},
                    {1,1,0,0,1,1,0},
            },
            { //B
                    {1,1,1,1,1,1,0},
                    {0,1,1,0,0,1,1},
                    {0,1,1,0,0,1,1},
                    {0,1,1,1,1,1,0},
                    {0,1,1,0,0,1,1},
                    {0,1,1,0,0,1,1},
                    {1,1,1,1,1,1,0},
            },
            { //C
                    {0,0,1,1,1,1,0},
                    {0,1,1,0,0,1,1},
                    {1,1,0,0,0,0,0},
                    {1,1,0,0,0,0,0},
                    {1,1,0,0,0,0,0},
                    {0,1,1,0,0,1,1},
                    {0,0,1,1,1,1,0},
            },
            { //D
                    {1,1,1,1,1,0,0},
                    {0,1,0,0,1,1,0},
                    {0,1,0,0,0,1,1},
                    {0,1,0,0,0,1,1},
                    {0,1,0,0,0,1,1},
                    {0,1,0,0,1,1,0},
                    {1,1,1,1,1,0,0},
            },

            { //E
                    {1,1,1,1,1,1,1},
                    {0,1,1,0,0,0,1},
                    {0,1,1,0,1,0,0},
                    {0,1,1,1,1,0,0},
                    {0,1,1,0,1,0,0},
                    {0,1,1,0,0,0,1},
                    {1,1,1,1,1,1,1},
            },
            { //F
                    {1,1,1,1,1,1,1},
                    {0,1,1,0,0,0,1},
                    {0,1,1,0,1,0,0},
                    {0,1,1,1,1,0,0},
                    {0,1,1,0,1,0,0},
                    {0,1,1,0,0,0,0},
                    {1,1,1,0,0,0,0},
            },
            { //G
                    {0,0,1,1,1,1,0},
                    {0,1,1,0,0,1,1},
                    {1,1,0,0,0,0,0},
                    {1,1,0,0,0,0,0},
                    {1,1,0,0,1,1,1},
                    {0,1,1,0,0,1,1},
                    {0,0,1,1,1,1,1},
            },
            { //H
                    {1,1,0,0,0,1,1},
                    {1,1,0,0,0,1,1},
                    {1,1,0,0,0,1,1},
                    {1,1,1,1,1,1,1},
                    {1,1,0,0,0,1,1},
                    {1,1,0,0,0,1,1},
                    {1,1,0,0,0,1,1},
            },
            { //I
                    {0,1,1,1,1,0,0},
                    {0,0,1,1,0,0,0},
                    {0,0,1,1,0,0,0},
                    {0,0,1,1,0,0,0},
                    {0,0,1,1,0,0,0},
                    {0,0,1,1,0,0,0},
                    {0,1,1,1,1,0,0},
            },
            { //J
                    {0,0,0,1,1,1,1},
                    {0,0,0,0,1,1,0},
                    {0,0,0,0,1,1,0},
                    {0,0,0,0,1,1,0},
                    {1,1,0,0,1,1,0},
                    {1,1,0,0,1,1,0},
                    {0,1,1,1,1,0,0},
            },

    };
}
