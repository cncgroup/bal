package base;

import util.QuicksortArr;
import util.Util;

import java.util.HashSet;
import java.util.Random;

public class NetUtil {

	public static final double binTreshold = 0.5;
//	public static final double binTresholdO = 1.0/256;
//	public static final double binTresholdI = 1.0/144;
	public static final double binTresholdO = binTreshold;
	public static final double binTresholdI = binTreshold;

	public static double[][][] reverseWeights(double[][][] weights) {
		double[][][] out = new double[weights.length][][];
		for (int i = 0; i < weights.length; i++) {
			out[i] = weights[weights.length-1-i];
		}
		return out;
	}

	public static double[][] initWeights(int sizex, int sizey, double wmin, double wmax, Random r, boolean gauss) {
		double[][] newWeights = new double[sizex][sizey];
		int bias = sizex-1;
		for (int i = 0; i < sizex; i++) {
			for (int j = 0; j < sizey; j++) {
                double minw = wmin;
                double maxw = wmax;
				if (gauss)
					newWeights[i][j] = Util.randomGauss((minw + maxw) / 2, maxw - (minw + maxw) / 2, r);
				else
					newWeights[i][j] = ((maxw - minw) * r.nextDouble()) + minw;
//                if (i!=bias) {
//                    newWeights[bias][j] -= newWeights[i][j];
//                }
			}
		}
		return newWeights;
	}

	public static double[][] initWeightsGauss(int sizex, int sizey, double mean, double variance, Random r) {
		double[][] newWeights = new double[sizex][sizey];
		for (int i = 0; i < sizex; i++) {
			for (int j = 0; j < sizey; j++) {
				newWeights[i][j] = Util.randomGauss(mean, variance, r);
			}
		}
		return newWeights;
	}

	public static double[][] initWeights(int sizex, int sizey, double wmin, double wmax, Random generator) {
		return initWeights(sizex, sizey, wmin, wmax, generator, true);
	}

	public static double[][][] initAllWeightsF(int[] arch, double wmin, double wmax, Random generator, boolean gauss) {
		double[][][] weights = new double[arch.length-1][][];
		for (int i = 0; i < arch.length-1; i++) {
			weights[i] = initWeights(arch[i]+1, arch[i+1], wmin, wmax, generator, gauss);
		}
		return weights;
	}

	public static double[][][] initAllWeightsF(int[] arch, double wmin, double wmax, Random generator) {
		return initAllWeightsF(arch, wmin, wmax, generator,true);
	}

	public static double[][][] initAllWeightsF(int[] arch, double wmin, double wmax) {
		Random generator = new Random(System.currentTimeMillis());
		return initAllWeightsF(arch, Conf.wmin, Conf.wmax, generator);
	}

	public static double[][][] initAllWeightsF(int[] arch) {
		Random generator = new Random(System.currentTimeMillis());
		return initAllWeightsF(arch, Conf.wmin, Conf.wmax, generator);
	}

	public static double[][][] initAllWeightsB(int[] arch, double wmin, double wmax, Random generator, boolean gauss) {
		double[][][] weights = new double[arch.length-1][][];
		for (int i = 0; i < arch.length-1; i++) {
			weights[i] = initWeights(arch[i+1]+1, arch[i], wmin, wmax, generator, gauss);
		}
		return weights;
	}

	public static double[][][] initAllWeightsB(int[] arch, double wmin, double wmax, Random generator) {
		return initAllWeightsB(arch, wmin, wmax, generator,true);
	}

	public static double[][][] initAllWeightsB(int[] arch, double wmin, double wmax) {
		Random generator = new Random(System.currentTimeMillis());
		return initAllWeightsB(arch, wmin, wmax, generator);
	}

	public static double[][][] initAllWeightsB(int[] arch) {
		Random generator = new Random(System.currentTimeMillis());
		return initAllWeightsB(arch, Conf.wmin, Conf.wmax, generator);
	}

	public static double[][] initWeightsMatL(int sizex, int sizey, Random r) {
//		net.w1 = randn(nin, nhidden)/sqrt(nin + 1);
//		net.b1 = randn(1, nhidden)/sqrt(nin + 1);
		double[][] newWeights = new double[sizex][sizey];
		for (int i = 0; i < sizex; i++) {
			for (int j = 0; j < newWeights[i].length; j++) {
				newWeights[i][j] = Util.randomGauss(0.0,1.0,r)/Math.sqrt(sizex+1);
			}
		}
		return newWeights;
	}
	
	public static double[][] initWeightsStaticBias(int sizex, int sizey, double wmin, double wmax, Random generator) {
		double[][] newWeights = new double[sizex][sizey];
		for (int i = 0; i < newWeights.length; i++) {
			for (int j = 0; j < newWeights[i].length; j++) {
				if (i == (sizex-1) || j == (sizey-1))
					newWeights[i][j] = 1.0;
				else
					newWeights[i][j] = ((wmax - wmin) * generator.nextDouble()) + wmin;				
			}
		}
		return newWeights;
	}
	
	public static double[][] initWeightsStaticBias(int sizex, int sizey, double wmin, double wmax, Random generator, double bias) {
		double[][] newWeights = new double[sizex][sizey];
		for (int i = 0; i < newWeights.length; i++) {
			for (int j = 0; j < newWeights[i].length; j++) {
				if (i == (sizex-1) || j == (sizey-1))
					newWeights[i][j] = bias;
				else
					newWeights[i][j] = ((wmax - wmin) * generator.nextDouble()) + wmin;				
			}
		}
		return newWeights;
	}

	public static double[][] initWeightsIdentity(int sizex, int sizey) {
		double[][] weights = new double[sizex][sizey];
		for (int i = 0; i < sizex; i++) {
			for (int j = 0; j < sizey; j++) {
				if (i == j) {
					weights[i][j] = 1.0;
				} else {
					weights[i][j] = 0.0;
				}
			}
		}
		return weights;
	}

	public static double[][][] initWeightsMLP(int[] arch, double wmin, double wmax) {
		Random rand = new Random(System.currentTimeMillis());
		double[][][] weights = new double[arch.length][][];
		for (int i = 0; i < arch.length-1; i++) {
			weights[i] = NetUtil.initWeights(arch[i]+1, arch[i+1], wmin, wmax, rand);
		}
		return weights;
	}

	public static double[] winnerDist(double[] input) {
		double[] out = new double[input.length];
		double winVal = 0.0;
		for (int i = 0; i < input.length; i++) {
			winVal = (input[i] > winVal) ? input[i] : winVal;
		}
		for (int i = 0; i < input.length; i++) {
			out[i] = Math.exp(-Conf.distExpConst*(winVal-input[i]));
		}
		return out;
	}

	public static double[] softmax(double[] input) {
		double[] out = new double[input.length];
		double expSum = 0.0;
		for (int i = 0; i < input.length; i++) {
			expSum += Math.exp(input[i]);
		}
		for (int i = 0; i < input.length; i++) {
			out[i] = Math.exp(input[i]) / expSum;
		}
		return out;
	}

	public static double[] kWTAMin(double[] input, int k) {
		double[] out = new double[input.length];
		double[][] tmp = new double[input.length][2];
		for (int i = 0; i < tmp.length; i++) {
			tmp[i][0] = input[i];
			tmp[i][1] = i;
		}
		QuicksortArr sort = new QuicksortArr();
		sort.sort(tmp);
		for (int i = 0; i < k; i++) {
			out[(int)tmp[i][1]] = 1.0;// - i*0.5/k;
		}
		return out;
	}
	
	public static HashSet<Integer> kWTAMinIndices(double[] input, int k) {
		double[][] tmp = new double[input.length][2];
		for (int i = 0; i < tmp.length; i++) {
			tmp[i][0] = input[i];
			tmp[i][1] = i;
		}
		QuicksortArr sort = new QuicksortArr();
		sort.sort(tmp);
		HashSet<Integer> out = new HashSet<Integer>();
		for (int i = 0; i < k; i++) {
			out.add((int)tmp[i][1]);
		}
		return out;
	}
	
	public static double[] kWTAMax(double[] input, int k) {
		double[] out = new double[input.length];
		double[][] tmp = new double[input.length][2];
		for (int i = 0; i < tmp.length; i++) {
			tmp[i][0] = input[i];
			tmp[i][1] = i;
		}
		QuicksortArr sort = new QuicksortArr();
		sort.sort(tmp);
		for (int i = 0; i < k; i++) {
			out[(int)tmp[input.length-1-i][1]] = 1.0;
		}
		return out;
	}
	
	public static double actFun(double val) {
		// y = f(net) = 1 / (1+e^-net)
		return 1.0 / (1.0 + Math.exp(-val));
	}

	public static double actFunDeriv(double val) {
		return val * (1 - val);
	}
	
	public static double[] addBiasInput(double[] v) {
		double[] result = new double[v.length + 1];
		for (int i = 0; i < v.length; i++) {
			result[i] = v[i];
		}
		result[result.length - 1] = 1.0;
		return result;
	}
	
	public static double[] addBiasInput(double[] input, double val) {
		double[] result = new double[input.length + 1];
		for (int i = 0; i < input.length; i++) {
			result[i] = input[i];
		}
		result[result.length - 1] = val;
		return result;
	}
	
	public static double squareError(double real, double expected) {
		return (expected - real) * (expected - real);
	}
	
	public static double distD2(int x1, int y1, int x2, int y2) {
		return Math.sqrt((y2-y1)*(y2-y1)+(x2-x1)*(x2-x1));
	}
	
	public static double meanSquaredError(double[] output, double[] desired) {
		double out = 0.0;
		for (int i = 0; i < output.length; i++) {
			out += ((desired[i] - output[i]) * (desired[i] - output[i]));
		}
		return out/output.length;
	}
	
	public static int succBits(double[] output, double[] desired) {
		int succs = 0;
		for (int i = 0; i < output.length; i++) {
			if ((output[i] >= binTreshold && desired[i] >= binTreshold) || 
				(output[i] < binTreshold && desired[i] < binTreshold))
			succs++;	
		}
		return succs;
	}
	
	public static int truePositives(double[] output, double[] desired) {
		int c = 0;
		for (int i = 0; i < output.length; i++) {
			if (desired[i] >= binTreshold && output[i] >= binTreshold)
				c++;
		}
		return c;
	}

	public static int falsePositives(double[] output, double[] desired) {
		int c = 0;
		for (int i = 0; i < output.length; i++) {
			if (desired[i] < binTreshold && output[i] >= binTreshold)
				c++;
		}
		return c;
	}

	public static int falseNegatives(double[] output, double[] desired) {
		int c = 0;
		for (int i = 0; i < output.length; i++) {
			if (desired[i] >= binTreshold && output[i] < binTreshold)
				c++;
		}
		return c;
	}

	public static double precision(double[] output, double[] desired) {
		double tp = truePositives(output, desired);
		double fn = falseNegatives(output, desired);
		if ((tp+fn) == 0)
			return 0;
		return tp/(tp+fn);
	}

	public static double recall(double[] output, double[] desired) {
		double tp = truePositives(output, desired);
		double fp = falsePositives(output, desired);
		if ((tp+fp) == 0)
			return 0;
		return tp/(tp+fp);
	}

	public static double f1score(double[] output, double[] desired) {
		double precision = precision(output, desired);
		double recall = recall(output, desired);
		if (precision == 0 || recall == 0)
			return 0.0;
		return 2*(recall * precision)/(recall + precision);
	}

	public static int[] centerCoordinates(double[] input) {
		int size = (int)Math.sqrt(input.length);
		int minX = size-1; 
		int maxX = 0; 
		int minY = size-1; 
		int maxY = 0;
		for (int i = 0; i < input.length; i++) {
			if (input[i] >= binTreshold) {
				minX = (i%size < minX) ? i%size : minX;
				maxX = (i%size > maxX) ? i%size : maxX;
				minY = (i/size < minY) ? i/size : minY;
				maxY = (i/size > maxY) ? i/size : maxY;
			}
		}
		return new int[] {(minX+((maxX-minX)/2)),(minY+((maxY-minY)/2))};
	}
	
	public static double avgDistCenter(double[] output, double[] desired) {
		// compute x and y of the central pixel for both vectors
		int[] coordO = centerCoordinates(output);
		int[] coordD = centerCoordinates(desired);
		// compute distance
		return distD2(coordO[0],coordO[1],coordD[0],coordD[1]);
	}

	public static double[] errorsMSE(double[] inputs, double[] actIplus, double[] actOminus, double[] desired) {
		double[] out = new double[2];
		out[0] = meanSquaredError(actOminus, desired);
		out[1] = meanSquaredError(actIplus, inputs);
		return out;
	}

	public static double[] errors(double[] inputs, double[] actIplus, double[] actOminus, double[] desired) {
		double[] out = new double[4];
		out[0] = meanSquaredError(actOminus, desired);
		out[1] = meanSquaredError(actIplus, inputs);
		out[2] = succBits(actOminus, desired) / desired.length;
		out[3] = succBits(actIplus, inputs) / inputs.length;
		return out;
	}

	public static double[] errorsF1(double[] inputs, double[] actIplus, double[] actOminus, double[] desired) {
		double[] out = new double[4];
		out[0] = meanSquaredError(actOminus, desired);
		out[1] = meanSquaredError(actIplus, inputs);
		out[2] = f1score(actOminus, desired);
		out[3] = f1score(actIplus, inputs);
		return out;
	}

	public static double[] errorsF1bidir(double[] inputs, double[] actIplus, double[] actOminus, double[] desired) {
		double[] out = new double[Conf.errorLabelsF1bidir.length];
		out[0] = meanSquaredError(actOminus, desired);
		out[1] = meanSquaredError(actIplus, inputs);
		out[2] = f1score(actOminus, desired);
		out[3] = f1score(actIplus, inputs);
		out[4] = f1score(Util.merge(actOminus,actIplus),Util.merge(desired,inputs));
		return out;
	}

	public static double[] errorsUnidir(double[] actOminus, double[] desired) {
		double[] out = new double[3];
		out[0] = meanSquaredError(actOminus, desired);
		int succBits = succBits(actOminus, desired);
		out[1] = succBits/(actOminus.length*1.0);
		out[2] = actOminus.length - succBits;
		return out;
	}
	
	public static double[] errorsDist(double[] inputs, double[] actIplus, double[] actOminus, double[] desired) {
		double[] out = new double[6];
		out[0] = meanSquaredError(actOminus, desired);
		out[1] = meanSquaredError(actIplus, inputs);
		out[2] = succBits(actOminus, desired);
		out[3] = succBits(actIplus, inputs);
		out[4] = avgDistCenter(actOminus, desired);
		out[5] = avgDistCenter(actIplus, inputs);
		return out;
	}
	
	public static double[] errorsExt(double[] inputs, double[] actIplus, double[] actOminus, double[] desired) {
		double[] out = new double[8];
		int outs = desired.length;
		int inps = inputs.length;
		double sseF = 0.0;
		double sseB = 0.0;
		double successF = 0.0;
		double successB = 0.0;
		int kwtaErrF = 0;
		int kwtaErrB = 0;
		int k = Util.positiveBits(inputs);
		double[] actOminuskWTA = kWTAMax(actOminus, k);
		double[] actIpluskWTA = kWTAMax(actIplus, k); 
		for (int i = 0; i < outs; i++) {
			double outf = actOminus[i];
			sseF += squareError(outf, desired[i]);
			if ((desired[i] >= binTreshold && outf >= binTreshold) || (desired[i] < binTreshold && outf < binTreshold)) 
				successF += 1.0;
			if (actOminuskWTA[i] != inputs[i])
				kwtaErrF++;
		}
		for (int i = 0; i < inps; i++) {
			double outb = actIplus[i];
			sseB += squareError(outb, inputs[i]);
			if ((inputs[i] >= binTreshold && outb >= binTreshold) || (inputs[i] < binTreshold && outb < binTreshold)) 
				successB += 1.0;
			if (actIpluskWTA[i] != desired[i])
				kwtaErrB++;
		}
		out[0] = sseF/outs;
		out[1] = sseB/inps;
		out[2] = successF/outs;
		out[3] = successB/inps;
		out[4] = outs-successF;
		out[5] = outs-successB;
		out[6] = kwtaErrF;
		out[7] = kwtaErrB;
		return out;
	}

	public static double[] errorsBAL(int[] arch, double[][] dataF, double[][] dataB, BAL.Activations[] netAct) {
		double[] errors = new double[Conf.errorLabelsF1.length];
		for (int i = 0; i < dataF.length; i++) {
			double[] tmperrors = NetUtil.errors(dataF[i], netAct[i].actBPrediction()[0], netAct[i].actFPrediction()[arch.length-1], dataB[i]);
			for (int j = 0; j < errors.length; j++) {
				errors[j] += tmperrors[j];
			}
		}
		for (int j = 0; j < errors.length; j++) {
			errors[j] /= dataF.length;
		}
		return errors;
	}

	public static double[] errorsF1bidirBAL(int[] arch, double[][] dataF, double[][] dataB, BAL.Activations[] netAct) {
		double[] errors = new double[Conf.errorLabelsF1bidir.length];
		for (int i = 0; i < dataF.length; i++) {
			double[] tmperrors = NetUtil.errorsF1bidir(dataF[i], netAct[i].actBPrediction()[0], netAct[i].actFPrediction()[arch.length-1], dataB[i]);
			for (int j = 0; j < errors.length; j++) {
				errors[j] += tmperrors[j];
			}
		}
		for (int j = 0; j < errors.length; j++) {
			errors[j] /= dataF.length;
		}
		return errors;
	}

	public static double[] errorsF1BP(double[][][] netAct, double[][] desired) {
		double[] out = new double[2];
		for (int i = 0; i < desired.length; i++) {
			double[] netOutput = netAct[i][netAct[i].length-1];
			out[0] += NetUtil.meanSquaredError(netOutput,desired[i]);
			out[1] += NetUtil.f1score(netOutput,desired[i]);
		}
		for (int i = 0; i < out.length; i++) {
			out[i] /= desired.length;
		}
		return out;
	}

	public static double[] errorsF1bidirBP(double[][][] actF, double[][][] actB, double[][] inputs, double[][] desired) {
		double[] out = new double[5];
		for (int i = 0; i < actF.length; i++) {
			double[] outF = actF[i][actF[i].length-1];
			double[] outB = actB[i][actB[i].length-1];
			out[0] += NetUtil.meanSquaredError(outF,desired[i]);
			out[1] += NetUtil.meanSquaredError(outB,inputs[i]);
			out[2] += NetUtil.f1score(outF,desired[i]);
			out[3] += NetUtil.f1score(outB,inputs[i]);
			out[4] += NetUtil.f1score(Util.merge(outF,outB),Util.merge(desired[i],inputs[i]));
		}
		for (int i = 0; i < out.length; i++) {
			out[i] /= inputs.length;
		}
		return out;
	}

	public static boolean errorsUnsatisfied(double[] errors) {
		return (errors[2] < Conf.minPatSucc || errors[3] < Conf.minPatSucc);
	}
	
	public static boolean errorsUnsatisfiedBP(double[] errors) {
		return !(
			(errors[1] > Conf.minPatSucc)
		);
	}
	
	public static boolean errorsUnsatisfiedSingle(double[] errors) {
		return !(errors[0] < Conf.minMSE || errors[1] > Conf.minPatSucc); // errors[2] > Conf.minBitSucc);
	}
	
	public static String weightsToStr(double[][] weights) {
		String out = "\t";
		for (int i = 0; i < weights[0].length; i++) {
			if (i == weights[0].length-1)
				out += "b\t";
			else
				out += i+"\t";
		}
		out += "\n";
		for (int i = 0; i < weights.length; i++) {
			if (i == weights.length-1)
				out += "b\t";
			else
				out += i+"\t";
			for (int j = 0; j < weights[i].length; j++) {
				if (i == weights.length-1 &&  j == weights[i].length-1)
					;
				else
					out += Util.round(weights[i][j],3)+"\t";
			}
			out += "\n";
		}
		return out;
	}

	public static String archToStr(int[] arch) {
		String out = new String("");
		for (int i = 0; i < arch.length-1; i++) {
			out += arch[i] + "-";
		}
		out += arch[arch.length-1];
		return out;
	}

	public static String[] paramsToStr(double[] in) {
		String out[] = new String[in.length];
		for (int i = 0; i < in.length; i++) {
			out[i] = in[i] + "";
		}
		return out;
	}

	public static String[][] weightsToStrArr(double[][] weights) {
		String[][] out = new String[weights.length+1][weights[0].length+1];
		for (int i = 0; i < weights[0].length; i++) {
			if (i == weights[0].length-1)
				out[0][i] = "b";
			else
				out[0][i] = ""+i;
		}
		for (int i = 0; i < weights.length; i++) {
			if (i == weights.length-1)
				out[i+1][0] = "b";
			else
				out[i+1][0] = ""+i;
			for (int j = 0; j < weights[i].length; j++) {
				if (i == weights.length-1 &&  j == weights[i].length-1)
					;
				else
					out[i+1][j+1] = ""+Util.round(weights[i][j],3);
			}
		}
		return out;
	}
	
	public static String weightsToStr2(double[][] weights) {
		String out = "\t";
		for (int i = 0; i < weights[0].length; i++) {
			out += i+"\t";
		}
		out += "\n";
		for (int i = 0; i < weights.length; i++) {
			out += i+"\t";
			for (int j = 0; j < weights[i].length; j++) {
				out += Util.round(weights[i][j],3)+"\t";
			}
			out += "\n";
		}
		return out;
	}
	
	public static double[] boundActivation(double[] input) {
		double[] out = input;
		for (int i = 0; i < out.length; i++) {
			if (out[i] < 0.05)
				out[i] = 0.05;
			else if (out[i] > 0.95)
				out[i] = 0.95;
		}
		return out;
	}

	public static double[][] mapDistFunc(double[][] input, double mi) {
		double[][] out = input;
		for (int i = 0; i < input.length; i++) {
			for (int j = 0; j < input[0].length; j++) {
				out[i][j] = Math.exp(-mi*input[i][j]);	
			}
		}
		return out;
	}

	public static void main(String[] args) {
//		double[] tmp = {20.9703, 23.546, 21.0095, 8.75616, 5.17831, 3.92128, 3.75595, 3.774, 4.05175, 4.18055, 16.7722, 18.2791, 15.9248, 9.94269, 4.96743, 3.98778, 3.41068, 3.10179, 3.22482, 3.53159, 13.3947, 12.4756, 11.7762, 9.40793, 6.34519, 4.48849, 3.19096, 2.66844, 2.84541, 3.18284, 10.0256, 9.05394, 8.67853, 7.87298, 6.5345, 4.96786, 4.03302, 3.03225, 2.82185, 2.97854, 6.35498, 5.90043, 5.64507, 5.93079, 5.61413, 5.76058, 6.35482, 5.80667, 3.88235, 3.08469, 3.11572, 3.30291, 3.3254, 3.55145, 4.79846, 5.0224, 5.43414, 5.54289, 5.27235, 5.23148, 2.21022, 1.89482, 1.66906, 0.98979, 2.80644, 4.73478, 5.02366, 5.20346, 5.38971, 5.93192, 2.05477, 1.39513, 0.77192, 0.466031, 0.557119, 3.90069, 4.87683, 5.07087, 5.34024, 6.08889, 1.62839, 1.13275, 0.813843, 0.292619, 0.101303, 0.461359, 3.45169, 5.03155, 5.66545, 6.38652, 1.49858, 1.1033, 1.08097, 0.405735, 0.0164955, 0.129109, 0.761833, 4.73728, 6.89667, 7.27288};
//		System.out.println(Util.arrayToBinaryString(kWTAMax(tmp,8)));
		
//		double[] tmp = {0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.07, 0.071, 0.07, 0.071, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.013, 0.072, 0.07, 0.07, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.015, 0.015, 0.07, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.007, 0.015, 0.015, 0.015, 0.001, 0.001, 0.001, 0.001, 0.001, 0.007, 0.017, 0.017, 0.001, 0.015, 0.001, 0.001, 0.001, 0.001, 0.001, 0.007, 0.017, 0.028, 0.027, 0.058, 0.022, 0.012, 0.001, 0.02, 0.001, 0.001, 0.416, 0.789, 0.793, 0.031, 0.026, 0.11, 0.02, 0.044, 0.049, 0.001, 0.788, 0.788, 0.787, 0.057, 0.111, 0.11, 0.114, 0.05, 0.049, 0.049, 0.093, 0.79, 0.028, 0.028, 0.112, 0.111, 0.111, 0.131, 0.05, 0.05, 0.036, 0.028, 0.017, 0.017};
//		double[] tmpwta = kWTAMax(tmp, 8);
//		for (int i = 0; i < tmp.length; i++) {
//			System.out.print(tmp[i]+"\t");
//		}
//		System.out.println();
//		for (int i = 0; i < tmpwta.length; i++) {
//			System.out.print(tmpwta[i]+"\t");
//		}
//		System.out.println();
//		System.out.println(avgDist(new double[] {0,0,0,1,1,1,1,0},new double[] {0,0,0,1,1,1,1,0}));
		
		double[] tmp = {0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.07, 0.071, 0.07, 0.071, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.013, 0.072, 0.07, 0.07, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.015, 0.015, 0.07, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.007, 0.015, 0.015, 0.015, 0.001, 0.001, 0.001, 0.001, 0.001, 0.007, 0.017, 0.017, 0.001, 0.015, 0.001, 0.001, 0.001, 0.001, 0.001, 0.007, 0.017, 0.028, 0.027, 0.058, 0.022, 0.012, 0.001, 0.02, 0.001, 0.001, 0.416, 0.789, 0.793, 0.031, 0.026, 0.11, 0.02, 0.044, 0.049, 0.001, 0.788, 0.788, 0.787, 0.057, 0.111, 0.11, 0.114, 0.05, 0.049, 0.049, 0.093, 0.79, 0.028, 0.028, 0.112, 0.111, 0.111, 0.131, 0.05, 0.05, 0.036, 0.028, 0.017, 0.017};
		tmp = kWTAMax(tmp, 8);
		double[] tmp2 = tmp.clone();
		tmp2[0] = 1.0; tmp2[1] = 1.0; tmp2[2] = 1.0; 
		
		int size = (int)Math.sqrt(tmp.length);
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				System.out.print(Math.round(tmp[i*size+j]));
			}
			System.out.print("\t");
			for (int j = 0; j < size; j++) {
				System.out.print(Math.round(tmp2[i*size+j]));
			}
			System.out.println();
		}
		
		System.out.println(avgDistCenter(tmp,tmp2));
	}

}
