package base;

import util.Util;

import java.util.Random;

public class BAL2 {

	protected int inps;
	protected int hids;
	protected int outs;
	protected double lambdaHidden; //learning rate
	protected double lambdaVisible; //learning rate
	protected double biasLT = 0.1; //bias learning threshold

	protected double[][] weightsIH; //weights from inputs to hidden units:: [inputs][hiddens]		//plus 1 for bias
	protected double[][] weightsHO; //weights from hidden to output units:: [hiddens][outputs]	//plus 1 for bias
	protected double[][] weightsOH; //weights from output to hidden:: [outputs+1][hiddens+1]	//plus 1 for bias
	protected double[][] weightsHI; //weights from hidden to inputs:: [hiddens+1][inputs+1]		//plus 1 for bias

	public BAL2(int inputSize, int hiddenSize, int outputSize, double lHid, double lVis, double wmax, double wmin) {
		inps = inputSize;
		hids = hiddenSize;
		outs = outputSize;
		lambdaHidden = lHid;
		lambdaVisible = lVis;
		Random rand = new Random(System.currentTimeMillis());
//		weightsIH = NetUtil.initWeights(inps+1, hids, wmin, wmax, rand);
		weightsHO = NetUtil.initWeights(hids+1, outs, wmin, wmax, rand);
//		weightsOH = NetUtil.initWeights(outs+1, hids, wmin, wmax, rand);
		weightsHI = NetUtil.initWeights(hids+1, inps, wmin, wmax, rand);
		weightsIH = NetUtil.initWeightsIdentity(inps+1, hids);
//		weightsHO = NetUtil.initWeightsIdentity(hids+1, outs);
		weightsOH = NetUtil.initWeightsIdentity(outs+1, hids);
//		weightsHI = NetUtil.initWeightsIdentity(hids+1, inps);
	}

	public BAL2(int inputSize, int hiddenSize, int outputSize, double lHid, double lVis, double wmax, double wmin, boolean gauss) {
		inps = inputSize;
		hids = hiddenSize;
		outs = outputSize;
		lambdaHidden = lHid;
		lambdaVisible = lVis;
		Random rand = new Random(System.currentTimeMillis());
		weightsIH = NetUtil.initWeights(inps+1, hids, wmin, wmax, rand, gauss);
		weightsHO = NetUtil.initWeights(hids+1, outs, wmin, wmax, rand, gauss);
		weightsOH = NetUtil.initWeights(outs+1, hids, wmin, wmax, rand, gauss);
		weightsHI = NetUtil.initWeights(hids+1, inps, wmin, wmax, rand, gauss);
	}

	public BAL2(int inputSize, int hiddenSize, int outputSize, double lHid, double lVis) {
		inps = inputSize;
		hids = hiddenSize;
		outs = outputSize;
		lambdaHidden = lHid;
		lambdaVisible = lVis;
		Random rand = new Random(System.currentTimeMillis());		
		weightsIH = NetUtil.initWeightsMatL(inps+1, hids, rand);
		weightsHO = NetUtil.initWeightsMatL(hids+1, outs, rand);
		weightsOH = NetUtil.initWeightsMatL(outs+1, hids, rand);
		weightsHI = NetUtil.initWeightsMatL(hids+1, inps, rand);
	}
	
	protected double[] actHminus(double[] input) {
		double[] out = new double[hids];
		double[] biasedIn = NetUtil.addBiasInput(input,Conf.biasValue);
		for (int i = 0; i < inps+1; i++) {
			for (int j = 0; j < hids; j++) {
				out[j] += weightsIH[i][j] * biasedIn[i];
			}
		}
		for (int i = 0; i < out.length; i++) {
			out[i] = NetUtil.actFun(out[i]);
		}
		return out;
	}
	
	protected double[] actOminus(double[] input) {
		double[] out = new double[outs];
		double[] biasedIn = NetUtil.addBiasInput(input,Conf.biasValue);
		for (int j = 0; j < hids+1; j++) {
			for (int k = 0; k < outs; k++) {
				out[k] += weightsHO[j][k] * biasedIn[j];
			}
		}
		for (int i = 0; i < out.length; i++) {
			out[i] = NetUtil.actFun(out[i]);
		}
		return out;
	}

	protected double[] actHplus(double[] desired) {	
		double[] out = new double[hids];
		double[] biasedDes = NetUtil.addBiasInput(desired,Conf.biasValue);
		for (int k = 0; k < outs+1; k++) {
			for (int j = 0; j < hids; j++) {
				out[j] += weightsOH[k][j] * biasedDes[k];
			}
		}			
		for (int i = 0; i < out.length; i++) {
			out[i] = NetUtil.actFun(out[i]);
		}
		return out;
	}
	
	protected double[] actIplus(double[] actHid) {	
		double[] out = new double[inps];
		double[] biasedIn = NetUtil.addBiasInput(actHid,Conf.biasValue);
		for (int j = 0; j < hids+1; j++) {
			for (int i = 0; i < inps; i++) {
				out[i] += weightsHI[j][i] * biasedIn[j];
			}
		}				
		for (int i = 0; i < out.length; i++) {
			out[i] = NetUtil.actFun(out[i]);
		}
		return out;
	}
	
	protected void adjustWeights(double[] inputs, double[] actHminus, double[] actOminus, double[] desired, double[] actHplus, double[] actIplus) {
		//weights IN - HID
		for (int i = 0; i < inps+1; i++) {
			for (int j = 0; j < hids; j++) {
				//biases
				if (i == inps) 
					weightsIH[i][j] += lambdaHidden * (actHplus[j] - actHminus[j]);
				else 	
					weightsIH[i][j] += lambdaHidden * inputs[i] * (actHplus[j] - actHminus[j]);
			}
		}				
		//weights HID - OUT
		for (int j = 0; j < hids+1; j++) {
			for (int k = 0; k < outs; k++) {
				//biases		
				if (j == hids) 
					weightsHO[j][k] += lambdaVisible * (desired[k] - actOminus[k]);
				else 	
					weightsHO[j][k] += lambdaVisible * actHminus[j] * (desired[k] - actOminus[k]);
			}
		}
		//weights OUT - HID
		for (int k = 0; k < outs+1; k++) {
			for (int j = 0; j < hids; j++) {
				//biases
				if (k == outs)
					weightsOH[k][j] += lambdaHidden * (actHminus[j] - actHplus[j]);
				else
					weightsOH[k][j] += lambdaHidden * desired[k] * (actHminus[j] - actHplus[j]);
			}
		}
		//weights HID - IN
		for (int j = 0; j < hids+1; j++) {
			for (int i = 0; i < inps; i++) {
				//biases
				if (j == hids)
					weightsHI[j][i] += lambdaVisible * (inputs[i] - actIplus[i]);
				else
					weightsHI[j][i] += lambdaVisible * actHplus[j] * (inputs[i] - actIplus[i]);
			}
		}
	}
	
	public double[] epochPD(double[][] inputs, double[][] desired, boolean learn, boolean log) {
		double[] out = new double[8];
		int[] indexer = Util.permutedIndexes(inputs.length);
		for (int n = 0; n < indexer.length; n++) {
			int s = indexer[n];
			//phase1
			double[] actHminus = actHminus(inputs[s]);
			double[] actOminus = actOminus(actHminus);
			//phase2
			double[] actHplus = actHplus(desired[s]);
			double[] actIplus = actIplus(actHplus);
			double[] tmperrors = NetUtil.errorsDist(inputs[s], actIplus,actOminus, desired[s]);
			//errors
			out[0] += tmperrors[0]; //mseF
			out[1] += tmperrors[1]; //mseB
			out[2] += (tmperrors[2] == outs)?1:0; //pattern success
			out[3] += (tmperrors[3] == inps)?1:0; //pattern success
			out[4] += tmperrors[2]/outs; //bit success
			out[5] += tmperrors[3]/inps; //bit success
			out[6] += tmperrors[4]/outs; //pattern distance
			out[7] += tmperrors[5]/inps; //pattern distance	
			//weights
			if (learn)
				adjustWeights(inputs[s], actHminus, actOminus, desired[s], actHplus, actIplus);
			//log
			if (log) {
				System.out.println(tmperrors[2]+" "+Util.arrayToBinaryString(inputs[s])+"\t"+Util.arrayToBinaryString(actIplus));
				System.out.println(tmperrors[3]+" "+Util.arrayToBinaryString(desired[s])+"\t"+Util.arrayToBinaryString(actOminus));
			}
		}
		for (int i = 0; i < out.length; i++) {
			out[i] /= indexer.length;
		}
		return out;
	}
	
	public double[] epoch(double[][] inputs, double[][] desired, boolean learn, boolean log) {
		double[] out = new double[6];
		int[] indexer = Util.permutedIndexes(inputs.length);
		for (int n = 0; n < indexer.length; n++) {
			int s = indexer[n];
			//phase1
			double[] actHminus = actHminus(inputs[s]);
			double[] actOminus = actOminus(actHminus);
			//phase2
			double[] actHplus = actHplus(desired[s]);
			double[] actIplus = actIplus(actHplus);
			double[] tmperrors = NetUtil.errors(inputs[s], actIplus,actOminus, desired[s]);
			//errors
			out[0] += tmperrors[0]; //mseF
			out[1] += tmperrors[1]; //mseB
			out[2] += (tmperrors[2] == outs)?1:0; //pattern success
			out[3] += (tmperrors[3] == inps)?1:0; //pattern success
			out[4] += tmperrors[2]/outs; //bit success
			out[5] += tmperrors[3]/inps; //bit success
			//weights
			if (learn)
				adjustWeights(inputs[s], actHminus, actOminus, desired[s], actHplus, actIplus);
			//log
			if (log) {
				System.out.println(tmperrors[2]+" "+Util.arrayToBinaryString(inputs[s])+"\t"+Util.arrayToBinaryString(actIplus));
				System.out.println(tmperrors[3]+" "+Util.arrayToBinaryString(desired[s])+"\t"+Util.arrayToBinaryString(actOminus));
			}
		}
		for (int i = 0; i < out.length; i++) {
			out[i] /= indexer.length;
		}
		return out;
	}

	public double[] epochF1(double[][] inputs, double[][] desired, boolean learn) {
		double[] out = new double[4];
		int[] indexer = Util.permutedIndexes(inputs.length);
		for (int n = 0; n < indexer.length; n++) {
			int s = indexer[n];
			//phase1
			double[] actHminus = actHminus(inputs[s]);
			double[] actOminus = actOminus(actHminus);
			//phase2
			double[] actHplus = actHplus(desired[s]);
			double[] actIplus = actIplus(actHplus);
			double[] tmperrors = NetUtil.errorsF1(inputs[s],actIplus,actOminus,desired[s]);
			//errors
			out[0] += tmperrors[0]; //mseF
			out[1] += tmperrors[1]; //mseB
			out[2] += tmperrors[2]; //f1 success
			out[3] += tmperrors[3]; //f1 success
			//weights
			if (learn)
				adjustWeights(inputs[s], actHminus, actOminus, desired[s], actHplus, actIplus);
		}
		for (int i = 0; i < out.length; i++) {
			out[i] /= indexer.length;
		}
		return out;
	}
	
	public double[] epoch(double[][] inputs, double[][] desired) {
		return epoch(inputs,desired,true,false);
	}
	
	public double[] testEpoch(double[][] inputs, double[][] desired) {
		return epoch(inputs,desired,false,false);
	}
		
	public double[] testEpoch(double[][] inputs, double[][] desired, boolean log) {
		return epoch(inputs,desired,false,true);
	}
	
	public double[] epochExtInfo(double[][] inputs, double[][] desired) {
		double[] out = new double[11];
		int[] indexer = Util.permutedIndexes(inputs.length);
		for (int n = 0; n < indexer.length; n++) {
			int s = indexer[n];
			//phase1
			double[] actHminus = actHminus(inputs[s]);
			double[] actOminus = actOminus(actHminus);
			//phase2
			double[] actHplus = actHplus(desired[s]);
			double[] actIplus = actIplus(actHplus);
			double[] tmperrors = NetUtil.errors(inputs[s], actIplus,actOminus, desired[s]);
			//errors
			out[0] += tmperrors[0]; //mseF
			out[1] += tmperrors[1]; //mseB
			out[2] += (tmperrors[2] == outs)?1:0; //pattern success
			out[3] += (tmperrors[3] == inps)?1:0; //pattern success
			out[4] += tmperrors[2]/outs; //bit success
			out[5] += tmperrors[3]/inps; //bit success
			out[6] += Util.binaryResemblance(actHminus);
			out[7] += Util.binaryResemblance(actHplus);
			out[8] += Util.distanceEuclid(actHminus, actHplus);
			out[9] += Util.distanceEuclid(weightsIH,weightsOH); //to hidden
			out[10] += Util.distanceEuclid(weightsHO,weightsHI); //from hidden
			//weights
			adjustWeights(inputs[s], actHminus, actOminus, desired[s], actHplus, actIplus);
		}
		for (int i = 0; i < out.length; i++) {
			out[i] /= indexer.length;
		}
		return out;
	}
	
	public double[][][] testEpochPatterns(double[][] inputs, double[][] desired) {
		double[][][] out = new double[4][inputs.length][inputs[0].length];	
		for (int s = 0; s < inputs.length; s++) {
			//phase1
			double[] actHminus = actHminus(inputs[s]);
			double[] actOminus = actOminus(actHminus);
			//phase2
			double[] actHplus = actHplus(desired[s]);
			double[] actIplus = actIplus(actHplus);
			for (int i = 0; i < actOminus.length; i++) {
				actOminus[i] = (actOminus[i] >= NetUtil.binTreshold)?1:0;
			}
			for (int i = 0; i < actIplus.length; i++) {
				actIplus[i] = (actIplus[i] >= NetUtil.binTreshold)?1:0;
			}
			out[0][s] = desired[s];
			out[1][s] = actOminus;
			out[2][s] = inputs[s];
			out[3][s] = actIplus;
		}
		return out;
	}
	
	public double[][][] testEpochPatternskWTA(double[][] inputs, double[][] desired) {
		double[][][] out = new double[4][inputs.length][inputs[0].length];	
		for (int s = 0; s < inputs.length; s++) {
			//phase1
			double[] actHminus = actHminus(inputs[s]);
			double[] actOminus = actOminus(actHminus);
			//phase2
			double[] actHplus = actHplus(desired[s]);
			double[] actIplus = actIplus(actHplus);
			out[0][s] = desired[s];
			out[1][s] = NetUtil.kWTAMax(actOminus, Util.positiveBits(desired[s]));
			out[2][s] = inputs[s];
			out[3][s] = NetUtil.kWTAMax(actIplus, Util.positiveBits(inputs[s]));
		}
		return out;
	}

	public double[][] getWeightsIH() {
		return weightsIH;
	}

	public void setWeightsIH(double[][] weightsIH) {
		this.weightsIH = weightsIH;
	}

	public double[][] getWeightsHO() {
		return weightsHO;
	}

	public void setWeightsHO(double[][] weightsHO) {
		this.weightsHO = weightsHO;
	}

	public double[][] getWeightsOH() {
		return weightsOH;
	}

	public void setWeightsOH(double[][] weightsOH) {
		this.weightsOH = weightsOH;
	}

	public double[][] getWeightsHI() {
		return weightsHI;
	}

	public void setWeightsHI(double[][] weightsHI) {
		this.weightsHI = weightsHI;
	}
	
	public void printWeights() {
		System.out.println(NetUtil.weightsToStr(weightsIH));
		System.out.println(NetUtil.weightsToStr(weightsHO));
		System.out.println(NetUtil.weightsToStr(weightsOH));
		System.out.println(NetUtil.weightsToStr(weightsHI));
	}
	
}