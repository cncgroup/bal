package base;

import a.d.P;
import util.Util;

import java.util.Random;


public class MLP {

	protected int[] layers;
	private double[][][] weights; //[arch.length][][]
	private double[][][] wchanges; //for momentum
	private double lRate;
	private double momentum;
	private double wDecay;	
	
	public MLP(int[] layerSzs, double[][][] newWeights, double lrnsp, double mmntm, double wdecay) {
		layers = layerSzs;
		lRate = lrnsp;
		momentum = mmntm;
		wDecay = wdecay;
		weights = newWeights;
		wchanges = new double[layerSzs.length][][];
		for (int i = 0; i < layerSzs.length-1; i++) {
			wchanges[i] = new double[layerSzs[i]+1][layerSzs[i+1]];
		}
	}
	
	public MLP(int[] layerSzs, double lrnsp, double mmntm, double wdecay) {
		this(layerSzs,null,lrnsp,mmntm,wdecay);
		Random rand = new Random(System.currentTimeMillis());
		weights = new double[layerSzs.length-1][][];
		for (int i = 0; i < layerSzs.length-1; i++) {
			weights[i] = NetUtil.initWeightsMatL(layerSzs[i]+1, layerSzs[i+1], rand);
		}
	}

	public MLP(int[] layerSzs, double lrnsp, double mmntm, double wdecay, double wmin, double wmax, boolean gauss) {
		this(layerSzs,null,lrnsp,mmntm,wdecay);
		Random rand = new Random(System.currentTimeMillis());
		weights = new double[layerSzs.length-1][][];
		for (int i = 0; i < layerSzs.length-2; i++) {
			weights[i] = NetUtil.initWeights(layerSzs[i]+1, layerSzs[i+1], wmin, wmax, rand, gauss);
		}
	}

	public MLP(int[] layerSzs, double lrnsp, double mmntm, double wdecay, double wmin, double wmax) {
		this(layerSzs,lrnsp,mmntm,wdecay,wmin,wmax,true);
	}

	public MLP(int[] layerSzs, double[][][] newWeights, double lrnsp) {
		this(layerSzs,newWeights,lrnsp,Conf.bp_momentum,Conf.bp_wdecay);
	}

	public MLP(int[] layerSzs, double lrnsp) {
		this(layerSzs,lrnsp,Conf.bp_momentum,Conf.bp_wdecay);
	}
	
	public double[] activation(double[] inputAct, double[][] weights) {
		double[] output = new double[weights[0].length];
		double[] biasedInput = NetUtil.addBiasInput(inputAct);
		for (int i = 0; i < biasedInput.length; i++) {
			for (int j = 0; j < output.length; j++) {
				output[j] += weights[i][j] * biasedInput[i];
			}
		}
		for (int i = 0; i < output.length; i++) {
			output[i] = NetUtil.actFun(output[i]);
		}
		return output;
	}

	// w_{jk} += \alpha \delta_{k}h_{j}
	// \delta_{k}=(d_{k} - y_{k}) f'_{k},
	// v_{ij} += \alpha \delta_{j} x_{i}
	// \delta_{j}=(\sum_{k}w_{jk}\delta_{k}) f'_{j},
	public void adjustWeights(double[][] activations, double[] desired) {
		double[][] deltas = new double[layers.length][];
		for (int i = 1; i < deltas.length; i++) {
			deltas[i] = new double[layers[i]];
		}
		//deltas output
		for (int j = 0; j < layers[layers.length-1]; j++) {
			deltas[layers.length-1][j] = (desired[j] - activations[layers.length-1][j]) * NetUtil.actFunDeriv(activations[layers.length-1][j]);
		}
		for (int i = layers.length-2; i < 0; i--) {
			for (int j = 0; j < layers[i]; j++) {
				for (int k = 0; k < layers[i+1]; k++) {
					deltas[i][j] += weights[i][j][k] * deltas[i+1][k];
				}
				deltas[i][j] *= NetUtil.actFunDeriv(activations[i][j]);
			}
		}

		for (int li = 0; li < layers.length-1; li++) {
			for (int j = 0; j < layers[li]+1; j++) {
				for (int k = 0; k < layers[li+1]; k++) {
					double wchange = lRate * (j == layers[li] ? 1.0 : activations[li][j]) * deltas[li+1][k];
					wchanges[li][j][k] = wchange + momentum * wchanges[li][j][k];
					weights[li][j][k] = (weights[li][j][k] + wchanges[li][j][k]) * (1 - wDecay);
				}
			}
		}
	}

	public double[][][] epoch(double[][] inputs, double[][] desired, boolean learn) {
		double[][][] outputActivations = new double[inputs.length][layers[layers.length-1]][];
		int[] indexer = Util.permutedIndexes(inputs.length);
		for (int ind = 0; ind < indexer.length; ind++) {
			int s = indexer[ind];
			double[][] activations = new double[layers.length][];
			activations[0] = inputs[s];
			for (int i = 1; i < layers.length; i++) {
				activations[i] = activation(activations[i-1],weights[i-1]);
			}
			if (learn) {
				adjustWeights(activations, desired[s]);
			}
			outputActivations[s] = activations;
		}
		return outputActivations;
	}

	public double[][][] testEpochPatternsBinary(double[][] inputs, double[][] desired) {
		double[][][] out = new double[2][inputs.length][];
		int[] indexer = Util.permutedIndexes(inputs.length);
		for (int s = 0; s < indexer.length; s++) {
			double[][] activations = new double[layers.length][];
			activations[0] = inputs[s];
			for (int i = 1; i < layers.length; i++) {
				activations[i] = activation(activations[i-1],weights[i-1]);
			}
			out[0][s] = desired[s];
			out[1][s] = Util.binarize(activations[layers.length-1]);
		}
		return out;
	}

    public double[][][] testEpochPatterns(double[][] inputs, double[][] desired) {
        double[][][] out = new double[2][inputs.length][];
        int[] indexer = Util.permutedIndexes(inputs.length);
        for (int s = 0; s < indexer.length; s++) {
            double[][] activations = new double[layers.length][];
            activations[0] = inputs[s];
            for (int i = 1; i < layers.length; i++) {
                activations[i] = activation(activations[i-1],weights[i-1]);
            }
            out[0][s] = desired[s];
            out[1][s] = activations[layers.length-1];
        }
        return out;
    }
}