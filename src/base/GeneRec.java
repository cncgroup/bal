package base;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import util.Util;

public class GeneRec {
	
	protected int inps;
	protected int hids;
	protected int outs;
	protected double lr; //learning rate
	
	protected double binTreshold = 0.5;
	protected double biasLearningThr = 0.1;
	
	protected double[][] weightsIH; //weights from inputs to hidden units:: [inputs][hiddens]		//plus 1 for bias
	protected double[][] weightsHO; //weights from hidden to output units:: [hiddens][outputs]	//plus 1 for bias
	
	public GeneRec(int inputSize, int hiddenSize, int outputSize, double lrnsp, double wmin, double wmax, boolean gauss) {
		inps = inputSize;
		hids = hiddenSize;
		outs = outputSize;
		lr = lrnsp;
		Random rand = new Random(System.currentTimeMillis());
		weightsIH = NetUtil.initWeights(inps+1, hids, wmin, wmax, rand, gauss);
		weightsHO = NetUtil.initWeights(hids+1, outs+1, wmin, wmax, rand, gauss);
	}
	
	public GeneRec(int inputSize, int hiddenSize, int outputSize, double lrnsp, double wmin, double wmax) {
		this(inputSize, hiddenSize, outputSize, lrnsp, wmin, wmax, true);
	}
	
	public GeneRec(int inputSize, int hiddenSize, int outputSize, double lrnsp) {
		inps = inputSize;
		hids = hiddenSize;
		outs = outputSize;
		lr = lrnsp;
		Random rand = new Random(System.currentTimeMillis());	
		weightsIH = NetUtil.initWeightsMatL(inps+1, hids, rand);
		weightsHO = NetUtil.initWeightsMatL(hids+1, outs+1, rand);
	}
	
	//minus phase
	public double[] actIH(double[] input) {
		double[] out = new double[hids];
		double[] biasedIn = NetUtil.addBiasInput(input);
		for (int i = 0; i < biasedIn.length; i++) {
			for (int j = 0; j < hids; j++) {
				out[j] += weightsIH[i][j] * biasedIn[i];
			}
		}
		for (int i = 0; i < out.length; i++) {
			out[i] = NetUtil.actFun(out[i]);
		}
		return out;
	}
	
	public double[] actHO(double[] input) {
		double[] out = new double[outs];
		double[] biasedIn = NetUtil.addBiasInput(input);
		for (int j = 0; j < hids+1; j++) {
			for (int k = 0; k < outs; k++) {
				out[k] += weightsHO[j][k] * biasedIn[j];
			}
		}
		for (int i = 0; i < out.length; i++) {
			out[i] = NetUtil.actFun(out[i]);
		}
		return out;
	}
	
	//plus phase
	public double[] actOH(double[] input, double[] desired) {	
		double[] out = new double[hids];
		double[] biasedIn = NetUtil.addBiasInput(input);
		for (int i = 0; i < biasedIn.length; i++) {
			for (int j = 0; j < hids; j++) {
				out[j] += weightsIH[i][j] * biasedIn[i];
			}
		}
		double[] biasedDes = NetUtil.addBiasInput(desired);
		for (int k = 0; k < biasedDes.length; k++) {
			for (int j = 0; j < hids; j++) {
				out[j] += weightsHO[j][k] * biasedDes[k];
			}
		}			
		for (int i = 0; i < out.length; i++) {
			out[i] = NetUtil.actFun(out[i]);
		}
		return out;
	}
	
	public double[] actHI(double[] actHid) {	
		double[] out = new double[inps];
		double[] biasedIn = NetUtil.addBiasInput(actHid);
		for (int i = 0; i < hids+1; i++) {
			for (int j = 0; j < inps; j++) {
				out[j] += weightsIH[j][i] * biasedIn[i];
			}
		}				
		for (int i = 0; i < out.length; i++) {
			out[i] = NetUtil.actFun(out[i]);
		}
		return out;
	}
	
	protected void adjustWeights(double[] inputs, double[] actHminus, double[] actOminus, double[] desired, double[] actHplus) {
		//weights IN - HID
		for (int i = 0; i < inps+1; i++) {
			for (int j = 0; j < hids; j++) {
				//biases
				if (i == inps) 
					weightsIH[i][j] += lr * (actHplus[j] - actHminus[j]);
				else 	
					weightsIH[i][j] += lr * inputs[i] * (actHplus[j] - actHminus[j]);
			}
		}				
		//weights HID - OUT
		for (int j = 0; j < hids+1; j++) {
			for (int k = 0; k < outs+1; k++) {
				//biases		
				if (j == hids || k == outs) {  
					if (j == hids && k < outs)
						weightsHO[j][k] += lr * (desired[k] - actOminus[k]);
//					if (k == outs && j < hids) 
//						weightsOI[j][k] += lr * (actPlus[j] - activationForward[j]);
				}
				else 	
					weightsHO[j][k] += lr * actHminus[j] * (desired[k] - actOminus[k]);
			}
		}
	}
	
	public double[] epoch(double[][] inputs, double[][] desired, boolean learn, boolean log) {
		double[] out = new double[3];
		int[] indexer = Util.permutedIndexes(inputs.length);
		for (int n = 0; n < indexer.length; n++) {
			int s = indexer[n];
			//forward pass
			double[] actHminus = actIH(inputs[s]);
			double[] actOminus = actHO(actHminus);
			//backward pass
			double[] actHplus = actOH(inputs[s],desired[s]);
			double[] tmperrors = NetUtil.errorsUnidir(actOminus, desired[s]);
			for (int i = 0; i < tmperrors.length; i++) {
				out[i] += tmperrors[i];
			}
			if (log)
				System.out.println("I-:"+Util.arrayToStringRounded(inputs[s],1)+"\tH-:"+Util.arrayToStringRounded(actHminus,3)+"\tO-:"+Util.arrayToStringRounded(actOminus,3));
			if (learn)
				adjustWeights(inputs[s], actHminus, actOminus, desired[s], actHplus);
		}
		for (int i = 0; i < out.length; i++) {
			out[i] /= indexer.length;
		}
		return out;
	}
	
	public double[] epoch(double[][] inputs, double[][] desired) {
		return epoch(inputs,desired,true,false);
	}
	
	public double[] testEpoch(double[][] inputs, double[][] desired) {
		return epoch(inputs,desired,false,false);
	}
	
	public void printWeights() {
		System.out.println(NetUtil.weightsToStr(weightsIH));
		System.out.println(NetUtil.weightsToStr(weightsHO));
	}
	
	public void saveWeights(String file) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(new File(file)));
		bw.write("Weights Input>Hidden");
		bw.newLine();
		for (int i = 0; i < weightsIH.length; i++) {
			for (int j = 0; j < weightsIH[i].length; j++) {
				bw.write(Double.toString(weightsIH[i][j])+",");
			}
			bw.newLine();
		}
		bw.write("Weights Hidden>Output");
		bw.newLine();
		for (int i = 0; i < weightsHO.length; i++) {
			for (int j = 0; j < weightsHO[i].length; j++) {
				bw.write(Double.toString(weightsHO[i][j])+",");
			}
			bw.newLine();
		}
		bw.flush();
		bw.close();
	}
	
}