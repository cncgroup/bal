package base;

import util.Util;

import java.util.Random;

public class SOM {
	private double eta;	
	private double sigma;
	private double sigma0;
	private double lambda;
	private double gama;
	private double gama0;
	
	private int dimX;
	private int dimY;
	private SOMNeuron[] map;

	public SOM(int input_dim, int map_dim_x, int map_dim_y, double newgama) {
		map = new SOMNeuron[map_dim_x*map_dim_y];
		this.dimX = map_dim_x;
		this.dimY = map_dim_y;
		for(int i = 0; i < map_dim_y; i++) {
			for(int j = 0; j < map_dim_x; j++) {
				map[i*map_dim_x+j] = new SOMNeuron(input_dim, j, i);
			}
		}
		sigma0 = Math.sqrt((double)map_dim_x);
		gama0 = newgama;
	}

	public double[] train(int epochs, double[][] data, int logAfterN) {
		//init
		lambda = epochs/Math.log(Util.max(dimX, dimY)/2.0);
		eta = epochs;
		//train
		double[] qerr = new double[epochs];
		for (int i = 0; i < epochs; i++) {
			int[] indexer = Util.permutedIndexes(data.length);
			qerr[i] = 0.0;
			param_decay(i);
			for (int j = 0; j < data.length; j++) {
				qerr[i] += train_one_sample(data[indexer[j]]);
			}
			if (logAfterN != 0 && (i+1)%logAfterN == 0)
				System.out.println("e"+(i+1)+": "+qerr[i]+"\t"+gama+"\t"+sigma);
		}
		return qerr;
	}

	public double[] activation(double[] sample) {
		double[] out = new double[dimX*dimY];
		for (int i = 0; i < dimX*dimY; i++) {
			out[i] = Math.exp(-map[i].distance(sample));
		} 
		return out;
	}

	public double train_one_sample(double[] sample) {
		SOMNeuron w = find_winner(sample);
		double qerr = w.distance(sample);
		for(int i = 0; i < dimX*dimY; i++) {
			map[i].updateWeights(gama, w, sample, sigma);
		}
		return qerr;
	}

	public SOMNeuron find_winner(double[] sample) {
		int	mini = 0;
		double mind = -1;
		for (int i = 1; i < dimX*dimY; i++) {
			double i_d = map[i].distance(sample);
			if (mind > i_d || mind == -1) {
				mini = i;
				mind = i_d;
			}
		}
		return map[mini];
	}

	public void param_decay(int t) {
		sigma = sigma0 * Math.exp(-t/lambda);
		gama = gama0 * Math.exp(-t/(eta*1.0));
	}
	
	public double[][] outputData(double[][] data, int mode) {
		double[][] out = new double[dimY*dimX][data[0].length];
		for (int i = 0; i < out.length; i++) {
			out[i] = activation(data[i]);
		}
		return out;
	}

	public static class SOMNeuron {
		private final int GAUSSIAN = 1;
		private final int MEXICAN = 2;
		private	double[] weight;
		private int x;
		private int y;
		private int in_dim;
		private int id;

		public SOMNeuron(int in_dim, int x, int y) {
			weight = new double[in_dim];
			this.x = x;
			this.y = y;
			this.in_dim = in_dim;
			for(int i = 0; i < in_dim; i++) {
				weight[i] = Util.randomGauss(0.0, 0.1, new Random(System.currentTimeMillis()));
			}
		}

		public double distanceFrom(SOMNeuron n2) {
			double sum = 0;
			sum += Util.pow((double)(n2.x - x), 2);
			sum += Util.pow((double)(n2.y - y), 2);
			return sum;
		}

		public double distance(double[] input) {
			return Util.distanceEuclid(weight, input);
		}

		public void updateWeights(double gama, SOMNeuron winner, double[] xt, double sigma) {
			double gd = Util.distanceGauss(sigma, this.distanceFrom(winner));
			for (int i = 0; i < in_dim; i++) {
				weight[i] += gama * gd * (xt[i] - weight[i]);
			}
		}

		double[] getWeight() {
			return weight;
		}

		int getX() {
			return x;
		}

		int getY() {
			return y;
		}

	}

}
