package test.data;

import base.Conf;
import data.DataBinary;
import util.visu.DataPairsRandom;

import java.io.IOException;

public class DisplaySomeData {

    public static void main(String[] args) throws IOException {
        DataBinary data = new DataBinary("input/binary-random/inp144x100", "input/binary-random/des144x100");
        DataPairsRandom vis = new DataPairsRandom(data);
        vis.display();
        vis.saveImage(Conf.pathOutput+"randomdata.png");
    }

}
