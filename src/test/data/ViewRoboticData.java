package test.data;

import java.io.IOException;

import util.visu.DataPairsVis;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class ViewRoboticData {

	public static final String pathVisual = Conf.dataSetsVisPaths[0];
	public static final String pathMotor = Conf.dataSetsMotPaths[1];
	public static final int k = 8;
	public static final int[] visParams1 = {5, 10, 10, 10, 8}; // pxs, margX, margY, matrixS, rowHeight
	public static final int[] visParams2 = {5, 10, 10, 10, 10}; // pxs, margX, margY, matrixS, rowHeight
	public static final int[] visParams3 = {4, 5, 10, 10, 16}; // pxs, margX, margY, matrixS, rowHeight
	
	public static void main(String[] args) throws IOException {
		
//		DataSetBinary data = new DataSetBinary(
//				Conf.pathDataKWTA+"k"+k+"-in"+Conf.patternSize+"x"+(k*Conf.kToSizeRatio), 
//				Conf.pathDataKWTA+"k"+k+"-des"+Conf.patternSize+"x"+(k*Conf.kToSizeRatio));
//		
//		DataPairsVis vis = new DataPairsVis(visParams1,data.getInputs(),data.getDesired());
//		vis.display();
//		vis.saveImage(Conf.pathOutput+"data-random-kWTA-k8.png");
		
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(k);
		dataMot.kWinnerTakeAll(k);
//		DataPairsVis visR = new DataPairsVis(visParams2,dataVis.valuesFirstPersp(),dataMot.values());
//		visR.display();
//		visR.saveImage(Conf.pathOutput+"data-msom-ends-firstPersp-k8.png");
		dataMot.multiply();
		DataPairsVis visR2 = new DataPairsVis(visParams3,dataMot.values(),dataVis.values());
		visR2.display();
		visR2.saveImage(Conf.pathOutput+"data-msom-ends-k8.png");

	}
	
}
