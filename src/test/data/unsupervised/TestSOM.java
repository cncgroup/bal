package test.data.unsupervised;

import base.SOM;
import data.DataRoboticMot;
import data.DataRoboticVis;
import util.Util;
import util.visu.Plot;

public class TestSOM {

	// DATA params
	public static final int sV = 16;
	public static final int sM = 8;
	public static final String pathVisual = "input/msom/visual_"+sV+"_"+sV+".txt";
	public static final String pathMotor = "input/msom/motor_"+sM+"_"+sM+".txt";
	public static final int kVis = 16;
	public static final int kMot = 8;
	// SOM params
	public static final int dimX = 8;
	public static final int dimY = 8;
	public static final int maxEpcs = 300;
	public static final double gama = 0.2;
	public static void main(String[] args) {
		
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
//		dataMot.multiply();
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
//		double[][] dataIn = dataVis.values();
		double[][] dataIn = Util.merge2(dataVis.valuesFirstPersp(),dataMot.values());
		
		SOM net = new SOM(dataIn[0].length, dimX, dimY, gama);
		double[] errorProgress = net.train(maxEpcs, dataIn, 10);
		Plot plot = new Plot(new String[] {"quantization error"});
		for (int i = 0; i < errorProgress.length; i++) {
			plot.addPoint(0, i, errorProgress[i]);
		}
		plot.display();
	}
}
