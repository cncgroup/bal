package test.data.supervised;

import base.MLP;
import base.Conf;
import base.NetUtil;

import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

import java.io.IOException;

public class ClassifierEpcs {
	private static final int M = Conf.movements;
//	private static final int P = Conf.perspectives;
	public static final int sV = 16;
	public static final int sM = 8;
	public static final String pathVisual = "input/msom/visual_"+sV+"_"+sV+".txt";
	public static final String pathMotor = "input/msom/motor_"+sM+"_"+sM+".txt";
	public static final int kVis = 16;
	public static final int kMot = 8;
	public static final double[][] motorCodes = {{1,0,0},{0,1,0},{0,0,1}};
	public static final String[] errorLabels = Conf.errorLabelsBP;
	public static final int[] maxEpcs = {5,10,15,20,25,40,50,100};
	public static final int hids = 200;
	public static final double lRate = 0.1;
	public static final double mmnt = Conf.bp_momentum;
	public static final double wDec = Conf.bp_wdecay;
	public static final int[] seqSizes = Conf.seqSizes;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataMot.multiply();
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		double[][] dataIn = Util.merge2(dataVis.values(),dataMot.values());
		double[][] dataDes = new double[dataIn.length][3];
		int ind = 0;
		for (int m = 0; m < M; m++) {
			for (int i = 0; i < 4*seqSizes[m]; i++) {
				dataDes[ind] = motorCodes[m];
				ind++;
			}
		}

		int[] arch = {(sV*sV+sM*sM), hids, M};
		for (int e = 0; e < maxEpcs.length; e++) {
			MLP net = new MLP(arch, lRate, mmnt, wDec);
			int epoch = 0;
			double[] errors = new double[3];
			while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs[e]) {
				double[][][] act = net.epoch(dataIn, dataDes, true);
				errors = NetUtil.errorsF1BP(act, dataDes);
				epoch++;
				if (epoch % 10 == 0)
					System.out.print(".");
			}
			System.out.println();
			System.out.println(epoch);
			for (int i = 0; i < errors.length; i++) {
				System.out.println(errorLabels[i]+": "+errors[i]);
			}
			System.out.println();
		}
	}
}
