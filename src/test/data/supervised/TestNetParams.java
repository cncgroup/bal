package test.data.supervised;

import java.io.IOException;

import base.NetUtil;
import test.helpers.MLPTest;
import util.TexLog;
import base.Conf;
import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class TestNetParams {
	
	private static final int M = Conf.movements;
	public static final int sV = 16;
	public static final int sM = 8;
	public static final String pathV = "input/msom/visual_"+sV+"_"+sV+".txt";
	public static final String pathM = "input/msom/motor_"+sM+"_"+sM+".txt";
	public static final int kV = 16;
	public static final int kM = 8;
	public static final double[][] motorCodes = {{1,0,0},{0,1,0},{0,0,1}};
	public static final String[] errorLabels = Conf.errorLabelsBP;
	public static final String logName = "log-classifier-mlp-params.tex";
	public static final int[] seqSizes = Conf.seqSizes;
	
	public static final int netCount = 50;
	public static final int maxEpcs = 100;
	public static final int hids = 250;
	public static final double lRate = 0.15;
	
	public static final double[] learningRates = {0.02, 0.05, 0.1, 0.15, 0.2, 0.3, 0.5};
	public static final int[] hiddenSizes = {150, 200, 250, 300, 350, 400};
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis(pathV);
		DataRoboticMot dataMot = new DataRoboticMot(pathM);
		dataMot.multiply();
		dataVis.kWinnerTakeAll(kV);
		dataMot.kWinnerTakeAll(kM);
		double[][] dataIn = Util.merge2(dataVis.values(),dataMot.values());
		double[][] dataDes = new double[dataIn.length][3];
		int ind = 0;
		for (int m = 0; m < M; m++) {
			for (int i = 0; i < 4*seqSizes[m]; i++) {
				dataDes[ind] = motorCodes[m];
				ind++;
			}
		}
		int[][] architectures = new int[hiddenSizes.length][3];
		String[] archsString = new String[hiddenSizes.length];
		for (int i = 0; i < hiddenSizes.length; i++) {
			architectures[i][0] = dataIn[0].length;
			architectures[i][1] = hiddenSizes[i];
			architectures[i][2] = dataDes[0].length;
			archsString[i] = NetUtil.archToStr(architectures[i]);
		}
		MLPTest test = new MLPTest(netCount, maxEpcs);
		TexLog log = new TexLog();
		log.addLog(test.logData(test.testArchitectures(architectures,dataIn,dataDes,lRate),"hidden size",archsString));
		int[] archDefault = {dataIn[0].length, hids, dataDes[0].length};
		log.addLog(test.logData(test.testLRate(archDefault, learningRates, dataIn, dataDes),"learning rate", NetUtil.paramsToStr(learningRates)));
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
