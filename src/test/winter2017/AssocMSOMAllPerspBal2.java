package test.winter2017;

import base.BAL2;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;
import util.visu.DataPairsRes;
import util.visu.Plot;

import java.io.IOException;

public class AssocMSOMAllPerspBal2 {
	
	public static final String simName = "balold-msom-olddata-with-magic";
	public static final String[] errNames = Conf.errorLabelsF1;
	public static final int[] visParams = {5, 10, 30, 10, 6};
	public static final int rowHeight = 10;
	public static final int sizeVis = 16;
	public static final int sizeMot = 12;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final double lRate = 0.2;
	public static final int hids = 200;
	public static final int maxEpcs = 20000;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		double[][] dataV = dataVis.dataPatternsOnly(); //all perspectives
		double[][] dataM = new double[dataV.length][dataV[0].length];
		for (int i = 0; i < dataM.length; i++) {
			int[] coord =  dataVis.findIndex(dataV[i]);
			dataM[i] = dataMot.dataSeq()[coord[0]].get(coord[2]);
		}

		BAL2 net = new BAL2(dataM[0].length,hids,dataV[0].length,0,0.1);
		System.out.println("architecture: " + dataM[0].length + "-" + hids + "-" + dataV[0].length+ " LR: "+lRate);
		int epoch = 0;
		double[] testErr = new double[errNames.length];
		Plot plot = new Plot(Conf.errorLabels);

		while ((testErr[2] < 1.0 || epoch < 1) && epoch < maxEpcs) {
			net.epochF1(dataM,dataV,true);
			testErr = net.epochF1(dataM, dataV,false);
			for (int i = 0; i < testErr.length; i++) {
				plot.addPoint(i, epoch, testErr[i]);
			}
			if (epoch % 500 == 0)
				System.out.print(".");
			epoch++;
		}
		System.out.println();
		System.out.println("epcs: "+epoch);
		System.out.println();
		for (int i = 0; i < testErr.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErr[i]);
		}
		System.out.println();
		double[] testErrors = net.epochF1(dataM, dataV,false);
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
		plot.display();
		double[][][] patterns = net.testEpochPatterns(dataM,dataV);
		DataPairsRes visRes = new DataPairsRes(patterns);
		visRes.setRowH(rowHeight);
		visRes.display();
//		visRes.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-patterns"+Conf.figExt);
	}
}
