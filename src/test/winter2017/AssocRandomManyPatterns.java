package test.winter2017;

import base.BAL1;
import base.Conf;
import base.NetUtil;
import data.DataBinary;
import util.visu.DataPairsRes;
import util.visu.Plot;

import java.io.IOException;

public class AssocRandomManyPatterns {

//	String simName = "balold-random-10x10-300patterns";
	static String[] errorLabels = Conf.errorLabelsF1;
	static int patternsPerRow = 16;

	static int k = 10;
	static int sampleSize  = 100;
	static int sampleCount = 100;
	static String fileIn  = Conf.pathDataKWTA+"k"+k+"-in"+sampleSize+"x"+sampleCount;
	static String fileDes = Conf.pathDataKWTA+"k"+k+"-des"+sampleSize+"x"+sampleCount;

	static double lr = 0.00002;
	static int hids = 100;
	static int maxEpcs = 20000;

	public static void main(String[] args) throws IOException {	
		DataBinary data = new DataBinary(fileIn,fileDes);
		BAL1 net = new BAL1(sampleSize,hids,sampleSize,lr,Conf.wmax,Conf.wmin);
		Plot plot = new Plot(errorLabels);
		double[] err = new double[errorLabels.length];
		int epoch = 0;
		while ((NetUtil.errorsUnsatisfied(err) || epoch < 1) && epoch < maxEpcs) {
			err = net.epochF1(data.dataX(),data.dataY(),true);
			//err = net.epochF1(data.dataX(),data.dataY(),false);
			for (int i = 0; i < err.length; i++) {
				plot.addPoint(i, epoch, err[i]);
			}
			epoch++;
			if (epoch % 500 == 0)
				System.out.print(".");
		}
		System.out.println();
		System.out.println(epoch);
		for (int i = 0; i < err.length; i++) {
			System.out.println(errorLabels[i]+": "+err[i]);
		}
		System.out.println();
		double[] testErrors = net.epochF1(data.dataX(),data.dataY(),false);
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(errorLabels[i]+": "+testErrors[i]);
		}
		plot.display();
		double[][][] patterns = net.testEpochPatterns(data.dataX(),data.dataY());
		DataPairsRes visRes = new DataPairsRes(patterns);
		visRes.setRowH(patternsPerRow);
		visRes.display();
	}
}
