package test.winter2017;

import base.BAL2;
import base.Conf;
import base.NetUtil;
import data.DataRoboticMotNew;
import data.DataRoboticVisNew;
import util.visu.DataPairsRes;
import util.visu.Plot;

import java.io.IOException;

public class AssocMSOMBal2 {
	
	public static final String simName = "balold-msom-olddata-with-magic";
	public static final String[] errNames = Conf.errorLabelsF1;
	public static final int rowHeight = 10;
	public static final int sizeVis = 16;
	public static final int sizeMot = 12;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final int hids = 200;
	public static final int maxEpcs = 1500;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVisNew dataVis = new DataRoboticVisNew("input/msomnew/1513111457_stsp.act",",");
		DataRoboticMotNew dataMot = new DataRoboticMotNew("input/msomnew/1513111457_pmc.act", ",");
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		double[][] valuesMot = dataMot.values();
 		double[][] valuesVis = dataVis.valuesFirstPersp();
//		int[][] indices = dataVis.uniqPatIndices(dataMot.uniqPatIndices());
//		indices[1] = Util.duplicateAtIndex(indices[1], 0);
//		double[][] valuesMot = dataMot.valuesIndices(indices);
//		double[][] valuesVis = dataVis.valuesPerspIndices(0,indices);

		BAL2 net = new BAL2(valuesMot[0].length,hids,valuesVis[0].length,0,0.1);
		System.out.println("architecture: " + valuesMot[0].length + "-" + hids + "-" + valuesVis[0].length);
		int epoch = 0;
		double[] testErr = new double[errNames.length];
		Plot plot = new Plot(errNames);

		//while (((Util.round(testErr[2], 12) < 1.0 || Util.round(testErr[3], 12) < 1.0) || epoch < 1) && epoch < epcsPhase1) {
		while ((NetUtil.errorsUnsatisfied(testErr) || epoch < 1) && epoch < maxEpcs) {
			net.epochF1(valuesMot,valuesVis,true);
			testErr = net.epochF1(valuesMot, valuesVis,false);
			for (int i = 0; i < testErr.length; i++) {
				plot.addPoint(i, epoch, testErr[i]);
			}
			if (epoch % 100 == 0)
				System.out.print(".");
			epoch++;
		}
		System.out.println();
		System.out.println("epcs: "+epoch);
		System.out.println();
		for (int i = 0; i < testErr.length; i++) {
			System.out.println(errNames[i]+": "+testErr[i]);
		}
		System.out.println();
		double[] testErrors = net.epochF1(valuesMot, valuesVis,false);
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(errNames[i]+": "+testErrors[i]);
		}
		plot.display();
		double[][][] patterns = net.testEpochPatterns(valuesMot,valuesVis);
		DataPairsRes visRes = new DataPairsRes(patterns);
		visRes.setRowH(rowHeight);
		visRes.display();
//		visRes.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-patterns"+Conf.figExt);
	}
}
