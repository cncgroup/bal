package test.winter2017;

import base.BAL1;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;
import util.Util;
import util.visu.DataPairsRes;

import java.io.IOException;

public class PerfSingleAll {
	
	public static final String simName = "balold-robotic-single-allp";
	public static final String[] errNames = Conf.errorLabels;	
	public static final int rowHeight = 15;
	public static final int sizeVis = 16;
	public static final int sizeMot = 12;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final double lRate = 0.2;
	public static final int hids = 240;
	public static final int maxEpcs = 5000;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		int[][] indices = dataVis.uniqPatIndices(dataMot.uniqPatIndices());
 		double[][] valuesMot = dataMot.valuesIndices(indices);
 		double[][] valuesVis = dataVis.valuesPerspIndices(0,indices);
 		double[][] valuesMot2 = Util.merge(valuesMot, Util.merge(valuesMot, Util.merge(valuesMot, valuesMot)));
 		double[][] valuesVis2 = Util.merge(valuesVis,Util.merge(dataVis.valuesPerspIndices(1,indices),Util.merge(
 				dataVis.valuesPerspIndices(2,indices),dataVis.valuesPerspIndices(3,indices))));
 		
		BAL1 net = new BAL1(valuesMot[0].length,hids,valuesVis[0].length,lRate);
		int epoch = 0;
		double[] testErr = new double[errNames.length];
		while (((Util.round(testErr[2], 12) < 1.0 || Util.round(testErr[3], 12) < 1.0) || epoch < 1) && epoch < maxEpcs) {
			net.epoch(valuesMot2,valuesVis2);
			testErr = net.testEpoch(valuesMot,valuesVis);
			System.out.print(".");
			if (epoch%150 == 0 && epoch > 1)
				System.out.println();
			epoch++;
		}
		System.out.println();
		System.out.println("epcs: "+epoch);		
		for (int i = 0; i < testErr.length; i++) {
			System.out.println(errNames[i]+": "+testErr[i]);
		}
		System.out.println();	
		double[][][] patterns = net.testEpochPatterns(valuesMot2,valuesVis2);
		DataPairsRes visRes = new DataPairsRes(patterns);
		visRes.setRowH(rowHeight);
		visRes.display();
		visRes.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-patterns"+Conf.figExt);
	}
}
