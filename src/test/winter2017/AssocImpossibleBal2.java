package test.winter2017;

import base.BAL2;
import base.Conf;
import data.DataBinary;
import util.visu.DataPairsVisResults;
import util.visu.Plot;

import java.io.IOException;

public class AssocImpossibleBal2 {

	public static final String[] errorLabels = Conf.errorLabels;
	public static final int k = 10;
	public static final int sampleSize  = 100;
	public static final int sampleCount = 96;//144;
	public static final int maxEpcs = 20000;
	public static final String fileIn  = Conf.pathDataRandomHard+"data-impossible-k"+k+"-in"+sampleSize+"x"+sampleCount;
	public static final String fileDes = Conf.pathDataRandomHard+"data-impossible-k"+k+"-des"+sampleSize+"x"+sampleCount;
	public static final int hids = 200;
	public static final int[] visParams = {5, 0, 40, 10, 8}; // pxs, margX, margY, matrixS, rowHeight
	
	public static void main(String[] args) throws IOException {	
		DataBinary data = new DataBinary(fileIn,fileDes);
		BAL2 net = new BAL2(sampleSize,hids,sampleSize,0.0002,500,Conf.wmax,Conf.wmin);
		Plot plot = new Plot(errorLabels);
		double[] errors = new double[6];
		int epoch = 0;
		//while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < epcsPhase1) {
		while ((errors[2] < 1.0 || epoch < 1) && epoch < maxEpcs) {
			errors = net.epoch(data.dataX(),data.dataY());
			for (int i = 0; i < errors.length; i++) {
				plot.addPoint(i, epoch, errors[i]);
			}
			epoch++;
			if (epoch % 100 == 0) 
				System.out.print(".");
		}
		System.out.println(epoch);
		System.out.println();
		for (int i = 0; i < errors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+errors[i]);
		}
		System.out.println();
		double[] testErrors = net.testEpoch(data.dataX(),data.dataY());
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
		plot.display();
		double[][][] patterns = net.testEpochPatterns(data.dataX(),data.dataY());
		DataPairsVisResults visu = new DataPairsVisResults(visParams, patterns);
		visu.display();
	}
}
