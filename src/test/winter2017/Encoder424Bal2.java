package test.winter2017;

import base.BAL2;
import base.Conf;
import base.NetUtil;
import util.visu.Plot;

import java.io.IOException;

public class Encoder424Bal2 {
	public static final int maxEpcs = 5000;
	public static final int inps = 4;
	public static final int hids = 2;
	public static final int outs = 4;
	public static final double lambdaH = 0;// .0002;
	public static final double lambdaV = 0.2;//500;
	public static final double wmax = -1.0;
	public static final double wmin = 1.0;
	public static final double[][] inputs = Conf.inputs424;
	
	public static void main(String[] args) throws IOException {
		BAL2 net = new BAL2(inps,hids,outs,lambdaH,lambdaV,wmin,wmax);
		Plot plot = new Plot(Conf.errorLabels);
		double[] trainErrors = new double[6];
		int epoch = 0;
		while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < maxEpcs) {
			if (epoch == 0 || (epoch+1) % 100 == 0) {
				trainErrors = net.epoch(inputs,inputs,true,true);
				for (int i = 0; i < trainErrors.length; i++) {
					plot.addPoint(i, epoch, trainErrors[i]);
				}				
			} else {
				trainErrors = net.epoch(inputs,inputs,true,false);
			}
			epoch++;
		}
		System.out.println();
		System.out.println(epoch);
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+trainErrors[i]);
		}
		plot.display();
		double[] testErrors = net.testEpoch(inputs,inputs);
		System.out.println();
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
	}
}
