package test.backprop;

import java.io.IOException;

import util.visu.DataPairsRes;
import util.visu.Plot;
import base.MLP;
import base.Conf;
import base.NetUtil;
import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class AssocFPAlt {
	public static final int sVis = 14;
	public static final int sMot = 12;
	public static final int kVis = 12;//16;
	public static final int kMot = 6;//8;
	public static final String pathVisual = "input/msom/visual_"+sVis+"_"+sVis+".txt";
	public static final String pathMotor = "input/msom/motor_"+sMot+"_"+sMot+".txt";
	public static final int[] visParams = {3, 5, 5, 11};
	public static final String[] errNames = Conf.errorLabelsF1bidir;
	public static final int hids = 200;
	public static final double lRate = 0.2;
	public static final int maxEpcs = 10000;
	public static final int plotRes = 50;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		double[][] valuesVis = dataVis.valuesFirstPersp();
		double[][] valuesMot = dataMot.values();
		Util.scale(valuesVis);
		Util.flip(valuesVis);
		Util.scale(valuesMot);
		Util.flip(valuesMot);
		
		Plot plot = new Plot(Util.merge(errNames,errNames)); 
		MLP netF = new MLP(new int[] {dataVis.patternSize(),hids,dataMot.patternSize()},lRate);
		MLP netB = new MLP(new int[] {dataMot.patternSize(),hids,dataVis.patternSize()},lRate);
		double[] trainErrors = new double[errNames.length];
		double[][][] actNetF;
		double[][][] actNetB;
		int epoch = 0;
		System.out.println("2xMLP "+dataVis.patternSize()+"-"+hids+"-"+dataMot.patternSize());
		while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < maxEpcs) {
			actNetF = netF.epoch(valuesVis, valuesMot,true);
			actNetB = netB.epoch(valuesMot, valuesVis,true);
			trainErrors = NetUtil.errorsF1bidirBP(actNetF, actNetB, valuesMot, valuesVis);
			if ((epoch+1) % plotRes == 0) {
				for (int i = 0; i < trainErrors.length; i++) {
					plot.addPoint(i, epoch, trainErrors[i]);
				}
				System.out.print(".");
			}
			epoch++;
		}
		System.out.println();
		System.out.println("epcs: "+epoch);
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(errNames[i]+":\t"+Util.round(trainErrors[i],3));
		}
		plot.display();
		double[][][] patterns = Util.merge(netF.testEpochPatternsBinary(valuesVis,valuesMot),netB.testEpochPatternsBinary(valuesMot,valuesVis));
		DataPairsRes visRes = new DataPairsRes(patterns);
		visRes.display();
	}
}
