package test.backprop;

import java.io.IOException;

import base.NetUtil;
import util.visu.DataPairsRes;
import util.visu.Plot;
import base.MLP;
import base.Conf;
import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class AssocAllPersp {
	
	public static final String simName = "assoc-mlp-robotic-allpersp";
	public static final String[] errNames = Conf.errorLabelsF1bidir;
	public static final int[] visParams = {4, 12, 10, 30};
	public static final int sizeVis = 14;
	public static final int sizeMot = 8;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final int hids = 160;
	public static final double lRate = 0.1;
	public static final int maxEpcs = 500;
	public static final int plotRes = 100;

	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
 		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
//		dataMot.multiply();
//		double[][] valuesVis = dataVis.values();
//		double[][] valuesMot = dataMot.values();
		int[][] indices = dataVis.uniqPatIndices(dataMot.uniqPatIndices());
		indices[1] = Util.duplicateAtIndex(indices[1], 0);
		double[][] valuesVis = dataVis.valuesAllPerspIndices(indices);
		double[][] valuesMot = new double[valuesVis.length][valuesVis[0].length];
		for (int i = 0; i < valuesMot.length; i++) {
			int[] coord =  dataVis.findIndex(valuesVis[i]);
			valuesMot[i] = dataMot.dataSeq()[coord[0]].get(coord[2]);
		}

		Plot plot = new Plot();
		int[] layers = new int[] {dataVis.patternSize(),hids,dataMot.patternSize()};
		MLP netF = new MLP(layers,lRate);
		MLP netB = new MLP(layers,lRate);
		double[] trainErrors = new double[errNames.length];
		double[][][] actNetF;
		double[][][] actNetB;
		int epoch = 0;
		System.out.println("2xMLP "+dataVis.patternSize()+"-"+hids+"-"+dataMot.patternSize());
		while (epoch < maxEpcs) {
			actNetF = netF.epoch(valuesVis, valuesMot,true);
			actNetB = netB.epoch(valuesMot, valuesVis,true);
			trainErrors = NetUtil.errorsF1bidirBP(actNetF, actNetB, valuesMot, valuesVis);
			if ((epoch+1) % plotRes == 0) {
				for (int i = 0; i < trainErrors.length; i++) {
					plot.addPoint(i, epoch, trainErrors[i]);
				}
				System.out.print(".");
			}
			epoch++;
		}
		System.out.println();
		System.out.println("epcs: "+epoch);
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(errNames[i]+":\t"+Util.round(trainErrors[i],3));
		}
//		plot.display();
//		plot.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-results");
		double[][][] patterns = Util.merge(netB.testEpochPatternsBinary(valuesMot,valuesVis),netF.testEpochPatternsBinary(valuesVis,valuesMot));
		DataPairsRes visRes = new DataPairsRes(patterns,visParams);
		visRes.display();
//		visRes.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-patterns");
		
		// write log
//		String[] setup = {
//			"architecture: "+dataVis.patternSize()+"-"+hids+"-"+dataMot.patternSize(),
//			"learning rate: "+ lRate,
//			"momentum: "+ Conf.bp_momentum,
//			"weight decay: "+ Conf.bp_wdecay,
//			"min pattern succ: "+ Conf.minPatSucc,
//			"max epochs: "+ epcsPhase1,
//			"data motor: "+mapSizeM+"x"+mapSizeM+" k="+kMot,
//			"data visual: "+mapSizeV+"x"+mapSizeV+" k="+kVis,
//		};
//		TexLog log = new TexLog();
//		log.addLine("\\subsection{MLP Associator with robotic data - all perspectives}");
//		log.addLine("{\\bf Setup parameters:}\\\\");
//		log.addItemize(setup);
//
//		String[][] resultsTable = new String[errNames.length][3];
//		for (int i = 0; i < trainErrF.length; i++) {
//			resultsTable[i][0] = errNames[i];
//			resultsTable[i][1] = ""+Util.round(trainErrF[i],3);
//			resultsTable[i][2] = ""+Util.round(trainErrB[i],3);
//		}
//		log.addResultsTable(
//				new String[] {"error","front","back"},
//				resultsTable,
//				"MLP associator - all perspectives - performance",
//				""
//		);
//		log.addFigure(
//				Conf.pathFigures+simName+"-results"+Conf.figExt,
//				"MLP associator - all perspectives - performance",
//				"",
//				0.9);
//		log.addFigure(
//				Conf.pathFigures+simName+"-data"+"-patterns"+Conf.figExt,
//				"MLP associator - all perspectives - pattern match",
//				"",
//				0.5);
//		log.echo();
//		log.exportPartial(Conf.pathOutput+"log-"+simName+".tex");
	}
}
