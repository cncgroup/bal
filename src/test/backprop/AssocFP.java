package test.backprop;

import java.io.IOException;

import util.visu.DataPairsRes;
import util.visu.Plot;
import base.MLP;
import base.Conf;
import base.NetUtil;
import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class AssocFP {
	
	public static final String simName = "assoc-mlp-robotic-firstpersp";
	public static final String[] errNames = Conf.errorLabelsF1bidir;
	public static final int sizeVis = 16;
	public static final int sizeMot = 12;
	public static final String pathVisual = "input/msom/visual_"+sizeVis+"_"+sizeVis+".txt";
	public static final String pathMotor = "input/msom/motor_"+sizeMot+"_"+sizeMot+".txt";
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final int hids = 200;
	public static final double lRate = 0.1;
	public static final int maxEpcs = 150;
	public static final int plotRes = 1;

	public static final int[] visParams = {4, 10, 30, 30};

	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		double[][] valuesVis = dataVis.valuesFirstPersp();
		double[][] valuesMot = dataMot.values();

		int[] archF = {dataMot.patternSize(),hids,dataVis.patternSize()};
		int[] archB = {dataVis.patternSize(),hids,dataMot.patternSize()};
		Plot plot = new Plot(Util.merge(errNames,errNames)); 
		MLP netF = new MLP(archF, lRate);
		MLP netB = new MLP(archB, lRate);
		int epoch = 0;
		System.out.println("2xMLP "+NetUtil.archToStr(archF)+" "+NetUtil.archToStr(archB));

		double[] trainErrors = new double[errNames.length];
		while (((trainErrors[2] <= 1.0 && trainErrors[3] <= 1.0) || epoch < 1) && epoch < maxEpcs) {
			double[][][] actF = netF.epoch(valuesMot, valuesVis,true);
			double[][][] actB = netB.epoch(valuesVis, valuesMot, true);
			trainErrors = NetUtil.errorsF1bidirBP(actF, actB, valuesMot, valuesVis);
			if ((epoch+1) % plotRes == 0) {
				for (int i = 0; i < trainErrors.length; i++) {
					plot.addPoint(i, epoch, trainErrors[i]);
				}
				System.out.print(".");
			}
			epoch++;
		}
		System.out.println();

		System.out.println("epcs: "+epoch);
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(errNames[i]+":\t\t"+Util.round(trainErrors[i],3));
		}
		plot.display();
//		plot.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-results");
		double[][][] patterns = Util.merge(netF.testEpochPatternsBinary(valuesMot,valuesVis),netB.testEpochPatternsBinary(valuesVis,valuesMot));
		DataPairsRes visRes = new DataPairsRes(patterns,visParams);
		visRes.display();
//		visRes.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-patterns");
		
		// write log
//		String[] setup = {
//			"architecture: "+dataVis.patternSize()+"-"+hids+"-"+dataMot.patternSize(),
//			"learning rate: "+ lRate,
//			"momentum: "+ Conf.bp_momentum,
//			"weight decay: "+ Conf.bp_wdecay,
//			"min pattern succ: "+ Conf.minPatSucc,
//			"max epochs: "+ epcsPhase1,
//			"data motor: "+mapSizeM+"x"+mapSizeM+" k="+kMot,
//			"data visual: "+mapSizeV+"x"+mapSizeV+" k="+kVis,
//		};
//		TexLog log = new TexLog();
//		log.addLine("\\subsection{MLP Associator with robotic data - first perspective}");
//		log.addLine("{\\bf Setup parameters:}\\\\");
//		log.addItemize(setup);
//
//		String[][] resultsTable = new String[errNames.length][3];
//		for (int i = 0; i < trainErrF.length; i++) {
//			resultsTable[i][0] = errNames[i];
//			resultsTable[i][1] = ""+Util.round(trainErrF[i],3);
//			resultsTable[i][2] = ""+Util.round(trainErrB[i],3);
//		}
//		log.addResultsTable(
//				new String[] {"error","front","back"},
//				resultsTable,
//				"MLP associator - all perspectives - performance",
//				""
//		);
//		log.addFigure(
//				Conf.pathFigures+simName+"-results"+Conf.figExt,
//				"MLP associator - all perspectives - performance",
//				"",
//				0.9);
//		log.addFigure(
//				Conf.pathFigures+simName+"-data"+"-patterns"+Conf.figExt,
//				"MLP associator - all perspectives - pattern match",
//				"",
//				0.5);
//		log.echo();
//		log.exportPartial(Conf.pathOutput+"log-"+simName+".tex");
	}
}
