package test.backprop;

import java.io.IOException;

import util.TexLog;
import util.visu.DataPairsSel;
import util.visu.Plot;
import base.MLP;
import base.Conf;
import base.NetUtil;
import util.Util;
import data.DataBinary;

public class AssocRandMaplike {
	
	public static final String simName = "assoc-mlp-random-maplike";
	public static final String[] errNames = Conf.errorLabelsF1bidir;
	public static final int k = 3;
	public static final int samples  = 16;
	public static final int samplec = 16;
	public static final String fileIn  = Conf.pathDataMap+"k"+k+"-in-"+samples+"x"+samplec;
	public static final String fileDes = Conf.pathDataMap+"k"+k+"-des-"+samples+"x"+samplec;
	public static final int hids = 140;
	public static final double lRate = 0.1;
	public static final int maxEpcs = 50;
	public static final int plotRes = 1;
	
	public static void main(String[] args) throws IOException {
		DataBinary data = new DataBinary(fileIn,fileDes);
		Plot plot = new Plot(Util.merge(errNames,errNames));
		MLP netF = new MLP(new int[] {data.patSizeX(),hids,data.patSizeY()},lRate);
		MLP netB = new MLP(new int[] {data.patSizeY(),hids,data.patSizeX()},lRate);

		System.out.println("2xMLP "+data.patSizeX()+"-"+hids+"-"+data.patSizeY());

		double[] trainErrors = new double[errNames.length];
		double[][][] actNetF;
		double[][][] actNetB;
		int epoch = 0;
		while (((NetUtil.errorsUnsatisfiedBP(trainErrors)) || epoch < 1) && epoch < maxEpcs) {
			actNetF = netF.epoch(data.dataX(), data.dataY(),true);
			actNetB = netB.epoch(data.dataY(), data.dataX(),true);
			trainErrors = NetUtil.errorsF1bidirBP(actNetF, actNetB, data.dataX(), data.dataY());
			if ((epoch+1) % plotRes == 0) {
				for (int i = 0; i < trainErrors.length; i++) {
					plot.addPoint(i, epoch+1, trainErrors[i]);
				}
				System.out.print(".");
			}
			epoch++;
		}
		System.out.println();
		System.out.println("epcs: "+epoch);
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(errNames[i]+":\t"+Util.round(trainErrors[i],3));
		}
		plot.display();
		plot.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-results");
		DataPairsSel visRes = new DataPairsSel(netF.testEpochPatternsBinary(data.dataX(),data.dataY()),netB.testEpochPatternsBinary(data.dataY(),data.dataX()));
		visRes.display();
		visRes.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-patterns");
		
		// write log
		String[] setup = {
			"architecture: "+data.patSizeX()+"-"+hids+"-"+data.patSizeY(),
			"learning rate: "+ lRate,
			"momentum: "+ Conf.bp_momentum,
			"weight decay: "+ Conf.bp_wdecay,
			"min pattern succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			"data: binary-maplike, patterns 4x4, k="+k+", "+samplec+" vectorSize",
		};
		TexLog log = new TexLog();	
		log.addLine("\\subsection{MLP Associator with robotic data - first perspective}");
		log.addLine("{\\bf Setup parameters:}\\\\");
		log.addItemize(setup);
		
		String[][] resultsTable = new String[errNames.length][2];
		for (int i = 0; i < trainErrors.length; i++) {
			resultsTable[i][0] = errNames[i];
			resultsTable[i][1] = ""+Util.round(trainErrors[i],3);
		}
		log.addResultsTable(
				new String[] {"error","front","back"}, 
				resultsTable, 
				"MLP associator - all perspectives - performance", 
				""
		);
		log.addFigure(
				Conf.pathFigures+simName+"-results"+Conf.figExt, 
				"MLP associator - all perspectives - performance", 
				"",
				0.9);
		log.addFigure(
				Conf.pathFigures+simName+"-data"+"-patterns"+Conf.figExt, 
				"MLP associator - all perspectives - pattern match", 
				"",
				0.5);
		log.echo();
		log.exportPartial(Conf.pathOutput+"log-"+simName+".tex");
	}
}
