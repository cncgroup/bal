package test.helpers;

import base.BAL;
import base.Conf;
import base.NetUtil;
import util.TexLog;
import util.Util;
import util.visu.DataPairsRes;
import util.visu.Plot;

import java.util.Random;

public class BALTest {

	private int netCount;
	private int maxEpcs;
	private String[] errNames = Conf.testErrorsF1Bidir;

	public BALTest(int nc, int mxep) {
		netCount = nc;
		maxEpcs = mxep;
	}

	/* returns train errors with F1 bidir measure */
	public void singleNetWithPlot(double[][] dataX, double[][] dataY, double[][] dataXT, double[][] dataYT, int[] arch, double[][][] weightsF, double[][][] weightsB,
								  double lrates[], double[] clampstrength, double[] estimateStrengthF, double[] estimateStrengthB,
                                  int[] visParams, boolean patterns, boolean log) {
		BAL net = new BAL(arch, weightsF, weightsB,
				lrates,
				clampstrength,
                estimateStrengthF,
                estimateStrengthB);
		System.out.println("architecture: " + NetUtil.archToStr(arch) + " LR: "+Util.arrayToStringRounded(lrates,3));
		Plot plot = new Plot(errNames);

		DataPairsRes visResBal = null;
		if (patterns) {
			double[][][] patternsBal = net.testEpochPatterns(dataX, dataY);
			visResBal = new DataPairsRes(patternsBal, visParams);
			visResBal.display();
			//		visRes.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-patterns"+Conf.figExt);
		}

		int epoch = 0;
		double[] trainErrors = new double[errNames.length];
		while (epoch < maxEpcs) {
			BAL.Activations[] act = net.epoch(dataX, dataY,true);
			trainErrors = NetUtil.errorsF1bidirBAL(arch, dataX, dataY, act);
			for (int i = 0; i < trainErrors.length; i++) {
				plot.addPoint(i, epoch, trainErrors[i]);
			}

			epoch++;
			if (epoch % 1 == 0) {
				if (epoch % 1 == 0)
					if (epoch % 5 == 0)
						if (epoch % 10 == 0)
							if (epoch % 50 == 0)
								System.out.print(":\n");
							else
								System.out.print(";");
						else
							System.out.print(",");
					else
						System.out.print(".");
                if (epoch % 1 == 0)
				if (patterns && visResBal != null)
					visResBal.setPatterns(BAL.Activations.getPatterns(act,dataX,dataY));
			}
		}
		System.out.println();
		System.out.println("errors train:");
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(errNames[i]+": "+trainErrors[i]);
		}
		plot.display();

		System.out.println("errors test:");
		BAL.Activations[] actTest = net.epoch(dataXT, dataYT,false);
		double[] testErrors = NetUtil.errorsF1bidirBAL(arch, dataXT, dataYT, actTest);
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(errNames[i]+": "+testErrors[i]);
		}


		if (log) {

		}
		//TODO LOG
	}

    public void singleNetWithPlot(double[][] dataX, double[][] dataY, int[] arch, double[][][] weightsF, double[][][] weightsB,
                                  double lrate, double[] clampstrength, int[] visParams, boolean patterns, boolean log) {
        singleNetWithPlot(dataX,dataY,dataX,dataY,arch,weightsF,weightsB,
                BAL.paramSame(lrate,arch.length-1),
                clampstrength,
                BAL.paramSame(Conf.clampEchoStrength, arch.length),
                BAL.paramSame(Conf.clampEchoStrength, arch.length),
                visParams,patterns,log);
    }

	public void singleNetWithPlot(double[][] dataX, double[][] dataY, int[] arch, double[][][] weightsF, double[][][] weightsB,
								  double lrate, double clampstrength, int[] visParams, boolean patterns, boolean log) {
		singleNetWithPlot(dataX,dataY,arch,weightsF,weightsB,lrate,
				BAL.paramSymmetricSimple(clampstrength, Conf.clampTargetStrengthHidden, arch.length),
				visParams,patterns,log);
	}

	public void singleNetWithPlot(double[][] dataX, double[][] dataY, int[] arch,  double[][][] weightsF, double[][][] weightsB, double lrate, double clampstrength,
								  int[] visParams, boolean log) {
		singleNetWithPlot(dataX, dataY, arch, weightsF, weightsB, lrate, clampstrength, visParams, true, log);
	}

	public void singleNetWithPlot(double[][] dataX, double[][] dataY, int[] arch, double lrate, double clampstrength, int[] visParams, boolean log) {
		singleNetWithPlot(dataX, dataY, arch, NetUtil.initAllWeightsF(arch), NetUtil.initAllWeightsB(arch), lrate, clampstrength, visParams, true, log);
	}

	public void singleNetWithPlot(double[][] dataX, double[][] dataY, int[] arch, double lrate, double clampstrength, int[] visParams) {
		singleNetWithPlot(dataX, dataY, arch, NetUtil.initAllWeightsF(arch), NetUtil.initAllWeightsB(arch), lrate, clampstrength, visParams, false, false);
	}

	public NetTestResults batchSimple(int[] arch, double[][] dataF, double[][] dataB) {
		NetTestResults out = new NetTestResults(errNames);
		double[][] outData = new double[errNames.length][netCount];
		for (int n = 0; n < netCount; n++) {
			double[] errors = new double[6];
			int epoch = 0;
			BAL net = new BAL(arch,Conf.wmin,Conf.wmax,Conf.lRate,Conf.clampStrength);
			while (((Util.round(errors[2], 12) < 1.0 || Util.round(errors[3], 12) < 1.0) || epoch < 1) && epoch < maxEpcs) {
				BAL.Activations[] netAct = net.epoch(dataF, dataB, true);
				errors = NetUtil.errorsF1bidirBAL(arch, dataF, dataB, netAct);
				epoch++;
			}
			for (int i = 0; i < errors.length; i++) {
				outData[i][n] = errors[i];
			}
			outData[outData.length-1][n] = epoch;
			System.out.print(".");
		}
		System.out.println();
		out.addData(outData);
		return out;
	}

	public NetTestResults batchLRate(double[] lRates, double[][] dataF, double[][] dataB, int[] arch, double[][][] weightsF, double[][][] weightsB,
                                     double[] clampstrength, double[] estimateStrengthF, double[] estimateStrengthB) {
	    String[] errNames = Conf.testErrorsF1Bidir;
		NetTestResults out = new NetTestResults(errNames);
		for (int l = 0; l < lRates.length; l++) {
			System.out.println("testing learning rate: "+lRates[l]);
			double[][] outData = new double[errNames.length][netCount];
			for (int n = 0; n < netCount; n++) {
				double[] errors = new double[6];
				int epoch = 0;
				BAL net = new BAL(arch, weightsF, weightsB,lRates[l], clampstrength, estimateStrengthF, estimateStrengthB);
				while (((Util.round(errors[2], 12) < 1.0 || Util.round(errors[3], 12) < 1.0) || epoch < 1) && epoch < maxEpcs) {
					BAL.Activations[] netAct = net.epoch(dataF, dataB, true);
					errors = NetUtil.errorsF1bidirBAL(arch, dataF, dataB, netAct);
					epoch++;
				}
				for (int i = 0; i < errors.length; i++) {
					outData[i][n] = errors[i];
				}
				outData[outData.length-1][n] = epoch;
				System.out.print(".");
			}
			System.out.println();
            out.addData(outData); //TODO test epoch at the end
            System.out.println("LR: " + lRates[l]);
            for (int i = 0; i < errNames.length; i++) {
                System.out.println(errNames[i] + out.getResult(errNames[i], l));
            }
            System.out.println();
        }
		return out;
	}

    public NetTestResults batchLRate(double[] lRates, double[][] dataF, double[][] dataB, int[] arch, double[][][] weightsF, double[][][] weightsB,
                                     double clampstrength) {
	    return batchLRate(lRates, dataF, dataB, arch, weightsF, weightsB,
                BAL.paramSame(clampstrength, arch.length),
                BAL.paramSame(Conf.clampEchoStrength, arch.length),
                BAL.paramSame(Conf.clampEchoStrength, arch.length));
    }
	
	public NetTestResults batchArchitectures(int[][] archs, double[][] dataF, double[][] dataB, double wmin, double wmax, double lrate, double clampstrength) {
		NetTestResults out = new NetTestResults(Conf.testErrorsF1);
		for (int a = 0; a < archs.length; a++) {
			System.out.println("testing architecture: "+NetUtil.archToStr(archs[a]));
			double[][][] initWeightsF = NetUtil.initAllWeightsF(archs[a], wmin, wmax, new Random(System.currentTimeMillis()));
			double[][][] initWeightsB = NetUtil.initAllWeightsB(archs[a], wmin, wmax, new Random(System.currentTimeMillis()));
			double[][] outData = new double[7][netCount];
			for (int n = 0; n < netCount; n++) {
				double[] errors = new double[Conf.testErrorsF1.length];
				int epoch = 0;
				BAL net = new BAL(archs[a], initWeightsF, initWeightsB, lrate, clampstrength);
				while (((Util.round(errors[2], 12) < 1.0 || Util.round(errors[3], 12) < 1.0) || epoch < 1) && epoch < maxEpcs) {
					BAL.Activations[] netAct = net.epoch(dataF, dataB, true);
					errors = NetUtil.errorsF1bidirBAL(archs[a], dataF, dataB, netAct);
				}
				for (int i = 0; i < errors.length; i++) {
					outData[i][n] = errors[i];
				}
				outData[outData.length-1][n] = epoch;
				System.out.print(".");
			}
			System.out.println();
			out.addData(outData);
		}
		return out;
	}

	public TexLog logData(NetTestResults results) {	
		return logData(results,"",new String[1] , Conf.testErrorsF1Bidir);
	}

	public TexLog logData(NetTestResults results, String testName, String[] testedParams) {
		return logData(results, testName, testedParams, Conf.testErrorsF1Bidir);
	}

	public TexLog logData(NetTestResults results, String testName, String[] testedParams, String[] errorNames) {
		TexLog log = new TexLog();
		int size = testedParams.length;
		String[][] resultsTable = new String[size][errorNames.length+1];
		String[][] plotData = new String[errorNames.length][size];
		for (int i = 0; i < size; i++) {
			resultsTable[i][0] = testedParams[i];
			for (int j = 0; j < errorNames.length; j++) {
				double mean = results.getResult(errorNames[j], i)[0];
				double std = results.getResult(errorNames[j], i)[1];
				resultsTable[i][j+1] = (Math.round(mean) == mean) ? Math.round(mean)+"" : Util.round(mean,3)+""; //" $\\pm$ " + Util.round(results.mseF(i)[1],3);
				plotData[j][i] = "("+testedParams[i]+","+mean+") +- (0,"+std+")";
			}
		}
		log.addResultsTable(
				Util.addStringFront(errorNames,testName),
				resultsTable, 
				"BAL: comparing "+testName,
				"tab:gr-"+testName.replace(" ", "-")
		);
		log.addLine("\\begin{figure}");
//		log.addPlotErrBars(plotData,errorNames,testName,"error");
		log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
		log.addPlotErrBars(Util.subArray(plotData, 0, 1),Conf.mseLabels,testName,"mean square error");
		log.addLine("\\end{subfigure}");
		log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
		log.addPlotErrBars(Util.subArray(plotData, 2, 3),Conf.f1Labels,testName,"F1 score");
		log.addLine("\\end{subfigure}");
		log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
		log.addPlotErrBars(Util.subArray(plotData, 4, 4),Conf.epcLabels,testName,"epochs");
		log.addLine("\\end{subfigure}");
		log.addLine("\\caption{BAL batch test results: "+testName+"}");
		log.addLine("\\label{tab:gr-"+testName.replace(" ", "-")+"}");
		log.addLine("\\end{figure}");
		return log;
	}

}
