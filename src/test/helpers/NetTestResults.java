package test.helpers;

import java.util.ArrayList;
import java.util.HashMap;

import util.Util;
public class NetTestResults {
	
	private String[] errorNames;
	private HashMap<String,ArrayList<double[]>> data;
	
	public NetTestResults(String[] en) {
		errorNames = en;
		data = new HashMap<String, ArrayList<double[]>>();
		for (int i = 0; i < errorNames.length; i++) {
			data.put(errorNames[i], new ArrayList<double[]>());
		}
	}
	
	public void addData(double[][] newdata) {
		for (int i = 0; i < errorNames.length; i++) {
			data.get(errorNames[i]).add(Util.MeanAndStD(newdata[i]));
		}
	}
	
	public void addData(ArrayList<Double>[] newdata) {
		for (int i = 0; i < errorNames.length; i++) {
			if (errorNames[i] == "nets") {
				data.get(errorNames[i]).add(new double[] {Util.sum(newdata[i]),0.0});
			} else	
				data.get(errorNames[i]).add(Util.MeanAndStD(newdata[i]));
		}
	}
	
	public double[] getResult(String errname, int i) {
		return data.get(errname).get(i);
	}
	
	public void mergeWith(NetTestResults r2) {
		for (int i = 0; i < errorNames.length; i++) {
			data.get(errorNames[i]).addAll(r2.data.get(errorNames[i]));
		}
	}
	
	public int size() {
		return data.get(errorNames[0]).size();
	}
	
	public void write() {
		for (int i = 0; i < errorNames.length; i++) {
			String tmp = errorNames[i] + ": ";
			for (double[] val : data.get(errorNames[i])) {
				tmp += val[0] + "+-" + val[1] + ",";
			}
			System.out.println(tmp);
		}
	}
}
