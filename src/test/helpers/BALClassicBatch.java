package test.helpers;

import util.TexLog;
import base.BAL1;
import base.Conf;
import util.Util;

public class BALClassicBatch {
	@SuppressWarnings("unused")
	private String simName;
	private String[] errNames;
	private double lRate;
	private int hids;
	private int maxEpcs1;
	private int maxEpcs2;	
	private int plotRes;
	private int netCount;
	
	public BALClassicBatch(int netCount, String simName, String[] errNames, int hids, double lRate, int maxEpcs, int plotRes) {
		this.netCount = netCount;
		this.simName = simName;
		this.errNames = errNames;
		this.hids = hids;
		this.lRate = lRate;
		this.maxEpcs1 = maxEpcs;
		this.plotRes = plotRes;
	}
	
	public BALClassicBatch(int netCount, String simName, String[] errNames, int hids, double lRate, int maxEpcs, int maxEp2, int plotRes) {
		this(netCount, simName, errNames, hids, lRate, maxEpcs, plotRes);
		this.maxEpcs2 = maxEp2;
	}
	
	public TexLog runAndLog(double[][] valuesX, double[][] valuesY) {
		
		double[][][] perfInTime = new double[maxEpcs1/plotRes+1][errNames.length][netCount];
		int[] epochs = new int[netCount];
		
		for (int n = 0; n < netCount; n++) {
			System.out.println("net "+n);
			BAL1 net = new BAL1(valuesX[0].length,hids,valuesY[0].length,lRate);
			double[] testErr = new double[errNames.length];
			testErr = net.testEpoch(valuesX,valuesY);
			for (int i = 0; i < testErr.length; i++) {
				perfInTime[0][i][n] = testErr[i];
			}
			int epoch = 1;
			int index = 1;
			while (epoch < maxEpcs1) {
				net.epoch(valuesX,valuesY);
				testErr = net.testEpoch(valuesX,valuesY);
				if ((epoch+1) % plotRes == 0) {
					for (int i = 0; i < testErr.length; i++) {
						perfInTime[index][i][n] = testErr[i];
					}
					index++;
					System.out.print(".");
				}
				epochs[n] = epoch;
				epoch++;
			}
			System.out.println();
		}	
//		save results
		String[][][] plotData = new String[errNames.length/2][2][maxEpcs1/plotRes+1];
		for (int i = 0; i < perfInTime.length; i++) {
			for (int j = 0; j < errNames.length; j++) {
				double[] meanstd = Util.MeanAndStD(perfInTime[i][j]);
				plotData[j/2][j%2][i] = "("+(plotRes*i)+","+meanstd[0]+") +- (0,"+meanstd[1]+")";
			}
		}
		double[][] trainErrTmp = new double[errNames.length][netCount*(maxEpcs1/plotRes+1)];
		for (int n = 0; n < perfInTime[0][0].length; n++) {
			for (int e = 0; e < errNames.length; e++) {
				for (int i = 0; i < perfInTime.length; i++) {
					trainErrTmp[e][n*(maxEpcs1/plotRes+1)+i] = perfInTime[i][e][n];
				}
			}
		}
		String[][] resultsTable = new String[errNames.length+1][2];
		for (int i = 0; i < errNames.length; i++) {
			resultsTable[i][0] = errNames[i];
			double[] tmp = Util.MeanAndStD(trainErrTmp[i]);
			resultsTable[i][1] = ""+Util.round(tmp[0],3)+" $\\pm$ "+Util.round(tmp[1],3);
		}
		resultsTable[errNames.length][0] = "train epochs";
		double[] tmp = Util.MeanAndStD(epochs);
		resultsTable[errNames.length][1] = ""+Util.round(tmp[0],3)+" $\\pm$ "+Util.round(tmp[1],3);
//		write log
		TexLog log = new TexLog();
		log.addLine("");
		log.addResultsTable(
				new String[] {"measure","value"}, 
				resultsTable, 
				"", 
				""
		);
		log.addLine("");
		log.addLine("\\begin{figure}[!htbp]");
		log.addLine("\\centering");
		for (int i = 0; i < plotData.length; i++) {
			log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
			log.addLine("\\centering");
			log.addPlotErrBars(plotData[i],Conf.plotLabels[i],"epoch",Conf.errorNames[i]);
			log.addLine("\\end{subfigure}");
		}
		log.addLine("\\caption{Results for single instance of GeneRecBidir Associator with robotic data}");
		log.addLine("\\label{fig:assoc-single-robotic}");
		log.addLine("\\end{figure}");	
		return log;
	}
	
	public TexLog runAndLog2Phase(double[][] valuesX1, double[][] valuesY1, double[][] valuesX2, double[][] valuesY2) {

		double[][][] perfInTime = new double[(maxEpcs1+maxEpcs2)/plotRes+1][errNames.length][netCount];
		double[][][] perfFinal = new double[2][errNames.length][netCount];
		
		for (int n = 0; n < netCount; n++) {
			System.out.println("net "+n);			
	 		BAL1 net = new BAL1(valuesX1[0].length,hids,valuesY1[0].length,Conf.lRate,Conf.wmax,Conf.wmin);
			//phase 1
			double[] testErr1 = new double[errNames.length];
			testErr1 = net.testEpoch(valuesX1,valuesY1);
			for (int i = 0; i < testErr1.length; i++) {
				perfInTime[0][i][n] = testErr1[i];
			}
			int epoch1 = 1;
			int plotIndex = 1;
			while (epoch1 < maxEpcs1) {
				testErr1 = net.epoch(valuesX1,valuesY1,true,false);
				if ((epoch1+1) % plotRes == 0) {
					for (int i = 0; i < testErr1.length; i++) {
						perfInTime[plotIndex][i][n] = testErr1[i];
					}
					plotIndex++;
					System.out.print(".");
				}
				epoch1++;
			}
			for (int i = 0; i < testErr1.length; i++) {
				perfFinal[0][i][n] = testErr1[i];
			}
			//phase 2
			double[] trainErr2 = new double[errNames.length];
			int epoch2 = 0;
			while (epoch2 < maxEpcs2) {
				trainErr2 = net.epoch(valuesX2,valuesY2,true,false);
				if ((epoch2+1) % plotRes == 0) {
					for (int i = 0; i < errNames.length; i++) {
						perfInTime[plotIndex][i][n] = trainErr2[i];
					}
					plotIndex++;
					System.out.print(".");
				}			
				epoch2++;
			}
			for (int i = 0; i < testErr1.length; i++) {
				perfFinal[1][i][n] = trainErr2[i];
			}
			System.out.println();
		}
		//write log
		TexLog log = new TexLog();
		String[][] resultsTable = new String[errNames.length/2][5];
		for (int i = 0; i < Conf.errorLabelsBP.length; i++) {
			resultsTable[i][0] = Conf.errorLabelsBP[i];
			double[] meanstd = Util.MeanAndStD(perfFinal[0][2*i]);
			resultsTable[i][1] = Util.round(meanstd[0],3)+" $\\pm$ "+Util.round(meanstd[1],3);
			meanstd = Util.MeanAndStD(perfFinal[1][2*i]);
			resultsTable[i][2] = Util.round(meanstd[0],3)+" $\\pm$ "+Util.round(meanstd[1],3);
			meanstd = Util.MeanAndStD(perfFinal[0][2*i+1]);
			resultsTable[i][3] = Util.round(meanstd[0],3)+" $\\pm$ "+Util.round(meanstd[1],3);
			meanstd = Util.MeanAndStD(perfFinal[1][2*i+1]);
			resultsTable[i][4] = Util.round(meanstd[0],3)+" $\\pm$ "+Util.round(meanstd[1],3);
		}
		log.addResultsTable(
				new String[] {"measure","F","B","F","B"}, 
				resultsTable, 
				"BAL learning robotic data - performance",
				""
		);
		String[][][] plotData = new String[errNames.length/2][2][(maxEpcs1+maxEpcs2)/plotRes+1];
		for (int i = 0; i < perfInTime.length; i++) {
			for (int j = 0; j < errNames.length; j++) {
				double[] meanstd = Util.MeanAndStD(perfInTime[i][j]);
				plotData[j/2][j%2][i] = "("+(plotRes*i)+","+meanstd[0]+") +- (0,"+meanstd[1]+")";
			}
		}
		log.addLine("");
		log.addLine("\\begin{figure}[!htbp]");
		log.addLine("\\centering");
		for (int i = 0; i < plotData.length; i++) {
			log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
			log.addLine("\\centering");
			log.addPlotErrBars(plotData[i],Conf.plotLabels[i],"epoch",Conf.errorNames[i]);
			log.addLine("\\end{subfigure}");
		}
		log.addLine("\\caption{Results for single instance of GeneRecBidir Associator with robotic data}");
		log.addLine("\\label{fig:assoc-single-robotic}");
		log.addLine("\\end{figure}");
		return log;
	}
	
}
