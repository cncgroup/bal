package test.helpers;

import base.BAL1;
import base.Conf;
import util.TexLog;
import util.Util;

public class BALClassicTest {
	
	private int netCount;
	private int maxEpcs;
	
	public BALClassicTest(int nc, int mxep) {
		netCount = nc;
		maxEpcs = mxep;
	}
	
	public NetTestResults testSimple(double[][] dataF, double[][] dataB, int hidSize) {
		NetTestResults out = new NetTestResults(Conf.testErrors);
		double[][] outData = new double[7][netCount];
		for (int n = 0; n < netCount; n++) {
			double[] errors = new double[6];
			int epoch = 0;
			BAL1 net = new BAL1(dataF[0].length,hidSize,dataB[0].length,Conf.lRate,Conf.wmin,Conf.wmax);
//			while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < epcsPhase1) {
//				errors = net.epoch(dataF, dataB);
			while (((Util.round(errors[2], 12) < 1.0 || Util.round(errors[3], 12) < 1.0) || epoch < 1) && epoch < maxEpcs) {
				net.epoch(dataF, dataB);
				errors = net.testEpoch(dataF, dataB);
				epoch++;
			}
			for (int i = 0; i < errors.length; i++) {
				outData[i][n] = errors[i];
			}
			outData[6][n] = epoch;
			System.out.print(".");
		}
		System.out.println();
		out.addData(outData);
		return out;
	}
	
	public NetTestResults testSimple(double[][] dataF, double[][] dataB) {
		return testSimple(dataF,dataB,Conf.hids);
	}
	
	public NetTestResults testLRate(double[] lRates, double[][] dataF, double[][] dataB, int hids) {
		NetTestResults out = new NetTestResults(Conf.testErrors);
		for (int l = 0; l < lRates.length; l++) {
			System.out.println("testing learning rate: "+lRates[l]);
			double[][] outData = new double[Conf.testErrors.length][netCount];
			for (int n = 0; n < netCount; n++) {
				double[] errors = new double[6];
				int epoch = 0;
				BAL1 net = new BAL1(dataF[0].length,hids,dataB[0].length,lRates[l]);
				while (((Util.round(errors[2], 12) < 1.0 || Util.round(errors[3], 12) < 1.0) || epoch < 1) && epoch < maxEpcs) {
					net.epoch(dataF, dataB);
					errors = net.testEpoch(dataF, dataB);
					epoch++;
				}
				for (int i = 0; i < errors.length; i++) {
					outData[i][n] = errors[i];
				}
				outData[6][n] = epoch;
				System.out.print(".");
			}
			System.out.println();
			out.addData(outData);
		}
		return out;
	}
	
	public NetTestResults testHidSize(int[] hidSizes, double[][] dataF, double[][] dataB, double lrate) {
		NetTestResults out = new NetTestResults(Conf.testErrors);
		for (int h = 0; h < hidSizes.length; h++) {
			System.out.println("testing hidden size: "+hidSizes[h]);
			double[][] outData = new double[7][netCount];
			for (int n = 0; n < netCount; n++) {
				double[] errors = new double[6];
				int epoch = 0;
				BAL1 net = new BAL1(dataF[0].length,hidSizes[h],dataB[0].length,lrate);
				while (((Util.round(errors[2], 12) < 1.0 || Util.round(errors[3], 12) < 1.0) || epoch < 1) && epoch < maxEpcs) {
					net.epoch(dataF, dataB);
					errors = net.testEpoch(dataF, dataB);
					epoch++;
				}
				for (int i = 0; i < errors.length; i++) {
					outData[i][n] = errors[i];
				}
				outData[6][n] = epoch;
				System.out.print(".");
			}
			System.out.println();
			out.addData(outData);
		}
		return out;
	}
	
	public TexLog testSimpleLog(double[][] dataF, double[][] dataB, int hidSize) {
		return logData(testSimple(dataF,dataB,hidSize));
	}
	
	public TexLog logData(NetTestResults results) {	
		return logData(results,"",new double[1]);
	}
	
	public TexLog logData(NetTestResults results, String testName, double[] testedParams) {	
		TexLog log = new TexLog();
		int size = testedParams.length;
		String[][] resultsTable = new String[size][8];
		String[][] plotData = new String[Conf.testErrors.length][size];
		for (int i = 0; i < size; i++) {
			resultsTable[i][0] = (Math.round(testedParams[i]) == testedParams[i]) ? Math.round(testedParams[i])+"" : testedParams[i]+"";
			for (int j = 0; j < Conf.testErrors.length; j++) {
				double mean = results.getResult(Conf.testErrors[j], i)[0];
				double std = results.getResult(Conf.testErrors[j], i)[1];
				resultsTable[i][j+1] = (Math.round(mean) == mean) ? Math.round(mean)+"" : Util.round(mean,3)+""; //" $\\pm$ " + Util.round(results.mseF(i)[1],3);
				plotData[j][i] = "("+testedParams[i]+","+mean+") +- (0,"+std+")";
			}
		}
		log.addResultsTable(
				Util.addStringFront(Conf.testErrors,testName), 
				resultsTable, 
				"BAL: comparing "+testName,
				"tab:gr-"+testName.replace(" ", "-")
		);
		log.addLine("\\begin{figure}");
//		log.addPlotErrBars(plotData,Conf.testErrors,testName,"error");
		log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
		log.addPlotErrBars(Util.subArray(plotData, 0, 1),Conf.mseLabels,testName,"mean square error");
		log.addLine("\\end{subfigure}");
		log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
		log.addPlotErrBars(Util.subArray(plotData, 2, 3),Conf.patSuccLabels,testName,"epoch success");
		log.addLine("\\end{subfigure}");
		log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
		log.addPlotErrBars(Util.subArray(plotData, 4, 5),Conf.bitSuccLabels,testName,"error bits");
		log.addLine("\\end{subfigure}");
		log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
		log.addPlotErrBars(Util.subArray(plotData, 6, 6),Conf.epcLabels,testName,"epochs");
		log.addLine("\\end{subfigure}");
		log.addLine("\\caption{BAL batch test results: "+testName+"}");
		log.addLine("\\label{tab:gr-"+testName.replace(" ", "-")+"}");
		log.addLine("\\end{figure}");
		return log;
	}
}
