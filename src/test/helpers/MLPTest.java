package test.helpers;

import base.BAL;
import util.TexLog;
import base.MLP;
import base.Conf;
import base.NetUtil;
import util.Util;
import util.visu.DataPairsRes;
import util.visu.Plot;

import java.util.Arrays;

public class MLPTest {
	
	private int netCount;
	private int maxEpcs;
	private String[] errNames = Conf.testErrorsF1BP;
	
	public MLPTest(int nc, int mxep) {
		netCount = nc;
		maxEpcs = mxep;
	}

    public void singleNetWithPlot(double[][] dataX, double[][] dataY, double[][] dataXT, double[][] dataYT, int[] arch, double lrate) {
        MLP net = new MLP(arch, NetUtil.initAllWeightsF(arch), lrate, Conf.bp_momentum, Conf.bp_wdecay);
        System.out.println("architecture: " + NetUtil.archToStr(arch) + " LR: " + lrate);
        Plot plot = new Plot(errNames);

        int epoch = 0;
        double[] trainErrors = new double[errNames.length];
        double[][][] activations;
        while (epoch < maxEpcs) {
            activations = net.epoch(dataX, dataY, true);
            trainErrors = NetUtil.errorsF1BP(activations, dataY);
            epoch++;
            for (int i = 0; i < trainErrors.length; i++) {
                plot.addPoint(i, epoch, trainErrors[i]);
            }

            epoch++;
            if (epoch % 1 == 0) {
                if (epoch % 1 == 0)
                    if (epoch % 5 == 0)
                        if (epoch % 10 == 0)
                            if (epoch % 50 == 0)
                                System.out.print(":\n");
                            else
                                System.out.print(";");
                        else
                            System.out.print(",");
                    else
                        System.out.print(".");
            }
        }
        System.out.println();
        System.out.println("errors train:");
        for (int i = 0; i < trainErrors.length; i++) {
            System.out.println(errNames[i] + ": " + trainErrors[i]);
        }
        plot.display();

        System.out.println("errors test:");
        double[][][] actTest = net.epoch(dataXT, dataYT, false);
            double[] testErrors = NetUtil.errorsF1BP(actTest, dataY);
        for (int i = 0; i < testErrors.length; i++) {
            System.out.println(errNames[i] + ": " + testErrors[i]);
        }
    }

	public NetTestResults testSimple(double[][] inputs, double[][] desired, int[] arch) {
		NetTestResults out = new NetTestResults(errNames);
		double[][][] weights =  NetUtil.initWeightsMLP(arch, Conf.wmin, Conf.wmax);
		double[][] outData = new double[errNames.length][netCount];
		for (int n = 0; n < netCount; n++) {
			double[] errors = new double[2];
			double[][][] activations;
			int epoch = 0;
			MLP net = new MLP(arch, weights, Conf.lRate, Conf.bp_momentum, Conf.bp_wdecay);
			while ((NetUtil.errorsUnsatisfiedBP(errors) || epoch < 1) && epoch < maxEpcs) {
				activations = net.epoch(inputs, desired, true);
				errors = NetUtil.errorsF1BP(activations, desired);
				epoch++;
			}
			for (int i = 0; i < errors.length; i++) {
				outData[i][n] = errors[i];
			}
			outData[3][n] = epoch;
			System.out.print(".");
		}
		System.out.println();
		out.addData(outData);
		return out;
	}

	public NetTestResults testLRate(int[] arch, double[] lRates, double[][] inputs, double[][] desired) {
		NetTestResults out = new NetTestResults(errNames);
		double[][][] weights =  NetUtil.initWeightsMLP(arch, Conf.wmin, Conf.wmax);
		double[][] outData = new double[errNames.length][netCount];
		for (int l = 0; l < lRates.length; l++) {
			System.out.println("testing learning rate: "+lRates[l]);
			for (int n = 0; n < netCount; n++) {
				double[] errors = new double[errNames.length-1];
				double[][][] activations;
				int epoch = 0;
				MLP net = new MLP(arch, weights, lRates[l], Conf.bp_momentum, Conf.bp_wdecay);
				while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs) {
					activations = net.epoch(inputs, desired, true);
					errors = NetUtil.errorsF1BP(activations, desired);
					epoch++;	
				}
				for (int i = 0; i < errors.length; i++) {
					outData[i][n] = errors[i];
				}
				outData[errNames.length-1][n] = epoch;
				System.out.print(".");
			}
			System.out.println();
			out.addData(outData);
		}
		return out;
	}
	
	public NetTestResults testArchitectures(int[][] archs, double[][] inputs, double[][] desired, double lrate) {
		NetTestResults out = new NetTestResults(errNames);
		for (int a = 0; a < archs.length; a++) {
			System.out.println("testing arch: "+ Arrays.toString(archs[a]));
			double[][][] weights =  NetUtil.initWeightsMLP(archs[a], Conf.wmin, Conf.wmax);
			double[][] outData = new double[7][netCount];
			for (int n = 0; n < netCount; n++) {
				double[] errors = new double[errNames.length-1];
				double[][][] activations;
				int epoch = 0;
				MLP net = new MLP(archs[a], weights, lrate, Conf.bp_momentum, Conf.bp_wdecay);
				while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs) {
					activations = net.epoch(inputs, desired, true);
					errors = NetUtil.errorsF1BP(activations, desired);
					epoch++;
				}
				for (int i = 0; i < errors.length; i++) {
					outData[i][n] = errors[i];
				}
				outData[errNames.length-1][n] = epoch;
				System.out.print(".");
			}
			System.out.println();
			out.addData(outData);
		}
		return out;
	}

	public TexLog logData(NetTestResults results) {	
		return logData(results,"",new String[1]);
	}
	
	public TexLog logData(NetTestResults results, String testName, String[] testedParams) {
		TexLog log = new TexLog();
		int size = testedParams.length;
		String[][] resultsTable = new String[size][4];
		String[][] plotData = new String[Conf.errorLabelsBP2.length][size];
		for (int i = 0; i < size; i++) {
			resultsTable[i][0] = testedParams[i];
			for (int j = 0; j < Conf.errorLabelsBP2.length; j++) {
				double mean = results.getResult(Conf.errorLabelsBP2[j], i)[0];
				double std = results.getResult(Conf.errorLabelsBP2[j], i)[1];
				resultsTable[i][j+1] = (Math.round(mean) == mean) ? Math.round(mean)+"" : Util.round(mean,3)+""; //" $\\pm$ " + Util.round(results.mseF(i)[1],3);
				plotData[j][i] = "("+testedParams[i]+","+mean+") +- (0,"+std+")";
			}
		}
		log.addResultsTable(
				Util.addStringFront(Conf.errorLabelsBP2,testName), 
				resultsTable, 
				"BPNetwork comparing "+testName, 
				"tab:gr-"+testName.replace(" ", "-")
		);
		log.addLine("\\begin{figure}");
		log.addPlotErrBars(plotData,Conf.errorLabelsBP2,testName,"");
		log.addLine("\\caption{Batch test results: "+testName+"}");
		log.addLine("\\label{tab:bp-"+testName.replace(" ", "-")+"}");
		log.addLine("\\end{figure}");
		return log;
	}
}
