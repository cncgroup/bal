package test.helpers;

import util.TexLog;
import util.visu.DataPairsRes;
import util.visu.Plot;
import base.BAL1;
import base.Conf;
import util.Util;

public class BALClassicSingle {
	private String simName;
	private String[] errNames;
	private double lRate;
	private int hids;
	private int maxEpcs;
	private int maxEpcs2;	
	private int plotRes;
	private int rowHeight;
	
	public BALClassicSingle(String simName, String[] errNames, int hids, double lRate, int maxEpcs, int plotRes, int rowHeight) {
		this.simName = simName;
		this.errNames = errNames;
		this.hids = hids;
		this.lRate = lRate;
		this.maxEpcs = maxEpcs;
		this.plotRes = plotRes;
		this.rowHeight = rowHeight;
	}
	
	public BALClassicSingle(String simName, String[] errNames, int hids, double lRate, int maxEpcs, int maxEp2, int plotRes, int rowHeight) {
		this.simName = simName;
		this.errNames = errNames;
		this.hids = hids;
		this.lRate = lRate;
		this.maxEpcs = maxEpcs;
		this.maxEpcs2 = maxEp2;
		this.plotRes = plotRes;
		this.rowHeight = rowHeight;
	}
	
	public TexLog runAndLog(double[][] valuesX, double[][] valuesY) {
		Plot plot = new Plot(errNames); 
		BAL1 net = new BAL1(valuesX[0].length,hids,valuesY[0].length,lRate);
		int epoch = 0;
		double[] testErr = net.testEpoch(valuesX,valuesY);
		for (int i = 0; i < testErr.length; i++) {
			plot.addPoint(i, epoch, testErr[i]);
		}
		epoch++;
		while (((Util.round(testErr[2], 12) < 1.0 || Util.round(testErr[3], 12) < 1.0) || epoch < 1) && epoch < maxEpcs) {
			net.epoch(valuesX,valuesY);
			testErr = net.testEpoch(valuesX,valuesY);
			if (epoch % plotRes == 0) {
				for (int i = 0; i < testErr.length; i++) {
					plot.addPoint(i, epoch, testErr[i]);
				}
				System.out.print(".");
			}
			epoch++;
		}
		for (int i = 0; i < testErr.length; i++) {
			plot.addPoint(i, epoch, testErr[i]);
		}
		System.out.println();
		System.out.println("epcs: "+epoch);		
		for (int i = 0; i < testErr.length; i++) {
			System.out.println(errNames[i]+": "+testErr[i]);
		}
		System.out.println();
		plot.display();		
		double[][][] patterns = net.testEpochPatterns(valuesX,valuesY);
		DataPairsRes visRes = new DataPairsRes(patterns);
		if (valuesX.length > 100) {
			visRes.setPxs(Conf.pxsAllP);
			visRes.setRowH(Conf.rowHAllP);
		} else {
			visRes.setRowH(rowHeight);
		}
		visRes.display();
		// save images + write log
//		plot.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-results"+Conf.figExt);
//		visRes.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-patterns"+Conf.figExt);
		TexLog log = new TexLog();	
		String[][] resultsTable = new String[errNames.length/2+1][3];
		for (int i = 0; i < Conf.errorLabelsBP.length; i++) {
			resultsTable[i][0] = Conf.errorLabelsBP[i];
			resultsTable[i][1] = ""+Util.round(testErr[2*i],3);
			resultsTable[i][2] = ""+Util.round(testErr[2*i+1],3);
		}
		resultsTable[resultsTable.length-1][0] = "epochs";
		resultsTable[resultsTable.length-1][1] = ""+epoch;
		resultsTable[resultsTable.length-1][2] = "";
		log.addResultsTable(
				new String[] {"error","perf F","perf B"}, 
				resultsTable, 
				"BAL with robotic data - performance", 
				""
		);
		log.addFigure(
				Conf.pathFigures+simName+"-results"+Conf.figExt, 
				"BAL with robotic data - performance", 
				"",
				0.7);
		log.addFigure(
				Conf.pathFigures+simName+"-patterns"+Conf.figExt, 
				"BAL with robotic data - pattern match", 
				"",
				0.6);
		return log;
	}
	
	public TexLog runAndLog2Phase(double[][] valuesX1, double[][] valuesY1, double[][] valuesX2, double[][] valuesY2) {
		System.out.println("phase one: first perspective");
 		BAL1 net = new BAL1(valuesX1[0].length,hids,valuesY1[0].length,Conf.lRate,Conf.wmax,Conf.wmin);
		Plot plot = new Plot(errNames); 
		double[] testErr1 = new double[errNames.length];
		testErr1 = net.testEpoch(valuesX1,valuesY1);
		int epoch1 = 1;
		int epochAbs = 1;
		while (epoch1 < maxEpcs) {
			net.epoch(valuesX1,valuesY1);
			testErr1 = net.testEpoch(valuesX1,valuesY1);
			if (epoch1 % plotRes == 0) {
				for (int i = 0; i < testErr1.length; i++) {
					plot.addPoint(i, epochAbs+1, testErr1[i]);
				}
				System.out.print(".");
			}
			epoch1++;
			epochAbs++;
		}
		System.out.println();
		System.out.println("epcs: "+epoch1);
		for (int i = 0; i < errNames.length; i++) {
			System.out.println(errNames[i]+": "+testErr1[i]);
		}
		System.out.println();

		System.out.println("phase two: other perspectives");
		double[] testErr2 = new double[errNames.length];
		int epoch2 = 0;
		while (epoch2 < maxEpcs2) {
			net.epoch(valuesX2,valuesY2);
			testErr2 = net.testEpoch(valuesX2,valuesY2);
			if ((epoch2+1) % plotRes == 0) {
				for (int i = 0; i < errNames.length; i++) {
					plot.addPoint(i, epochAbs+1, testErr2[i]);
				}
				System.out.print(".");
			}			
			epoch2++;
			epochAbs++;
		}
		System.out.println();
		System.out.println("epcs: "+epoch2);
		for (int i = 0; i < errNames.length; i++) {
			System.out.println(errNames[i]+": "+testErr2[i]);
		}
		
		double[][][] patterns = net.testEpochPatterns(valuesX2,valuesY2);
		DataPairsRes visRes = new DataPairsRes(patterns);
		if (valuesX2.length > 100) {
			visRes.setPxs(Conf.pxsAllP);
			visRes.setRowH(Conf.rowHAllP);
		} else {
			visRes.setRowH(rowHeight);
		}
		visRes.display();
		plot.display();
		plot.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-performance"+Conf.figExt);
		visRes.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-patterns2"+Conf.figExt);
		
		TexLog log = new TexLog();
		log.addLine("\\subsection{BAL learning in 2 phases}");
		String[][] resultsTable = new String[errNames.length+1][3];
		for (int i = 0; i < errNames.length; i++) {
			resultsTable[i][0] = errNames[i];
			resultsTable[i][1] = ""+Util.round(testErr1[i],3);
			resultsTable[i][2] = ""+Util.round(testErr2[i],3);
		}
		resultsTable[resultsTable.length-1][0] = "epochs";
		resultsTable[resultsTable.length-1][1] = ""+epoch1;
		resultsTable[resultsTable.length-1][2] = ""+epoch2;
		log.addResultsTable(
				new String[] {"error","phase one","phase two"}, 
				resultsTable, 
				"BAL with robotic data - performance", 
				""
		);
		log.addFigure(
				Conf.pathFigures+simName+"-performance"+Conf.figExt, 
				"BAL with robotic data - performance", 
				"",
				0.9);
		log.addFigure(
				Conf.pathFigures+simName+"-patterns2"+Conf.figExt, 
				"BAL with robotic data - pattern match in second phase", 
				"",
				0.7);
		return log;
	}
	
}
