package test.balnew.mnist;

import base.Conf;
import base.NetUtil;
import data.DataMNIST;
import test.helpers.BALTest;
import test.helpers.MLPTest;

import java.io.IOException;
import java.util.Arrays;

public class ClassifyMNISTtoPatternsMLP {

	private static final double NaN = Double.NaN;
	public static final String[] errNames = Conf.errorLabelsF1;
    public static final int[] visParams = {2, 16, 30, 30};

	public static final int sizePic = 28; // sizePic * sizePic
	public static final int sizeCat = 10; // # of categories
	public static final int kPic = sizePic*sizePic*20/100; // 20% of picture is digit, the rest is whitespace
	public static final int kCat = 1;
	public static final double[] lRates = {.1,.1};
//	public static final double clampStrength = 1.0;
//	public static final double[] clampStrength = {0.3, 0.15, 0.0};
	public static final double[] clampStrength     = {0.01,  1.0,  0.0}; // quasi-RBM + quasi-supervised
	public static final double[] estimateStrengthF = {NaN,  0.0,  0.0};
	public static final double[] estimateStrangthB =       {0.0,  0.0,  NaN};
//	public static final double[] clampStrength     =    {1.0,  1.0,  0.0}; // quasi-RBM + quasi-supervised
//	public static final double[] estimateStrengthF = {NaN,  0.1,  0.5};
//	public static final double[] estimateStrangthB =       {0.1,  0.5,  NaN};
//	public static final int hids2 = 200;
//	public static final int[] arch = {sizePic*sizePic, 300, 100, sizeCat};
//	public static final int[] arch = {sizePic*sizePic, hids, hids2, sizeCat};
//	public static final int[] arch = {784, 2500,2000,1500,1000,500, 10}; // deep
//	public static final int[] arch = {784, 50,100,500,1000,10, 10};      // convo

    public static final int hids = 800;
    public static final int[] arch = {sizePic*sizePic, hids, 15};
	public static final int maxEpcs = 20;//0;


	public static void main(String[] args) throws IOException {

        DataMNIST dataMNIST = new DataMNIST(Conf.dataSetsMNISTPaths,  Conf.defaultMNISTDelimiter, Conf.digitsBinary3x5,5000, 1000);//60000);
		System.out.println(Arrays.toString(dataMNIST.catCountTrain));
		System.out.println(Arrays.toString(dataMNIST.catCountTest));
//		double[][] valuesVis = dataVis.valuesPerspIndices(0,indices);
//		DataPairs2 visData = new DataPairs2(3,10,dataVis,dataMot);
//		visData.display();
		MLPTest test = new MLPTest(1,maxEpcs);
		test.singleNetWithPlot(
                dataMNIST.valuesPicsTrain(), dataMNIST.valuesCatsTrain(),
                dataMNIST.valuesPicsTest(), dataMNIST.valuesCatsTest(),
                arch,lRates[0]);
//				NetUtil.initAllWeightsF(arch),
//				NetUtil.initAllWeightsB(arch),
//				lRates, clampStrength, estimateStrengthF, estimateStrangthB,
//				visParams, false, false);
	}
}
