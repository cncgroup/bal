package test.balnew.mnist;

import base.BAL;
import base.Conf;
import base.MLP;
import base.NetUtil;
import data.DataMNIST;
import util.Util;
import util.visu.DataPairsRes;
import util.visu.Plot;

import java.io.IOException;
import java.util.Random;

public class ClassifyMNISTBALvsBP {

    private static final double NaN = Double.NaN;
	public static final String[] errNames = Conf.errorLabelsF1bidir;
    public static final int[] visParams = {1, 16, 30, 30};

    public static final int sizePic = 28; // sizePic * sizePic
    public static final int sizeCat = 10; // # of categories
    public static final int hids = 800;

	public static final int[] archF = new int[] {sizePic*sizePic, hids, sizeCat};
	public static final int[] archB = new int[] {sizeCat, hids, sizePic*sizePic};

    public static final double wmax = -.1;
    public static final double wmin = .1;

    public static final double lRateBP = .1;
    public static final double[] lRatesBal = {1.2,1.2};

    public static final double[] clampStrength     = {0.01,  1.0,  0.0}; // quasi-RBM + quasi-supervised
    public static final double[] estimateStrengthF = {NaN,  0.0,  0.0};
    public static final double[] estimateStrangthB =       {0.0,  0.0,  NaN};

//	public static final int epcsPhase1 = 500;
	public static final int epcs = 20;

	public static void main(String[] args) throws IOException {

        DataMNIST dataMNIST = new DataMNIST(Conf.dataSetsMNISTPaths,  10000, 2000);//60000);

		// shared init weights
		Random rand = new Random(System.currentTimeMillis());
		double[][][] weightsF = NetUtil.initAllWeightsF(archF, wmin, wmax);
		double[][][] weightsB = NetUtil.initAllWeightsB(archF, wmin, wmax);

		Plot plot = new Plot(Util.merge(errNames,errNames));
        int epoch = 0;
        double[] trainErrors = new double[errNames.length];
        double[] testErrors = new double[errNames.length];

        // 2 BP NETS
		MLP mlpF = new MLP(archF, weightsF, lRateBP);
		MLP mlpB = new MLP(archB, NetUtil.reverseWeights(weightsB), lRateBP);
		System.out.println("2xMLP "+NetUtil.archToStr(archF)+" "+NetUtil.archToStr(archB));
		while (epoch < epcs) {
			double[][][] activationNetF = mlpF.epoch(dataMNIST.valuesPicsTrain(), dataMNIST.valuesCatsTrain(), true);
			double[][][] activationNetB = mlpB.epoch(dataMNIST.valuesCatsTrain(), dataMNIST.valuesPicsTrain(), true);
			trainErrors = NetUtil.errorsF1bidirBP(activationNetF, activationNetB, dataMNIST.valuesPicsTrain(), dataMNIST.valuesCatsTrain());
			for (int i = 0; i < trainErrors.length; i++) {
				plot.addPoint(i, epoch, trainErrors[i]);
			}
			System.out.print(".");
			epoch++;
		}
		System.out.println();
		System.out.println("errors train:");
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(errNames[i]+": "+trainErrors[i]);
		}
		System.out.println("errors test:");
		double[][][] activationNetFTest = mlpF.epoch(dataMNIST.valuesPicsTest(), dataMNIST.valuesCatsTest(), false);
		double[][][] activationNetBTest = mlpB.epoch(dataMNIST.valuesCatsTest(), dataMNIST.valuesPicsTest(), false);
		testErrors = NetUtil.errorsF1bidirBP(activationNetFTest, activationNetBTest, dataMNIST.valuesPicsTest(), dataMNIST.valuesCatsTest());
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(errNames[i]+": "+testErrors[i]);
		}
//		double[][][] patternsBP = Util.merge(mlpF.testEpochPatternsBinary(dataMNIST.valuesPicsTest(),dataMNIST.valuesCatsTest()),mlpB.testEpochPatternsBinary(dataMNIST.valuesCatsTest(),dataMNIST.valuesPicsTest()));
//		DataPairsRes visResBP = new DataPairsRes(patternsBP,visParams);
//		visResBP.display();

		// 1 BAL NET
		BAL netBal = new BAL(archF, weightsF, weightsB, lRatesBal, clampStrength, estimateStrengthF, estimateStrangthB);
		System.out.println("architecture: " + NetUtil.archToStr(archF) + " LR: "+Util.arrayToStringRounded(lRatesBal,3));
		epoch = 0;
		trainErrors = new double[errNames.length];
		while (epoch < epcs) {
			BAL.Activations act[] = netBal.epoch(dataMNIST.valuesPicsTrain(), dataMNIST.valuesCatsTrain(),true);
			trainErrors = NetUtil.errorsF1bidirBAL(archF, dataMNIST.valuesPicsTrain(), dataMNIST.valuesCatsTrain(), act);
		 	for (int i = 0; i < trainErrors.length; i++) {
				plot.addPoint(errNames.length+i, epoch, trainErrors[i]);
			}
			System.out.print(".");
			epoch++;
		}
		System.out.println();
		System.out.println("errors train:");
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(errNames[i]+": "+trainErrors[i]);
		}
		System.out.println("errors test:");
		BAL.Activations actTest[] = netBal.epoch(dataMNIST.valuesPicsTest(), dataMNIST.valuesCatsTest(),false);
		testErrors = NetUtil.errorsF1bidirBAL(archF, dataMNIST.valuesPicsTest(), dataMNIST.valuesCatsTest(), actTest);
		 for (int i = 0; i < testErrors.length; i++) {
			System.out.println(errNames[i]+": "+testErrors[i]);
		}
//		double[][][] patternsBal = netBal.testEpochPatterns(dataMNIST.valuesPicsTest(),dataMNIST.valuesCatsTest());
//		DataPairsRes visResBal = new DataPairsRes(patternsBal,visParams);
//		visResBal.display();
//		visRes.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-patterns"+Conf.figExt);

		plot.display();
	}
}
