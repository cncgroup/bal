package test.balnew.mnist;

import base.Conf;
import base.NetUtil;
import data.DataBinary;
import data.DataMNIST;
import test.helpers.BALTest;
import util.TexLog;

import java.io.IOException;
import java.util.Arrays;

public class MNISTLRate {

    private static final double NaN = Double.NaN;
    public static final String[] errNames = Conf.errorLabelsF1;
    public static final int sizePic = 28; // sizePic * sizePic
    public static final int sizeCat = 10; // # of categories
    public static final int kPic = sizePic*sizePic*20/100; // 20% of picture is digit, the rest is whitespace
    public static final int kCat = 1;

    public static final double wmax = -.1;
    public static final double wmin = .1;

    public static final double[] clampStrength     = {0.01,  1.0,  0.0}; // quasi-RBM + quasi-supervised
    public static final double[] estimateStrengthF = {NaN,  0.0,  0.0};
    public static final double[] estimateStrangthB =       {0.0,  0.0,  NaN};

    public static final int hids = 800;
    public static final int[] arch = {sizePic*sizePic, hids, sizeCat};

    public static final int netc = 10;
    public static final int maxEpcs = 20;//0;

	public static final String logName = "mnist-batch-lrate.tex";
//	public static final double[] learningRates = {0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.5};
//	public static final double[] learningRates = {0.05, 0.1, 0.2, 0.5, 1.0, 1.3};
	public static final double[] learningRates = {1.5, 2.0, 5.0};

	public static void main(String[] args) throws IOException {

        DataMNIST dataMNIST = new DataMNIST(Conf.dataSetsMNISTPaths, 3000, 500);//60000);
        System.out.println(Arrays.toString(dataMNIST.catCountTrain));
        System.out.println(Arrays.toString(dataMNIST.catCountTest));

        BALTest test = new BALTest(netc, maxEpcs);
		TexLog log = new TexLog();
		double[][][] initWeightsF = NetUtil.initAllWeightsF(arch, wmin, wmax);
		double[][][] initWeightsB = NetUtil.initAllWeightsB(arch, wmin, wmax);
		log.addLog(test.logData(
				test.batchLRate(learningRates, dataMNIST.valuesPicsTrain(), dataMNIST.valuesCatsTrain(),
                arch, initWeightsF, initWeightsB, clampStrength, estimateStrengthF, estimateStrangthB),
				"learning rate",
				NetUtil.paramsToStr(learningRates)));
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
