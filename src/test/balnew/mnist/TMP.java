package test.balnew.mnist;

import base.BAL;
import base.Conf;
import data.DataMNIST;
import util.visu.DataPairsResRectangular;

import java.io.IOException;

public class TMP {

    private static final double NaN = Double.NaN;
	public static final String[] errNames = Conf.errorLabelsF1bidir;

    public static final int sizePic = 28; // sizePic * sizePic

    public static final int[] visParams = {sizePic, sizePic, 2, 2, 5, 8, 15, 10, 30};//dimXX dimXY pxsX dimYX dimYY pxsY rowh margX margY

    public static final int hids = 800;
	public static final int[] archF = new int[] {sizePic*sizePic, hids, 15};
    public static final double wmax = -.1;
    public static final double wmin = .1;
    public static final double[] lRatesBal = {.1,.1};

    public static final double[] clampStrength     = {0.01,  1.0,  0.0}; // quasi-RBM + quasi-supervised
    public static final double[] estimateStrengthF = {NaN,  0.0,  0.0};
    public static final double[] estimateStrangthB =       {0.0,  0.0,  NaN};

//	public static final int epcsPhase1 = 500;
	public static final int epcs = 1;

	public static void main(String[] args) throws IOException {

        DataMNIST dataMNIST = new DataMNIST(Conf.dataSetsMNISTPaths,  Conf.defaultMNISTDelimiter, Conf.digitsBinary3x5,100, 50);//60000);

		BAL netBal = new BAL(archF, wmin, wmax, lRatesBal, clampStrength, estimateStrengthF, estimateStrangthB);
        netBal.epoch(dataMNIST.valuesPicsTrain(), dataMNIST.valuesCatsTrain(),true);

		double[][][] patternsBal = netBal.testEpochPatterns(dataMNIST.valuesPicsTest(),dataMNIST.valuesCatsTest());
        DataPairsResRectangular visResBal = new DataPairsResRectangular(patternsBal,visParams);
		visResBal.display();

	}
}
