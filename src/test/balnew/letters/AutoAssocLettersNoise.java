package test.balnew.letters;

import base.Conf;
import base.NetUtil;
import test.helpers.BALTest;
import util.Util;
import util.visu.DataPairsVis;

import java.io.IOException;
import java.util.Random;

public class AutoAssocLettersNoise {

    private static final double NaN = Double.NaN;
    static String[] errorLabels = Conf.errorLabelsF1bidir;
	public static final int[] visParams = {4, 15, 10, 10};

	static double[][][] letters = Conf.lettersBinary7x7;
	static int side = 7;
	static int vectorSize = side*side;

//	static double lr = 1.3;
//	static double clampStrength = 1.0;

    static double[] lRates = {1.3,1.3};
    static double[] clampStrength     = {1.0,  0.5,  0.0}; // quasi-RBM + quasi-supervised
    static double[] estimateStrengthF = {NaN,  0.5,  0.5};
    static double[] estimateStrangthB =       {0.5,  0.5,  NaN};

	static int hids = 100;
	static int[] arch = {vectorSize,hids,vectorSize};
	static int maxEpcs = 200;

	static double noiseMean =  0.0;
	static double noiseVariance =  0.25;
	static int dataMultiply = 20;
	static int dataMultiplyTest = 5;

	public static void main(String[] args) throws IOException {

        Random generator = new Random(System.currentTimeMillis());
		double[][] dataX = new double[dataMultiply*letters.length][vectorSize];
		double[][] dataY = new double[dataMultiply*letters.length][vectorSize];
        double[][] dataXT = new double[dataMultiplyTest*letters.length][vectorSize];
        double[][] dataYT = new double[dataMultiplyTest*letters.length][vectorSize];

        for (int n = 0; n < dataMultiply; n++) {
            for (int i = 0; i < letters.length; i++) {
                double[] letterNoise = Util.flatten(letters[i]);
                for (int j = 0; j < letterNoise.length; j++) {
                    letterNoise[j] += Util.randomGauss(noiseMean, noiseVariance, generator);
                }
                dataX[n*letters.length+i] = letterNoise;
                dataY[n*letters.length+i] = Util.flatten(letters[i]);
            }
        }
        for (int n = 0; n < dataMultiplyTest; n++) {
            for (int i = 0; i < letters.length; i++) {
                double[] letterNoise = Util.flatten(letters[i]);
                for (int j = 0; j < letterNoise.length; j++) {
                    letterNoise[j] += Util.randomGauss(noiseMean, noiseVariance, generator);
                }
                dataXT[n*letters.length+i] = letterNoise;
                dataXT[n*letters.length+i] = Util.flatten(letters[i]);
            }
        }

        DataPairsVis v = new DataPairsVis(new int[] {5, 30, 30, 7, 15}, dataX, dataY);
        v.display();
        v.saveImage(Conf.pathOutput+Conf.pathFigures+"data-letters-noised"+Conf.figExt);

        BALTest test = new BALTest(1,maxEpcs);
        test.singleNetWithPlot(
                dataX, dataY, dataX, dataY, arch,
                NetUtil.initAllWeightsF(arch),
                NetUtil.initAllWeightsB(arch),
                lRates, clampStrength, estimateStrengthF, estimateStrangthB,
                visParams, true, false);

//        BAL net = new BAL(arch,Conf.wmin,Conf.wmax,lr,clampStrength);
//        Plot plot = new Plot(errorLabels);
//		double[][][] patterns = net.testEpochPatterns(dataX,dataY);
//		DataPairsRes visRes = new DataPairsRes(patterns,visParams);
//		visRes.display();
//		double[] err = new double[errorLabels.length];
//		int epoch = 0;
//		while ((NetUtil.errorsUnsatisfied(err) || epoch < 1) && epoch < maxEpcs) {
//			err = NetUtil.errorsF1bidirBAL(arch, dataX, dataY, net.epoch(dataX, dataY,true));
//			for (int i = 0; i < err.length; i++) {
//				plot.addPoint(i, epoch, err[i]);
//			}
//			epoch++;
//			if (epoch % 2 == 0) {
//				if (epoch % 10 == 0)
//					if (epoch % 50 == 0)
//						if (epoch % 100 == 0)
//							if (epoch % 500 == 0)
//								System.out.print(":\n");
//							else
//								System.out.print(";");
//						else
//							System.out.print(",");
//					else
//						System.out.print(".");
//				visRes.setPatterns(net.testEpochPatterns(dataX,dataY));
//			}
//		}
//		System.out.println();
//		System.out.println(epoch);
//		for (int i = 0; i < err.length; i++) {
//			System.out.println(errorLabels[i]+": "+err[i]);
//		}
//		System.out.println();
//		double[] testErrors = NetUtil.errorsF1bidirBAL(arch, dataX, dataY, net.epoch(dataX, dataY,false));
//		for (int i = 0; i < testErrors.length; i++) {
//			System.out.println(errorLabels[i]+": "+testErrors[i]);
//		}
//		plot.display();
	}
}
