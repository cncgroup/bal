package test.balnew.letters;

import base.BAL;
import base.Conf;
import base.NetUtil;
import test.helpers.BALTest;
import util.Util;
import util.visu.DataPairsRes;
import util.visu.Plot;

import java.io.IOException;

public class HeteroAssocLetters {

    private static final double NaN = Double.NaN;
    static String[] errorLabels = Conf.errorLabelsF1bidir;
	public static final int[] visParams = {7, 4, 10, 10};

	static double[][][] letters = Conf.lettersBinary7x7;
	static int side = 7;
	static int vectorSize = side*side;

//	static double lr = 1.3;
//	static double clampStrength = 1.0;

    static double[] lRates = {1.3,1.3};
    static double[] clampStrength     = {1.0,  0.5,  0.0}; // quasi-RBM + quasi-supervised
    static double[] estimateStrengthF = {NaN,  0.5,  0.5};
    static double[] estimateStrangthB =       {0.5,  0.5,  NaN};

	static int hids = 30;
	static int[] arch = {vectorSize,hids,vectorSize};
	static int maxEpcs = 100;

	public static void main(String[] args) throws IOException {	

		double[][] dataX = new double[letters.length][vectorSize];
		double[][] dataY = new double[letters.length][vectorSize];
		int[] indexer = Util.permutedIndexes(letters.length);
        for (int i = 0; i < letters.length; i++) {
//            dataX[i] = Util.flatten(Util.transpose(letters[i]));
//            dataY[i] = Util.flatten(Util.transpose(letters[indexer[i]]));
            dataX[i] = Util.flatten(letters[i]);
            dataY[i] = Util.flatten(letters[indexer[i]]);
        }

        BALTest test = new BALTest(1,maxEpcs);
        test.singleNetWithPlot(
                dataX, dataY, dataX, dataY, arch,
                NetUtil.initAllWeightsF(arch),
                NetUtil.initAllWeightsB(arch),
                lRates, clampStrength, estimateStrengthF, estimateStrangthB,
                visParams, true, false);

//        BAL net = new BAL(arch,Conf.wmin,Conf.wmax,lr,clampStrength);
//        Plot plot = new Plot(errorLabels);
//		double[][][] patterns = net.testEpochPatterns(dataX,dataY);
//		DataPairsRes visRes = new DataPairsRes(patterns,visParams);
//		visRes.display();
//		double[] err = new double[errorLabels.length];
//		int epoch = 0;
//		while ((NetUtil.errorsUnsatisfied(err) || epoch < 1) && epoch < maxEpcs) {
//			err = NetUtil.errorsF1bidirBAL(arch, dataX, dataY, net.epoch(dataX, dataY,true));
//			for (int i = 0; i < err.length; i++) {
//				plot.addPoint(i, epoch, err[i]);
//			}
//			epoch++;
//			if (epoch % 2 == 0) {
//				if (epoch % 10 == 0)
//					if (epoch % 50 == 0)
//						if (epoch % 100 == 0)
//							if (epoch % 500 == 0)
//								System.out.print(":\n");
//							else
//								System.out.print(";");
//						else
//							System.out.print(",");
//					else
//						System.out.print(".");
//				visRes.setPatterns(net.testEpochPatterns(dataX,dataY));
//			}
//		}
//		System.out.println();
//		System.out.println(epoch);
//		for (int i = 0; i < err.length; i++) {
//			System.out.println(errorLabels[i]+": "+err[i]);
//		}
//		System.out.println();
//		double[] testErrors = NetUtil.errorsF1bidirBAL(arch, dataX, dataY, net.epoch(dataX, dataY,false));
//		for (int i = 0; i < testErrors.length; i++) {
//			System.out.println(errorLabels[i]+": "+testErrors[i]);
//		}
//		plot.display();
	}
}
