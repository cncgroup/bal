package test.balnew.mns;

import base.BAL;
import base.Conf;
import base.NetUtil;
import data.DataRoboticMot;
import data.DataRoboticVis;
import test.helpers.BALTest;
import util.visu.DataPairsRes;
import util.visu.Plot;

import java.io.IOException;

public class AssocMSOMFirstPerspConti {

    private static final double NaN = Double.NaN;

	public static final String simName = "balold-msom-olddata-with-magic";
	public static final String[] errNames = {"mseF","mseB"};

	public static final int sizeVis = 16;
	public static final int sizeMot = 12;
	public static final int kVis = 16;
	public static final int kMot = 16;

    public static final double[] lRates = {1.2,1.2};
    public static final double[] clampStrength     = {0.5,  1.0,  0.0}; // quasi-RBM + quasi-supervised
    public static final double[] estimateStrengthF = {NaN,  0.0,  0.0};
    public static final double[] estimateStrangthB =       {0.0,  0.0,  NaN};
	public static final int hids = 200;
	public static final int maxEpcs = 100;

	public static final int[] visParams = {3, 9, 30, 10};

	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
//		dataVis.kWinnerTakeAll(kVis);
//		dataMot.kWinnerTakeAll(kMot);
//		dataVis.winDistExp();
//		dataMot.winDistExp();
		double[][] valuesMot = dataMot.values();
 		double[][] valuesVis = dataVis.valuesFirstPersp();
//		int[][] indices = dataVis.uniqPatIndices(dataMot.uniqPatIndices());
//		indices[1] = Util.duplicateAtIndex(indices[1], 0);
//		double[][] valuesMot = dataMot.valuesIndices(indices);
//		double[][] valuesVis = dataVis.valuesPerspIndices(0,indices);

		int[] arch = {valuesMot[0].length, hids, valuesVis[0].length};
        BALTest test = new BALTest(1,maxEpcs);
        test.singleNetWithPlot(
                valuesMot, valuesVis, valuesMot, valuesVis, arch,
                NetUtil.initAllWeightsF(arch),NetUtil.initAllWeightsB(arch),
                lRates, clampStrength, estimateStrengthF, estimateStrangthB,
                visParams, true, false);
	}
}
