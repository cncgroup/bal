package test.balnew.mns;

import base.BAL;
import base.Conf;
import base.NetUtil;
import data.DataRoboticMot;
import data.DataRoboticVis;
import test.helpers.BALTest;
import util.Util;
import util.visu.DataPairsRes;
import util.visu.Plot;

import java.io.IOException;

public class AssocMSOMAllPersp {

    private static final double NaN = Double.NaN;

	public static final String simName = "balold-msom-olddata-with-magic-allpersp";
	public static final String[] errNames = {"mseF","mseB","F1scoreF","F1scoreB","F1bidir"};
	public static final int[] visParams = {4, 12, 10, 30};

	public static final int rowHeight = 10;
	public static final int sizeVis = 14;//16;
	public static final int sizeMot = 8;//12;
	public static final int kVis = 16;//12;
	public static final int kMot = 16;//7;

    public static final double[] lRates = {.2,.2};
    public static final double[] clampStrength     = {0.5,  1.0,  0.0}; // quasi-RBM + quasi-supervised
    public static final double[] estimateStrengthF = {NaN,  0.0,  0.0};
    public static final double[] estimateStrangthB =       {0.0,  0.0,  NaN};
	public static final int hids = 160;
    public static final double wmax = -.1;//Conf.wmax;
    public static final double wmin = .1;//Conf.wmin;

    public static final int maxEpcs = 100;
	
	public static void main(String[] args) throws IOException {

		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
//		dataMot.multiply();
//		double[][] dataV = dataVis.values();
//		double[][] dataM = dataMot.values();
		int[][] indices = dataVis.uniqPatIndices(dataMot.uniqPatIndices());
		indices[1] = Util.duplicateAtIndex(indices[1], 0);
		double[][] dataV = dataVis.valuesAllPerspIndices(indices);
		double[][] dataM = new double[dataV.length][dataV[0].length];
		for (int i = 0; i < dataM.length; i++) {
			int[] coord =  dataVis.findIndex(dataV[i]);
			dataM[i] = dataMot.dataSeq()[coord[0]].get(coord[2]);
		}

		int[] arch = {dataM[0].length, hids, dataV[0].length};
        BALTest test = new BALTest(1,maxEpcs);
        test.singleNetWithPlot(
                dataM, dataV, dataM, dataV, arch,
                NetUtil.initAllWeightsF(arch),NetUtil.initAllWeightsB(arch),
                lRates, clampStrength, estimateStrengthF, estimateStrangthB,
                visParams, true, false);
	}
}
