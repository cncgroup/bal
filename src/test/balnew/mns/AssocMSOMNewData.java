package test.balnew.mns;

import base.Conf;
import data.DataRoboticMotNew;
import data.DataRoboticVisNew;
import test.helpers.BALTest;

import java.io.IOException;

public class AssocMSOMNewData {
	
	public static final String simName = "balold-msom-olddata-with-magic";
	public static final String[] errNames = Conf.errorLabelsF1;
	public static final int[] visParams = {4, 12, 10, 30};
	public static final int rowHeight = 10;
	public static final int kVis = 16;
	public static final int kMot = 12;
	public static final int hids = 200;
	public static final int maxEpcs = 200;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticMotNew dataMot = new DataRoboticMotNew("input/msomnew/1513336034_pmc.act", ",");
		DataRoboticVisNew dataVis = new DataRoboticVisNew("input/msomnew/1513336034_stsp.act",",");
		dataMot.kWinnerTakeAll(kMot);
		dataVis.kWinnerTakeAll(kVis);
		double[][] valuesVis = dataVis.valuesFirstPersp();
		double[][] valuesMot = dataMot.values();
//		DataPairsAllP vis = new DataPairsAllP(dataVis,kVis,dataMot,kMot);
//		vis.display();
//		int[][] indices = dataVis.uniqPatIndices(dataMot.uniqPatIndices());
//		indices[1] = Util.duplicateAtIndex(indices[1], 0);
//		double[][] valuesMot = dataMot.valuesIndices(indices);
//		double[][] valuesVis = dataVis.valuesPerspIndices(0,indices);

		int[] arch = {valuesMot[0].length, hids, valuesVis[0].length};
		BALTest test = new BALTest(1,maxEpcs);
		test.singleNetWithPlot(valuesMot, valuesVis, arch, Conf.lRate, Conf.clampStrength, visParams, false);
	}
}
