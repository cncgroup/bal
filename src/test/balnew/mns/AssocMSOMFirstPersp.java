package test.balnew.mns;

import base.Conf;
import base.NetUtil;
import data.DataRoboticMot;
import data.DataRoboticVis;
import test.helpers.BALTest;

import java.io.IOException;
import java.util.Random;

public class AssocMSOMFirstPersp {

	public static final String[] errNames = Conf.errorLabelsF1;
	public static final int sizeVis = 16;
	public static final int sizeMot = 12;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final double lRate = 0.2;
	public static final double clampStrength = 0.9;
	public static final int hids = 200;
	public static final int[] arch = {sizeMot*sizeMot,200,sizeVis*sizeVis};
	public static final int maxEpcs = 500;

	public static final int[] visParams = {4, 10, 30, 30};

	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
//		dataVis.softmax();
//		dataMot.softmax();
//		dataVis.winDistExp();
//		dataMot.winDistExp();
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		double[][] valuesMot = dataMot.values();
 		double[][] valuesVis = dataVis.valuesFirstPersp();
//		int[][] indices = dataVis.uniqPatIndices(dataMot.uniqPatIndices());
//		indices[1] = Util.duplicateAtIndex(indices[1], 0);
//		double[][] valuesMot = dataMot.valuesIndices(indices);
//		double[][] valuesVis = dataVis.valuesPerspIndices(0,indices);
//		DataPairs2 visData = new DataPairs2(3,10,dataVis,dataMot);
//		visData.display();
		BALTest test = new BALTest(1,maxEpcs);
		test.singleNetWithPlot(
				valuesMot, valuesVis, arch,
				NetUtil.initAllWeightsF(arch),
				NetUtil.initAllWeightsB(arch),
				lRate, clampStrength, visParams, false);
	}
}
