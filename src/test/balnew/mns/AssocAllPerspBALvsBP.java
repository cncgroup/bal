package test.balnew.mns;

import base.BAL;
import base.MLP;
import base.Conf;
import base.NetUtil;
import data.DataRoboticMot;
import data.DataRoboticVis;
import util.Util;
import util.visu.DataPairsRes;
import util.visu.Plot;

import java.io.IOException;
import java.util.Random;

public class AssocAllPerspBALvsBP {
	
	public static final String simName = "assoc-mlp-robotic-allpersp";
	public static final String[] errNames = Conf.errorLabelsF1bidir;
	public static final int[] visParams = {4, 12, 10, 30};
	public static final int mapSizeV = 14;
	public static final int mapSizeM = 8;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final int hids = 160;
	public static final int[] archF = new int[] {mapSizeM * mapSizeM, hids, mapSizeV * mapSizeV};
	public static final int[] archB = new int[] {mapSizeV * mapSizeV, hids, mapSizeM * mapSizeM};
	public static final double lRateBP = .1;
	public static final double lRateBal = .3;
	public static final double clampstrength = .88;
	public static final double wmax = -.1;
	public static final double wmin = .1;
//	public static final int epcsPhase1 = 500;
	public static final int epcs = 500;
	public static final int plotRes = 100;

	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+ mapSizeV +"_"+ mapSizeV +".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+ mapSizeM +"_"+ mapSizeM +".txt");
 		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
//		dataMot.multiply();
//		double[][] valuesVis = dataVis.values();
//		double[][] valuesMot = dataMot.values();
		int[][] indices = dataVis.uniqPatIndices(dataMot.uniqPatIndices());
		indices[1] = Util.duplicateAtIndex(indices[1], 0);
		double[][] valuesVis = dataVis.valuesAllPerspIndices(indices);
		double[][] valuesMot = new double[valuesVis.length][valuesVis[0].length];
		for (int i = 0; i < valuesMot.length; i++) {
			int[] coord =  dataVis.findIndex(valuesVis[i]);
			valuesMot[i] = dataMot.dataSeq()[coord[0]].get(coord[2]);
		}

		// shared init weights
		Random rand = new Random(System.currentTimeMillis());
		double[][][] weightsF = NetUtil.initAllWeightsF(archF, wmin, wmax);
		double[][][] weightsB = NetUtil.initAllWeightsB(archF, wmin, wmax);

		// 2 BP NETS
		Plot plot = new Plot(Util.merge(errNames,errNames));
		MLP mlpF = new MLP(archF, weightsF, lRateBP);
		MLP mlpB = new MLP(archB, NetUtil.reverseWeights(weightsB), lRateBP);
		int epoch = 0;
		System.out.println("2xMLP "+NetUtil.archToStr(archF)+" "+NetUtil.archToStr(archB));
		double[] trainErrors = new double[errNames.length];
		while (epoch < epcs) {
			double[][][] activationNetF = mlpF.epoch(valuesMot, valuesVis, true);
			double[][][] activationNetB = mlpB.epoch(valuesVis, valuesMot, true);
			trainErrors = NetUtil.errorsF1bidirBP(activationNetF, activationNetB, valuesMot, valuesVis);
			for (int i = 0; i < trainErrors.length; i++) {
				plot.addPoint(i, epoch, trainErrors[i]);
			}
			if (epoch % 50 == 0)
				System.out.print(".");
			epoch++;
		}
		System.out.println();
		System.out.println("errors train:");
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(errNames[i]+": "+trainErrors[i]);
		}
		System.out.println("errors test:");
		double[][][] activationNetFTest = mlpF.epoch(valuesMot, valuesVis, false);
		double[][][] activationNetBTest = mlpB.epoch(valuesVis, valuesMot, false);
		double[] testErrors = NetUtil.errorsF1bidirBP(activationNetFTest, activationNetBTest, valuesMot, valuesVis);
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(errNames[i]+": "+testErrors[i]);
		}
		double[][][] patternsBP = Util.merge(mlpF.testEpochPatternsBinary(valuesMot,valuesVis),mlpB.testEpochPatternsBinary(valuesVis,valuesMot));
		DataPairsRes visResBP = new DataPairsRes(patternsBP,visParams);
		visResBP.display();

		// 1 BAL NET
		BAL netBal = new BAL(archF, weightsF, weightsB, lRateBal, clampstrength);
		System.out.println("architecture: " + NetUtil.archToStr(archF) + " LR: "+lRateBal);
		epoch = 0;
		trainErrors = new double[errNames.length];
		while (epoch < epcs) {
			BAL.Activations act[] = netBal.epoch(valuesMot, valuesVis,true);
			trainErrors = NetUtil.errorsF1bidirBAL(archF, valuesMot, valuesVis, act);
		 	for (int i = 0; i < trainErrors.length; i++) {
				plot.addPoint(errNames.length+i, epoch, trainErrors[i]);
			}
			if (epoch % 50 == 0)
				System.out.print(".");
			epoch++;
		}
		System.out.println();
		System.out.println("errors train:");
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(errNames[i]+": "+trainErrors[i]);
		}
		System.out.println("errors test:");
		BAL.Activations actTest[] = netBal.epoch(valuesMot, valuesVis,false);
		testErrors = NetUtil.errorsF1bidirBAL(archF, valuesMot, valuesVis, actTest);
		 for (int i = 0; i < testErrors.length; i++) {
			System.out.println(errNames[i]+": "+testErrors[i]);
		}
		double[][][] patternsBal = netBal.testEpochPatterns(valuesMot,valuesVis);
		DataPairsRes visResBal = new DataPairsRes(patternsBal,visParams);
		visResBal.display();
//		visRes.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-patterns"+Conf.figExt);

		plot.display();
	}
}
