package test.balnew.mns;

import base.BAL;
import base.Conf;
import base.NetUtil;
import data.DataRoboticMot;
import data.DataRoboticVis;
import util.Util;
import util.visu.DataPairsRes;
import util.visu.Plot;

import java.io.IOException;

public class AssocUMotUVisLearning {
	
	public static final String simName = "balold-learning-unique-allpersp";
//	public static final String[] errNames = Conf.errorLabelsF1;
	public static final String[] errNames = {"mseF","mseB","F1scoreF","F1scoreB","F1bidir"};
	public static final int rowHeight = 10;
	public static final int sizeVis = 14;
	public static final int sizeMot = 8;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final double lRate = 0.2;
	public static final double clampStrength = 0.88;//1.0;
	public static final double wmax = -.1;
	public static final double wmin = .1;
	public static final int hids = 160;
	public static final int epcsPhase1 = 200;
	public static final int epcsPhase2 = 2000;
	public static final int plotRes = 100;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		
		int[][] indices = dataVis.uniqPatIndices(dataMot.uniqPatIndices());
 		double[][] valuesMot = dataMot.valuesIndices(indices);
 		double[][] valuesVis = dataVis.valuesPerspIndices(0,indices);
 		double[][] valuesMot2 = Util.merge(valuesMot, Util.merge(valuesMot, Util.merge(valuesMot, valuesMot)));
 		double[][] valuesVis2 = Util.merge(valuesVis,Util.merge(dataVis.valuesPerspIndices(1,indices),Util.merge(
 				dataVis.valuesPerspIndices(2,indices),dataVis.valuesPerspIndices(3,indices))));

		int[] arch = {valuesMot[0].length, hids, valuesVis[0].length};
		BAL net = new BAL(arch,lRate,wmax,wmin,clampStrength);
		Plot plot = new Plot(errNames);
		int epoch1 = 1;
		int epochAbs = 1;
		double[] trainErr1 = new double[errNames.length];

		System.out.println("phase one: first perspective ("+epcsPhase1+" epcs)");

		while (epoch1 < epcsPhase1) {
			BAL.Activations[] act = net.epoch(valuesMot, valuesVis,true);
			trainErr1 = NetUtil.errorsF1bidirBAL(arch, valuesMot, valuesVis, act);
			if (epoch1 % 5 == 0) {
				for (int i = 0; i < trainErr1.length; i++) {
					plot.addPoint(i, epochAbs+1, trainErr1[i]);
				}
				System.out.print(".");
			}
			epoch1++;
			epochAbs++;
		}
		System.out.println();
//		System.out.println("epcs: "+epoch1);
		BAL.Activations act[] = net.epoch(valuesMot, valuesVis,false);
		double[] testErr1 = NetUtil.errorsF1bidirBAL(arch, valuesMot, valuesVis, act);
		for (int i = 0; i < errNames.length; i++) {
			System.out.println(errNames[i]+"\t\t"+trainErr1[i]+"\t\t"+testErr1[i]);
		}
		System.out.println();

		System.out.println("phase two: other perspectives ("+epcsPhase2+" epcs)");
		double[] trainErr2 = new double[errNames.length];
		int epoch2 = 0;
		while (epoch2 < epcsPhase2) {
			act = net.epoch(valuesMot2, valuesVis2,true);
			trainErr2 = NetUtil.errorsF1bidirBAL(arch, valuesMot2, valuesVis2, act);
			if ((epoch2+1) % plotRes == 0) {
				for (int i = 0; i < errNames.length; i++) {
					plot.addPoint(i, epochAbs+1, trainErr2[i]);
				}
			}
			if (epoch1 % 500 == 0)
				System.out.print(".");
			epoch2++;
			epochAbs++;
		}
		System.out.println();
//		System.out.println("epcs: "+epoch2);
		System.out.println("train errors:");
		act = net.epoch(valuesMot2, valuesVis2,false);
		double[] testErr2 = NetUtil.errorsF1bidirBAL(arch, valuesMot2, valuesVis2, act);
		for (int i = 0; i < errNames.length; i++) {
			System.out.println(errNames[i]+"\t\t"+trainErr2[i]+"\t\t"+testErr2[i]);
		}

		double[][][] patterns = net.testEpochPatterns(valuesMot2,valuesVis2);
		DataPairsRes visRes = new DataPairsRes(patterns);
		if (valuesMot2.length > 100) {
			visRes.setPxs(Conf.pxsAllP);
			visRes.setRowH(Conf.rowHAllP);
		} else {
			visRes.setRowH(rowHeight);
		}
		visRes.display();
		plot.display();
		plot.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-performance"+Conf.figExt);
//		visRes.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-patterns2"+Conf.figExt);
	}
}
