package test.balnew.encoder424;

import base.BAL;
import base.Conf;
import base.NetUtil;
import util.visu.Plot;

import java.io.IOException;

public class Encoder424 {
	public static final int maxEpcs = 5000;
	public static final int[] arch = {4,2,4};
	public static final double lRate = 1;//Conf.lRate;
//	public static final double clampStrength = 1.0;
    public static final double[] clampStrength     = {1.0, .5, 0.0}; // quasi-RBM + quasi-supervised
    public static final double[] estimateStrangthF = {0.0, 0.5, 0.5};
    public static final double[] estimateStrangthB = {0.5, 0.5, 0.0};
//    public static final double[] clampStrength     = {1.0, .5, 0.0}; // quasi-RBM + quasi-supervised
//    public static final double[] estimateStrengthF = {0.0, 0.0, 0.0};
//    public static final double[] estimateStrangthB = {0.0, 0.0, 0.0};
	public static final double wmin = -1.0;//Conf.wmin;
	public static final double wmax = 1.0;//Conf.wmax;
	public static final double[][] inputs = Conf.inputs424;
	
	public static void main(String[] args) throws IOException {
        BAL net = new BAL(arch,wmin,wmax,lRate,clampStrength, estimateStrangthF, estimateStrangthB);
		Plot plot = new Plot(Conf.errorLabels);
		double[] trainErrors = new double[6];
		int epoch = 0;
		int succepochs = 0;
		while (succepochs < 10 && epoch < maxEpcs) {
			trainErrors = NetUtil.errorsF1bidirBAL(arch, inputs, inputs, net.epoch(inputs, inputs,true));
			for (int i = 0; i < trainErrors.length; i++) {
				plot.addPoint(i, epoch, trainErrors[i]);
			}
			epoch++;
			if (trainErrors[2] == 1.0 && trainErrors[3] == 1.0)
				succepochs++;
			else
				succepochs = 0;
		}
		System.out.println();
		System.out.println(epoch);
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+trainErrors[i]);
		}
		plot.display();
		double[] testErrors = NetUtil.errorsF1bidirBAL(arch, inputs, inputs, net.epoch(inputs, inputs,false));
		System.out.println();
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
	}
}
