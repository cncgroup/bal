package test.balnew.encoder424;

import base.BAL;
import base.Conf;
import base.NetUtil;
import util.TexLog;
import util.Util;

import java.io.IOException;

public class Enc424LRate {

	public static final String logName = "log-newbal-424-lrate.tex";
	public static final String[] errNm = Conf.errorLabelsF1bidir;

	public static final int maxEpcs = 1000;
	public static final int netc = 1000;
	public static final int[] arch = {4,2,4};
	public static final double clampStrength = 1.0;
	public static final double wmax = -.1;
	public static final double wmin = .1;
	public static final double[][] inputs = Conf.inputs424;
	
	public static void main(String[] args) throws IOException {
		double[] lRates = new double[30];
		for (int i = 0; i < lRates.length; i++) {
			lRates[i] = Util.round(0.1 + 0.1*i,3);
		}
		int size = lRates.length;
		
		double epcs[][] = new double[size][netc];
		double nets[] = new double[size];
		// run tests			
		for (int l = 0; l < lRates.length; l++) {
			System.out.println("testing learning rate: "+lRates[l]);
			for (int n = 0; n < netc; n++) {
				BAL net = new BAL(arch,lRates[l],wmin,wmax,clampStrength);
				double[] errors = new double[errNm.length];
				int epoch = 0;
				int succepochs = 0;
				while (succepochs < 10 && epoch < maxEpcs) {
					net.epoch(inputs, inputs, true);
					errors = NetUtil.errorsF1bidirBAL(arch, inputs, inputs, net.epoch(inputs, inputs,false));
					epoch++;
					if (errors[2] == 1.0 && errors[3] == 1.0)
						succepochs++;
					else
						succepochs = 0;
				}
				if (errors[2] == 1.0 && errors[3] == 1.0) {
					nets[l]++;
				}
				epcs[l][n] = epoch*1.0;
//				System.out.print(".");
			}
//			System.out.println();
		}
		// compute write results
		String[][] resultsTable = new String[2][3];
		
		resultsTable[0][0] = "mean and std";
		double[] mnstd = Util.MeanAndStD(nets);
		resultsTable[0][1] = Util.round(mnstd[0],3)+" $\\pm$ "+Util.round(mnstd[1],3);
		mnstd = Util.MeanAndStD(Util.flatten(epcs));
		resultsTable[0][2] = Util.round(mnstd[0],3)+" $\\pm$ "+Util.round(mnstd[1],3);
		
		resultsTable[1][0] = "maximum";
		int mi = Util.maxIndex(nets);
		resultsTable[1][1] = Math.round(nets[mi])+" learning rate: "+lRates[mi];
		mi = Util.maxIndex(Util.flatten(epcs));
		int mlr = mi/netc;
		int mind = mi%netc;
		resultsTable[1][2] = Math.round(epcs[mlr][mind])+" learning rate: "+lRates[mlr];
		
		String[][] plotDataNets = new String[1][size];
		String[][] plotDataEpochs = new String[1][size];
		for (int l = 0; l < size; l++) {
			plotDataNets[0][l] = "("+lRates[l]+","+nets[l]+")";
			double[] meannstd = Util.MeanAndStD(epcs[l]);
			plotDataEpochs[0][l] = "("+lRates[l]+","+meannstd[0]+") +- (0,"+meannstd[1]+")";
		}
		
		TexLog log = new TexLog();
		log.addLine("Testing learning rate: "+netc+" nets per batch. Success criterion: pattern success at 100\\% in " +
				"10 successive epochs. Maximum "+epcs+" epochs.");
		log.addLine("Other net params: weights from range <"+wmin+","+wmax+"> (Gaussian distribution). Clamping strength: "+clampStrength);
		log.addResultsTable(
				new String[] {"","nets","epochs"}, 
				resultsTable, 
				"Encoder 4-2-4: comparing learning rates", 
				"tab:424-lrate"
		);
		log.addLine("\\begin{figure}");
		log.addLine("\\begin{subfigure}[b]{.45\\textwidth}");
		log.addPlot(plotDataNets,null,"learning rate","nets successful");
		log.addLine("\\end{subfigure}");
		log.addLine("\\begin{subfigure}[b]{.45\\textwidth}");
		log.addPlotErrBars(plotDataEpochs,null,"learning rate","training epochs");
		log.addLine("\\end{subfigure}");
		log.addLine("\\caption{Encoder 4-2-4: experiments with learning rate}");
		log.addLine("\\label{fig:balold-encoder424-learningrate}");
		log.addLine("\\end{figure}");
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
}
