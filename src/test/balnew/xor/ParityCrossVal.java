package test.balnew.xor;

import base.BAL;
import base.Conf;
import base.NetUtil;
import util.Util;
import util.visu.DataPairsRes;
import util.visu.Plot;

import java.io.IOException;
import java.util.Arrays;

public class ParityCrossVal {

    public static final int[] visParams = {5, 30, 10, 30};
	public static final String[] errNames = Conf.errorLabelsBal; // Conf.errorLabels[i]

//	public static final double[] lRate = {0.5};                  // pregenerated separation + quasi-supervised layer
//	public static final double[] clampStrength     = {1.0, 0.0};
//	public static final double[] estimateStrengthF = {0.0, 1.0};
//	public static final double[] estimateStrangthB = {0.0, 0.0};

//	public static final double[] lRate = {0.01, 0.1};                  // pregenerated separation + quasi-supervised layer
//	public static final double[] clampStrength     = {0.0, 1.0, 0.0};
//	public static final double[] estimateStrengthF = {0.0, 0.0, 1.0};
//	public static final double[] estimateStrangthB = {1.0, 0.0, 0.0};

	public static final double lRate = 1;//1e200;//Conf.lRate;
	public static final double[] clampStrength     = {0.0, 1.0, 0.0}; // quasi-RBM + quasi-supervised
	public static final double[] estimateStrangthF = {0.0, 0.0, 0.0};
	public static final double[] estimateStrangthB = {0.0, 0.0, 0.0};

	public static final double wmax = 3.0;//Conf.wmax;
	public static final double wmin =-1.0;//Conf.wmin;

    public static final int insize = 8;//Conf.wmin;
	public static final double[][] inputs = parityInputs(insize);
	public static final double[][] outputs = parityOutputs(insize);
    public static final int k = 5;//Conf.wmin;

	public static final int inps = inputs[0].length;
	public static final int hids = 100;
	public static final int outs = outputs[0].length;
	public static final int[] arch = new int[] {inps,hids,outs};
//	public static final int[] arch = new int[] {inps,outs};

    public static final int maxEpcs = 10000;//20000;

    public static double[][] parityInputs(int n) {
        double[][] out = new double[1 << n][n];
        for (int i=0; i<out.length; i++) {
            for (int j=0; j<n; j++) {
                out[i][j] = 1.0*((i >> j) & 1);
            }
        }
        return out;
    }

    public static double[][] parityOutputs(int n) {
        double[][] out = new double[1 << n][1];
        for (int i=0; i<out.length; i++) {
            int x = 0;
            for (int j=0; j<n; j++) {
                x ^= ((i >> j) & 1);
            }
            out[i][0] = 1.0 * x;
        }
        return out;
    }

    public static void main(String[] args) throws IOException {

        System.out.println("Parity problem with "+insize+" bits");

//        for (int sim = 0; sim < 1; sim++) {
            BAL net = new BAL(arch, wmin, wmax, lRate, clampStrength, estimateStrangthF, estimateStrangthB);
            Plot plot = new Plot(errNames, true);

            int[] indexer = Util.permutedIndexes(inputs.length);
            double[][] inputsShuffled = Util.rearrange(inputs, indexer);
            double[][] outputsShuffled = Util.rearrange(outputs, indexer);

            double[][] trainInps = Util.subArray(inputsShuffled, 0, (inputs.length/k)*(k-1)-1);
            double[][] trainOuts = Util.subArray(outputsShuffled, 0, (outputs.length/k)*(k-1)-1);
            double[][] testInps = Util.subArray(inputsShuffled, (inputs.length/k)*(k-1), inputs.length-1);
            double[][] testOuts = Util.subArray(outputsShuffled, (inputs.length/k)*(k-1), inputs.length-1);

            double[][][] patternsBal = net.testEpochPatterns(trainInps, trainOuts);
            DataPairsRes visResBal = new DataPairsRes(patternsBal, visParams);
            visResBal.display();

            double[] trainErrors = new double[errNames.length];
            double[] testErrors = new double[errNames.length];
            int epoch = 0;
            int succepochs = 0;
            while (succepochs < 100 && epoch < maxEpcs) {
                trainErrors = NetUtil.errorsBAL(arch, trainInps, trainOuts, net.epoch(trainInps, trainOuts,true));
                testErrors = NetUtil.errorsBAL(arch, testInps, testOuts, net.epoch(testInps, testOuts,false));
                for (int i = 0; i < testErrors.length; i++) {
                    plot.addPoint(i, epoch, testErrors[i]);
                }
//                for (int i = 0; i < trainErrors.length; i++) {
//                    plot.addPoint(i, epoch, trainErrors[i]);
//                }
                epoch++;
                if (epoch%10 == 0)
                    visResBal.setPatterns(net.testEpochPatterns(trainInps, trainOuts));

                //			if (trainErrors[2] == 1.0 && trainErrors[3] == 1.0)
                if (testErrors[2] == 1.0) // test.bitSuccF
                    succepochs++;
                else
                    succepochs = 0;
            }
            System.out.println("Net: " + NetUtil.archToStr(arch));
            System.out.println();
            System.out.println("TRAIN: "+trainInps.length+" patterns");

            visResBal.setPatterns(net.testEpochPatterns(trainInps, trainOuts));

            for (int i = 0; i < trainErrors.length; i++) {
                System.out.println(errNames[i]+": "+trainErrors[i]);
            }
            plot.display();

            // TEST
            System.out.println();
            System.out.println("TEST: "+testInps.length+" patterns");
            BAL.Activations[] act = net.epoch(testInps, testOuts,false);
            //		double[] testErrors = NetUtil.errorsF1bidirBAL(arch, inputs, outputs, net.epoch(inputs,outputs,false));
            testErrors = NetUtil.errorsBAL(arch, testInps, testOuts, act);
            for (int i = 0; i < testErrors.length; i++) {
                System.out.println(errNames[i]+": "+testErrors[i]);
            }
            System.out.println();
            for (int i = 0; i < act.length; i++) {
                System.out.println(
                        Arrays.toString(testInps[i])+" ==>> "+Arrays.toString(testOuts[i])+" ... "+
                                Arrays.toString(act[i].actBPrediction()[0])+" ~~~> "+Arrays.toString(act[i].actFPrediction()[arch.length-1])+"");
            }
//        }

	}
}
