package test.balnew.xor;

import base.BAL;
import base.Conf;
import base.NetUtil;
import util.TexLog;
import util.Util;

import java.io.IOException;
import java.util.ArrayList;

public class XORBatch {

	public static final String logName = "balnew-XOR-batch-errors.tex";
	public static final String[] errNm = Conf.errorLabelsF1;

	public static final int maxEpcs = 30;//0000;
	public static final int netCount = 1000;
	public static final int plotRes = 500;
	public static final int plotTics = maxEpcs/plotRes+1;

    public static final double lRate = .2;//Conf.lRate;
    public static final double[] clampStrength     = {0.0, 1.0, 0.0}; // quasi-RBM + quasi-supervised
    public static final double[] estimateStrangthF = {0.0, 0.0, 0.0};
    public static final double[] estimateStrangthB = {0.0, 0.0, 0.0};

    public static final double wmax = 3.0;//Conf.wmax;
    public static final double wmin =-1.0;//Conf.wmin;

    public static final double[][] inputs = Conf.inputsXOR;
    public static final double[][] outputs = Conf.outputsXOR;

    public static final int inps = inputs[0].length;
    public static final int hids = 7;
    public static final int outs = outputs[0].length;
    public static final int[] arch = new int[] {inps,hids,outs};
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException {
		
		ArrayList<Double>[] errData = new ArrayList[errNm.length];
		for (int i = 0; i < errData.length; i++) {
			errData[i] = new ArrayList<Double>();
		}
		ArrayList<Double>[][] plotDataRaw = new ArrayList[plotTics][errNm.length];
		for (int i = 0; i < plotDataRaw.length; i++) {
			for (int j = 0; j < plotDataRaw[i].length; j++) {
				plotDataRaw[i][j] = new ArrayList<Double>();
			}
		}
		int netsSuccessful = 0;
		// run tests
		for (int n = 0; n < netCount; n++) {
			BAL net = new BAL(arch,wmin,wmax,lRate,clampStrength, estimateStrangthF, estimateStrangthB);
			double[][] tmp = new double[plotTics][errNm.length];
			double[] errors = new double[errNm.length];
			tmp[0] = NetUtil.errorsBAL(arch, inputs, outputs, net.epoch(inputs, outputs,false));
			int ind = 1;
			int epoch = 0;
			int succepochs = 0;
			while (succepochs < 10 && epoch < maxEpcs) {
				errors = NetUtil.errorsBAL(arch, inputs, outputs, net.epoch(inputs, outputs,true));
				if ((epoch+1) % plotRes == 0) {
					tmp[ind] = errors;
					ind++;
				}
				epoch++;
				if (errors[2] == 1.0 && errors[3] == 1.0)
					succepochs++;
				else
					succepochs = 0;
			}
			System.out.print(".");
			if ((n+1) % 100 == 0)
				System.out.println();
			//count only succ
//			if (errors[2] == 1.0 && netsSuccessful < 50) {
			if (errors[2] == 1.0) {
				netsSuccessful++;
			}
			for (int i = 0; i < errNm.length; i++) {
				errData[i].add(errors[i]);
			}
			for (int i = 0; i < tmp.length; i++) {
				for (int j = 0; j < errNm.length; j++) {
					plotDataRaw[i][j].add(tmp[i][j]);
				}
			}
		}
		System.out.println();
//		System.out.println(plotDataRaw[plotTics-1][4].size());
		System.out.println(netsSuccessful);
		System.out.println();
		// result table
		double[] meanstd = new double[2];
		String[][] resultsTable = new String[errNm.length][2];
		for (int i = 0; i < errNm.length; i++) {
			resultsTable[i][0] = errNm[i];
			meanstd = Util.MeanAndStD(errData[i]);
			resultsTable[i][1] = Util.round(meanstd[0],3)+" +- "+Util.round(meanstd[1],2);
		}
		for (int i = 0; i < resultsTable.length; i++) {
			for (int j = 0; j < resultsTable[0].length; j++) {
				System.out.print(resultsTable[i][j]+"\t");
			}
			System.out.println();
		}
//		// plots
//		String[][] plotData = new String[errNm.length][maxEpcs];
//		for (int i = 0; i < errNm.length; i++) {
//			for (int j = 0; j < plotTics; j++) {
//				meanstd = Util.MeanAndStD(plotDataRaw[j][i]);
//				plotData[i][j] = "("+(j*plotRes)+","+meanstd[0]+") +- (0,"+meanstd[1]+")";
//			}
//		}
		// write log
//		TexLog log = new TexLog();
//		log.addResultsTable(
//				new String[] {"performance measure","value"},
//				resultsTable,
//				"Encoder 424: results",
//				"tab:424-results"
//		);
//		log.addLine("");
//		log.addLine("\\begin{figure}[!htbp]");
//		log.addLine("\\centering");
//		log.addPlotPaper(plotData,errNm,"epoch","performance measure");
//		log.addLine("\\caption{Encoder 4-2-4: development of errors over time}");
//		log.addLine("\\label{fig:424-results}");
//		log.addLine("\\end{figure}");
//		log.addLine("");
////		log.echo();
//		log.export(Conf.pathOutput+logName);
//		System.out.println("Finished writing \""+logName+"\"");
	}
}
