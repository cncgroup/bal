package test.balnew.xor;

import base.BAL;
import base.Conf;
import base.NetUtil;
import util.visu.DataPairsRes;
import util.visu.Plot;

import java.io.IOException;
import java.util.Arrays;

public class Parity {
	public static final int maxEpcs = 20000;

    public static final int[] visParams = {3, 20, 10, 30};
	public static final String[] errNames = Conf.errorLabelsF1; // Conf.errorLabels[i]

//	public static final double[] lRate = {0.5};                  // pregenerated separation + quasi-supervised layer
//	public static final double[] clampStrength     = {1.0, 0.0};
//	public static final double[] estimateStrengthF = {0.0, 1.0};
//	public static final double[] estimateStrangthB = {0.0, 0.0};

//	public static final double[] lRate = {0.01, 0.1};                  // pregenerated separation + quasi-supervised layer
//	public static final double[] clampStrength     = {0.0, 1.0, 0.0};
//	public static final double[] estimateStrengthF = {0.0, 0.0, 1.0};
//	public static final double[] estimateStrangthB = {1.0, 0.0, 0.0};

	public static final double lRate = 1;//1e200;//Conf.lRate;
	public static final double[] clampStrength     = {0.0, 1.0, 0.0}; // quasi-RBM + quasi-supervised
	public static final double[] estimateStrangthF = {0.0, 0.0, 0.0};
	public static final double[] estimateStrangthB = {0.0, 0.0, 0.0};

	public static final double wmax = 3.0;//Conf.wmax;
	public static final double wmin =-1.0;//Conf.wmin;

    public static final int insize = 6;//Conf.wmin;
	public static final double[][] inputs = parityInputs(insize);
	public static final double[][] outputs = parityOutputs(insize);

	public static final int inps = inputs[0].length;
	public static final int hids = 1327;
	public static final int outs = outputs[0].length;
	public static final int[] arch = new int[] {inps,hids,outs};
//	public static final int[] arch = new int[] {inps,outs};

    public static double[][] parityInputs(int n) {
        double[][] out = new double[1 << n][n];
        for (int i=0; i<out.length; i++) {
            for (int j=0; j<n; j++) {
                out[i][j] = 1.0*((i >> j) & 1);
            }
        }
        return out;
    }

    public static double[][] parityOutputs(int n) {
        double[][] out = new double[1 << n][1];
        for (int i=0; i<out.length; i++) {
            int x = 0;
            for (int j=0; j<n; j++) {
                x ^= ((i >> j) & 1);
            }
            out[i][0] = 1.0 * x;
        }
        return out;
    }

    public static void main(String[] args) throws IOException {
//		BAL net = new BAL(arch,wmin,wmax,BAL.paramSame(lRate,arch.length-1),clampStrength, estimateStrengthF, estimateStrangthB);
		BAL net = new BAL(arch,wmin,wmax,lRate,clampStrength, estimateStrangthF, estimateStrangthB);
		Plot plot = new Plot(errNames, true);

		double[][][] patternsBal = net.testEpochPatterns(inputs, outputs);
		DataPairsRes visResBal = new DataPairsRes(patternsBal, visParams);
		visResBal.display();

		double[] trainErrors = new double[errNames.length];
		int epoch = 0;
		int succepochs = 0;
		while (succepochs < 100 && epoch < maxEpcs) {
//			trainErrors = NetUtil.errorsF1bidirBAL(arch, inputs, outputs, net.epoch(inputs,outputs,true));
			trainErrors = NetUtil.errorsBAL(arch, inputs, outputs, net.epoch(inputs, outputs,true));
			for (int i = 0; i < trainErrors.length; i++) {
				plot.addPoint(i, epoch, trainErrors[i]);
			}
			epoch++;
			if (epoch%10 == 0)
				visResBal.setPatterns(net.testEpochPatterns(inputs, outputs));

//			if (trainErrors[2] == 1.0 && trainErrors[3] == 1.0)
			if (trainErrors[2] == 1.0)
				succepochs++;
			else
				succepochs = 0;
		}
		System.out.println();
		System.out.println(epoch);

		visResBal.setPatterns(net.testEpochPatterns(inputs, outputs));

		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(errNames[i]+": "+trainErrors[i]);
		}
		plot.display();
		BAL.Activations[] act = net.epoch(inputs,outputs,false);
//		double[] testErrors = NetUtil.errorsF1bidirBAL(arch, inputs, outputs, net.epoch(inputs,outputs,false));
		double[] testErrors = NetUtil.errorsBAL(arch, inputs, outputs, act);
		System.out.println();
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(errNames[i]+": "+testErrors[i]);
		}

		for (int i = 0; i < act.length; i++) {
			System.out.println(
					Arrays.toString(inputs[i])+" ==>> "+Arrays.toString(outputs[i])+" ... "+
					Arrays.toString(act[i].actBPrediction()[0])+" ~~~> "+Arrays.toString(act[i].actFPrediction()[arch.length-1])+"");
		}

	}
}
