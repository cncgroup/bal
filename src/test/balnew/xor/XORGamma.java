package test.balnew.xor;

import base.BAL;
import base.Conf;
import base.NetUtil;
import util.TexLog;
import util.Util;

import java.io.IOException;
import java.util.Arrays;

public class XORGamma {

//    public static final String paramNameFull = "gamma (prediction-vs-echo rate)";
//    public static final String paramName     = "gamma";
    public static final String paramNameFull = "input-beta-gamma (RBM 0 vs 1 HEBB)";
    public static final String paramName     = "input-beta-gamma";

    public static final String logName = "log-newbal-XOR-beta-gama-inp.tex";
	public static final String[] errNm = {"mse","pattern success", "epcs"};

	public static final int netc = 1000;
    public static final int maxEpcs = 5000;
    public static final int sufficientSuccEpcs = 100;

//    public static final double[] gam = {0.0, 0.1, 0.3, 0.5, 0.7, 0.9, 1.0}; //Conf.wmax;
    public static final double[] gam = {0.0, 0.001, 0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.99, 0.999, 1.0}; //Conf.wmax;
    public static final double[] clampStrength     = {0.0, 1.0, 0.0}; // quasi-RBM + supervised resonator
    public static final double[] estimateStrengthF = {0.0, 0.0, 0.0};
    public static final double[] estimateStrengthB = {0.0, 0.0, 0.0};

    public static final double wmax = 3.0;//Conf.wmax;
    public static final double wmin =-1.0;//Conf.wmin;

    public static final double[][] inputs = Conf.inputsXOR;
    public static final double[][] outputs = Conf.outputsXOR;

    public static final int inps = inputs[0].length;
    public static final int hids = 20;
//    public static final int[] hids = {2, 3, 4, 5, 6, 7, 8};
//    public static final int[] hids = {6, 7, 8, 9, 10, 12, 15, 20};
//    public static final int[] hids = {20, 30, 50, 100, 200};
//    public static final int[] hids = {200, 500, 1000, 2000};
    public static final int outs = outputs[0].length;
    public static final int[] arch = {inps, hids, outs};

//    public static final double[] lRates = {0.2,2.0};
    public static final double lRate = 2.0;

    public static void makeLRates(int count) {
        double[] lRates = new double[count];
        for (int i = 0; i < lRates.length; i++) {
            lRates[i] = Util.round(0.1 + 0.1*i,3);
        }

    }

	public static void main(String[] args) throws IOException {

        int size = gam.length;
		double epcs[][] = new double[size][netc];
		double nets[] = new double[size];
		// run tests			
		for (int l = 0; l < gam.length; l++) {
			System.out.println("testing "+paramName+": "+gam[l]);
			for (int n = 0; n < netc; n++) {
//                double[] estimateStrangthF = {0.0, 0.0, gam[l]}; // outputs
//                double[] estimateStrangthB = {gam[l], 0.0, 0.0};
//                double[] estimateStrangthF = {0.0, 0.0, 0.0}; // hid_back
//                double[] estimateStrangthB = {0.0, gam[l], 0.0};
//                double[] estimateStrangthF = {0.0, 0.0, gam[l]}; // out+hid_back
//                double[] estimateStrangthB = {gam[l], gam[l], 0.0};
//                double[] estimateStrengthF = {0.0, gam[l], 0.0}; // hid_forw
//                double[] estimateStrengthB = {0.0, 0.0, 0.0};
//                double[] estimateStrengthF = {gam[l], 1.0, 0.0}; // input
//                double[] estimateStrengthB = {0.0, 0.0, 0.0};
                double[] clampStrength     = {gam[l], 1.0, 0.0}; // beta-gamma-inp
                double[] estimateStrengthF = {0.0,    0.0, 0.0};
                double[] estimateStrengthB = {gam[l], 0.0, 0.0};
                BAL net = new BAL(arch,wmin,wmax,lRate,clampStrength, estimateStrengthF, estimateStrengthB);
				double[] errors = new double[errNm.length];
				int epoch = 0;
				int succepochs = 0;
				while (succepochs < sufficientSuccEpcs && epoch < maxEpcs) {
					net.epoch(inputs, outputs, true);
					errors = NetUtil.errorsBAL(arch, inputs, outputs, net.epoch(inputs, outputs,false));
					epoch++;
					if (errors[2] == 1.0) {
                        succepochs++;
//                        System.out.print('.');
                    }
                    else {
//					    if (succepochs>0)
//                            System.out.print(':');
                        succepochs = 0;
                    }
				}
				if (errors[2] == 1.0) {
					nets[l]++;
                    System.out.print("+");
				}
				else
                    System.out.print("-");
				epcs[l][n] = epoch-sufficientSuccEpcs+1.0;
//				System.out.print(".");
			}
			System.out.println("succ: " + nets[l] + "nets");
		}
		// compute write results
		String[][] resultsTable = new String[2][3];
		
		resultsTable[0][0] = "mean and std";
		double[] mnstd = Util.MeanAndStD(nets);
		resultsTable[0][1] = Util.round(mnstd[0],3)+" $\\pm$ "+Util.round(mnstd[1],3);
		mnstd = Util.MeanAndStD(Util.flatten(epcs));
		resultsTable[0][2] = Util.round(mnstd[0],3)+" $\\pm$ "+Util.round(mnstd[1],3);
		
		resultsTable[1][0] = "maximum";
		int mi = Util.maxIndex(nets);
		resultsTable[1][1] = Math.round(nets[mi])+" "+paramName+": "+gam[mi];
		mi = Util.maxIndex(Util.flatten(epcs));
		int mlr = mi/netc;
		int mind = mi%netc;
		resultsTable[1][2] = Math.round(epcs[mlr][mind])+" "+paramName+": "+gam[mlr];

		String[][] plotDataNets = new String[1][size];
		String[][] plotDataEpochs = new String[1][size];
		for (int l = 0; l < size; l++) {
			plotDataNets[0][l] = "("+gam[l]+","+nets[l]+")";
			double[] meannstd = Util.MeanAndStD(epcs[l]);
			plotDataEpochs[0][l] = "("+gam[l]+","+meannstd[0]+") +- (0,"+meannstd[1]+")";
		}
		
		TexLog log = new TexLog();
		log.addLine("Testing "+paramNameFull+": "+netc+" nets per batch. Success criterion: pattern success at 100\\% in " +
                sufficientSuccEpcs+" successive epochs. Maximum "+maxEpcs+" epochs.");
		log.addLine("Other net params: weights from range <"+wmin+","+wmax+"> (Gaussian distribution). Clamping strength: "+ Arrays.toString(clampStrength));
		log.addResultsTable(
				new String[] {"","nets","epochs"}, 
				resultsTable, 
				"XOR: comparing "+paramName,
				"tab:424-lrate"
		);
		log.addLine("\\begin{figure}");
		log.addLine("\\begin{subfigure}[b]{.45\\textwidth}");
		log.addPlot(plotDataNets,null,paramName,"nets successful");
		log.addLine("\\end{subfigure}");
		log.addLine("\\begin{subfigure}[b]{.45\\textwidth}");
		log.addPlotErrBars(plotDataEpochs,null,paramName,"training epochs");
		log.addLine("\\end{subfigure}");
		log.addLine("\\caption{XOR: experiments with "+paramName+"}");
		log.addLine("\\label{fig:balold-encoder424-lrate}");
		log.addLine("\\end{figure}");
		//log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
}
