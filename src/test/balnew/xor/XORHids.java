package test.balnew.xor;

import base.BAL;
import base.Conf;
import base.NetUtil;
import util.TexLog;
import util.Util;

import java.io.IOException;
import java.util.Arrays;

public class XORHids {

    public static final String paramNameFull = "hidden layer size";
    public static final String paramName     = "hid size";

    public static final String logName = "log-newbal-XOR-hidsize-small.tex";
	public static final String[] errNm = {"mse","pattern success", "epcs"};

	public static final int netc = 1000;
    public static final int maxEpcs = 100000;
    public static final int sufficientSuccEpcs = 100;

    public static final double[] clampStrength     = {0.0, 1.0, 0.0}; // quasi-RBM + supervised resonator
    public static final double[] estimateStrangthF = {0.0, 0.0, 0.0};
    public static final double[] estimateStrangthB = {0.0, 0.0, 0.0};

    public static final double wmax = 3.0;//Conf.wmax;
    public static final double wmin =-1.0;//Conf.wmin;

    public static final double[][] inputs = Conf.inputsXOR;
    public static final double[][] outputs = Conf.outputsXOR;

    public static final int inps = inputs[0].length;
    public static final int[] hids = {2, 3, 4, 5, 6, 7, 8};
//    public static final int[] hids = {6, 7, 8, 9, 10, 12, 15, 20};
//    public static final int[] hids = {20, 30, 50, 100, 200};
//    public static final int[] hids = {200, 500, 1000, 2000};
    public static final int outs = outputs[0].length;
    public static int[] arch;

//    public static final double[] lRates = {0.2,2.0};
    public static final double lRate = 1e200;

    public static void makeLRates(int count) {
        double[] lRates = new double[count];
        for (int i = 0; i < lRates.length; i++) {
            lRates[i] = Util.round(0.1 + 0.1*i,3);
        }

    }

	public static void main(String[] args) throws IOException {

        int size = hids.length;
		double epcs[][] = new double[size][netc];
		double nets[] = new double[size];
		// run tests			
		for (int l = 0; l < hids.length; l++) {
			System.out.println("testing "+paramNameFull+": "+hids[l]);
			for (int n = 0; n < netc; n++) {
			    arch = new int[] {inps,hids[l],outs};
                BAL net = new BAL(arch,wmin,wmax,lRate,clampStrength, estimateStrangthF, estimateStrangthB);
				double[] errors = new double[errNm.length];
				int epoch = 0;
				int succepochs = 0;
				while (succepochs < sufficientSuccEpcs && epoch < maxEpcs) {
					net.epoch(inputs, outputs, true);
					errors = NetUtil.errorsBAL(arch, inputs, outputs, net.epoch(inputs, outputs,false));
					epoch++;
					if (errors[2] == 1.0) {
                        succepochs++;
//                        System.out.print('.');
                    }
                    else {
//					    if (succepochs>0)
//                            System.out.print(':');
                        succepochs = 0;
                    }
				}
				if (errors[2] == 1.0) {
					nets[l]++;
                    System.out.print("+");
				}
				else
                    System.out.print("-");
				epcs[l][n] = epoch-sufficientSuccEpcs+1.0;
//				System.out.print(".");
			}
			System.out.println("succ: " + nets[l] + "nets");
		}
		// compute write results
		String[][] resultsTable = new String[2][3];
		
		resultsTable[0][0] = "mean and std";
		double[] mnstd = Util.MeanAndStD(nets);
		resultsTable[0][1] = Util.round(mnstd[0],3)+" $\\pm$ "+Util.round(mnstd[1],3);
		mnstd = Util.MeanAndStD(Util.flatten(epcs));
		resultsTable[0][2] = Util.round(mnstd[0],3)+" $\\pm$ "+Util.round(mnstd[1],3);
		
		resultsTable[1][0] = "maximum";
		int mi = Util.maxIndex(nets);
		resultsTable[1][1] = Math.round(nets[mi])+" "+paramName+": "+hids[mi];
		mi = Util.maxIndex(Util.flatten(epcs));
		int mlr = mi/netc;
		int mind = mi%netc;
		resultsTable[1][2] = Math.round(epcs[mlr][mind])+" "+paramName+": "+hids[mlr];

		String[][] plotDataNets = new String[1][size];
		String[][] plotDataEpochs = new String[1][size];
		for (int l = 0; l < size; l++) {
			plotDataNets[0][l] = "("+hids[l]+","+nets[l]+")";
			double[] meannstd = Util.MeanAndStD(epcs[l]);
			plotDataEpochs[0][l] = "("+hids[l]+","+meannstd[0]+") +- (0,"+meannstd[1]+")";
		}

        TexLog log = new TexLog();
        log.addLine("Testing "+paramNameFull+": "+netc+" nets per batch. Success criterion: pattern success at 100\\% in " +
                sufficientSuccEpcs+" successive epochs. Maximum "+maxEpcs+" epochs.");
        log.addLine("Other net params: weights from range <"+wmin+","+wmax+"> (Gaussian distribution). Clamping strength: "+ Arrays.toString(clampStrength));
        log.addResultsTable(
                new String[] {"","nets","epochs"},
                resultsTable,
                "XOR: comparing "+paramName,
                "tab:424-lrate"
        );
        log.addLine("\\begin{figure}");
        log.addLine("\\begin{subfigure}[b]{.45\\textwidth}");
        log.addPlot(plotDataNets,null,paramName,"nets successful");
        log.addLine("\\end{subfigure}");
        log.addLine("\\begin{subfigure}[b]{.45\\textwidth}");
        log.addPlotErrBars(plotDataEpochs,null,paramName,"training epochs");
        log.addLine("\\end{subfigure}");
        log.addLine("\\caption{XOR: experiments with "+paramName+"}");
        log.addLine("\\label{fig:balold-encoder424-lrate}");
        log.addLine("\\end{figure}");
        //log.echo();
        log.export(Conf.pathOutput+logName);
        System.out.println("Finished writing \""+logName+"\"");
	}
}
