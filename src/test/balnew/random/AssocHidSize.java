package test.balnew.random;

import base.Conf;
import base.NetUtil;
import data.DataBinary;
import test.helpers.BALTest;
import util.TexLog;

import java.io.IOException;

public class AssocHidSize {
	
	public static final int sampleCount = 200;
	public static final int vectorSize = 100;
	public static final int netc = 50;
	public static final int maxEpcs = 1000;
	public static final int k = 10;
	public static final String fileIn  = Conf.pathDataKWTA+"k"+k+"-in"+ vectorSize +"x"+ sampleCount;
	public static final String fileDes = Conf.pathDataKWTA+"k"+k+"-des"+ vectorSize +"x"+ sampleCount;
	public static final String logName = "assoc-"+ vectorSize +"x"+ sampleCount +"-k"+k+"-hidsize.tex";
	public static final double clampstrength = 1.0;
	public static final double wmax = -.1;
	public static final double wmin = .1;
	public static final double lrate = 0.2;
	public static final int[] hiddenSizes = {70, 80, 90, 100, 110, 120, 130, 150, 170, 200};
//	public static final int[] hiddenSizes = {100, 150};

	public static void main(String[] args) throws IOException {
				
		BALTest test = new BALTest(netc, maxEpcs);
		DataBinary data = new DataBinary(fileIn,fileDes);
		TexLog log = new TexLog();
		int[][] architectures = new int[hiddenSizes.length][3];
		String[] archsString = new String[hiddenSizes.length];
		for (int i = 0; i < hiddenSizes.length; i++) {
			architectures[i][0] = data.dataX().length;
			architectures[i][1] = hiddenSizes[i];
			architectures[i][2] = data.dataX().length;
			archsString[i] = NetUtil.archToStr(architectures[i]);
		}
		log.addLog(test.logData(
				test.batchArchitectures(architectures, data.dataX(), data.dataY(), wmin, wmax, lrate, clampstrength),
				"architectures",
				archsString));
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
