package test.balnew.random;

import base.BAL;
import base.Conf;
import base.NetUtil;
import data.DataBinary;
import util.visu.DataPairsRes;
import util.visu.Plot;

import java.io.IOException;

public class AssocRandomBigSet {

//	String simName = "balold-random-10x10-300patterns";
	static String[] errorLabels = Conf.errorLabelsF1bidir;
	public static final int[] visParams = {2, 15, 10, 10};

	static int k = 10;
	static int vectorSize = 100;
	static int sampleCount = 200;
	static String fileIn  = Conf.pathDataKWTA+"k"+k+"-in"+ vectorSize +"x"+sampleCount;
	static String fileDes = Conf.pathDataKWTA+"k"+k+"-des"+ vectorSize +"x"+sampleCount;

	static double lr = 0.1;
	static double clampStrength = 1.0;
	static int hids = 100;
	static int[] arch = {vectorSize,hids,vectorSize};
	static int maxEpcs = 6000;

	public static void main(String[] args) throws IOException {	
		DataBinary data = new DataBinary(fileIn,fileDes);
		BAL net = new BAL(arch,Conf.wmin,Conf.wmax,lr,clampStrength);
		Plot plot = new Plot(errorLabels);

		double[][][] patterns = net.testEpochPatterns(data.dataX(),data.dataY());
		DataPairsRes visRes = new DataPairsRes(patterns,visParams);
		visRes.display();

		double[] err = new double[errorLabels.length];
		int epoch = 0;
		while ((NetUtil.errorsUnsatisfied(err) || epoch < 1) && epoch < maxEpcs) {
			err = NetUtil.errorsF1bidirBAL(arch, data.dataX(), data.dataY(), net.epoch(data.dataX(), data.dataY(),true));
			for (int i = 0; i < err.length; i++) {
				plot.addPoint(i, epoch, err[i]);
			}
			epoch++;
			if (epoch % 2 == 0) {
				if (epoch % 10 == 0)
					if (epoch % 50 == 0)
						if (epoch % 100 == 0)
							if (epoch % 500 == 0)
								System.out.print(":\n");
							else
								System.out.print(";");
						else
							System.out.print(",");
					else
						System.out.print(".");
				visRes.setPatterns(net.testEpochPatterns(data.dataX(),data.dataY()));
			}
		}
		System.out.println();
		System.out.println(epoch);
		for (int i = 0; i < err.length; i++) {
			System.out.println(errorLabels[i]+": "+err[i]);
		}
		System.out.println();
		double[] testErrors = NetUtil.errorsF1bidirBAL(arch, data.dataX(), data.dataY(), net.epoch(data.dataX(), data.dataY(),false));
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(errorLabels[i]+": "+testErrors[i]);
		}
		plot.display();
	}
}
