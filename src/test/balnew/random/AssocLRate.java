package test.balnew.random;

import base.Conf;
import base.NetUtil;
import data.DataBinary;
import test.helpers.BALTest;
import util.TexLog;

import java.io.IOException;

public class AssocLRate {
	
	public static final int sampleCount = 300;
	public static final int vectorSize = 100;
	public static final int netc = 20;
	public static final int maxEpcs = 2000;
	public static final int k = 10;
	public static final int hids = 100;
	public static final int[] arch = {vectorSize,hids,vectorSize};
	public static final double clampstrength = 1.0;
	public static final double wmax = -.1;
	public static final double wmin = .1;
	public static final String logName = "ubal-assoc-"+ vectorSize +"x"+ sampleCount +"-k"+k+"-lrate.tex";
//	public static final double[] learningRates = {0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.5};
	public static final double[] learningRates = {0.1, 0.2, 0.5, 1.0, 1.3, 1.5, 2.0};

	public static void main(String[] args) throws IOException {
				
		BALTest test = new BALTest(netc, maxEpcs);
		String pathIn = "input/binary-kWTA/k"+k+"-in"+ vectorSize +"x"+ sampleCount;
		String pathDes = "input/binary-kWTA/k"+k+"-des"+ vectorSize +"x"+ sampleCount;
		DataBinary data = new DataBinary(pathIn,pathDes);
		TexLog log = new TexLog();
		double[][][] initWeightsF = NetUtil.initAllWeightsF(arch, wmin, wmax);
		double[][][] initWeightsB = NetUtil.initAllWeightsB(arch, wmin, wmax);
		log.addLog(test.logData(
				test.batchLRate(learningRates,data.dataX(),data.dataY(),arch,initWeightsF,initWeightsB,clampstrength),
				"learning rate",
				NetUtil.paramsToStr(learningRates)));
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
