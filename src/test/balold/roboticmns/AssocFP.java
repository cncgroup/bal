package test.balold.roboticmns;

import java.io.IOException;

import test.helpers.BALClassicSingle;
import util.TexLog;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class AssocFP {
	
	public static final String simName = "balold-firstpersp";
	public static final String[] errNames = Conf.errorLabels;	
	public static final int rowHeight = 5;
	public static final int sizeVis = 14;
	public static final int sizeMot = 8;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final double lRate = 0.2;
	public static final int hids = 160;
	public static final int maxEpcs = 2000;
	public static final int plotRes = 100;
	
	public static void main(String[] args) throws IOException {
		
	///////DATA/////////////////////////////////////////////
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
 		double[][] valuesMot = dataMot.values();
 		double[][] valuesVis = dataVis.valuesFirstPersp();
 	///////SIMULATION/////////////////////////////////////////////
 		BALClassicSingle sim = new BALClassicSingle(simName, errNames, hids, lRate, maxEpcs, plotRes,rowHeight);
 		TexLog log = new TexLog();	
 		log.addLine("\\subsection{Disambiguated motor data with disambiguated visual data}");
		String[] setup = {
			"architecture: "+dataVis.patternSize()+"-"+hids+"-"+dataMot.patternSize(),
			"learning rate: "+ Conf.lRate,
			"min pattern succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			"data motor: "+sizeMot+"x"+sizeMot+" k="+kMot,
			"data visual: "+sizeVis+"x"+sizeVis+" k="+kVis,
		};		
		log.addLine("\\subsubsection{Setup parameters}");
		log.addItemize(setup);
		log.addLine("");
		log.addFigure(
				Conf.pathFigures+simName+"-data"+Conf.figExt, 
				"BAL with robotic data - unique motor - unique visual dataset ", 
				"",
				0.7);
		log.addLine("");	
		log.addLog(sim.runAndLog(valuesMot, valuesVis));
		log.echo();
		log.exportPartial(Conf.pathOutput+"log-"+simName+".tex");
	}
}
