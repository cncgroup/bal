package test.balold.roboticmns.learning;

import java.io.IOException;

import test.helpers.BALClassicSingle;
import util.TexLog;
import base.Conf;
import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class AssocUMotUVis2P {
	
	public static final String simName = "balold-learning-unique-2persp";
	public static final String[] errNames = Conf.errorLabelsNew;	
	public static final int rowHeight = 5;
	public static final int sizeVis = 14;
	public static final int sizeMot = 8;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final double lRate = 0.2;
	public static final int hids = 160;
	public static final int maxEpcs = 2000;
	public static final int plotRes = 50;
	
	public static void main(String[] args) throws IOException {
	///////DATA/////////////////////////////////////////////
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		int[][] indices = dataVis.uniqPatIndices(dataMot.uniqPatIndices());
 		double[][] valuesMot = dataMot.valuesIndices(indices);
 		double[][] valuesVis = dataVis.valuesPerspIndices(0,indices);
 		double[][] valuesMot2 = Util.merge(valuesMot, valuesMot);
 		double[][] valuesVis2 = Util.merge(valuesVis, dataVis.valuesPerspIndices(1,indices));
 	///////NET/////////////////////////////////////////////
		TexLog log = new TexLog();
//		String[] setup = {
//				"architecture: "+mapSizeV*mapSizeV+"-"+hids+"-"+mapSizeM*mapSizeM,
//				"learning rate: "+ Conf.lRate,
//				"min pattern succ: "+ Conf.minPatSucc,
//				"max epochs: "+epcsPhase1,
//				"data motor: "+mapSizeM+"x"+mapSizeM+" k="+kMot,
//				"data visual: "+mapSizeV+"x"+mapSizeV+" k="+kVis,
//			};
//		log.addLine("\\subsection{Setup parameters}");
//		log.addItemize(setup);
 		BALClassicSingle sim = new BALClassicSingle(simName, errNames, hids, lRate, maxEpcs, plotRes, rowHeight);
		log.addLog(sim.runAndLog2Phase(valuesMot, valuesVis, valuesMot2, valuesVis2));
		log.echo();
	}
}
