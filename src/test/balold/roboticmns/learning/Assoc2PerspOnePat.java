package test.balold.roboticmns.learning;

import java.io.IOException;

import test.helpers.BALClassicSingle;
import util.TexLog;
import base.Conf;
import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class Assoc2PerspOnePat {
	public static final String simName = "balold-learning-2persp-1pat";
	public static final String[] errNames = Conf.errorLabelsNew;	
	public static final int rowHeight = 2;
	public static final int sizeVis = 14;
	public static final int sizeMot = 8;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final double lRate = 0.2;
	public static final int hids = 160;
	public static final int maxEpcs = 200;
	public static final int plotRes = 10;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		double[][] valuesVis = dataVis.valuesOnePatOnePersp(0);
		double[][] valuesVis2 = Util.merge(valuesVis,dataVis.valuesOnePatOnePersp(1));
		double[][] valuesMot = dataMot.valuesOnePat();
		double[][] valuesMot2 = dataMot.valuesOnePat(2);
		for (int i = 0; i < valuesMot2.length; i++) {
			System.out.println(Util.arrayToBinaryString(valuesMot2[i]));
		}
		
		TexLog log = new TexLog();
 		BALClassicSingle sim = new BALClassicSingle(simName, errNames, hids, lRate, maxEpcs, plotRes, rowHeight);
		log.addLog(sim.runAndLog2Phase(valuesMot, valuesVis, valuesMot2, valuesVis2));
		log.echo();
	}
}
