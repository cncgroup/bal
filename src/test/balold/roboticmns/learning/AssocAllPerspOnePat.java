package test.balold.roboticmns.learning;

import java.io.IOException;

import util.TexLog;
import util.visu.DataPairsRes;
import util.visu.Plot;
import base.BAL1;
import base.Conf;
import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class AssocAllPerspOnePat {
	public static final String simName = "balold-learning-all-1pat";
	public static final String[] errNames = Conf.errorLabelsNew;	
	public static final int rowHeight = 4;
	public static final int sizeVis = 14;
	public static final int sizeMot = 8;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final double lRate = 0.2;
	public static final int hids = 160;
	public static final int maxEpcs = 200;
	public static final int plotRes = 10;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		double[][] valuesVis = dataVis.valuesOnePatOnePersp(0);
		double[][] valuesVis2 = dataVis.valuesOnePat();
		double[][] valuesMot = dataMot.valuesOnePat();
		double[][] valuesMot2 = dataMot.valuesOnePat(4);
		int inps = dataVis.patternSize();
		int outs = dataMot.patternSize();
		for (int i = 0; i < valuesMot2.length; i++) {
			System.out.println(Util.arrayToBinaryString(valuesMot2[i]));
		}
		
		Plot plot = new Plot(errNames);		
		BAL1 net = new BAL1(inps,hids,outs,lRate);
		int epoch = 0;
		System.out.println("phase one: first perspective");
		double[] trainErr1 = new double[errNames.length];
		for (int e = 0; e < maxEpcs; e++) {
			trainErr1 = net.epochPD(valuesVis,valuesMot,true,false);
			if ((epoch+1) % plotRes == 0) {
				for (int i = 0; i < trainErr1.length; i++) {
					plot.addPoint(i, epoch+1, trainErr1[i]);
				}
				System.out.print(".");
			}
			epoch++;
		}
		System.out.println();
		for (int i = 0; i < trainErr1.length; i++) {
			System.out.println(errNames[i]+": "+trainErr1[i]);
		}
		System.out.println();
		
		System.out.println("phase two: first + second perspective");
		double[] trainErr2 = new double[errNames.length];
		for (int e = 0; e < maxEpcs; e++) {
			trainErr2 = net.epochPD(valuesVis2,valuesMot2,true,false);
			if ((epoch+1) % plotRes == 0) {
				for (int i = 0; i < trainErr2.length; i++) {
					plot.addPoint(i, epoch+1, trainErr2[i]);
				}
				System.out.print(".");
			}			
			epoch++;
		}
		System.out.println();
		for (int i = 0; i < trainErr2.length; i++) {
			System.out.println(errNames[i]+": "+trainErr2[i]);
		}
		
		double[][][] patterns = net.testEpochPatterns(valuesVis2,valuesMot2);
		DataPairsRes visRes2 = new DataPairsRes(patterns);
		visRes2.display();
		visRes2.setRowH(rowHeight);
		plot.display();
		plot.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-performance"+Conf.figExt);
		visRes2.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-patterns2"+Conf.figExt);
		
		TexLog log = new TexLog();
		log.addLine("\\subsection{BAL learning in 2 phases}");
		String[] setup = {
				"architecture: "+sizeVis*sizeVis+"-"+hids+"-"+sizeMot*sizeMot,
				"learning rate: "+ Conf.lRate,
				"min pattern succ: "+ Conf.minPatSucc,
				"epochs: "+maxEpcs,
				"data motor: "+sizeMot+"x"+sizeMot+" k="+kMot,
				"data visual: "+sizeVis+"x"+sizeVis+" k="+kVis,
				"phase 1: first perspective",
				"phase 1: first and second perspective",
			};
		log.addLine("\\subsubsection{Setup parameters}");
		log.addItemize(setup);
		String[][] resultsTable = new String[errNames.length+1][3];
		for (int i = 0; i < errNames.length; i++) {
			resultsTable[i][0] = errNames[i];
			resultsTable[i][1] = ""+Util.round(trainErr1[i],3);
			resultsTable[i][2] = ""+Util.round(trainErr2[i],3);
		}
		resultsTable[resultsTable.length-1][0] = "epochs";
		resultsTable[resultsTable.length-1][1] = ""+epoch;
		resultsTable[resultsTable.length-1][2] = "";
		log.addResultsTable(
				new String[] {"error","phase one","phase two"}, 
				resultsTable, 
				"BAL with robotic data - performance", 
				""
		);
		log.addFigure(
				Conf.pathFigures+simName+"-performance"+Conf.figExt, 
				"BAL with robotic data - performance", 
				"",
				0.9);
		log.addFigure(
				Conf.pathFigures+simName+"-patterns2"+Conf.figExt, 
				"BAL with robotic data - pattern match in second phase", 
				"",
				0.7);
		log.echo();
	}
}
