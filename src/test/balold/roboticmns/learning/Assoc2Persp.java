package test.balold.roboticmns.learning;

import java.io.IOException;

import test.helpers.BALClassicSingle;
import util.TexLog;
import base.Conf;
import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class Assoc2Persp {
	public static final String simName = "balold-learning-2persp";
	public static final String[] errNames = Conf.errorLabelsNew;	
	public static final int rowHeight = 8;
	public static final int sizeVis = 14;
	public static final int sizeMot = 8;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final double lRate = 0.2;
	public static final int hids = 160;
	public static final int maxEpcs = 2000;
	public static final int plotRes = 100;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		double[][] valuesVis = dataVis.valuesPersp(0);
		double[][] valuesVis2 = Util.merge(valuesVis,dataVis.valuesPersp(1));
		double[][] valuesMot = dataMot.values();
		double[][] valuesMot2 = Util.merge(dataMot.values(),dataMot.values());
		
		TexLog log = new TexLog();
 		BALClassicSingle sim = new BALClassicSingle(simName, errNames, hids, lRate, maxEpcs, plotRes, rowHeight);
		log.addLog(sim.runAndLog2Phase(valuesMot, valuesVis, valuesMot2, valuesVis2));
		log.echo();
	}
}
