package test.balold.roboticmns;

import java.io.IOException;

import test.helpers.BALClassicSingle;
import util.TexLog;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class TestDistFunc {
	public static final String simName = "balold-distancefunc-FP";
	public static final String[] errNames = Conf.errorLabelsNew;	
	public static final int rowHeight = 8;
	public static final int sizeVis = 14;
	public static final int sizeMot = 8;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final double lRate = 0.2;
	public static final int hids = 160;
	public static final int maxEpcs = 2000;
	public static final int plotRes = 100;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt", Conf.defaultDelimiter, true);
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt", Conf.defaultDelimiter,true);
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		double[][] valuesVis = dataVis.valuesPersp(0);
		double[][] valuesMot = dataMot.values();
		
		TexLog log = new TexLog();
 		BALClassicSingle sim = new BALClassicSingle(simName, errNames, hids, lRate, maxEpcs, plotRes, rowHeight);
		log.addLog(sim.runAndLog(valuesMot, valuesVis));
		log.echo();
	}
}
