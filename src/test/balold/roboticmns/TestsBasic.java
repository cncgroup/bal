package test.balold.roboticmns;

import java.io.IOException;

import test.helpers.BALClassicSingle;
import util.TexLog;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class TestsBasic {
	public static final String[] simNames = {
		"balold-allpersp",
		"balold-firstpersp",
		"balold-firstpersp-conti",
		"balold-onepat-fp",
		"balold-onepat-allp",
	};
	public static final String[] headings = {
		"All data pairs",
		"First perspective",
		"First perspective without kWTA",
		"One pattern - first perspective",
		"One pattern - all perspectives",		
	};
	public static final String[] errNames = Conf.errorLabels;
	public static final int sizeVis = 14;
	public static final int sizeMot = 8;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final int hids = 160;
	public static final double lRate = 0.2;
	public static final int maxEpcs[] = {2000,2000,2000,100,100};
	public static final int plotRes[] = {50,50,50,1,1};
	public static final int rowHeight[] = {Conf.rowHAllP,Conf.rowHeight,Conf.rowHeight,Conf.rowHSmallSet,Conf.rowHSmallSet};
	
	public static void main(String[] args) {
		
		TexLog log = new TexLog();
		String[] setup = {
				"architecture: "+sizeVis*sizeVis+"-"+hids+"-"+sizeMot*sizeMot,
				"learning rate: "+ Conf.lRate,
				"min pattern succ: "+ Conf.minPatSucc,
				"max epochs: "+maxEpcs[0]+"/"+maxEpcs[3],
				"data motor: "+sizeMot+"x"+sizeMot+" k="+kMot,
				"data visual: "+sizeVis+"x"+sizeVis+" k="+kVis,
			};
		log.addLine("\\subsection{Setup parameters}");
		log.addItemize(setup);
		
		for (int i = 0; i < simNames.length; i++) {
			System.out.println("processing sim "+i);
			DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
			DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
			double[][] dataX;
			double[][] dataY;
			switch (i) {
			case 0:
				dataVis.kWinnerTakeAll(kVis);
				dataMot.kWinnerTakeAll(kMot);
				dataMot.multiply();
				dataX = dataVis.values();
				dataY = dataMot.values();
				break;
			case 1:
				dataVis.kWinnerTakeAll(kVis);
				dataMot.kWinnerTakeAll(kMot);
				dataX = dataVis.valuesFirstPersp();
				dataY = dataMot.values();
				break;
			case 2:
				dataX = dataVis.valuesFirstPersp();
				dataY = dataMot.values();
				break;
			case 3:
				dataVis.kWinnerTakeAll(kVis);
				dataMot.kWinnerTakeAll(kMot);
				dataX = dataVis.valuesOnePatFirstPersp();
				dataY = dataMot.valuesOnePat();
				break;
			case 4:
				dataVis.kWinnerTakeAll(kVis);
				dataMot.kWinnerTakeAll(kMot);
				dataX = dataVis.valuesOnePat();
				dataY = dataMot.valuesOnePat(Conf.perspectives);
				break;	
			default:
				dataVis.kWinnerTakeAll(kVis);
				dataMot.kWinnerTakeAll(kMot);
				dataX = dataVis.valuesFirstPersp();
				dataY = dataMot.values();
				break;
			}
			BALClassicSingle sim = new BALClassicSingle(simNames[i], errNames, hids, lRate, maxEpcs[i], plotRes[i], rowHeight[i]);
			log.addLine("\\subsection{"+headings[i]+"}");
			log.addLog(sim.runAndLog(dataX,dataY));
			log.addLine("");
		}
		try {
			log.exportPartial(Conf.pathOutput+"log-robotic-mns-basic.tex");
			System.out.println("log written successfully!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
