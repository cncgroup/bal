package test.balold.msom.single;

import java.io.IOException;

import util.Util;
import base.Conf;
import base.BAL1;
import base.NetUtil;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class TestEpcs {
	
	public static final String pathVisual = Conf.dataSetsVisPaths[0];
	public static final String pathMotor = Conf.dataSetsMotPaths[1];	
	public static final int k = 8;	
	public static final int[] maxEpcs = {1000,2000,3000};
//	public static final int[] epcsPhase1 = {100,200,500,800,1000};
	public static final int hids = 100;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
//		dataMot.multiply();
		dataVis.kWinnerTakeAll(k);
		dataMot.kWinnerTakeAll(k);
		double[][] dataIn = dataVis.values();
//		double[][] dataIn = dataMot.values();
		double[][] dataDes = dataVis.values();
		
 		double[][] results = new double[maxEpcs.length][6];
 		for (int e = 0; e < maxEpcs.length; e++) {
 			BAL1 net = new BAL1(dataIn[0].length,hids,dataDes[0].length,Conf.lRate,Conf.wmax,Conf.wmin);
 			double[] errors = new double[6];
 			int epoch = 0;
 			while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs[e]) {
 				errors = net.epoch(dataIn,dataDes);
 				epoch++;
 				if (epoch % 100 == 0) 
 					System.out.print(".");
 			}
 			System.out.println();
 			System.out.println("epochs: "+epoch);
 			double[] testErrors = net.testEpoch(dataIn,dataDes);
// 			for (int i = 0; i < testErrors.length; i++) {
// 				System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
// 			}
 			results[e] = testErrors;
		}
 		System.out.println();
		System.out.print("epcsPhase1\t");
		for (int i = 0; i < maxEpcs.length; i++) {
			System.out.print(Conf.errorLabels[i]+"\t");
		}
		System.out.println();
		for (int i = 0; i < results.length; i++) {
			System.out.print(maxEpcs[i]+"\t");
			for (int j = 0; j < results[i].length; j++) {
				System.out.print(Util.round(results[i][j],3)+"\t");
			}
			System.out.println();
		}

	}
}
