package test.balold.msom.single;

import java.io.IOException;

import base.BAL1;
import base.Conf;
import base.NetUtil;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class AssocFP {
	public static final int sizeVis = 14;
	public static final int sizeMot = 12;
	public static final int kVis = 16;
	public static final int kMot = 8;
	public static final String pathVisual = "input/msom/visual_"+sizeVis+"_"+sizeVis+".txt";
	public static final String pathMotor = "input/msom/motor_"+sizeMot+"_"+sizeMot+".txt";
	public static final int[] visParams = {3, 5, 5, 11};
	public static final String[] errNames = Conf.errorLabelsNew;
	public static final int maxEpcs = 10000;
	public static final int hids = 200;
	public static final double lRate = 0.2;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		double[][] dataV = dataVis.valuesFirstPersp();
		double[][] dataM = dataMot.values();
		int inps = dataVis.patternSize();
		int outs = dataMot.patternSize();	
		
//		DataPairsAssymFP vis = new DataPairsAssymFP(visParams, dataV, dataM, dataMot.sequences());
// 		vis.display(); 		
//		Plot plot = new Plot(errNames); 
		
		BAL1 net = new BAL1(inps,hids,outs,lRate);
		double[] trainErr = new double[errNames.length];
		int epoch = 0;
		
		System.out.println("BALNet "+inps+"-"+hids+"-"+outs);
		while ((NetUtil.errorsUnsatisfied(trainErr) || epoch < 1) && epoch < maxEpcs) {
			trainErr = net.epoch(dataV,dataM);
//			for (int i = 0; i < trainErr.length; i++) {
//				plot.addPoint(i, epoch, trainErr[i]);
//			}			
			epoch++;
		}
//		plot.display();
		
		for (int i = 0; i < trainErr.length; i++) {
			System.out.println(errNames[i]+": "+trainErr[i]);
		}
		System.out.println("epcs: "+epoch);
		System.out.println();
		double[] testErr = net.testEpoch(dataV,dataM);
		for (int i = 0; i < testErr.length; i++) {
			System.out.println(errNames[i]+": "+testErr[i]);
		}
		System.out.println();
		testErr = net.testEpoch(dataV,dataM);
		for (int i = 0; i < testErr.length; i++) {
			System.out.println(errNames[i]+": "+testErr[i]);
		}
		
//		double[][][] patterns = net.testEpochPatternskWTA(dataV,dataM);
//		DataPairsVisResultsAssym visRes = new DataPairsVisResultsAssym(visParams,patterns);
//		visRes.display();
	}
}
