package test.balold.msom.single;

import java.io.IOException;

import base.BAL1;
import util.visu.Plot;
import base.Conf;
import base.NetUtil;
import data.DataRoboticMot;

public class EncoderMot {
	
	public static final String figName = "generec-msom-endpoint-assoc.png";
	public static final String logName = "log-assoc-simple.tex";
	public static final String[] errorLabels = Conf.errorLabels;
	public static final int size = 8;
	public static final int k = 8;
	public static final int hids = 80;
	public static final int maxEpcs = 3000;
	
	public static void main(String[] args) throws IOException {
				
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+size+"_"+size+".txt");
		dataMot.kWinnerTakeAll(k);
		BAL1 net = new BAL1(dataMot.patternSize(),hids,dataMot.patternSize(),Conf.lRate,Conf.wmax,Conf.wmin);
		Plot plot = new Plot(errorLabels); 
		double[] errors = new double[6];
		int epoch = 0;
		while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs) {
			errors = net.epoch(dataMot.values(),dataMot.values());
			for (int i = 0; i < errors.length; i++) {
				plot.addPoint(i, epoch, errors[i]);
			}			
			epoch++;
			if (epoch % 100 == 0) 
				System.out.print(".");
		}
		System.out.println();
//		double[] testErrors = net.testEpoch(dataVis.values(),dataMot.values());
		for (int i = 0; i < errors.length; i++) {
			System.out.println(errorLabels[i]+": "+errors[i]);
		}
		plot.display();
//		plot.saveAsImage(Conf.pathOutput+figName);
//		String[] setup = {
//			"architecture: "+Conf.inps+"-"+Conf.hids+"-"+Conf.outs,
//			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
//			"learning rate: "+ Conf.lRate,
//			"min mse: "+ Conf.minMSE,
//			"min succ"+ Conf.minSucc,
//			"max epochs: "+ epcsPhase1,
//		};
//		TexLog log = new TexLog();	
//		log.addLine("\\\\subsection{GeneRec Bidir Robotic Associator}");
//		log.addLine("{\\bf Setup parameters:}\\\\");
//		log.addItemize(setup);
//		log.addFigure(figName, "generec robotic associator", "fig:r1", 1.0);
//		log.echo();
//		log.export(logName);
//		System.out.println("Finished writing \""+logName+"\"");
	}
}
