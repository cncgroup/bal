package test.balold.msom.single;

import java.io.IOException;

import base.BAL1;
import util.visu.DataPairsVisResultsAssym;
import util.visu.Plot;
import base.Conf;
import base.NetUtil;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class Assoc {
	public static final int sizeVis = 16;
	public static final int sizeMot = 8;
	public static final String pathVisual = "input/msom/visual_"+sizeVis+"_"+sizeVis+".txt";
	public static final String pathMotor = "input/msom/motor_"+sizeMot+"_"+sizeMot+".txt";
	public static final int kVis = 16;
	public static final int kMot = 8;
	public static final int[] visParams = {5, 10, 30, 15};
	public static final String[] errorLabels = Conf.errorLabels;
	public static final int maxEpcs = 100;
	public static final int hids = 300;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataMot.multiply();
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		double[][] dataIn = dataVis.values();
		double[][] dataDes = dataMot.values();
		
		BAL1 net = new BAL1(dataIn[0].length,hids,dataDes[0].length,Conf.lRate,Conf.wmax,Conf.wmin);
		Plot plot = new Plot(errorLabels); 
		double[] errors = new double[6];
		int epoch = 0;
		
		while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs) {
			errors = net.epoch(dataIn,dataDes);
			for (int i = 0; i < errors.length; i++) {
				plot.addPoint(i, epoch, errors[i]);
			}			
			epoch++;
			if (epoch % 50 == 0)
				System.out.print(".");
		}
		
		System.out.println(epoch);
		for (int i = 0; i < errors.length; i++) {
			System.out.println(errorLabels[i]+": "+errors[i]);
		}
		System.out.println();
		double[] testErrors = net.testEpoch(dataIn,dataDes);
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
		
		plot.display();
		double[][][] patterns = net.testEpochPatterns(dataIn,dataDes);
		System.out.println(patterns[0][0].length);
		System.out.println(patterns[2][0].length);
		DataPairsVisResultsAssym visRes = new DataPairsVisResultsAssym(visParams,patterns);
		visRes.display();
	}
}
