package test.balold.msom.single;

import java.io.IOException;

import util.Util;
import util.visu.DataPairsVisResultsAssym;
import util.visu.Plot;
import base.Conf;
import base.BAL1;
import base.NetUtil;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class Assoc2P {
	public static final int sizeVis = 14;
	public static final int sizeMot = 12;
	public static final int kVis = 16;
	public static final int kMot = 8;
	public static final String pathVisual = "input/msom/visual_"+sizeVis+"_"+sizeVis+".txt";
	public static final String pathMotor = "input/msom/motor_"+sizeMot+"_"+sizeMot+".txt";
	public static final int[] visParams = {3, 5, 5, 11};
	public static final String[] errNames = Conf.errorLabelsNew;
	public static final int maxEpcs = 8000;
	public static final int plotRes = 200;
	public static final int hids = 200;
	public static final double lRate = 0.2;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		double[][] dataV = dataVis.valuesPersp(0);
		double[][] dataV2 = Util.merge(dataV,dataVis.valuesPersp(1));
		double[][] dataM = dataMot.values();
		dataMot.multiply(2);
		double[][] dataM2 = dataMot.values();
		int inps = dataVis.patternSize();
		int outs = dataMot.patternSize();
		
//		DataPairsAssymFP vis = new DataPairsAssymFP(visParams, dataV, dataM, dataMot.sequences());
// 		vis.display();
//		DataPairsAssymFP vis2 = new DataPairsAssymFP(visParams, dataV2, dataM, dataMot.sequences());
// 		vis2.display(); 	
		Plot plot = new Plot(errNames); 
		
		BAL1 net = new BAL1(inps,hids,outs,lRate);
		double[] trainErr = new double[errNames.length];
		int epoch = 0;
		
		while ((NetUtil.errorsUnsatisfied(trainErr) || epoch < 1) && epoch < maxEpcs) {
			if (epoch < maxEpcs/2)
				trainErr = net.epoch(dataV,dataM);
			else
				trainErr = net.epoch(dataV2,dataM2);
			if ((epoch+1) % plotRes == 0) {
				for (int i = 0; i < trainErr.length; i++) {
					plot.addPoint(i, epoch+1, trainErr[i]);
				}
				System.out.print(".");
			}			
			epoch++;
		}
		System.out.println();
		plot.display();
		
		for (int i = 0; i < trainErr.length; i++) {
			System.out.println(errNames[i]+": "+trainErr[i]);
		}
		System.out.println();
		double[] testErr = net.testEpoch(dataV2,dataM2);
		for (int i = 0; i < testErr.length; i++) {
			System.out.println(errNames[i]+": "+testErr[i]);
		}
		
		double[][][] patterns = net.testEpochPatternskWTA(dataV,dataM);
		DataPairsVisResultsAssym visRes = new DataPairsVisResultsAssym(visParams,patterns);
		visRes.display();
		patterns = net.testEpochPatternskWTA(dataV2,dataM2);
		DataPairsVisResultsAssym visRes2 = new DataPairsVisResultsAssym(visParams,patterns);
		visRes2.display();
	}
}
