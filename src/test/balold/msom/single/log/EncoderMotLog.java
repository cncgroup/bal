package test.balold.msom.single.log;

import java.io.IOException;

import base.BAL1;
import util.TexLog;
import base.Conf;
import base.NetUtil;
import data.DataRoboticMot;

public class EncoderMotLog {
	
	public static final String pathVisual = Conf.dataSetsVisPaths[0];
	public static final String pathMotor = Conf.dataSetsMotPaths[1];	
	public static final int k = 8;
	public static final String figName = "grbidir-encoder-robotic-";
	public static final String logName = "log-robotic-single-encoder.tex";
	public static final int hids = 100;
	public static final int maxEpcs = 200;
	
	public static void main(String[] args) throws IOException {
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataMot.kWinnerTakeAll(k);
		double[][] dataIn = dataMot.values();
		double[][] dataDes = dataMot.values();
		
		BAL1 net = new BAL1(dataIn[0].length,hids,dataDes[0].length,Conf.lRate,Conf.wmax,Conf.wmin);
		String[][] plotDataMSE = new String[2][maxEpcs/2+1];
		String[][] plotDataSucc = new String[2][maxEpcs/2+1];
		String[][] plotDataEB = new String[2][maxEpcs/2+1];
		
		double[] trainErrors = new double[6];
		int epoch = 0;
		int index = 0;
		while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < maxEpcs) {
			trainErrors = net.epoch(dataIn,dataDes);
			if (epoch == 0 || epoch % 2 == 0) {
				plotDataMSE[0][index] = "("+epoch+","+trainErrors[0]+")";
				plotDataMSE[1][index] = "("+epoch+","+trainErrors[1]+")";
				plotDataSucc[0][index] = "("+epoch+","+trainErrors[2]+")";
				plotDataSucc[1][index] = "("+epoch+","+trainErrors[3]+")";
				plotDataEB[0][index] = "("+epoch+","+trainErrors[4]+")";
				plotDataEB[1][index] = "("+epoch+","+trainErrors[5]+")";	
				index++;
			}
			epoch++;
			if (epoch % 100 == 0) 
				System.out.print(".");
		}
		System.out.println();
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+trainErrors[i]);
		}
		System.out.println();
		double[] testErrors = net.testEpoch(dataIn,dataDes);
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
		
		String[] setup = {
			"architecture: "+Conf.inps+"-"+Conf.hids+"-"+Conf.outs,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min succ"+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
		};
		TexLog log = new TexLog();	
		log.addLine("\\subsection{Associator: single instance}");
		log.addLine("{\\bf Setup parameters:}\\\\");
		log.addItemize(setup);
		log.addLine("\\begin{figure}");
		log.addLine("\\centering");
		log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
		log.addLine("\\centering");
		log.addPlotSmall(plotDataMSE,"epoch","mse");
		log.addLine("\\caption{mean square error (per neuron)}");
		log.addLine("\\end{subfigure}");
		log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
		log.addLine("\\centering");
		log.addPlotSmall(plotDataSucc,"epoch","success");
		log.addLine("\\caption{epoch success (per neuron)}");
		log.addLine("\\end{subfigure}");
		log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
		log.addLine("\\centering");
		log.addPlotSmall(plotDataEB,"epoch","error bits");	
		log.addLine("\\caption{error bits}");
		log.addLine("\\end{subfigure}");
		log.addLine("\\caption{Results for single instance of BALNet Encoder with robotic data}");
		log.addLine("\\label{fig:enc-single-robotic}");
		log.addLine("\\end{figure}");
		log.addLine("");
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");	
	}
}