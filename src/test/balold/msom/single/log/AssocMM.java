package test.balold.msom.single.log;

import java.io.IOException;

import base.BAL1;
import util.TexLog;
import util.Util;
import util.visu.DataPairsVisResultsAssym;
import util.visu.Plot;
import util.visu.robotic.DataPairsAssymFP;
import base.Conf;
import base.NetUtil;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class AssocMM {
	
	public static final String figName = "motor-mesh";
	public static final String logName = "log-robotic-single-motor-mesh.tex";
	public static final String dataName = "robotic data with motor data meshed";
	public static final String[] errorLabels = Conf.errorLabels;
	public static final int[] visParams = {4, 10, 10, 10, 8};
	public static final int sizeVis = 16;
	public static final int sizeMot = 8;
	public static final String pathVisual = "input/msom/visual_"+sizeVis+"_"+sizeVis+".txt";
	public static final String pathMotor = "input/msom/motor_"+sizeMot+"_"+sizeMot+".txt";
	public static final int kVis = 16;
	public static final int kMot = 8;
	public static final int hids = 80;
	public static final int maxEpcs = 5000;
	
	public static void main(String[] args) throws IOException {
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(kVis);
		double[][] dataIn = dataVis.valuesFirstPersp();
		double[][] dataDes = dataMot.valuesMergMovkWTA(kMot);
		DataPairsAssymFP vis = new DataPairsAssymFP(visParams, dataIn, dataDes, dataMot.sequences());
 		vis.display();
 		vis.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"sampleset"+Conf.figExt);
		BAL1 net = new BAL1(dataIn[0].length,hids,dataDes[0].length,Conf.lRate,Conf.wmax,Conf.wmin);
		Plot plot = new Plot(errorLabels); 
		double[] trainErrors = new double[6];
		int epoch = 0;
		while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < maxEpcs) {
//			trainErrors = net.epoch(dataIn,dataDes,true,true);
			trainErrors = net.epoch(dataIn,dataDes);
			for (int i = 0; i < trainErrors.length; i++) {
				plot.addPoint(i, epoch, trainErrors[i]);
			}			
			epoch++;
			if (epoch % 50 == 0)
				System.out.print(".");
		}
		//test the net
		double[] testErrors = net.testEpoch(dataIn,dataDes);
		double[][][] patterns = net.testEpochPatterns(dataIn,dataDes);
		//write to console
		System.out.println(epoch);
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(errorLabels[i]+": "+trainErrors[i]);
		}
		System.out.println();
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
		// save results
		String[][] resultsTable = new String[7][3];
		for (int i = 0; i < resultsTable.length; i++) {
			resultsTable[i][0] = Conf.testErrors[i];
			if (i == 6) {
				resultsTable[i][1] = ""+epoch;
				resultsTable[i][2] = "1";
			} else if (i > 1) {
				resultsTable[i][1] = ""+Util.round(100.0*trainErrors[i],2)+"\\%";
				resultsTable[i][2] = ""+Util.round(100.0*testErrors[i],2)+"\\%";
			} else {
				resultsTable[i][1] = ""+Util.round(trainErrors[i],3);
				resultsTable[i][2] = ""+Util.round(testErrors[i],3);
			}
		}
		// save plots and figures
		plot.display();
		plot.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-results"+Conf.figExt);
		DataPairsVisResultsAssym visRes = new DataPairsVisResultsAssym(visParams,patterns);
		visRes.display();
		visRes.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-results-concrete"+Conf.figExt);
		// write log
		String[] setup = {
			"architecture: "+dataIn[0].length+"-"+hids+"-"+dataDes[0].length,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min pattern succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			"data: "+dataName,
		};
		TexLog log = new TexLog();	
		log.addLine("{\\bf Setup parameters:}\\\\");
		log.addItemize(setup);
		log.addResultsTable(
				new String[] {"error","train","test"}, 
				resultsTable, 
				"BALNet results: "+dataName,
				""
		);
		log.addFigure(
				Conf.pathFigures+figName+"-results"+Conf.figExt, 
				"BALNet results: "+dataName,
				"",
				1.0);
		log.addFigure(
				Conf.pathFigures+figName+"-data"+Conf.figExt, 
				"Dataset: "+dataName, 
				"",
				0.5);
		log.addFigure(
				Conf.pathFigures+figName+"-results-concrete"+Conf.figExt, 
				"BALNet results concrete F: "+dataName,
				"",
				0.5);
//		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");	
	}
}
