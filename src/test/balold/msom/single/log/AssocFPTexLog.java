package test.balold.msom.single.log;

import java.io.IOException;

import base.BAL1;
import util.TexLog;
import util.Util;
import util.visu.DataPairsVisResultsAssym;
import base.Conf;
import base.NetUtil;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class AssocFPTexLog {

	public static final String logName = "log-robotic-single-fp.tex";
	public static final int sizeVis = 14;
	public static final int sizeMot = 12;
	public static final int kVis = 16;
	public static final int kMot = 8;
	public static final String pathVisual = "input/msom/visual_"+sizeVis+"_"+sizeVis+".txt";
	public static final String pathMotor = "input/msom/motor_"+sizeMot+"_"+sizeMot+".txt";
	public static final int[] visParams = {3, 5, 5, 11};
	public static final String[] errNames = Conf.errorLabelsNew;
	public static final String dataName = sizeVis+"k"+kVis+"vs"+sizeMot+"k"+kMot;
	public static final String figResults = Conf.pathFigures+"assoc-robotic-fp-"+dataName+"-results"+Conf.figExt;
	public static final String figResultskWTA = Conf.pathFigures+"assoc-robotic-fp-"+dataName+"-results-kWTA"+Conf.figExt;
	public static final String dataSetLine = "datasets: vis "+sizeVis+"x"+sizeVis+", k="+kVis+", mot "+sizeMot+"x"+sizeMot+", k="+kMot;
	public static final int hids = 200;
	public static final int maxEpcs = 2000;
	public static final int plotRes = 50;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		double[][] dataV = dataVis.valuesFirstPersp();
		double[][] dataM = dataMot.values();
		int inps = dataVis.patternSize();
		int outs = dataMot.patternSize();
		
		BAL1 net = new BAL1(inps,hids,outs,Conf.lRate,Conf.wmax,Conf.wmin);
		System.out.println("BALNet "+inps+"-"+hids+"-"+outs);
		String[][][] plotData = new String[errNames.length/2][2][maxEpcs/plotRes+1];
		
		double[] trainErrors = new double[errNames.length];
		int epoch = 0;
		int index = 0;
		while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < maxEpcs) {
			trainErrors = net.epoch(dataV,dataM);
			if (epoch % plotRes == 0) {
				for (int i = 0; i < trainErrors.length/2; i++) {
					plotData[i][0][index] = "("+epoch+","+trainErrors[2*i]+")";
					plotData[i][1][index] = "("+epoch+","+trainErrors[2*i+1]+")";
				}	
				index++;
				System.out.print(".");
			}
			epoch++;
		}
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(errNames[i]+": "+trainErrors[i]);
		}
		System.out.println("train epochs: "+epoch);
		System.out.println();
		double[] testErrors = net.testEpoch(dataV,dataM);
		double[] testErrorskWTA = net.testEpoch(dataV,dataM);
//		save results
		String[][] resultsTable = new String[errNames.length][4];
		for (int i = 0; i < resultsTable.length; i++) {
			resultsTable[i][0] = errNames[i];
			resultsTable[i][1] = ""+Util.round(trainErrors[i],3);
			resultsTable[i][2] = ""+Util.round(testErrors[i],3);
			resultsTable[i][3] = ""+Util.round(testErrorskWTA[i],3);
		}
//		save patterns
		double[][][] patterns = net.testEpochPatterns(dataV,dataM);
		System.out.println(patterns[0][0].length);
		System.out.println(patterns[2][0].length);
		DataPairsVisResultsAssym visRes = new DataPairsVisResultsAssym(visParams,patterns);
		visRes.display();
		visRes.saveImage(Conf.pathOutput+figResults);
		double[][][] patternsKWTA = net.testEpochPatternskWTA(dataV,dataM);
		System.out.println(patternsKWTA[0][0].length);
		System.out.println(patternsKWTA[2][0].length);
		DataPairsVisResultsAssym visRes2 = new DataPairsVisResultsAssym(visParams,patternsKWTA);
		visRes2.display();
		visRes2.saveImage(Conf.pathOutput+figResultskWTA);
//		write log
		String[] setup = {
			"architecture: "+inps+"-"+hids+"-"+outs,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			dataSetLine,
		};
		TexLog log = new TexLog();
		log.addLine("");
		log.addLine("\\section{BALNet with robotic data: single simulation}");
		log.addLine("\\subsection{Setup parameters}");
		log.addItemize(setup);
		log.addLine("");
		log.addResultsTable(
				new String[] {"error","train","test","after kWTA"}, 
				resultsTable, 
				"MSOM robotic data first perspective "+dataName, 
				"tab:grbidir-single-"+dataName
		);
		log.addLine("");
		log.addLine("\\begin{figure}[!htbp]");
		log.addLine("\\centering");
		for (int i = 0; i < plotData.length; i++) {
			log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
			log.addLine("\\centering");
			log.addPlot(plotData[i],Conf.plotLabels[i],"epoch",Conf.errorNames[i]);
			log.addLine("\\end{subfigure}");
		}
		log.addLine("\\caption{Results for single instance of BALNet Associator with robotic data}");
		log.addLine("\\label{fig:assoc-single-robotic}");
		log.addLine("\\end{figure}");
		log.addLine("");
		log.addLine("");
		log.addLine("\\begin{figure}[!htbp]");
		log.addLine("\\centering");
		log.addSubFigure(figResults, "BALNet msom data "+sizeVis+"x"+sizeVis+" - "+sizeMot+"x"+sizeMot+": results visual",
			"fig:grbidir-single-"+dataName+"-results-vis", 0.9);	
		log.echo();
		log.addSubFigure(figResultskWTA, "BALNet msom data "+sizeVis+"x"+sizeVis+" - "+sizeMot+"x"+sizeMot+": results visual after kWTA",
				"fig:grbidir-single-"+dataName+"-results-vis-kWTA", 0.9);	
			log.echo();
		log.addLine("\\end{figure}");
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
}
