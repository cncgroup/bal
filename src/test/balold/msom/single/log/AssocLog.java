package test.balold.msom.single.log;

import java.io.IOException;

import base.BAL1;
import util.TexLog;
import util.Util;
import util.visu.DataPairsVisResultsAssym;
import util.visu.Plot;
import util.visu.robotic.DataPairsAssymFP;
import base.Conf;
import base.NetUtil;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class AssocLog {

	public static final String logName = "log-robotic-single.tex";
	public static final int sizeVis = 16;
	public static final int sizeMot = 8;
	public static final String pathVisual = "input/msom/visual_"+sizeVis+"_"+sizeVis+".txt";
	public static final String pathMotor = "input/msom/motor_"+sizeMot+"_"+sizeMot+".txt";
	public static final int kVis = 16;
	public static final int kMot = 8;
	public static final int[] visParams = {3, 5, 10, 15};
	public static final String[] errorLabels = Conf.errorLabels;
	public static final String dataName = sizeVis+"k"+kVis+"vs"+sizeMot+"k"+kMot;
	public static final String figName = "assoc-robotic-fp-"+dataName;;
	public static final String dataSetLine = "datasets: vis "+sizeVis+"x"+sizeVis+", k="+kVis+", mot "+sizeMot+"x"+sizeMot+", k="+kMot;
	public static final int hids = 200;
	public static final int maxEpcs = 5000;
	
	public static void main(String[] args) throws IOException {
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		dataMot.multiply();
		double[][] dataV = dataVis.values();
		double[][] dataM = dataMot.values();
		System.out.println(dataM.length);
		System.out.println(dataV.length);
		int inps = dataVis.patternSize();
		int outs = dataMot.patternSize();
		DataPairsAssymFP vis = new DataPairsAssymFP(visParams, dataV, dataM, dataMot.sequences());
 		vis.display();
 		vis.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"sampleset"+Conf.figExt);
		BAL1 net = new BAL1(inps,hids,outs,Conf.lRate,Conf.wmax,Conf.wmin);
		System.out.println("BALNet "+inps+"-"+hids+"-"+outs);
		Plot plot = new Plot(Conf.netErrors);
		double[] trainErrors = new double[6];
		int epoch = 0;
		while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < maxEpcs) {
			trainErrors = net.epoch(dataV,dataM);
			for (int i = 0; i < trainErrors.length; i++) {
				plot.addPoint(i, epoch, trainErrors[i]);	
			}	
			epoch++;
			if (epoch % 100 == 0) 
				System.out.print(".");
		}
		System.out.println(epoch);
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(errorLabels[i]+": "+trainErrors[i]);
		}
		System.out.println();
		double[] testErrors = net.testEpoch(dataV,dataM);
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
//		save results
		String[][] resultsTable = new String[6][3];
		for (int i = 0; i < resultsTable.length; i++) {
			resultsTable[i][0] = Conf.errorLabels[i];
			resultsTable[i][1] = ""+Util.round(trainErrors[i],3);
			resultsTable[i][2] = ""+Util.round(testErrors[i],3);
		}
//		save plot
		plot.display();
		plot.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-results"+Conf.figExt);
//		save patterns
		double[][][] patterns = net.testEpochPatterns(dataV,dataM);
		System.out.println(patterns[0][0].length);
		System.out.println(patterns[2][0].length);
		DataPairsVisResultsAssym visRes = new DataPairsVisResultsAssym(visParams,patterns);
		visRes.display();
		visRes.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-results-vis"+Conf.figExt);
//		write log
		String[] setup = {
			"architecture: "+inps+"-"+hids+"-"+outs,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			dataSetLine,
		};
		TexLog log = new TexLog();
		log.addLine("\\subsection{Simulation 2: "+sizeVis+"x"+sizeVis+"-k"+kVis+" vs "+sizeMot+"x"+sizeMot+"-k"+kMot+"}");
		log.addLine("{\\bf Setup parameters:}\\\\");
		log.addItemize(setup);
		log.addResultsTable(
				new String[] {"error","train","test"}, 
				resultsTable, 
				"MSOM robotic data first perspective "+dataName, 
				"tab:grbidir-single-"+dataName
		);
		log.addFigure(
			Conf.pathFigures+figName+Conf.figExt, 
			"BALNet single instance "+dataName+": results",
			"fig:grbidir-single-"+dataName, 
			1.0);
		log.addFigure(
			Conf.pathFigures+figName+"sampleset"+Conf.figExt, 
			"BALNet single instance: "+sizeVis+"x"+sizeVis+"vs"+sizeMot+"x"+sizeMot+": results visually",
			"fig:grbidir-single-"+dataName+"-results-vis", 
			0.8);		
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
}
