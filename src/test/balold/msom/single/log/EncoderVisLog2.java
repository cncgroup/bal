package test.balold.msom.single.log;

import java.io.IOException;

import base.BAL1;
import util.TexLog;
import util.Util;
import util.visu.Plot;
import base.Conf;
import base.NetUtil;
import data.DataRoboticVis;

public class EncoderVisLog2 {
	
	public static final int dataSetNo = 3;	
	public static final int[] sizes = {10,12,14,16};
	public static final int size = sizes[dataSetNo];
	public static final int k = 12;
	public static final String dataName = size+"x"+size+"-k"+k;
	public static final String dataSetLine = "dataset visual "+size+"x"+size+"-k"+k;
	public static final String figName = "grbidir-encoder-vis-results"+dataName+Conf.figExt;
	public static final String logName = "log-robotic-single-encoder-vis-tmp.tex";
	public static final int hids = 120;
	public static final int maxEpcs = 1000;
	
	public static void main(String[] args) throws IOException {
		DataRoboticVis dataVis = new DataRoboticVis(Conf.dataSetsVisPaths[dataSetNo]);
		int inps = dataVis.patternSize();
		dataVis.kWinnerTakeAll(k);
		double[][] dataIn = dataVis.values();
		double[][] dataDes = dataVis.values();
		BAL1 net = new BAL1(dataIn[0].length,hids,dataDes[0].length,Conf.lRate,Conf.wmax,Conf.wmin);
		Plot plot = new Plot(Conf.netErrors);
		double[] trainErrors = new double[6];
		int epoch = 0;
		while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < maxEpcs) {
			trainErrors = net.epoch(dataIn,dataDes);
			for (int i = 0; i < trainErrors.length; i++) {
				plot.addPoint(i, epoch, trainErrors[i]);	
			}
			epoch++;
			if (epoch % 100 == 0) 
				System.out.print(".");
		}
		System.out.println();
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+trainErrors[i]);
		}
		System.out.println();
		double[] testErrors = net.testEpoch(dataIn,dataDes);
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
		// save results
		String[][] resultsTable = new String[7][3];
		for (int i = 0; i < resultsTable.length-1; i++) {
			resultsTable[i][0] = Conf.errorLabels[i];
			resultsTable[i][1] = ""+Util.round(trainErrors[i],3);
			resultsTable[i][2] = ""+Util.round(testErrors[i],3);
		}
		resultsTable[6][0] = "epochs";
		resultsTable[6][1] = epoch+"";
		resultsTable[6][2] = "1";
		// save plot
		plot.display();
		plot.saveImage(Conf.pathOutput+Conf.pathFigures+figName);
		//write log
		String[] setup = {
			"architecture: "+inps+"-"+hids+"-"+inps,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			dataSetLine,
		};
		TexLog log = new TexLog();
		log.addLine("{\\bf Setup parameters:}\\\\");
		log.addItemize(setup);
		log.addResultsTable(
				new String[] {"error","train","test"}, 
				resultsTable, 
				"MSOM robotic data first perspective "+dataName, 
				""
		);
		log.addFigure(
			Conf.pathFigures+figName, 
			"BALNet single instance "+dataName+": results",
			"", 
			1.0);
		log.echo();
		log.exportPartial(Conf.pathOutput+logName);
//		System.out.println("Finished writing \""+logName+"\"");
	}
}