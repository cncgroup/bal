package test.balold.msom.single.log;

import java.io.IOException;

import util.TexLog;
import util.Util;
import util.visu.DataPairsVisResults;
import util.visu.Plot;
import base.Conf;
import base.BAL1;
import base.NetUtil;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class AssocVisLog2 {
	
	public static final int sizeVis = 10;
	public static final int sizeMot = 10;
	public static final String pathVisual = Conf.dataSetsVisPaths[0];
	public static final String pathMotor = Conf.dataSetsMotPaths[1];
	public static final int k = 8;
	public static final String dataName = sizeVis+"vs"+sizeMot;
	public static final String figName = "assoc-robotic-"+dataName;
	public static final String logName = "log-simple-robotic.tex";
	public static final String dataSetLine = "datasets: vis "+sizeVis+"x"+sizeVis+", mot "+sizeMot+"x"+sizeMot+", k=";
	public static final int[] visParams = {5, 5, 5, 10, 15};

	public static final String[] errorLabels = Conf.errorLabels;
	public static final int hids = 80;//140;
	public static final int maxEpcs = 1000;
	
	public static void main(String[] args) throws IOException {
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(k);
		dataMot.kWinnerTakeAll(k);
		dataMot.multiply();
		double[][] dataV = dataVis.values();
		double[][] dataM = dataMot.values();
// 		DataPairsVisAssym vis = new DataPairsVisAssym(visParams, dataV, dataM);
// 		vis.display();
// 		vis.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-sampleset"+Conf.figExt);
		int inps = dataVis.patternSize();
		int outs = dataMot.patternSize();
		
		Plot plot = new Plot(Conf.netErrors);
		double[] trainErrors = new double[Conf.netErrors.length];
		int epoch = 0;
		BAL1 net = new BAL1(inps,hids,outs,Conf.lRate,Conf.wmax,Conf.wmin);
		System.out.println("BALNet "+inps+"-"+hids+"-"+outs);
		
		while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < maxEpcs) {	
			epoch++;
			trainErrors = net.epoch(dataV,dataM);
			if (epoch % 100 == 0) {
				for (int i = 0; i < trainErrors.length; i++) {
					plot.addPoint(i, epoch, trainErrors[i]);	
				}
				System.out.print(".");
			}
		}
		System.out.println(epoch);
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(errorLabels[i]+": "+trainErrors[i]);
		}
		System.out.println();
		double[] testErrors = net.testEpoch(dataV,dataM);
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
		System.out.println();
		// save results
		String[][] resultsTable = new String[6][3];
		for (int i = 0; i < resultsTable.length; i++) {
			resultsTable[i][0] = Conf.errorLabels[i];
			resultsTable[i][1] = ""+Util.round(trainErrors[i],3);
			resultsTable[i][2] = ""+Util.round(testErrors[i],3);
		}
		// save plot
		plot.display();
		plot.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-results"+Conf.figExt);
		double[][][] patterns = net.testEpochPatterns(dataV,dataM);
		DataPairsVisResults visRes = new DataPairsVisResults(visParams,patterns);
		visRes.display();
		visRes.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-results-vis"+Conf.figExt);
		//write log
		String[] setup = {
			"architecture: "+inps+"-"+hids+"-"+outs,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			dataSetLine+k,
		};
		TexLog log = new TexLog();
		log.addLine("\\subsection{Associator: single instance}");
		log.addLine("{\\bf Setup parameters:}\\\\");
		log.addItemize(setup);
		log.addResultsTable(
				new String[] {"error","train","test"}, 
				resultsTable, 
				"BALNet robotic data "+dataName+": results",
				"tab:grbidir-single-"+dataName
		);
		log.addFigure(
				Conf.pathFigures+figName+"sampleset"+Conf.figExt, 
				"Robotic Data - unique patterns first perspective "+sizeVis+"x"+sizeVis+"vs"+sizeMot+"x"+sizeMot, 
				"fig:grbidir-single-"+dataName+"-sampleset", 
				0.8);
		log.addFigure(
				Conf.pathFigures+figName+"-results"+Conf.figExt, 
				"BALNet single instance: "+sizeVis+"x"+sizeVis+"vs"+sizeMot+"x"+sizeMot+": results",
				"fig:grbidir-single-"+dataName+"-results", 
				1.0);
		log.echo();
//		log.export(logName);
//		System.out.println("Finished writing \""+logName+"\"");
	}
}
