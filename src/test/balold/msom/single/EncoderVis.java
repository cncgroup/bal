package test.balold.msom.single;

import java.io.IOException;

import base.BAL1;
import util.visu.Plot;
import base.Conf;
import base.NetUtil;
import data.DataRoboticVis;

public class EncoderVis {
	
	public static final String figName = "generec-msom-endpoint-assoc.png";
	public static final String logName = "log-assoc-simple.tex";
	public static final String[] errorLabels = {"mse","succ","mseB","succB","kWTAerrF","kWTAerrB"};
	public static final int size = 12;
	public static final int k = 8;	
	public static final int hids = 80;
	public static final int maxEpcs = 1000;
	
	public static void main(String[] args) throws IOException {
				
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+size+"_"+size+".txt");
		dataVis.kWinnerTakeAll(k);
		BAL1 net = new BAL1(dataVis.patternSize(),hids,dataVis.patternSize(),Conf.lRate,Conf.wmax,Conf.wmin);
		Plot plot = new Plot(errorLabels); 
		double[] errors = new double[6];
		int epoch = 0;
		while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs) {
			errors = net.epoch(dataVis.values(),dataVis.values());
			for (int i = 0; i < errors.length; i++) {
				plot.addPoint(i, epoch, errors[i]);
			}			
			epoch++;
			if (epoch % 100 == 0) 
				System.out.print(".");
		}
		System.out.println(epoch);
//		double[] testErrors = net.testEpoch(dataVis.values(),dataMot.values());
		for (int i = 0; i < errors.length; i++) {
			System.out.println(errorLabels[i]+": "+errors[i]);
		}
		plot.display();
//		plot.saveAsImage(Conf.pathOutput+figName);
//		String[] setup = {
//			"architecture: "+Conf.inps+"-"+Conf.hids+"-"+Conf.outs,
//			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
//			"learning rate: "+ Conf.lRate,
//			"min mse: "+ Conf.minMSE,
//			"min succ"+ Conf.minSucc,
//			"max epochs: "+ epcsPhase1,
//		};
//		TexLog log = new TexLog();	
//		log.addLine("\\\\subsection{GeneRec Bidir Robotic Associator}");
//		log.addLine("{\\bf Setup parameters:}\\\\");
//		log.addItemize(setup);
//		log.addFigure(figName, "generec robotic associator", "fig:r1", 1.0);
//		log.echo();
//		log.export(logName);
//		System.out.println("Finished writing \""+logName+"\"");
	}
}
