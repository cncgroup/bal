package test.balold.msom.batch;

import java.io.IOException;

import test.helpers.BALClassicTest;
import test.helpers.NetTestResults;
import util.TexLog;
import base.Conf;
import data.DataRoboticMot;

public class TestkWTAEncMot {
	
	public static final String logName = "log-robotic-batch-encoder-kWTA-mot.tex";
	public static final String pathMotor = Conf.dataSetsMotPaths[1];
	public static final int netCount = 20;
	public static final int maxEpcs = 1500;

	public static void main(String[] args) throws IOException {
		
		int maxK = Conf.maxK;
		int minK = Conf.minK;
		
		BALClassicTest test = new BALClassicTest(netCount, maxEpcs);
		NetTestResults results = new NetTestResults(Conf.testErrors);
		
		TexLog log = new TexLog();
		log.addLine("\\subsection{Batch tests with different size of training set}");
		String[] setup = {
			"architecture: "+Conf.inps+"-"+Conf.hids+"-"+Conf.outs,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			"net count: " + netCount,
		};
		log.addLine("{\\bf Setup parameters:}\\\\");
		log.addItemize(setup);
		
		double[] testedParams = new double[maxK-minK+1];
		for (int k = minK; k <= maxK; k++) {
			System.out.println("Processing k = "+k);
			DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
			dataMot.kWinnerTakeAll(k);
			results.mergeWith(test.testSimple(dataMot.values(),dataMot.values()));
			testedParams[k-minK] = k;
		}
		log.addLog(test.logData(results, "kWTA", testedParams));
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
