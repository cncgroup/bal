package test.balold.msom.batch;

import java.io.IOException;

import test.helpers.BALClassicTest;
import util.TexLog;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class TestNetParamsFP {
	
	public static final int netCount = 20;
	public static final int maxEpcs = 3500;
	public static final int k = 8;
	public static final String pathVisual = Conf.dataSetsVisPaths[0];
	public static final String pathMotor = Conf.dataSetsMotPaths[1];
	public static final String logName = "log-robotic-batch-netparams-fp.tex";
	
	public static final double[] learningRates = {0.1, 0.2, 0.5, 1.0, 1.5, 2.0};
	public static final int[] hiddenSizes = {80, 100, 120, 150, 180};
	
	public static void main(String[] args) throws IOException {
		BALClassicTest test = new BALClassicTest(netCount, maxEpcs);
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(k);
		dataMot.kWinnerTakeAll(k);
		double[][] dataIn = dataVis.valuesFirstPersp();
		double[][] dataDes = dataMot.values();
		TexLog log = new TexLog();
//		log.addLog(test.testAndLog(dataIn, dataDes, hiddenSizes, learningRates)); TODO: opravit
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
