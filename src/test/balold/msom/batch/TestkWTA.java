package test.balold.msom.batch;

import java.io.IOException;

import test.helpers.BALClassicTest;
import test.helpers.NetTestResults;
import util.TexLog;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class TestkWTA {
	
	public static final String logName = "log-robotic-batch-kWTA.tex";	
	public static final String pathVisual = Conf.dataSetsVisPaths[0];
	public static final String pathMotor = Conf.dataSetsMotPaths[1];
	public static final int netCount = 50;
	public static final int maxEpcs = 3500;

	public static void main(String[] args) throws IOException {
		
		int[] ks = {4,8,12};
		BALClassicTest test = new BALClassicTest(netCount, maxEpcs);
		NetTestResults results = new NetTestResults(Conf.testErrors);
		
		TexLog log = new TexLog();
		log.addLine("\\subsection{Batch tests with different size of training set}");
		String[] setup = {
			"architecture: "+Conf.inps+"-"+Conf.hids+"-"+Conf.outs,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			"net count: " + netCount,
			"data set: visual 10x10 - motor 10x10",
		};
		log.addLine("{\\bf Setup parameters:}\\\\");
		log.addItemize(setup);
		
		double[] testedParams = new double[ks.length];
		for (int i = 0; i < ks.length; i++) {
			System.out.println("Processing k = "+ks[i]);
			DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
			DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
			dataMot.multiply();
			dataVis.kWinnerTakeAll(ks[i]);
			dataMot.kWinnerTakeAll(ks[i]);
			results.mergeWith(test.testSimple(dataVis.values(),dataMot.values()));
			testedParams[i] = ks[i];
		}
		log.addLog(test.logData(results, "kWTA", testedParams));
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
