package test.balold.msom.batch;

import java.io.IOException;

import test.helpers.BALClassicTest;
import test.helpers.NetTestResults;
import util.TexLog;
import base.Conf;
import data.DataRoboticVis;

public class TestkWTAEncVis {
	
	public static final String logName = "log-robotic-batch-encoder-kWTA-vis.tex";
	public static final int dataId = 0;
	public static final String pathVisual = Conf.dataSetsVisPaths[dataId];
	public static final int sampleSize  = Conf.dataMotSizes[dataId]*Conf.dataMotSizes[dataId];
	public static final int hidSize = Conf.hids;
	public static final int netCount = 20;
	public static final int maxEpcs = 1000;
	
	public static void main(String[] args) throws IOException {
		
		int maxK = Conf.maxK;
		int minK = Conf.minK;
		
		BALClassicTest test = new BALClassicTest(netCount, maxEpcs);
		NetTestResults results = new NetTestResults(Conf.testErrors);
		
		TexLog log = new TexLog();
		log.addLine("\\subsection{Batch tests with visual data: varying k}");
		String[] setup = {
			"architecture: "+sampleSize+"-"+hidSize+"-"+sampleSize,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			"net count: " + netCount,
		};
		log.addLine("{\\bf Setup parameters:}\\\\");
		log.addItemize(setup);
		
		double[] testedParams = new double[maxK-minK+1];
		for (int k = minK; k <= maxK; k++) {
			System.out.println("Processing k = "+k);
			DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
			dataVis.kWinnerTakeAll(k);
			results.mergeWith(test.testSimple(dataVis.values(),dataVis.values(),hidSize));
			testedParams[k-minK] = k;
		}
		log.addLog(test.logData(results, "kWTA", testedParams));
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
