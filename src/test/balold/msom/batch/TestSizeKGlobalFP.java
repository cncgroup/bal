package test.balold.msom.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import base.BAL1;
import util.Util;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class TestSizeKGlobalFP {
	
	public static final int netCount = 20;
	public static final int maxEpcs = 1000;
	public static final int[][] visKs = {
		{9,8,10,11},
		{12,13,15,6},
		{17,18,15,16},
		{19,4,20,16},
		{19,18,17,15},
		{19,17,4,20},
	};
	public static final int[][] motKs = {
		{4,5,8,6},
		{8,4,6,5},
		{8,4,6,5},
		{4,8,6,7},
		{4,5,11,7},
	};
	public static final int[] sizesV = Conf.dataVisSizes;
	public static final int[] sizesM = Conf.dataMotSizes;
	public static final String outPath = Conf.pathOutput + "log-msom-batch-sizes-ks-plain.txt";
	
	public static void main(String[] args) {
		
		ArrayList<String> log = new ArrayList<String>();
		String first = "mapSizeV"+"\t"+"mapSizeM"+"\t"+"kVis"+"\t"+"kMot"+"\t"+"mse"+"\t"+"patsucc"+"\t"+"bitsucc"+"\t"+"patdist";
		System.out.println(first);
		log.add(first);
		
		for (int sV = 0; sV < sizesV.length; sV++) {
			for (int sM = 0; sM < sizesM.length; sM++) {
				for (int kV = 0; kV < visKs[sV].length; kV++) {
					for (int kM = 0; kM < motKs[sM].length; kM++) {
						DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizesV[sV]+"_"+sizesV[sV]+".txt");
						DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizesM[sM]+"_"+sizesM[sM]+".txt");
						dataVis.kWinnerTakeAll(visKs[sV][kV]);
						dataMot.kWinnerTakeAll(motKs[sM][kM]);
						double[] errorsOveral = new double[Conf.errorLabelsNew.length];
						int inps = dataVis.patternSize();
						int outs = dataMot.patternSize();
						int hids = (int) ((inps+outs)/2.0);
						for (int n = 0; n < netCount; n++) {
							int epoch = 0;
							BAL1 net = new BAL1(inps,hids,outs,Conf.lRate,Conf.wmin,Conf.wmax);
							while (epoch < maxEpcs) {
								net.epoch(dataVis.valuesFirstPersp(), dataMot.values());
								epoch++;	
							}
							double[] errors = net.testEpoch(dataVis.valuesFirstPersp(), dataMot.values());
							for (int i = 0; i < errors.length; i++) {
								errorsOveral[i] += errors[i];
							}
						}
						for (int i = 0; i < errorsOveral.length; i++) {
							errorsOveral[i] /= netCount;
						}
						String line = sizesV[sV]+"\t"+sizesM[sM]+"\t"+visKs[sV][kV]+"\t"+motKs[sM][kM]+"\t";
						for (int i = 0; i < errorsOveral.length/2; i++) {
							line += Util.round((errorsOveral[2*i]+errorsOveral[2*i+1])/2,3)+"\t";
						}
						System.out.println(line);
						log.add(line);
					}
				}
			}
		}
		
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter(new File(outPath)));
			for (int i = 0; i < log.size(); i++) {
				bw.write(log.get(i));
				bw.newLine();
			}
			bw.close();
			System.out.println("log written sucessfully");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
