package test.balold.msom.batch;

import java.io.IOException;

import test.helpers.BALClassicTest;
import test.helpers.NetTestResults;
import util.TexLog;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class TestkWTAMega {
	
	public static final String logName = "log-robotic-batch-kWTA-mega.tex";	
	public static final int netCount = 10;
	public static final int maxEpcs = 1000;
	public static final int[] hidSizes = {80,120,180,240};
	public static final int[][] visKs = {
		{8,10,12}, //10x10
		{8,12,14}, //12x12
		{10,14,16}, //14x14
		{12,16,18}, //16x16
	};
	public static final int[][] motKs = {
		{6,8,10}, //8x8
		{8,10,12}, //10x10
		{10,12,14}, //12x12
	};
	public static final int[] sizesV = Conf.dataVisSizes;
	public static final int[] sizesM = Conf.dataMotSizes;
	
	public static void main(String[] args) throws IOException {
		

		TexLog log = new TexLog();
		for (int i = 0; i < sizesV.length; i++) {
			for (int j = 0; j < sizesM.length; j++) {
				System.out.println("Processing visdata: "+sizesV[i]+"x"+sizesV[i]+" with motdata: "+sizesM[j]+"x"+sizesM[j]);
				log.addLine("\\subsection{DataSet Visual "+sizesV[i]+"x"+sizesV[i]+" Motor "+sizesM[j]+"x"+sizesM[j]+"}");
				log.addLine("");
				BALClassicTest test = new BALClassicTest(netCount, maxEpcs);
				NetTestResults results = new NetTestResults(Conf.testErrors);
				double[] testedParams = new double[visKs[i].length];
				for (int k = 0; k < visKs[i].length; k++) {
					System.out.println("Processing kVis = "+visKs[i][k]+" kMot = "+motKs[j][1]);
					DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizesV[i]+"_"+sizesV[i]+".txt");
					DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizesM[j]+"_"+sizesM[j]+".txt");
//					dataMot.multiply();
					dataVis.kWinnerTakeAll(visKs[i][k]);
					dataMot.kWinnerTakeAll(motKs[j][1]);
					results.mergeWith(test.testSimple(dataVis.valuesFirstPersp(),dataMot.values(),hidSizes[i]));
					testedParams[k] = visKs[i][k];
				}
				log.addLog(test.logData(results, "k-vis", testedParams));
			}
		}
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
