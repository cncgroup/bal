package test.balold.msom.batch;

import java.io.IOException;

import test.helpers.BALClassicTest;
import test.helpers.NetTestResults;
import util.TexLog;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class TestkWTAFP {
	
	public static final int sizeVis = 16;
	public static final int sizeMot = 10;
	public static final String pathVisual = "input/msom/visual_"+sizeVis+"_"+sizeVis+".txt";
	public static final String pathMotor = "input/msom/motor_"+sizeMot+"_"+sizeMot+".txt";
	public static final String logName = "log-robotic-batch-firstpersp-kWTA-alt.tex";
	public static final int netCount = 20;
	public static final int maxEpcs = 1000;

	public static void main(String[] args) throws IOException {
		
		int[] ksVis = {10,14,18};
		int[] ksMot = {4,8,12};
		BALClassicTest test = new BALClassicTest(netCount, maxEpcs);
		NetTestResults results = new NetTestResults(Conf.testErrors);
		
		TexLog log = new TexLog();
		log.addLine("\\subsection{Batch tests with different size of training set}");
		String[] setup = {
			"architecture: "+Conf.inps+"-"+Conf.hids+"-"+Conf.outs,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			"net count: " + netCount,
		};
		log.addLine("{\\bf Setup parameters:}\\\\");
		log.addItemize(setup);
		
		double[] testedParams = new double[ksVis.length];
		for (int i = 0; i < ksVis.length; i++) {
			System.out.println("Processing k = "+ksVis[i]+"/"+ksMot[i]);
			DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
			DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
			dataVis.kWinnerTakeAll(ksVis[i]);
			dataMot.kWinnerTakeAll(ksMot[i]);
			results.mergeWith(test.testSimple(dataVis.valuesFirstPersp(),dataMot.values()));
			testedParams[i] = ksVis[i];
		}
		log.addLog(test.logData(results, "k", testedParams));
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
