package test.balold.msom.unique;

import java.io.IOException;

import test.helpers.BALClassicTest;
import test.helpers.NetTestResults;
import util.TexLog;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class TestkWTAFP {
	
	public static final int netCount = 100;
	public static final int maxEpcs = 3000;
	public static final String logName = "log-batch-robotic-kWTA.tex";
	public static final String figName = "data-msom-firstpersp-unique-k";
	public static final String figCaption = "Dataset robotic data first perspective unique patterns k = ";
	public static final int[] visParams = {5, 10, 30, 10, 6};
	public static final String pathVisual = Conf.dataSetsVisPaths[0];
	public static final String pathMotor = Conf.dataSetsMotPaths[1];
	
	public static void main(String[] args) throws IOException {
		
		int maxK = Conf.maxK;
		int minK = Conf.minK;
		
		BALClassicTest test = new BALClassicTest(netCount, maxEpcs);
		NetTestResults results = new NetTestResults(Conf.testErrors);
		
		TexLog log = new TexLog();
		log.addLine("\\subsection{Batch tests with different size of training set}");
		String[] setup = {
			"architecture: "+Conf.inps+"-"+Conf.hids+"-"+Conf.outs,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			"net count: " + netCount,
		};
		log.addLine("{\\bf Setup parameters:}\\\\");
		log.addItemize(setup);
		
		double[] testedParams = new double[maxK-minK+1];
		for (int k = minK; k <= maxK; k++) {
			System.out.println("Processing k = "+k);
			double[][][] data = prepareData(k);
			//visualize
//	 		DataPairsVis vis = new DataPairsVis(visParams, data[0], data[1]);
//	 		vis.display();
//	 		vis.saveImage(Conf.pathOutput+Conf.pathFigures+figName+k+Conf.figExt);
	 		//run sim
			results.mergeWith(test.testSimple(data[0], data[1]));
			testedParams[k-minK] = k;
		}
		log.addLog(test.logData(results, "kWTA", testedParams));
		for (int k = minK; k <= maxK; k++) {
			log.addFigure(Conf.pathFigures+figName+k+Conf.figExt, figCaption+k, "fig:"+figName+k, 1.0);
		}
		log.echo();
		log.export(Conf.pathOutput+logName);
	}
	
	public static double[][][] prepareData(int k) {
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(k);
		dataMot.kWinnerTakeAll(k);
		double[][] dataV = dataVis.dataPatternsOnly(0); //first perspective
		double[][] dataM = new double[dataV.length][dataV[0].length];
 		for (int i = 0; i < dataM.length; i++) {
 			int[] coord =  dataVis.findIndex(dataV[i]);
			dataM[i] = dataMot.dataSeq()[coord[0]].get(coord[2]);
		}
 		double[][][] out = new double[2][dataV.length][dataV[0].length];
 		out[0] = dataV;
 		out[1] = dataM;
 		return out;
	}
	
}
