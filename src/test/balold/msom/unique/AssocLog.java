package test.balold.msom.unique;

import java.io.IOException;

import base.BAL1;
import util.TexLog;
import util.visu.DataPairsVis;
import util.visu.Plot;
import base.Conf;
import base.NetUtil;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class AssocLog {
	
	public static final String figName = "grbidir-robotic-unique-patterns-";
	public static final String logName = "log-assoc-simple.tex";
	public static final int[] visParams = {5, 10, 10, 10, 6};
	public static final String pathVisual = Conf.dataSetsVisPaths[0];
	public static final String pathMotor = Conf.dataSetsMotPaths[1];
	public static final int k = 8;	
	public static final int maxEpcs = 3000;
	public static final double minMSE = Conf.minMSE;
	public static final double minSucc = Conf.minPatSucc;
	
	public static void main(String[] args) throws IOException {
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(k);
		dataMot.kWinnerTakeAll(k);
		double[][] dataV = dataVis.dataPatternsOnly(0); //first perspective
		double[][] dataM = new double[dataV.length][dataV[0].length];
 		for (int i = 0; i < dataM.length; i++) {
			dataM[i] = dataMot.dataPatternsOnly()[i];
		}
 		DataPairsVis vis = new DataPairsVis(visParams, dataM, dataV);
// 		vis.display();
 		vis.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"sampleset"+Conf.figExt);
		BAL1 net = new BAL1(Conf.inps,Conf.hids,Conf.outs,Conf.lRate,Conf.wmax,Conf.wmin);
		Plot[] plots = new Plot[3];
		plots[0] = new Plot(Conf.mseLabels);
		plots[1] = new Plot(Conf.patSuccLabels); 
		plots[2] = new Plot(Conf.bitSuccLabels); 
		
		double[] errors = new double[6];
		int epoch = 0;
		while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs) {
			errors = net.epoch(dataV,dataM);
			int plotindex = 0;
			for (int i = 0; i < errors.length; i++) {
				plots[plotindex].addPoint(i % 2, epoch, errors[i]);	
				if (i % 2 == 1)
					plotindex++;
			}		
			epoch++;
			if (epoch % 100 == 0) 
				System.out.print(".");
		}
		System.out.println();
		for (int i = 0; i < errors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+errors[i]);
		}
		System.out.println();
		double[] testErrors = net.testEpoch(dataV,dataM);
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
		System.out.println(epoch);
		System.out.println();
		for (int i = 0; i < plots.length; i++) {
//			plots[i].display();
			plots[i].saveImage(Conf.pathOutput+Conf.pathFigures+figName+Conf.errorFigNames[i] + Conf.figExt);
		}
		String[] setup = {
			"architecture: "+Conf.inps+"-"+Conf.hids+"-"+Conf.outs,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min succ"+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
		};
		TexLog log = new TexLog();
		log.addLine("\\subsection{Associator: single instance}");
		log.addLine("{\\bf Setup parameters:}\\\\");
		log.addItemize(setup);
		log.addFigure(
				Conf.pathFigures+figName+"sampleset"+Conf.figExt, 
				"Robotic Data - unique patterns first perspective", 
				"fig:grbidir-single-uniqpat-sampleSize",
				1.0);
		for (int i = 0; i < plots.length; i++) {
			log.addFigure(
					Conf.pathFigures+figName+Conf.errorFigNames[i]+Conf.figExt, 
					"BALNet single instance - "+Conf.errorNames[i],
					"fig:grbidir-single-uniqpat-"+Conf.errorFigNames, 
					1.0);
		}
//		log.echo();
//		log.export(logName);
//		System.out.println("Finished writing \""+logName+"\"");
	}
}
