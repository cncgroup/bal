package test.balold.msom.unique;

import java.io.IOException;

import util.visu.DataPairsVis;
import util.visu.Plot;
import base.Conf;
import base.BAL1;
import base.NetUtil;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class AssocFirstPersp {
	
	public static final String figName = "generec-msom-endpoint-assoc.png";
	public static final String logName = "log-assoc-simple.tex";
	public static final int[] visParams = {5, 10, 30, 10, 6};
	public static final String pathVisual = Conf.dataSetsVisPaths[0];
	public static final String pathMotor = Conf.dataSetsMotPaths[1];
	public static final int k = 8;	
	public static final int maxEpcs = 1000;
	
	public static void main(String[] args) throws IOException {
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(k);
		dataMot.kWinnerTakeAll(k);
		double[][] dataV = dataVis.dataPatternsOnly(0); //first perspective
		double[][] dataM = new double[dataV.length][dataV[0].length];
 		for (int i = 0; i < dataM.length; i++) {
 			int[] coord =  dataVis.findIndex(dataV[i]);
			dataM[i] = dataMot.dataSeq()[coord[0]].get(coord[2]);
		}
 		DataPairsVis vis = new DataPairsVis(visParams, dataV, dataM);
 		vis.display();
 				
		BAL1 net = new BAL1(Conf.inps,Conf.hids,Conf.outs,Conf.lRate,Conf.wmax,Conf.wmin);
		Plot plot = new Plot(Conf.errorLabels); 
		double[] errors = new double[6];
		int epoch = 0;
		while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs) {
			errors = net.epoch(dataV,dataM);
			for (int i = 0; i < errors.length; i++) {
				plot.addPoint(i, epoch, errors[i]);
			}			
			epoch++;
			if (epoch % 100 == 0) 
				System.out.print(".");
		}
		System.out.println();
		for (int i = 0; i < errors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+errors[i]);
		}
//		plot.display();
	}
}
