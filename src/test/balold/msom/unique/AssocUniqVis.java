package test.balold.msom.unique;

import java.io.IOException;

import util.TexLog;
import util.Util;
import util.visu.DataPairsVis;
import util.visu.Plot;
import base.Conf;
import base.BAL1;
import base.NetUtil;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class AssocUniqVis {
	
	public static final String figName = "generec-robotic-unique-vis.png";
	public static final String dataName = "unique patterns from visual data with associated motor patterns";
	public static final int[] visParams = {4, 10, 10, 10, 8};
	public static final String[] errorLabels = Conf.errorLabels;
	public static final String pathVisual = Conf.dataSetsVisPaths[0];
	public static final String pathMotor = Conf.dataSetsMotPaths[1];	
	public static final int k = 8;	
	public static final int hids = 80;
	public static final int maxEpcs = 2000;
	
	public static void main(String[] args) throws IOException {
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(k);
		dataMot.kWinnerTakeAll(k);
		double[][] dataIn = dataVis.dataPatternsOnly(); //all perspectives
		double[][] dataDes = new double[dataIn.length][dataIn[0].length];
 		for (int i = 0; i < dataDes.length; i++) {
 			int[] coord =  dataVis.findIndex(dataIn[i]);
			dataDes[i] = dataMot.dataSeq()[coord[0]].get(coord[2]);
		}
 		DataPairsVis vis = new DataPairsVis(visParams, dataDes, dataIn);
 		vis.display();
 				
 		BAL1 net = new BAL1(dataIn[0].length,hids,dataDes[0].length,Conf.lRate,Conf.wmax,Conf.wmin);
		Plot plot = new Plot(Conf.errorLabels); 
		double[] trainErrors = new double[6];
		int epoch = 0;
		while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < maxEpcs) {
			trainErrors = net.epoch(dataIn,dataDes);
			for (int i = 0; i < trainErrors.length; i++) {
				plot.addPoint(i, epoch, trainErrors[i]);
			}			
			epoch++;
			if (epoch % 100 == 0) 
				System.out.print(".");
		}
		//test the net
		double[] testErrors = net.testEpoch(dataMot.values(),dataVis.values());
		double[][][] patterns = net.testEpochPatterns(dataIn,dataDes);
		//write to console
		System.out.println(epoch);
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(errorLabels[i]+": "+trainErrors[i]);
		}
		System.out.println();
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
		// save results
		String[][] resultsTable = new String[7][3];
		for (int i = 0; i < resultsTable.length; i++) {
			resultsTable[i][0] = Conf.testErrors[i];
			if (i == 6) {
				resultsTable[i][1] = ""+epoch;
				resultsTable[i][2] = "1";
			} else if (i > 1) {
				resultsTable[i][1] = ""+Util.round(100.0*trainErrors[i],2)+"\\%";
				resultsTable[i][2] = ""+Util.round(100.0*testErrors[i],2)+"\\%";
			} else {
				resultsTable[i][1] = ""+Util.round(trainErrors[i],3);
				resultsTable[i][2] = ""+Util.round(testErrors[i],3);
			}
		}
		// save plots and figures
		plot.display();
		plot.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-results"+Conf.figExt);
		DataPairsVis visF = new DataPairsVis(visParams, patterns[0], patterns[1]);
		DataPairsVis visB = new DataPairsVis(visParams, patterns[2], patterns[3]);
		visF.display();
		visB.display();
		visF.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-results-concrete-front"+Conf.figExt);
		visB.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-results-concrete-back"+Conf.figExt);
		// write log
		String[] setup = {
			"architecture: "+dataIn[0].length+"-"+hids+"-"+dataDes[0].length,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min pattern succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			"data: "+dataName,
		};
		TexLog log = new TexLog();	
		log.addLine("{\\bf Setup parameters:}\\\\");
		log.addItemize(setup);
		log.addResultsTable(
				new String[] {"error","train","test"}, 
				resultsTable, 
				"BALNet results: "+dataName,
				""
		);
		log.addFigure(
				Conf.pathFigures+figName+"-results"+Conf.figExt, 
				"BALNet results: "+dataName,
				"",
				1.0);
		log.addFigure(
				Conf.pathFigures+figName+"-data"+Conf.figExt, 
				"Dataset: "+dataName, 
				"",
				0.5);
		log.addFigure(
				Conf.pathFigures+figName+"-data"+"-results-concrete-front"+Conf.figExt, 
				"BALNet results concrete F: "+dataName,
				"",
				0.5);
		log.addFigure(
				Conf.pathFigures+figName+"-data"+"-results-concrete-back"+Conf.figExt, 
				"BALNet results concrete B: "+dataName,
				"",
				0.5);
		log.echo();
	}
}
