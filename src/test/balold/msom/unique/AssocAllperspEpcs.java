package test.balold.msom.unique;

import java.io.IOException;

import util.Util;
import util.visu.DataPairsVis;
import base.Conf;
import base.BAL1;
import base.NetUtil;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class AssocAllperspEpcs {
	
	public static final String figName = "generec-msom-endpoint-assoc.png";
	public static final String logName = "log-assoc-simple.tex";
	public static final int[] visParams = {5, 10, 30, 10, 6};
	public static final String pathVisual = Conf.dataSetsVisPaths[0];
	public static final String pathMotor = Conf.dataSetsMotPaths[1];
	public static final int k = 8;	
	public static final int[] maxEpcs = {1000,2000,3000,5000,10000};
	
	public static void main(String[] args) throws IOException {
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(k);
		dataMot.kWinnerTakeAll(k);
		double[][] dataV = dataVis.dataPatternsOnly(); //all perspectives
		double[][] dataM = new double[dataV.length][dataV[0].length];
 		for (int i = 0; i < dataM.length; i++) {
 			int[] coord =  dataVis.findIndex(dataV[i]);
			dataM[i] = dataMot.dataSeq()[coord[0]].get(coord[2]);
		}
 		DataPairsVis vis = new DataPairsVis(visParams, dataV, dataM);
 		vis.display();
 				
 		double[][] results = new double[maxEpcs.length][6];
 		for (int e = 0; e < maxEpcs.length; e++) {
 			BAL1 net = new BAL1(Conf.inps,Conf.hids,Conf.outs,Conf.lRate,Conf.wmax,Conf.wmin);
 			double[] errors = new double[6];
 			int epoch = 0;
 			while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs[4]) {
 				errors = net.epoch(dataV,dataM);		
 				epoch++;
 				if (epoch % 100 == 0) 
 					System.out.print(".");
 			}
 			System.out.println();
 			double[] testErrors = net.testEpoch(dataV,dataM);
 			for (int i = 0; i < testErrors.length; i++) {
 				System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
 			}
 			results[e] = testErrors;
		}
 		System.out.println();
		System.out.print("epcsPhase1\t");
		for (int i = 0; i < maxEpcs.length; i++) {
			System.out.print(Conf.errorLabels[i]+"\t");
		}
		System.out.println();
		for (int i = 0; i < results.length; i++) {
			System.out.print(maxEpcs[i]+"\t");
			for (int j = 0; j < results[i].length; j++) {
				System.out.print(Util.round(results[i][j],3)+"\t");
			}
			System.out.println();
		}

	}
}
