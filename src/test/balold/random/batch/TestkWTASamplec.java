package test.balold.random.batch;

import java.io.IOException;

import base.Conf;
import data.DataBinary;
import test.helpers.BALClassicTest;
import test.helpers.NetTestResults;
import util.TexLog;

public class TestkWTASamplec {
	
	public static final int k = 8;
	public static final int sampleSize  = 100;
	public static final int sampleCount = k*Conf.kToSizeRatio;
	public static final String fileIn  = Conf.pathDataKWTA+"k"+k+"-in"+sampleSize+"x"+sampleCount;
	public static final String fileDes = Conf.pathDataKWTA+"k"+k+"-des"+sampleSize+"x"+sampleCount;
	public static final String logName = "log-batch-random-kwta-sampleCount.tex";
	public static final int maxEpcs = 3500;
	public static final int netCount = 50;
	
	public static void main(String[] args) throws IOException {
		
		BALClassicTest test = new BALClassicTest(netCount, maxEpcs);
		NetTestResults results = new NetTestResults(Conf.testErrors);
		
		TexLog log = new TexLog();
		log.addLine("\\subsection{Batch tests with different size of training set}");
		log.addLine("Batch tests with random kWTA data with k = "+k+" and varying sample size from "+40+" up to "+40+40*4);
		String[] setup = {
			"architecture: "+Conf.inps+"-"+Conf.hids+"-"+Conf.outs,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			"net count: " + netCount,
		};
		log.addLine("{\\bf Setup parameters:}\\\\");
		log.addItemize(setup);
		
		double[] testedParams = new double[4];
		for (int i = 0; i < 4; i++) {
			int samplec = 40+40*i;
			System.out.println("Processing sampleCount = "+samplec);
			DataBinary data = new DataBinary(fileIn,fileDes);
			results.mergeWith(test.testSimple(data.dataX(),data.dataY()));
			testedParams[i] = samplec;
		}
		log.addLog(test.logData(results, "sample count", testedParams));
		
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
