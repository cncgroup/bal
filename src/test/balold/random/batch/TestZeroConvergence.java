package test.balold.random.batch;

import java.io.IOException;
import java.util.Arrays;

import base.BAL1;
import base.Conf;
import data.DataBinary;
import util.visu.DataPairsVis;

public class TestZeroConvergence {

	public static final int sampleSize = 100;
	public static final int sampleCount = 144;//80;//100;
	public static final int k = 10;
//	public static final String pathIn = "input/binary-kWTA/k"+k+"-in"+sampleSize+"x"+sampleCount;
//	public static final String pathDes = "input/binary-kWTA/k"+k+"-des"+sampleSize+"x"+sampleCount;
	public static final String fileIn  = Conf.pathDataRandomHard+"data-impossible-k"+k+"-in"+ sampleSize +"x"+ sampleCount;
	public static final String fileDes = Conf.pathDataRandomHard+"data-impossible-k"+k+"-des"+ sampleSize +"x"+ sampleCount;

	public static final double lRate = .2;//Conf.lRate;	
	public static final int hids = 120;
	public static final int netCount = 10;
	public static final int maxEpcs = 50000;
	public static final int[] visParams = {5, 0, 40, 10, 8}; // pxs, margX, margY, matrixS, rowHeight
	
	public static void main(String[] args) throws IOException {
		
		DataBinary data = new DataBinary(fileIn,fileDes);
		DataPairsVis vis = new DataPairsVis(visParams, data.dataX(),data.dataY());
		vis.display();

		int succ = 0;
		int maxesucc = 0;
		int minesucc = maxEpcs;
		System.out.println("                                    "+Arrays.toString(Conf.errorLabels));
		for (int n = 0; n < netCount; n++) {
			System.out.print("net"+n+": ");
			double[] errors = new double[6];
			int epoch = 0;
			BAL1 net = new BAL1(sampleSize,hids, sampleSize,lRate);
//			while (!(errors[2]==1.0 && errors[3]==1.0) && epoch < epcsPhase1) {
			while (errors[2] < 1.0 && epoch < maxEpcs) {
				errors = net.epoch(data.dataX(),data.dataY());
				epoch++;
				if (epoch % 500 == 0) System.out.print(".");
			}
			System.out.print(epoch+"epochs ");
//			if (errors[2] == 1.0 && errors[3] == 1.0) {
			if (errors[2] == 1.0) {
				succ++;
				maxesucc = (epoch > maxesucc) ? epoch : maxesucc;
				minesucc = (epoch < minesucc) ? epoch : minesucc;
			}
			System.out.println(Arrays.toString(errors));
		}
		System.out.println("nets successful: "+succ+" of "+netCount);
		System.out.println("max epochs to success: "+maxesucc);
		System.out.println("min epochs to success: "+minesucc);
		System.out.println();
	}
}
