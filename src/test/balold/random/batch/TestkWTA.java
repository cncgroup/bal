package test.balold.random.batch;

import java.io.IOException;

import base.Conf;
import data.DataBinary;
import test.helpers.BALClassicTest;
import test.helpers.NetTestResults;
import util.TexLog;

public class TestkWTA {
	
	public static final int netCount = 100;
	public static final int maxEpcs = 3500;
	public static final int sampleSize  = 100;
	public static final int sampleCount = 50;
	public static final String fileIn = "-in100x";
	public static final String fileDes = "-des100x";
	public static final String logName = "log-batch-random-easy-kwta.tex";

	public static void main(String[] args) throws IOException {
		
		int[] ks = {4,6,8,10,12};
		BALClassicTest test = new BALClassicTest(netCount, maxEpcs);
		NetTestResults results = new NetTestResults(Conf.testErrors);
		
		TexLog log = new TexLog();
		log.addLine("\\subsection{Tests with random kWTA data}");
		String[] setup = {
			"architecture: "+sampleSize+"-"+Conf.hids+"-"+sampleSize,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			"net count: " + netCount,
		};
		log.addLine("{\\bf Setup parameters:}\\\\");
		log.addItemize(setup);
		
		double[] testedParams = new double[ks.length];
		for (int i = 0; i < ks.length; i++) {
			System.out.println("Processing k = "+ks[i]);
			DataBinary data = new DataBinary(Conf.pathDataKWTA+"k"+ks[i]+fileIn+(ks[i]*Conf.kToSizeRatio), Conf.pathDataKWTA+"k"+ks[i]+fileDes+(ks[i]*Conf.kToSizeRatio));
			results.mergeWith(test.testSimple(data.dataX(),data.dataY()));
			testedParams[i] = ks[i];
		}
		log.addLog(test.logData(results, "k", testedParams));
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
