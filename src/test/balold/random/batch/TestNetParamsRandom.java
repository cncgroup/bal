package test.balold.random.batch;

import java.io.IOException;

import base.Conf;

import test.helpers.BALClassicTest;
import util.TexLog;
import data.DataBinary;

public class TestNetParamsRandom {
	
	public static final int netCount = 20;
	public static final int maxEpcs = 3000;
	public static final int sampleSize  = 100;
	public static final int sampleCount = 50;
	public static final String fileIn = "input/binary-random/inp100x"+sampleCount;
	public static final String fileDes = "input/binary-random/des100x"+sampleCount;
	public static final int k = 8;
	public static final String logName = "test-random-batch-netparams-rand50x100.tex";
	public static final double[] learningRates = {0.05, 0.1, 0.2, 0.5};
	public static final int[] hiddenSizes = {80, 100, 120, 150};
	
	public static void main(String[] args) throws IOException {
		BALClassicTest test = new BALClassicTest(netCount, maxEpcs);
		DataBinary data = new DataBinary(fileIn,fileDes);
		TexLog log = new TexLog();
//		log.addLog(test.testAndLog(data.dataX(),data.dataY(), hiddenSizes, learningRates)); TODO: opravit
//		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
