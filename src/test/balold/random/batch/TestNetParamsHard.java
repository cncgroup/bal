package test.balold.random.batch;

import java.io.IOException;

import base.Conf;

import test.helpers.BALClassicTest;
import util.TexLog;
import data.DataBinary;

public class TestNetParamsHard {
	
	public static final int netCount = 10;
	public static final int maxEpcs = 200;
	public static final int k = 8;
	public static final int samplec = 200;
	public static final String logName = "test-netparams-random-kwta-hard.tex";
	public static final String pathIn = "input/binary-kWTA-hard/k"+k+"-in100x"+samplec;
	public static final String pathDes = "input/binary-kWTA-hard/k"+k+"-des100x"+samplec;
	
	public static void main(String[] args) throws IOException {	
		BALClassicTest test = new BALClassicTest(netCount, maxEpcs);
		DataBinary data = new DataBinary(pathIn,pathDes);
		TexLog log = new TexLog();
//		log.addLog(test.testAndLog(data.dataX(),data.dataY())); TODO: opravit
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
