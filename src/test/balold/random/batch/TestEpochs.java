package test.balold.random.batch;

import java.io.IOException;

import base.BAL1;
import base.NetUtil;
import data.DataBinary;

public class TestEpochs {

	public static final int samplec = 100;
	public static final int samples = 100;
	public static final int k = 10;
	public static final String pathIn = "input/binary-kWTA/k"+k+"-in"+samplec+"x"+samples;
	public static final String pathDes = "input/binary-kWTA/k"+k+"-des"+samplec+"x"+samples;

	public static final double lRate = .2;//Conf.lRate;	
	public static final int hids = 80;
	public static final int netCount = 10;
	public static final int[] maxEpcs = {1000,5000,10000,20000};
	
	public static void main(String[] args) throws IOException {
		
		DataBinary data = new DataBinary(pathIn,pathDes);
		// run tests
		for (int e = 0; e < maxEpcs.length; e++) {
			int succ = 0;
			int maxesucc = 0;
			int minesucc = maxEpcs[e];
			System.out.println((e+1)+". max epcs: "+maxEpcs[e]);
			for (int n = 0; n < netCount; n++) {
				double[] errors = new double[11];
				int epoch = 0;
				BAL1 net = new BAL1(samples,hids,samples,lRate);
				while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs[e]) {
					errors = net.epochExtInfo(data.dataX(),data.dataY());
					epoch++;
					if (epoch % 500 == 0) System.out.print(".");
				}
				if (errors[2] == 1.0) {
					succ++;
					maxesucc = (epoch > maxesucc) ? epoch : maxesucc;
					minesucc = (epoch < minesucc) ? epoch : minesucc;
				}
			}
			System.out.println("nets successful: "+succ+" of "+netCount);
			System.out.println("max epochs to success: "+maxesucc);
			System.out.println("min epochs to success: "+minesucc);
			System.out.println();
		}
	}
}
