package test.balold.random.batch;

import java.io.IOException;

import base.Conf;

import test.helpers.BALClassicTest;
import util.TexLog;
import data.DataBinary;

public class TestNetParams {
	
	public static final int samplec = 100;
	public static final int samples = 100;
	public static final int netc = 50;
	public static final int maxEpcs = 5000;
	public static final int k = 10;
	public static final String logName = "log-random-batch-netparams-"+samples+"x"+samplec+"-k10.tex";
	
	public static void main(String[] args) throws IOException {
				
		BALClassicTest test = new BALClassicTest(netc, maxEpcs);
		String pathIn = "input/binary-kWTA/k"+k+"-in"+samplec+"x"+samples;
		String pathDes = "input/binary-kWTA/k"+k+"-des"+samplec+"x"+samples;
		DataBinary data = new DataBinary(pathIn,pathDes);
		TexLog log = new TexLog();
//		log.addLog(test.testAndLog(data.dataX(),data.dataY())); TODO: opravit
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
