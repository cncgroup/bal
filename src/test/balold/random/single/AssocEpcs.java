package test.balold.random.single;

import java.io.IOException;

import base.BAL1;
import util.Util;
import util.visu.DataPairsVis;
import base.Conf;
import base.NetUtil;
import data.DataBinary;

public class AssocEpcs {
	
	public static final int[] visParams = {5, 10, 30, 10, 15};
	public static final int[] maxEpcs = {1000,2000,3000,5000,10000};
	public static final int k = 8;	
	public static final int samplec = k*Conf.kToSizeRatio;
//	public static final int sampleCount = 300;
	public static final String fileIn = "-in100x";
	public static final String fileDes = "-des100x";
	
	public static void main(String[] args) throws IOException {
		
//		DataBinary data = new DataBinary(Conf.pathDataKWTA+"k"+k+fileIn+sampleCount, Conf.pathDataKWTA+"k"+k+fileDes+sampleCount);
		DataBinary data = new DataBinary(Conf.pathRandInputs, Conf.pathRandDesired);
		
		DataPairsVis vis = new DataPairsVis(visParams,data.dataX(),data.dataY());
		vis.display();
		
		double[][] results = new double[maxEpcs.length][6];
 		for (int e = 0; e < maxEpcs.length; e++) {
 			BAL1 net = new BAL1(Conf.inps,Conf.hids,Conf.outs,Conf.lRate,Conf.wmax,Conf.wmin);
 			double[] errors = new double[6];
 			int epoch = 0;
 			while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs[e]) {
 				errors = net.epoch(data.dataX(),data.dataY());
 				epoch++;
 				if (epoch % 100 == 0) 
 					System.out.print(".");
 			}
 			System.out.println();
 			double[] testErrors = net.testEpoch(data.dataX(),data.dataY());
 			for (int i = 0; i < testErrors.length; i++) {
 				System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
 			}
 			results[e] = testErrors;
		}
 		System.out.println();
		System.out.print("epcsPhase1\t");
		for (int i = 0; i < maxEpcs.length; i++) {
			System.out.print(Conf.errorLabels[i]+"\t");
		}
		System.out.println();
		for (int i = 0; i < results.length; i++) {
			System.out.print(maxEpcs[i]+"\t");
			for (int j = 0; j < results[i].length; j++) {
				System.out.print(Util.round(results[i][j],3)+"\t");
			}
			System.out.println();
		}

	}
}
