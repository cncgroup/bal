package test.balold.random.single;

import java.io.IOException;

import base.BAL1;
import util.Util;
import base.Conf;
import base.NetUtil;
import data.DataBinary;
import util.visu.DataPairsVisResults;

public class AssocRandomResults {
	
	public static final int maxEpcs = 5000;
	public static final String pathIn = "input/binary-random/inp100x100";
	public static final String pathDes = "input/binary-random/des100x100";
	public static final int[] visParams = {5, 10, 10, 10, 15}; 
	
	public static void main(String[] args) throws IOException {
				
		DataBinary data = new DataBinary(pathIn,pathDes);
		BAL1 net = new BAL1(Conf.inps,Conf.hids,Conf.outs,Conf.lRate,Conf.wmax,Conf.wmin);
		double[][] dataIn = data.dataX();
		double[][] dataDes = data.dataY();
		
		double[] trainErrors = new double[6];
		int epoch = 0;
		while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < maxEpcs) {
			trainErrors = net.epoch(dataIn,dataDes);
			epoch++;
			if (epoch % 100 == 0) 
				System.out.print(".");
		}
		System.out.println("training epochs: "+(epoch+1));
		System.out.println();
		for (int i = 0; i < trainErrors.length; i++) {
			if (i > 1)
				System.out.println(Conf.errorLabels[i]+": "+Util.round(100.0*trainErrors[i],3)+"%");
			else
				System.out.println(Conf.errorLabels[i]+": "+Util.round(trainErrors[i],3));
		}
		System.out.println();
		double[] testErrors = net.testEpoch(dataIn,dataDes);
		for (int i = 0; i < testErrors.length; i++) {
			if (i > 1)
				System.out.println(Conf.errorLabels[i]+": "+Util.round(100.0*testErrors[i],3)+"%");
			else
				System.out.println(Conf.errorLabels[i]+": "+Util.round(testErrors[i],3));
		}
		double[][][] patterns = net.testEpochPatterns(dataIn,dataDes);
//		DataPairsVis visF = new DataPairsVis(visParams, patterns[], patterns[1]);
//		DataPairsVis visB = new DataPairsVis(visParams, patterns[2], patterns[3]);
//		visF.display();
//		visB.display();
		DataPairsVisResults vis = new DataPairsVisResults(visParams, patterns);
		vis.display();
	}
}
