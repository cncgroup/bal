package test.balold.random.single;

import java.io.IOException;

import base.BAL1;
import base.Conf;
import base.NetUtil;
import data.DataBinary;

public class Assoc {

	public static final String[] errorLabels = Conf.errorLabels;
	public static final int k = 8;
	public static final int sampleSize  = 100;
	public static final int sampleCount = 120; //k*Conf.kToSizeRatio;
	public static final int maxEpcs = 1000;
	public static final String fileIn  = Conf.pathDataKWTA+"k"+k+"-in"+sampleSize+"x"+sampleCount;
	public static final String fileDes = Conf.pathDataKWTA+"k"+k+"-des"+sampleSize+"x"+sampleCount;
	public static final int hids = 60;
	
	public static void main(String[] args) throws IOException {	
//		DataSetBinary data = new DataSetBinary(Conf.pathRandInputs, Conf.pathRandDesired);
		DataBinary data = new DataBinary(fileIn,fileDes);
		BAL1 net = new BAL1(sampleSize,hids,sampleSize,Conf.lRate,Conf.wmax,Conf.wmin);
//		Plot plot = new Plot(errorLabels); 
		double[] errors = new double[6];
		int epoch = 0;
		while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs) {
			errors = net.epoch(data.dataX(),data.dataY());
//			for (int i = 0; i < errors.length; i++) {
//				plot.addPoint(i, epoch, errors[i]);
//			}
			epoch++;
			if (epoch % 100 == 0) 
				System.out.print(".");
		}
		System.out.println();
		for (int i = 0; i < errors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+errors[i]);
		}
		System.out.println();
		double[] testErrors = net.testEpoch(data.dataX(),data.dataY());
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
//		plot.display();
	}
}
