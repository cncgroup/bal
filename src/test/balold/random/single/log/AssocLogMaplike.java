package test.balold.random.single.log;

import java.io.IOException;

import util.TexLog;
import util.Util;
import util.visu.DataPairsVis;
import util.visu.DataPairsVisResults;
import util.visu.Plot;
import base.Conf;
import base.BAL1;
import data.DataBinary;

public class AssocLogMaplike {
	
	public static final int[] visParams = {8, 5, 5, 4, 4};
	public static final int k = 3;
	public static final int sampleSize  = 16;
	public static final int sampleCount = 16;
	public static final String fileIn  = Conf.pathDataMap+"k"+k+"-in-"+sampleSize+"x"+sampleCount;
	public static final String fileDes = Conf.pathDataMap+"k"+k+"-des-"+sampleSize+"x"+sampleCount;
	public static final String figName = "assoc-random-maplike-k"+k+"-"+"s"+sampleCount;
	public static final String logName = "log-single-random-maplike-k"+k+"s"+sampleCount+".tex";
	public static final int hids = 10;//Conf.hids;
	public static final int maxEpcs = 5000;
	
	public static void main(String[] args) throws IOException {
				
		DataBinary data = new DataBinary(fileIn,fileDes);
		double[][] dataIn = data.dataX();
		double[][] dataDes = data.dataY();
		DataPairsVis vis = new DataPairsVis(visParams, dataIn, dataDes);
		vis.display();
		vis.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-data"+Conf.figExt);
		
		BAL1 net = new BAL1(sampleSize,hids,sampleSize,Conf.lRate,Conf.wmax,Conf.wmin);
		Plot plot = new Plot(Conf.errorLabels);
		double[] trainErrors = new double[6];
		int epoch = 0;
		
//		while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < epcsPhase1) {
		while (epoch < maxEpcs) {
			trainErrors = net.epoch(dataIn,dataDes);
			if (epoch == 0 || (epoch+1) % 10 == 0) {
				for (int i = 0; i < trainErrors.length; i++) {
					plot.addPoint(i, epoch, trainErrors[i]);
				}
			}
			epoch++;
			if (epoch % 100 == 0) 
				System.out.print(".");
			if (epoch % 5000 == 0)
				System.out.println();
		}
		System.out.println();
		System.out.println("training epochs: "+epoch);
		System.out.println();
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+trainErrors[i]);
		}
		double[] testErrors = net.testEpoch(dataIn,dataDes);
		System.out.println();
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
		System.out.println();
		// save results
		String[][] resultsTable = new String[7][3];
		for (int i = 0; i < resultsTable.length; i++) {
			resultsTable[i][0] = Conf.testErrors[i];
			if (i == 6) {
				resultsTable[i][1] = ""+epoch;
				resultsTable[i][2] = "1";
			} else if (i > 1) {
				resultsTable[i][1] = ""+Util.round(100.0*trainErrors[i],2)+"\\%";
				resultsTable[i][2] = ""+Util.round(100.0*testErrors[i],2)+"\\%";
			} else {
				resultsTable[i][1] = ""+Util.round(trainErrors[i],3);
				resultsTable[i][2] = ""+Util.round(testErrors[i],3);
			}
		}
		// save plots
		plot.display();
		plot.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-results"+Conf.figExt);
		// plot results
		double[][][] patterns = net.testEpochPatterns(dataIn,dataDes);
		DataPairsVisResults visRes = new DataPairsVisResults(visParams,patterns);
		visRes.display();
		visRes.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-results-vis"+Conf.figExt);
		// write log
		String[] setup = {
			"architecture: "+sampleSize+"-"+hids+"-"+sampleSize,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min pattern succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			"data: random "+k+"/"+sampleSize+" positive bits, "+sampleCount+" sampleSize",
		};
		TexLog log = new TexLog();
//		log.addLine("{\\bf Data:}\\\\");
		log.addLine("\\section{Data}");
		log.addFigure(
				Conf.pathFigures+figName+"-data"+Conf.figExt, 
				"Maplike random data, "+sampleCount+" sampleSize, "+sampleSize+" bits, k = "+k,
				"fig:data-random-maplike-k"+k+"s"+sampleCount,
				0.6);
//		log.addLine("{\\bf Setup parameters:}\\\\");
		log.addLine("\\section{Setup}");
		log.addItemize(setup);
		log.addLine("\\newpage");
		log.addLine("\\section{Results}");
		log.addResultsTable(
				new String[] {"error","train","test"}, 
				resultsTable, 
				"BALNet with maplike random data: results (one net)",
				"tab:results-grbidir-random-maplike-k"+k+"s"+sampleCount
		);
		log.addFigure(
				Conf.pathFigures+figName+"-results"+Conf.figExt, 
				"BALNet with maplike random data: results (one net)",
				"fig:results-grbidir-random-maplike-k"+k+"s"+sampleCount,
				0.8);
		log.addFigure(
				Conf.pathFigures+figName+"-results-vis"+Conf.figExt, 
				"BALNet with maplike random data: result patterns (one net)",
				"fig:results-grbidir-random-maplike-k"+k+"s"+sampleCount,
				0.6);
//		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");	
	}
}
