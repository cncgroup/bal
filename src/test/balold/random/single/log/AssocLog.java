package test.balold.random.single.log;

import java.io.IOException;

import util.TexLog;
import util.Util;
import util.visu.Plot;
import base.Conf;
import base.BAL1;
import base.NetUtil;
import data.DataBinary;

public class AssocLog {
	
	public static final int maxEpcs = 1000;
	public static final int k = 8;
//	public static final int sampleCount = k*Conf.kToSizeRatio;
	public static final int samplec = 120;
	public static final String fileIn = "-in100x";
	public static final String fileDes = "-des100x";
	public static final String figName = "assoc-random-"+k+"-"+"s"+samplec;
	public static final String logName = "log-single-random-k"+k+"s"+samplec+".tex";
	
	public static void main(String[] args) throws IOException {
				
//		DataSetBinary data = new DataSetBinary(Conf.pathRandInputs, Conf.pathRandDesired);
		DataBinary data = new DataBinary(Conf.pathDataKWTA+"k"+k+fileIn+samplec, Conf.pathDataKWTA+"k"+k+fileDes+samplec);
		BAL1 net = new BAL1(Conf.inps,Conf.hids,Conf.outs,Conf.lRate,Conf.wmax,Conf.wmin);
		
		Plot[] plots = new Plot[3];
		plots[0] = new Plot(Conf.mseLabels);
		plots[1] = new Plot(Conf.patSuccLabels); 
		plots[2] = new Plot(Conf.bitSuccLabels); 
		
		double[] trainErrors = new double[6];
		int epoch = 0;
		while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < maxEpcs) {
			trainErrors = net.epoch(data.dataX(),data.dataY());
			int plotindex = 0;
			for (int i = 0; i < trainErrors.length; i++) {
				plots[plotindex].addPoint(i % 2, epoch, trainErrors[i]);	
				if (i % 2 == 1)
					plotindex++;
			}
			epoch++;
		}
		System.out.println();
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+trainErrors[i]);
		}
		System.out.println();
		double[] testErrors = net.testEpoch(data.dataX(),data.dataY());
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
		
		// save results
		String[][] resultsTable = new String[6][3];
		for (int i = 0; i < resultsTable.length; i++) {
			resultsTable[i][0] = Conf.errorLabels[i];
			resultsTable[i][1] = ""+Util.round(trainErrors[i],3);
			resultsTable[i][2] = ""+Util.round(testErrors[i],3);
		}
		// save plots
		for (int i = 0; i < plots.length; i++) {
			plots[i].display();
			plots[i].saveImage(Conf.pathOutput+Conf.pathFigures+figName+Conf.errorFigNames[i]+Conf.figExt);
		}
		
		String[] setup = {
			"architecture: "+Conf.inps+"-"+Conf.hids+"-"+Conf.outs,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			"data: random "+k+"/100 positive bits, "+samplec+" sampleSize",
		};
		TexLog log = new TexLog();	
		log.addLine("\\subsection{Associator: single instance}");
		log.addLine("{\\bf Setup parameters:}\\\\");
		log.addItemize(setup);
		log.addResultsTable(
				new String[] {"error","train","test"}, 
				resultsTable, 
				"GeneRec associator: results with random data ("+k+ "pos. bits/"+samplec+" sampleSize)",
				"tab:results-grbidir-random-k"+k+"-sampleCount-"+samplec
		);
		for (int i = 0; i < plots.length; i++) {
			log.addFigure(
					Conf.pathFigures+figName+Conf.errorFigNames[i]+Conf.figExt, 
					"BALNet single instance - "+Conf.errorNames[i],
					"fig:grbidir-single-"+Conf.errorFigNames, 
					1.0);
		}
//		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");	
	}
}
