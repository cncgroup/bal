package test.balold.random.single.log;

import java.io.IOException;

import base.BAL1;
import util.TexLog;
import util.Util;
import util.visu.Plot;
import base.Conf;
import base.NetUtil;
import data.DataBinary;

public class AssocMediumLog {
	
	public static final int maxEpcs = 200;
	public static final int k = 8;
	public static final int patternSize = 100;
	public static final int samplec = 300;
	public static final String pathIn = "input/binary-kWTA/k"+k+"-in100x"+samplec;
	public static final String pathDes = "input/binary-kWTA/k"+k+"-des100x"+samplec;
	public static final String logName = Conf.pathOutput+"log-grbidir-single-kwta-random-medium.tex";
	public static final String simName = "GeneRec Bidir with random medium kWTA data: a single case";
	public static final String figName = "assoc-random-medium-fig";
	
	public static void main(String[] args) throws IOException {
				
		DataBinary data = new DataBinary(pathIn,pathDes);
		BAL1 net = new BAL1(Conf.inps,Conf.hids,Conf.outs,Conf.lRate,Conf.wmax,Conf.wmin);
		
		Plot[] plots = new Plot[3];
		plots[0] = new Plot(Conf.mseLabels);
		plots[1] = new Plot(Conf.patSuccLabels); 
		plots[2] = new Plot(Conf.bitSuccLabels); 
		
		double[] trainErrors = new double[6];
		int epoch = 0;
		while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < maxEpcs) {
			trainErrors = net.epoch(data.dataX(),data.dataY());
			int plotindex = 0;
			for (int i = 0; i < trainErrors.length; i++) {
				plots[plotindex].addPoint(i % 2, epoch, trainErrors[i]);	
				if (i % 2 == 1)
					plotindex++;
			}
			epoch++;
			if (epoch % 10 == 0) 
				System.out.print(".");
		}
		System.out.println();
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+trainErrors[i]);
		}
		System.out.println();
		double[] testErrors = net.testEpoch(data.dataX(),data.dataY());
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
		
		for (int i = 0; i < plots.length; i++) {
			plots[i].display();
			plots[i].saveImage(Conf.pathOutput+Conf.pathFigures+figName+i+".png");
		}
		
		TexLog log = new TexLog();	
		
		log.addLine("\\subsection{"+simName+"}");
		log.addLine("{\\bf Setup parameters:}\\\\");
		String[] setup = {
				"architecture: " + Conf.inps + "-" + Conf.hids + "-" + Conf.outs,
				"data: kWTA random hard with "+k+" positive bits in "+patternSize+"-bits sample, "+samplec+" sampleSize",
				"weights from: (" + Conf.wmin + "," + Conf.wmax + ")",
				"learning rate: " + Conf.lRate,
				"min mse: " + Conf.minMSE,
				"min succ: " + Conf.minPatSucc,
				"max epochs: " + maxEpcs,
			};
		log.addItemize(setup);
		
		String[][] resultsTable = new String[trainErrors.length][3];
		for (int i = 0; i < resultsTable.length; i++) {
			resultsTable[i][0] = Conf.netErrors[i];
			resultsTable[i][1] = ""+Util.round(trainErrors[i],3);
			resultsTable[i][2] = ""+Util.round(testErrors[i],3);
		}
		
		log.addResultsTable(
				new String[] {"","trainError","testError"}, 
				resultsTable, 
				simName, 
				"tab:results-grbidir-hard-random-kWTA-k"+k
		);

		for (int i = 0; i < plots.length; i++) {
			log.addFigure(Conf.pathFigures+figName+i+".png", "generec robotic associator "+Conf.testErrors[i], "fig:r1", 1.0);
		}
		log.echo();
		log.export(logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
}
