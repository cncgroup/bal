package test.balold.random.single.log;

import java.io.IOException;

import util.TexLog;
import util.Util;
import util.visu.DataPairsVis;
import util.visu.Plot;
import base.Conf;
import base.BAL1;
import base.NetUtil;
import data.DataBinary;

public class AssocLogNew {
	
	public static final int[] visParams = {4, 10, 10, 10, 20};
//	public static final int[] visParams = {5, 10, 10, 5, 6};
	public static final int k = 8;
	public static final int sampleSize  = 100;
	public static final int sampleCount = 120;//100;//50;//
//	public static final int sampleCount = k*Conf.kToSizeRatio;
	public static final String fileIn  = Conf.pathDataKWTA+"k"+k+"-in"+sampleSize+"x"+sampleCount;
	public static final String fileDes = Conf.pathDataKWTA+"k"+k+"-des"+sampleSize+"x"+sampleCount;
//	public static final String fileIn = "input/binary-kWTA-hard/k"+k+"-in"+sampleSize+"x"+sampleCount;
//	public static final String fileDes = "input/binary-kWTA-hard/k"+k+"-des"+sampleSize+"x"+sampleCount;
//	public static final String fileIn = "input/binary-random/inp100x"+sampleCount;
//	public static final String fileDes = "input/binary-random/des100x"+sampleCount;
	public static final String figName = "assoc-random-k"+k+"-"+"s"+sampleCount;
	public static final String logName = "log-single-random-k"+k+"s"+sampleCount+".tex";
	public static final int hids = 80;//Conf.hids;
	public static final int maxEpcs = 1000;
	
	public static void main(String[] args) throws IOException {
				
//		DataSetBinary data = new DataSetBinary(Conf.pathRandInputs, Conf.pathRandDesired);
		DataBinary data = new DataBinary(fileIn,fileDes);
		double[][] dataIn = data.dataX();
		double[][] dataDes = data.dataY();
		DataPairsVis vis = new DataPairsVis(visParams, dataIn, dataDes);
		vis.display();
		vis.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-data"+Conf.figExt);
		
		BAL1 net = new BAL1(sampleSize,hids,sampleSize,Conf.lRate,Conf.wmax,Conf.wmin);
		Plot plot = new Plot(Conf.errorLabels);
		double[] trainErrors = new double[6];
		int epoch = 0;
		
		while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < maxEpcs) {
			trainErrors = net.epoch(dataIn,dataDes);
			if (epoch == 0 || (epoch+1) % 10 == 0) {
				for (int i = 0; i < trainErrors.length; i++) {
					plot.addPoint(i, epoch, trainErrors[i]);
				}
			}
			epoch++;
			if (epoch % 100 == 0) 
				System.out.print(".");
			if (epoch % 5000 == 0)
				System.out.println();
		}
		System.out.println("training epochs: "+epoch);
		double[] testErrors = net.testEpoch(dataIn,dataDes);
		// save results
		String[][] resultsTable = new String[7][3];
		for (int i = 0; i < resultsTable.length; i++) {
			resultsTable[i][0] = Conf.testErrors[i];
			if (i == 6) {
				resultsTable[i][1] = ""+epoch;
				resultsTable[i][2] = "1";
			} else if (i > 1) {
				resultsTable[i][1] = ""+Util.round(100.0*trainErrors[i],2)+"\\%";
				resultsTable[i][2] = ""+Util.round(100.0*testErrors[i],2)+"\\%";
			} else {
				resultsTable[i][1] = ""+Util.round(trainErrors[i],3);
				resultsTable[i][2] = ""+Util.round(testErrors[i],3);
			}
		}
		// save plots
		plot.display();
		plot.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-results"+Conf.figExt);
		// write log
		String[] setup = {
			"architecture: "+sampleSize+"-"+hids+"-"+sampleSize,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min pattern succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			"data: random "+k+"/"+sampleSize+" positive bits, "+sampleCount+" sampleSize",
		};
		TexLog log = new TexLog();	
		log.addLine("{\\bf Setup parameters:}\\\\");
		log.addItemize(setup);
		log.addResultsTable(
				new String[] {"error","train","test"}, 
				resultsTable, 
				"GeneRec associator: results with random data ("+k+ "pos. bits/"+sampleCount+" sampleSize)",
				"tab:results-grbidir-random-k"+k+"-sampleCount-"+sampleCount
		);
		log.addFigure(
				Conf.pathFigures+figName+"-results"+Conf.figExt, 
				"BALNet with harder random data: single instance",
				"fig:grbidir-single-random-harder-"+k, 
				1.0);
		log.addFigure(
				Conf.pathFigures+figName+"-data"+Conf.figExt, 
				"Harder random data with k = "+k, 
				"fig:grbidir-single-random-harder-"+k, 
				0.5);
		log.echo();
//		log.export(Conf.pathOutput+logName);
//		System.out.println("Finished writing \""+logName+"\"");	
	}
}
