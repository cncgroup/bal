package test.balold.random.single.log;

import java.io.IOException;

import base.BAL1;
import util.TexLog;
import util.Util;
import base.Conf;
import base.NetUtil;
import data.DataBinary;

public class AssocSamplecNew {
	
	public static final String fileIn = "-in100x";
	public static final String fileDes = "-des100x";
	public static final String logName = "log-single-random-samplecount.tex";
	public static final String simName = "Test with random kWTA data: varying the size of the train set";
	public static final int maxEpcs = 5000;
	public static final int prefK = 8;
	public static final int patternSize = 100;
	public static final int hids = 120;
	
	public static void main(String[] args) throws IOException {
		
		TexLog tmp = new TexLog();	
		String[][] resultsTable = new String[12][Conf.testErrors.length+1];
		for (int sim = 0; sim < 12; sim++) {
			int samplec = 40+20*sim;
			System.out.println("Processing sampleCount = "+samplec);
			DataBinary data = new DataBinary(Conf.pathDataKWTA+"k"+prefK+fileIn+samplec, Conf.pathDataKWTA+"k"+prefK+fileDes+samplec);
			BAL1 net = new BAL1(patternSize,hids,patternSize,Conf.lRate,Conf.wmax,Conf.wmin);
			String[][] plotData = new String[6][maxEpcs/50+1];
			double[] trainErrors = new double[6];
			int epoch = 0;
			int index = 0;
			while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < maxEpcs) {
				trainErrors = net.epoch(data.dataX(),data.dataY());
				if (epoch == 0 || (epoch+1) % 50 == 0) {
					for (int i = 0; i < plotData.length; i++) {
						plotData[i][index] = "("+epoch+","+trainErrors[i]+")";
					}
					index++;
				}
				epoch++;
			}
			resultsTable[sim][0] = ""+samplec;
			double[] testErrors = net.testEpoch(data.dataX(),data.dataY());
			for (int i = 0; i < testErrors.length; i++) {
				resultsTable[sim][i+1] = ""+Util.round(testErrors[i],3);
			}
			resultsTable[sim][resultsTable[sim].length-1] = ""+epoch;
			tmp.addLine("\\begin{figure}[!htpb]");
			tmp.addLine("\\centering");
			tmp.addPlot(plotData,Conf.errorLabels,"epoch","error");
			tmp.addLine("\\caption{Results for trainset size "+samplec+"}");
			tmp.addLine("\\label{fig:assoc-single-random-sc"+samplec+"}");
			tmp.addLine("\\end{figure}");
		}
		
		TexLog log = new TexLog();
		log.addLine("\\subsection{"+simName+"}");
		log.addLine("{\\bf Setup parameters:}\\\\");
		String[] setup = {
				"architecture: " + Conf.inps + "-" + Conf.hids + "-" + Conf.outs,
				"data: kWTA random with "+prefK+" positive bits in "+patternSize+"-bits sample",
				"weights from: (" + Conf.wmin + "," + Conf.wmax + ")",
				"learning rate: " + Conf.lRate,
				"min mse: " + Conf.minMSE,
				"min succ: " + Conf.minPatSucc,
				"max epochs: " + maxEpcs,
			};
		log.addItemize(setup);
		log.addLine("");
		log.addResultsTable(
				Util.addStringFront(Conf.testErrors,"trainset size"), 
				resultsTable, 
				"BALNet: varying the size of the train set",
				"tab:grbidir-kwta-random-samplecount"
		);
		log.addLine("");
		log.addLog(tmp);
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
}
