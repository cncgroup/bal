package test.balold.random.single.log;

import java.io.IOException;

import util.TexLog;
import util.Util;
import util.visu.Plot;
import base.Conf;
import base.BAL1;
import data.DataBinary;

public class AssocActLog {
	
	public static final int[] visParams = {5, 5, 20, 5, 6};
	public static final String fileIn = "-in25x";
	public static final String fileDes = "-des25x";
	public static final int k = 4;
	public static final int samplec = 15;
	public static final String[] errorLabels = Conf.errorLabels;
	public static final String figName = "assoc-random-"+k+"-"+"s"+samplec;
	public static final String logName = "log-single-random-k"+k+"s"+samplec+".tex";
	public static final int hids = 25;
	public static final int maxEpcs = 7000;
	
	public static void main(String[] args) throws IOException {
		
		DataBinary data = new DataBinary(Conf.pathDataKWTA+"k"+k+fileIn+samplec, Conf.pathDataKWTA+"k"+k+fileDes+samplec);
		double[][] dataIn = data.dataX();
		double[][] dataDes = data.dataY();
		
		System.out.println(dataIn.length+" vs "+dataIn[0].length);
		
//		DataPairsVis vis = new DataPairsVis(visParams, dataIn, dataDes);
//		vis.display();
//		vis.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-data"+Conf.figExt);
		
		BAL1 net = new BAL1(dataIn[0].length,hids,dataDes[0].length,Conf.lRate,Conf.wmax,Conf.wmin);
		Plot plot = new Plot(Conf.errorLabels);
		double[] trainErrors = new double[6];
		int epoch = 0;
		while (epoch < maxEpcs) {
			if (epoch == 0 || (epoch+1) % 1000 == 0) {
				System.out.println("epoch "+(epoch+1));
				trainErrors = net.epoch(dataIn,dataDes,true,true);
			} else {
				trainErrors = net.epoch(dataIn,dataDes);
			}
			if (epoch == 0 || (epoch+1) % 10 == 0) {
				for (int i = 0; i < trainErrors.length; i++) {
					plot.addPoint(i, epoch, trainErrors[i]);
				}
			}
			epoch++;
		}
		double[] testErrors = net.testEpoch(data.dataX(),data.dataY());
		// save results
		String[][] resultsTable = new String[6][3];
		for (int i = 0; i < resultsTable.length; i++) {
			resultsTable[i][0] = Conf.errorLabels[i];
			resultsTable[i][1] = ""+Util.round(trainErrors[i],3);
			resultsTable[i][2] = ""+Util.round(testErrors[i],3);
		}
		// save plots
		plot.display();
		plot.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-results"+Conf.figExt);
		// write log
		String[] setup = {
				"architecture: "+dataIn[0].length+"-"+hids+"-"+dataDes[0].length,
				"weights from: ("+Conf.wmin+","+Conf.wmax+")",
				"learning rate: "+ Conf.lRate,
				"max epochs: "+ maxEpcs,
				"data: random "+k+"/100 positive bits, "+samplec+" sampleSize",
			};
		TexLog log = new TexLog();	
		log.addLine("\\section{Associator: single instance}");
		log.addLine("\\subsection{Setup parameters:}");
		log.addItemize(setup);
		log.addLine("\\subsection{Results:}");
		log.addResultsTable(
				new String[] {"error","train","test"}, 
				resultsTable, 
				"GeneRec associator: results with random data ("+k+ "pos. bits/"+samplec+" sampleSize)",
				"tab:results-grbidir-random-k"+k+"-sampleCount-"+samplec
		);
		log.addFigure(
				Conf.pathFigures+figName+"-data"+Conf.figExt, 
				"BALNet with easy random data: single instance",
				"fig:grbidir-single-random-easy", 
				0.5);
		log.addFigure(
				Conf.pathFigures+figName+"-results"+Conf.figExt, 
				"BALNet with easy random data: single instance",
				"fig:grbidir-single-random-easy", 
				1.0);
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");	
	}
}
