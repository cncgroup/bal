package test.balold.random.single.log;

import java.io.IOException;

import util.TexLog;
import util.Util;
import util.visu.DataPairsVisResults;
import util.visu.Plot;
import base.Conf;
import base.BAL1;
import base.NetUtil;
import data.DataBinary;

public class AssocHard2 {
	
	public static final int maxEpcs = 200;
	public static final int k = 8;	
	public static final int patternSize = 100;
	public static final int samplec = 200;
	public static final String pathIn = "input/binary-kWTA-hard/k"+k+"-in100x"+samplec;
	public static final String pathDes = "input/binary-kWTA-hard/k"+k+"-des100x"+samplec;
	public static final String logName = Conf.pathOutput+"log-grbidir-single-kwta-random-hard.tex";
	public static final String simName = "GeneRec Bidir with random hard kWTA data: a single case";
	public static final String figName = "assoc-random-hard-fig";
	public static final int[] visParams = {4, 5, 5, 10, 12};
	
	public static void main(String[] args) throws IOException {
				
		DataBinary data = new DataBinary(pathIn,pathDes);
		double[][] dataIn = data.dataX();
		double[][] dataDes = data.dataY();
		BAL1 net = new BAL1(Conf.inps,Conf.hids,Conf.outs,Conf.lRate,Conf.wmax,Conf.wmin);
		Plot plot = new Plot(Conf.errorLabels);
		double[] trainErrors = new double[6];
		int epoch = 0;
		
		while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < maxEpcs) {
			trainErrors = net.epoch(dataIn,dataDes);
			for (int i = 0; i < trainErrors.length; i++) {
				plot.addPoint(i, epoch, trainErrors[i]);
			}
			epoch++;
			if (epoch % 10 == 0) 
				System.out.print(".");
		}
		
		System.out.println();
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+trainErrors[i]);
		}
		System.out.println();
		double[] testErrors = net.testEpoch(dataIn,dataDes);
		for (int i = 0; i < testErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
		// save plots
		plot.display();
		plot.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-results"+Conf.figExt);
		// plot patterns
		double[][][] patterns = net.testEpochPatterns(dataIn,dataDes);
		DataPairsVisResults visRes = new DataPairsVisResults(visParams,patterns);
		visRes.display();
		visRes.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-results-vis"+Conf.figExt);
		
		TexLog log = new TexLog();	
		
		log.addLine("\\subsection{"+simName+"}");
		log.addLine("{\\bf Setup parameters:}\\\\");
		String[] setup = {
				"architecture: " + Conf.inps + "-" + Conf.hids + "-" + Conf.outs,
				"data: kWTA random hard with "+k+" positive bits in "+patternSize+"-bits sample, "+samplec+" sampleSize",
				"weights from: (" + Conf.wmin + "," + Conf.wmax + ")",
				"learning rate: " + Conf.lRate,
				"min mse: " + Conf.minMSE,
				"min succ: " + Conf.minPatSucc,
				"max epochs: " + maxEpcs,
			};
		log.addItemize(setup);
		
		String[][] resultsTable = new String[trainErrors.length][3];
		for (int i = 0; i < resultsTable.length; i++) {
			resultsTable[i][0] = Conf.netErrors[i];
			resultsTable[i][1] = ""+Util.round(trainErrors[i],3);
			resultsTable[i][2] = ""+Util.round(testErrors[i],3);
		}
		
		log.addResultsTable(
				new String[] {"","trainError","testError"}, 
				resultsTable, 
				simName, 
				"tab:results-grbidir-hard-random-kWTA-k"+k
		);
		log.addFigure(
				Conf.pathFigures+figName+"-results"+Conf.figExt, 
				"BALNet with hard random data: results (one net)",
				"fig:results-grbidir-random-hard-k"+k+"s"+samplec,
				0.8);
		log.addFigure(
				Conf.pathFigures+figName+"-results-vis"+Conf.figExt, 
				"BALNet with hard random data: result patterns (one net)",
				"fig:results-grbidir-random-hard-k"+k+"s"+samplec,
				0.6);
		log.echo();
		log.export(logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
}
