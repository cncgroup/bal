package test.balold.icdl;

import java.io.IOException;
import java.util.Arrays;

import util.visu.DataPairsSel;
import base.Conf;
import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class TmpDataTest {
	
	public static final int sizeVis = 16;
	public static final int sizeMot = 12;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final int rowHeight = 10;
	
	public static void main(String[] args) throws IOException {
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		int[][] indices = dataVis.uniqPatIndices(dataMot.uniqPatIndices());
		indices[1] = Util.duplicateAtIndex(indices[1], 0);
		System.out.println(Arrays.toString(indices[0]));
		System.out.println(Arrays.toString(indices[1]));
		System.out.println(Arrays.toString(indices[2]));
 		double[][][] valuesMot = dataMot.valuesIndicesMov(indices);
 		double[][][] valuesVis = dataVis.valuesPerspIndicesMov(0,indices);
 		DataPairsSel vis = new DataPairsSel(valuesVis,valuesMot);
 		vis.setRowH(rowHeight);
 		vis.display();
 		vis.saveImage(Conf.pathOutput+"best-dataset.png");
	}
}
