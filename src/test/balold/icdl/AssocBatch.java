package test.balold.icdl;

import java.io.IOException;

import base.BAL1;
import util.TexLog;
import util.Util;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class AssocBatch {

	public static final String logName = "log-icdl.tex";
	public static final int sizeVis = 14;
	public static final int sizeMot = 12;
	public static final int kVis = 16;
	public static final int kMot = 8;
	public static final String pathVisual = "input/msom/visual_"+sizeVis+"_"+sizeVis+".txt";
	public static final String pathMotor = "input/msom/motor_"+sizeMot+"_"+sizeMot+".txt";
	public static final int[] visParams = {3, 5, 5, 11};
	public static final String[] errNames = Conf.errorLabelsNew;
	public static final String dataName = sizeVis+"k"+kVis+"vs"+sizeMot+"k"+kMot;
	public static final String figResults = Conf.pathFigures+"assoc-robotic-fp-"+dataName+"-results";
	public static final String dataSetLine = "datasets: vis "+sizeVis+"x"+sizeVis+", k="+kVis+", mot "+sizeMot+"x"+sizeMot+", k="+kMot;
	public static final int hids = ((sizeVis*sizeVis)+(sizeMot*sizeMot))/2;
	public static final int maxEpcs = 1500;
	public static final int plotRes = 100;
	public static final int netCount = 20;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis(pathVisual);
		DataRoboticMot dataMot = new DataRoboticMot(pathMotor);
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		double[][] dataV = dataVis.valuesFirstPersp();
		double[][] dataM = dataMot.values();
		int inps = dataVis.patternSize();
		int outs = dataMot.patternSize();
		
		double[][][] trainErr = new double[maxEpcs/plotRes+1][errNames.length][netCount];
		double[][] testErr = new double[errNames.length][netCount];
		
		for (int n = 0; n < netCount; n++) {
			BAL1 net = new BAL1(inps,hids,outs,Conf.lRate,Conf.wmax,Conf.wmin);
			System.out.println("BALNet "+inps+"-"+hids+"-"+outs);
			double[] trainErrors = new double[errNames.length];
			int epoch = 0;
			int index = 0;
			while (epoch <= maxEpcs) {
				trainErrors = net.epoch(dataV,dataM);
				if (epoch % plotRes == 0) {	
					for (int j = 0; j < errNames.length; j++) {
						trainErr[index][j][n] = trainErrors[j];
					}
					index++;
					System.out.print(".");
				}
				epoch++;
			}
			System.out.println();
			double[] testErrors = net.testEpoch(dataV,dataM);
			for (int i = 0; i < errNames.length; i++) {
				testErr[i][n] = testErrors[i];
			}
		}
		
		String[][][] plotData = new String[errNames.length/2][2][maxEpcs/plotRes+1];
		for (int i = 0; i < trainErr.length; i++) {
			for (int j = 0; j < errNames.length; j++) {
				double[] meanstd = Util.MeanAndStD(trainErr[i][j]);
				plotData[j/2][j%2][i] = "("+(plotRes*i)+","+meanstd[0]+") +- (0,"+meanstd[1]+")";
			}
		}
//		save results
		double[][] trainErrTmp = new double[errNames.length][netCount*(maxEpcs/plotRes+1)];
		for (int n = 0; n < trainErr[0][0].length; n++) {
			for (int e = 0; e < errNames.length; e++) {
				for (int i = 0; i < trainErr.length; i++) {
					trainErrTmp[e][n*(maxEpcs/plotRes+1)+i] = trainErr[i][e][n];
				}
			}
		}
		String[][] resultsTable = new String[errNames.length][3];
		for (int i = 0; i < resultsTable.length; i++) {
			resultsTable[i][0] = errNames[i];
			double[] tmp = Util.MeanAndStD(trainErrTmp[i]);
			resultsTable[i][1] = ""+Util.round(tmp[0],3)+" $\\pm$ "+Util.round(tmp[1],3);
			tmp = Util.MeanAndStD(testErr[i]);
			System.out.println(tmp[0]+" "+tmp[1]);
			resultsTable[i][2] = ""+Util.round(tmp[0],3)+" $\\pm$ "+Util.round(tmp[1],3);
		}
//		write log
		String[] setup = {
			"architecture: "+inps+"-"+hids+"-"+outs,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
			dataSetLine,
		};
		TexLog log = new TexLog();
		log.addLine("");
		log.addLine("\\section{BALNet with robotic data: single simulation}");
		log.addLine("\\subsection{Setup parameters}");
		log.addItemize(setup);
		log.addLine("");
		log.addResultsTable(
				new String[] {"error","train","test"}, 
				resultsTable, 
				"MSOM robotic data first perspective "+dataName, 
				"tab:grbidir-single-"+dataName
		);
		log.addLine("");
		log.addLine("\\begin{figure}[!htbp]");
		log.addLine("\\centering");
		for (int i = 0; i < plotData.length; i++) {
			log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
			log.addLine("\\centering");
			log.addPlotErrBars(plotData[i],Conf.plotLabels[i],"epoch",Conf.errorNames[i]);
			log.addLine("\\end{subfigure}");
		}
		log.addLine("\\caption{Results for single instance of BALNet Associator with robotic data}");
		log.addLine("\\label{fig:assoc-single-robotic}");
		log.addLine("\\end{figure}");
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
}
