package test.balold.icdl;

import java.io.IOException;

import test.helpers.BALClassicBatch;
import util.TexLog;
import base.Conf;
import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class LearningBatch {
	
	public static final String simName = "icdl-balold-learning";
	public static final String[] errNames = Conf.errorLabels;
	public static final int sizeVis = 16;
	public static final int sizeMot = 12;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final double lRate = 0.2;
	public static final int hids = 160;
	public static final int maxEpcs1 = 800;
	public static final int maxEpcs2 = 1000;	
	public static final int netCount = 50;
	public static final int plotRes = 50;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		int[][] indices = dataVis.uniqPatIndices(dataMot.uniqPatIndices());
		indices[1] = Util.duplicateAtIndex(indices[1], 0);
 		double[][] valuesMot = dataMot.valuesIndices(indices);
 		double[][] valuesVis = dataVis.valuesPerspIndices(0,indices);
 		double[][] valuesMot2 = Util.merge(valuesMot, Util.merge(valuesMot, Util.merge(valuesMot, valuesMot)));
 		double[][] valuesVis2 = Util.merge(valuesVis,Util.merge(dataVis.valuesPerspIndices(1,indices),Util.merge(
 				dataVis.valuesPerspIndices(2,indices),dataVis.valuesPerspIndices(3,indices))));
 	
		TexLog log = new TexLog();
 		BALClassicBatch sim = new BALClassicBatch(netCount, simName, errNames, hids, lRate, maxEpcs1, maxEpcs2, plotRes);
		log.addLog(sim.runAndLog2Phase(valuesMot, valuesVis, valuesMot2, valuesVis2));
		log.echo();
		log.export(Conf.logPath(simName));
	}
}
