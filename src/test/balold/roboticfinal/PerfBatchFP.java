package test.balold.roboticfinal;

import java.io.IOException;

import test.helpers.BALClassicBatch;
import util.TexLog;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class PerfBatchFP {
	
	public static final String simName = "balold-robotic-perf-fp";
	public static final String[] errNames = Conf.errorLabels;
	public static final int sizeVis = 16;
	public static final int sizeMot = 12;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final double lRate = 0.2;
	public static final int hids = 240;
	public static final int maxEpcs = 1300;
	public static final int netCount = 50;
	public static final int plotRes = 50;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		int[][] indices = dataVis.uniqPatIndices(dataMot.uniqPatIndices());
 		double[][] valuesMot = dataMot.valuesIndices(indices);
 		double[][] valuesVis = dataVis.valuesPerspIndices(0,indices);
 	
		TexLog log = new TexLog();
 		BALClassicBatch sim = new BALClassicBatch(netCount, simName, errNames, hids, lRate, maxEpcs, plotRes);
		log.addLog(sim.runAndLog(valuesMot, valuesVis));
		log.echo();
		log.export(Conf.logPath(simName));
	}
}
