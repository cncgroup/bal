package test.balold.roboticfinal;

import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;
import test.helpers.BALClassicBatch;
import util.TexLog;
import util.Util;

import java.io.IOException;

public class ExpLearningBatch {
	
	public static final String simName = "balold-robotic-learning";
	public static final String[] errNames = Conf.errorLabels;
	public static final int sizeVis = 16;
	public static final int sizeMot = 12;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final double lRate = 0.2;
	public static final int hids = 240;
	public static final int maxEpcs1 = 1200;
	public static final int maxEpcs2 = 3600;	
	public static final int netCount = 50;
	public static final int plotRes = 200;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		int[][] indices = dataVis.uniqPatIndices(dataMot.uniqPatIndices());
 		double[][] valuesMot = dataMot.valuesIndices(indices);
 		double[][] valuesVis = dataVis.valuesPerspIndices(0,indices);
 		double[][] valuesMot2 = Util.merge(valuesMot, Util.merge(valuesMot, Util.merge(valuesMot, valuesMot)));
 		double[][] valuesVis2 = Util.merge(valuesVis,Util.merge(dataVis.valuesPerspIndices(1,indices),Util.merge(
 				dataVis.valuesPerspIndices(2,indices),dataVis.valuesPerspIndices(3,indices))));
 	
		TexLog log = new TexLog();
 		BALClassicBatch sim = new BALClassicBatch(netCount, simName, errNames, hids, lRate, maxEpcs1, maxEpcs2, plotRes);
		log.addLog(sim.runAndLog2Phase(valuesMot, valuesVis, valuesMot2, valuesVis2));
		log.echo();
		log.export(Conf.logPath(simName));
	}
}
