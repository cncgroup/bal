package test.balold.roboticfinal;

import java.io.IOException;

import base.BAL1;
import base.Conf;
import base.NetUtil;
import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class EpochTestAllP {
	
	public static final String[] errNames = Conf.errorLabels;	
	public static final int sizeVis = 16;
	public static final int sizeMot = 12;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final double lRate = 0.2;
	public static final int hids = 160;
	public static final int maxEpcs = 6000;
	public static final int logRes = 1000;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		int[][] indices = dataVis.uniqPatIndices(dataMot.uniqPatIndices());
 		double[][] valuesMot = dataMot.valuesIndices(indices);
 		double[][] valuesVis = dataVis.valuesPerspIndices(0,indices);
 		double[][] valuesMot2 = Util.merge(valuesMot, Util.merge(valuesMot, Util.merge(valuesMot, valuesMot)));
 		double[][] valuesVis2 = Util.merge(valuesVis,Util.merge(dataVis.valuesPerspIndices(1,indices),Util.merge(
 				dataVis.valuesPerspIndices(2,indices),dataVis.valuesPerspIndices(3,indices))));
 		
		BAL1 net = new BAL1(dataVis.patternSize(),hids,dataMot.patternSize(),lRate);
		double[] trainErr = new double[errNames.length];
		int epoch = 0;
		trainErr = net.testEpoch(valuesVis2,valuesMot2);
		System.out.println("without training:");
		for (int i = 0; i < trainErr.length; i++) {
			System.out.println(errNames[i]+": "+trainErr[i]);
		}
		System.out.println();
		while ((NetUtil.errorsUnsatisfied(trainErr) || epoch < 1) && epoch <= maxEpcs) {
			trainErr = net.epoch(valuesVis2,valuesMot2);
			if (epoch % logRes == 0 && epoch > 0) {
				System.out.println("epoch "+epoch);
				for (int i = 0; i < trainErr.length; i++) {
					System.out.println(errNames[i]+": "+trainErr[i]);
				}
				System.out.println();
			}
			epoch++;
		}
		System.out.println("final performance");
		for (int i = 0; i < trainErr.length; i++) {
			System.out.println(errNames[i]+": "+trainErr[i]);
		}
	}
}
