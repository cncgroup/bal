package test.balold.roboticfinal;

import java.io.IOException;

import test.helpers.BALClassicTest;
import util.TexLog;
import base.Conf;
import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class ParamHidSizeBatchFP {
	
	public static final String logName = "balold-robotic-hidSize-FP.tex";
	public static final int sizeVis = 16;
	public static final int sizeMot = 12;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final int netc = 50;
	public static final int maxEpcs = 2000;
	public static final double lrate = 0.2;
//	public static final int[] hiddenSizes = {160,200,240,280,320};
	public static final int[] hiddenSizes = {10,50,100,150,200,250,300};

	public static void main(String[] args) throws IOException {
				
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		int[][] indices = dataVis.uniqPatIndices(dataMot.uniqPatIndices());
 		double[][] valuesMot = dataMot.valuesIndices(indices);
 		double[][] valuesVis = dataVis.valuesPerspIndices(0,indices);
 		
		BALClassicTest test = new BALClassicTest(netc, maxEpcs);
		TexLog log = new TexLog();
		log.addLog(test.logData(test.testHidSize(hiddenSizes,valuesMot,valuesVis,lrate),"hidden size",Util.intArrayToDbl(hiddenSizes)));
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
