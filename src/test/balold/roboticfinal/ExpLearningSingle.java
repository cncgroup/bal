package test.balold.roboticfinal;

import java.io.IOException;

import util.visu.DataPairsRes;
import util.visu.Plot;
import base.BAL1;
import base.Conf;
import util.Util;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class ExpLearningSingle {
	
	public static final String simName = "balold-learning-single";
	public static final String[] errNames = Conf.errorLabels;	
	public static final int rowHeight = 15;
	public static final int sizeVis = 16;
	public static final int sizeMot = 12;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final double lRate = 0.2;
	public static final int hids = 240;
	public static final int maxEpcs1 = 1200;
	public static final int maxEpcs2 = 3600;
	public static final int plotRes = 100;
	
	public static void main(String[] args) throws IOException {
		
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		int[][] indices = dataVis.uniqPatIndices(dataMot.uniqPatIndices());
 		double[][] valuesMot = dataMot.valuesIndices(indices);
 		double[][] valuesVis = dataVis.valuesPerspIndices(0,indices);
 		double[][] valuesMot2 = Util.merge(valuesMot, Util.merge(valuesMot, Util.merge(valuesMot, valuesMot)));
 		double[][] valuesVis2 = Util.merge(valuesVis,Util.merge(dataVis.valuesPerspIndices(1,indices),Util.merge(
 				dataVis.valuesPerspIndices(2,indices),dataVis.valuesPerspIndices(3,indices))));
 		
		System.out.println("phase one: first perspective");
 		BAL1 net = new BAL1(dataMot.patternSize(),hids,dataVis.patternSize(),Conf.lRate,Conf.wmax,Conf.wmin);
		Plot plot = new Plot(errNames); 
		double[] testErr1 = new double[errNames.length];
		testErr1 = net.testEpoch(valuesMot,valuesVis);
		int epoch1 = 1;
		int epochAbs = 1;
		while (epoch1 < maxEpcs1) {
			net.epoch(valuesMot,valuesVis);
			testErr1 = net.testEpoch(valuesMot,valuesVis);
			if (epoch1 % plotRes == 0) {
				for (int i = 0; i < testErr1.length; i++) {
					plot.addPoint(i, epochAbs+1, testErr1[i]);
				}
				System.out.print(".");
			}
			epoch1++;
			epochAbs++;
		}
		System.out.println();
		System.out.println("epcs: "+epoch1);
		for (int i = 0; i < errNames.length; i++) {
			System.out.println(errNames[i]+": "+testErr1[i]);
		}
		System.out.println();

		System.out.println("phase two: other perspectives");
		double[] testErr2 = new double[errNames.length];
		int epoch2 = 0;
		while (epoch2 < maxEpcs2) {
			net.epoch(valuesMot2,valuesVis2);
			testErr2 = net.testEpoch(valuesMot2,valuesVis2);
			if ((epoch2+1) % plotRes == 0) {
				for (int i = 0; i < errNames.length; i++) {
					plot.addPoint(i, epochAbs+1, testErr2[i]);
				}
				System.out.print(".");
			}			
			epoch2++;
			epochAbs++;
		}
		System.out.println();
		System.out.println("epcs: "+epoch2);
		for (int i = 0; i < errNames.length; i++) {
			System.out.println(errNames[i]+": "+testErr2[i]);
		}
		
		double[][][] patterns = net.testEpochPatterns(valuesMot2,valuesVis2);
		DataPairsRes visRes = new DataPairsRes(patterns);
		visRes.setPxs(Conf.pxsAllP);
		visRes.setRowH(rowHeight);
		visRes.display();
		plot.display();
//		plot.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-performance"+Conf.figExt);
		visRes.saveImage(Conf.pathOutput+Conf.pathFigures+simName+"-patterns"+Conf.figExt);
	}
}
