package test.balold.roboticfinal;

import java.io.IOException;

import test.helpers.BALClassicTest;
import util.TexLog;
import base.Conf;
import data.DataRoboticMot;
import data.DataRoboticVis;

public class ParamLRateBatchFP {
	
	public static final String logName = "balold-robotic-lrate-FP.tex";
	public static final int sizeVis = 16;
	public static final int sizeMot = 12;
	public static final int kVis = 16;
	public static final int kMot = 16;
	public static final int netc = 50;
	public static final int maxEpcs = 2000;
	public static final double[] learningRates = {0.01, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35};
	public static final int hids = 160;
	
	public static void main(String[] args) throws IOException {
				
		DataRoboticVis dataVis = new DataRoboticVis("input/msom/visual_"+sizeVis+"_"+sizeVis+".txt");
		DataRoboticMot dataMot = new DataRoboticMot("input/msom/motor_"+sizeMot+"_"+sizeMot+".txt");
		dataVis.kWinnerTakeAll(kVis);
		dataMot.kWinnerTakeAll(kMot);
		int[][] indices = dataVis.uniqPatIndices(dataMot.uniqPatIndices());
 		double[][] valuesMot = dataMot.valuesIndices(indices);
 		double[][] valuesVis = dataVis.valuesPerspIndices(0,indices);
		
		BALClassicTest test = new BALClassicTest(netc, maxEpcs);
		TexLog log = new TexLog();
		log.addLog(test.logData(test.testLRate(learningRates,valuesMot,valuesVis,hids),"learning rate",learningRates));
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
