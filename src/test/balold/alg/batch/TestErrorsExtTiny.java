package test.balold.alg.batch;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import base.BAL1;
import util.Util;
import base.Conf;
import base.NetUtil;

public class TestErrorsExtTiny {
	public static final int maxEpcs = 50000;
	public static final int netCount = 1000;
	public static final int inps = 4;
	public static final int hids = 2;
	public static final int outs = 4;
	public static final double lRate = 2.0;//Conf.lRate;	
	public static final double[][] inputs = Conf.inputs424;
	public static final String[] errorLabels = Util.addStringBack(Conf.errorLabelsExt,"epochs");
	public static final String outPath = Conf.pathOutput + "log-424-batch-ext-"+netCount+"nets.txt";	
	
	public static void main(String[] args) throws IOException {
		
		double[][][] wIH = new double[netCount][inps][hids];
		double[][][] wHO = new double[netCount][hids][outs];
		double[][][] wOH = new double[netCount][outs][hids];
		double[][][] wHI = new double[netCount][hids][inps];
		double[][] errorsOveral = new double[netCount][errorLabels.length+1];
		// run tests
		for (int n = 0; n < netCount; n++) {
			double[] errors = new double[11];
			int epoch = 0;
			BAL1 net = new BAL1(inps,hids,outs,lRate);
			while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs) {
				errors = net.epochExtInfo(inputs,inputs);
				epoch++;	
			}
			for (int i = 0; i < errors.length; i++) {
				errorsOveral[n][i] = errors[i];
			}
			errorsOveral[n][errors.length] = epoch-1;
			wIH[n] = net.getWeightsIH();
			wHI[n] = net.getWeightsHI();
			wOH[n] = net.getWeightsOH();
			wHO[n] = net.getWeightsHO();
//			System.out.print(".");
		}
		System.out.println();
		
		double[][] resConv = new double[errorLabels.length][2];
		double[][] resAll = new double[errorLabels.length][2];
		int netsConv = 0;
		for (int i = 0; i < resConv.length; i++) {
			ArrayList<Double> converg = new ArrayList<Double>();
			double[] all = new double[netCount];
			for (int j = 0; j < netCount; j++) {
				if (errorsOveral[j][2] == 1.0) {
					converg.add(errorsOveral[j][i]);
					if (i == 0)
						netsConv++;
				}
				all[j] = errorsOveral[j][i];
			}
			resConv[i] = Util.MeanAndStD(converg);
			resAll[i] = Util.MeanAndStD(all);
		}
		
		System.out.println(netsConv);
		
		// compute & write results
		BufferedWriter bw;
		try {
			bw = new BufferedWriter(new FileWriter(new File(outPath)));
			bw.write("learning rate: "+lRate);
			bw.newLine();
			bw.write("max epochs: "+maxEpcs);
			bw.newLine();
			bw.write("convergin nets: "+netsConv+" of "+netCount);
			bw.newLine();
			bw.newLine();
			bw.write("error\tall nets\tsucc nets");
			bw.newLine();
			for (int i = 0; i < resConv.length; i++) {
				bw.write(errorLabels[i]+"\t");
				bw.write(Util.round(resAll[i][0],3)+" +- "+Util.round(resAll[i][1],2)+"\t");
				bw.write(Util.round(resConv[i][0],3)+" +- "+Util.round(resConv[i][1],2));
				bw.newLine();
			}
			bw.newLine();
			bw.newLine();
//			bw.write("net"+"\t");
//			for (int i = 0; i < errorLabels.length; i++) {
//				bw.write(errorLabels[i]+"\t");
//			}
//			bw.newLine();
//			for (int i = 0; i < errorsOveral.length; i++) {
//				bw.write(i+"\t");
//				for (int j = 0; j < errorsOveral[i].length; j++) {
//					bw.write(Util.round(errorsOveral[i][j], 3)+"\t");
//				}
//				bw.newLine();
//			}
//			bw.newLine();
//			bw.newLine();
			bw.write("Weights after training: only converging nets");
			bw.newLine();
			for (int n = 0; n < netCount; n++) {
				if (errorsOveral[n][2] == 1.0) {
					bw.write(NetUtil.weightsToStr(wIH[n]));
					bw.newLine();
					bw.write(NetUtil.weightsToStr(wHO[n]));
					bw.newLine();
					bw.write(NetUtil.weightsToStr(wOH[n]));
					bw.newLine();
					bw.write(NetUtil.weightsToStr(wHI[n]));
					bw.newLine();
					bw.write("***********************************************");
					bw.newLine();
				}
			}
			bw.close();
			System.out.println("log written sucessfully");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
