package test.balold.alg.batch;

import java.io.IOException;

import util.Util;
import base.Conf;
import base.GeneRec;
import base.NetUtil;

public class TestLRateTinyGRClass {
	public static final int maxEpcs = 30000;
	public static final int netCount = 100;
	public static final int inps = 4;
	public static final int hids = 2;
	public static final int outs = 4;
	public static final double wmax = .1;//Conf.wmax;
	public static final double wmin = -.1;//Conf.wmin;
	public static final double[][] inputs = Conf.inputs424;
	
	public static void main(String[] args) throws IOException {
		
		double[] lRates = new double[30];
		for (int i = 0; i < lRates.length; i++) {
			lRates[i] = Util.round(0.1 + 0.1*i,1);
		}
		for (int l = 0; l < lRates.length; l++) {
			int netsSuccesful = 0;
			System.out.print("lambdaHidden: "+lRates[l]+" ");
			for (int n = 0; n < netCount; n++) {
				GeneRec net = new GeneRec(inps,hids,outs,lRates[l]);
				double[] testErrors = new double[3];
				int epoch = 0;
				while ((NetUtil.errorsUnsatisfiedSingle(testErrors) || epoch < 1) && epoch < maxEpcs) {
					net.epoch(inputs,inputs);
					testErrors = net.testEpoch(inputs,inputs);
					epoch++;
				}
				if (testErrors[1] == 1.0) {
					netsSuccesful++;
				}
			}
			System.out.print("succ:"+netsSuccesful+"/"+netCount);
			System.out.println();
//			System.out.println("lambdaHidden: "+lRates[l]+" nets:"+netsSuccesful);
		}
	}
}
