package test.balold.alg.batch;

import base.MLP;
import base.Conf;
import base.NetUtil;
import util.TexLog;
import util.Util;

import java.io.IOException;
import java.util.ArrayList;

public class TestLRateMLP {
	public static final int maxEpcs = 30000;
	public static final int netCount = 100;
	public static final int inps = 4;
	public static final int hids = 2;
	public static final int outs = 4;
	public static final double wmax = -.1;
	public static final double wmin = .1;
	public static final double[][] inputs = Conf.inputs424;
	public static final String logName = "log-424-batch-lrate-mlp.tex";
	public static final int nettypes = 3;
	public static final String[] simName = {"MatLab MLP Weights","Small Gaussian Weights","Small Uniform Weights"};
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException {
		
		double[] lRates = new double[20];
		for (int i = 0; i < lRates.length; i++) {
			lRates[i] = Util.round(0.1 + 0.1*i,1);
		}
		int size = lRates.length;
		
		TexLog log = new TexLog();
		log.addLine("\\subsection*{Setup}");
		log.addLine("\\begin{itemize}");
		log.addLine("\t"+"\\item net count: "+netCount);
		log.addLine("\t"+"\\item max epochs: "+maxEpcs);
		log.addLine("\t"+"\\item weight initialization:");
		log.addLine("\t"+"\\begin{enumerate}");
		log.addLine("\t"+"\\item {\\bf MatLab MLP Weights}: random numbers with normal distribution with mean 0.0 and std 1.0 divided by the squared of the size of presynaptic layer $s$ plus 1 \\");
		log.addLine("\t"+"\\begin{equation}");
		log.addLine("\t\t"+"w_{ij} = \\frac{rndn(0.0,4.0)}{\\sqrt{s+1}}");
		log.addLine("\t"+"\\end{equation}");
		log.addLine("\t"+"\\item {\\bf Small Gaussian Weights}: random numbers with normal distribution with\\ mean 0.0 and std 0.1");
		log.addLine("\t"+"\\item {\\bf Small Uniform Weights}: random numbers with uniform distribution from (-0.1,0.1)");
		log.addLine("\t"+"\\end{enumerate}");
		log.addLine("\t"+"\\end{itemize}");
		log.addLine("\\newpage");
		log.addLine("");
		
		for (int nt = 0; nt < nettypes; nt++) {
			int[] nets = new int[size];
			ArrayList<Double>[] epcs = new ArrayList[size];
			log.addLine("\\subsection{"+simName[nt]+"}");
			// run tests			
			for (int l = 0; l < lRates.length; l++) {
				System.out.println("testing learning rate: "+lRates[l]);
				epcs[l] = new ArrayList<Double>();
				for (int n = 0; n < netCount; n++) {
					double[] testErrors = new double[3];
					int epoch = 0;
					MLP net = new MLP(new int[]{inps,hids,outs},lRates[l],Conf.bp_momentum,Conf.bp_wdecay);
					if (nt == 1)
						net = new MLP(new int[]{inps,hids,outs},lRates[l],Conf.bp_momentum,Conf.bp_wdecay,wmin,wmax,true);
					if (nt == 2)
						net = new MLP(new int[]{inps,hids,outs},lRates[l],Conf.bp_momentum,Conf.bp_wdecay,wmin,wmax,false);
					while ((NetUtil.errorsUnsatisfiedSingle(testErrors) || epoch < 1) && epoch < maxEpcs) {
						net.epoch(inputs,inputs,true);
						testErrors = NetUtil.errorsF1BP(net.epoch(inputs,inputs,false),inputs);
						epoch++;	
					}
					if (testErrors[1] == 1.0) {
						nets[l]++;
						epcs[l].add(epoch*1.0);
					}
					System.out.print(".");
				}
				System.out.println();
			}
			// compute write results
			String[][] resultsTable = new String[size/2][6];
			String[][] plotDataNets = new String[1][size];
			String[][] plotDataEpochs = new String[1][size];
			for (int l = 0; l < size; l++) {
				//compute epcs
				double[] epcsData = Util.MeanAndStD(epcs[l]);
				int x = (l > (size/2-1)) ? l-(size/2) : l;
				int y = (l > (size/2-1)) ? 3 : 0;
				resultsTable[x][y] = lRates[l]+"";
				resultsTable[x][y+1] = nets[l]+"";
				resultsTable[x][y+2] = Util.round(epcsData[0],3)+"";//+" $\\pm$ " + Util.round(epcsData[1],3);
				plotDataNets[0][l] = "("+lRates[l]+","+nets[l]+")";
	//			plotDataEpochs[0][l] = "("+lRates[l]+","+epcsData[0]+") +- (0,"+epcsData[1]+")";
				plotDataEpochs[0][l] = "("+lRates[l]+","+epcsData[0]+")";
			}
			log.addResultsTable(
					new String[] {"lRate","nets","epochs","lRate","nets","epochs"}, 
					resultsTable, 
					"BALNet Encoder 424: comparing learning rate",
					"tab:grbidir-424-lambdaHidden"
			);
			log.addLine("\\begin{figure}");
			log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
			log.addPlot(plotDataNets,null,"learning rate","nets successful");
			log.addLine("\\end{subfigure}");
			log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
			log.addPlot(plotDataEpochs,null,"learning rate","training epochs");
			log.addLine("\\end{subfigure}");
			log.addLine("\\caption{Encoder 4-2-4: comparing learning rate}");
			log.addLine("\\label{fig:grbidir-424-lambdaHidden}");
			log.addLine("\\end{figure}");
			log.addLine("\\newpage");
			log.addLine("");
		}
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
}
