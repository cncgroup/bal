package test.balold.alg.batch;

import java.io.IOException;

import util.Util;
import base.Conf;
import base.BAL1;
import base.NetUtil;

public class TestEpochs {
	public static final int[] maxEpcs = {5000,10000,30000,50000,100000,500000};
	public static final int netCount = 200;
	public static final int inps = 4;
	public static final int hids = 2;
	public static final int outs = 4;
	public static final double lRate = 2.0;//Conf.lRate;	
	public static final double[][] inputs = Conf.inputs424;
	public static final String[] errorLabels = Util.addStringBack(Conf.errorLabelsExt,"epochs");
	public static final String outPath = Conf.pathOutput + "log-424-batch-ext-"+netCount+"nets.txt";	
	
	public static void main(String[] args) throws IOException {
		
		// run tests
		for (int e = 0; e < maxEpcs.length; e++) {
			int succ = 0;
			int maxesucc = 0;
			int minesucc = maxEpcs[maxEpcs.length-1];
			System.out.println((e+1)+". max epcs: "+maxEpcs[e]);
			for (int n = 0; n < netCount; n++) {
				double[] errors = new double[11];
				int epoch = 0;
				BAL1 net = new BAL1(inps,hids,outs,lRate);
				while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs[e]) {
					errors = net.epochExtInfo(inputs,inputs);
					epoch++;	
				}
				if (errors[2] == 1.0) {
					succ++;
					maxesucc = (epoch > maxesucc) ? epoch : maxesucc;
					minesucc = (epoch < minesucc) ? epoch : minesucc;
				}
			}
			System.out.println("nets successful: "+succ+" of "+netCount);
			System.out.println("max epochs to success: "+maxesucc);
			System.out.println("min epochs to success: "+minesucc);
			System.out.println();
		}
	}
}
