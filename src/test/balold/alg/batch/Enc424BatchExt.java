package test.balold.alg.batch;

import java.io.IOException;
import java.util.ArrayList;

import base.BAL1;
import util.TexLog;
import util.Util;
import base.Conf;

public class Enc424BatchExt {
	public static final int maxEpcs = 10000;
	public static final int netCount = 300;
	public static final int inps = 4;
	public static final int hids = 2;
	public static final int outs = 4;
	public static final double lRate = 1.2;//Conf.lRate;	
	public static final double[][] inputs = Conf.inputs424;
	public static final int plotRes = 100;
	public static final int plotTics = maxEpcs/plotRes+1;
	public static final String[] errNm = Conf.errorLabelsExt;
	public static final String logName = "log-424-batch-errors-ext.tex";
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException {
		
		ArrayList<Double>[] errData = new ArrayList[errNm.length];
		for (int i = 0; i < errData.length; i++) {
			errData[i] = new ArrayList<Double>();
		}
		ArrayList<Double>[][] plotDataRaw = new ArrayList[plotTics][errNm.length];
		for (int i = 0; i < plotDataRaw.length; i++) {
			for (int j = 0; j < plotDataRaw[i].length; j++) {
				plotDataRaw[i][j] = new ArrayList<Double>();
			}
		}
		int succ = 0;
		// run tests
		for (int n = 0; n < netCount; n++) {
			double[][] tmp = new double[plotTics][errNm.length];
			double[] errors = new double[errNm.length];
			int epoch = 0;
			int ind = 0;
			BAL1 net = new BAL1(inps,hids,outs,lRate);
			while (epoch < maxEpcs) {
				errors = net.epochExtInfo(inputs,inputs);
				if ((epoch+1) % plotRes == 0) {
					tmp[ind] = errors;
					ind++;
				}
				epoch++;	
			}
			tmp[plotTics-1] = errors;
			System.out.print(".");
			if ((n+1) % 100 == 0)
				System.out.println();
			//count only succ
			if (errors[2] == 1.0 && succ <= 50) {
				succ++;
				for (int i = 0; i < errNm.length; i++) {
					errData[i].add(errors[i]);
				}
				for (int i = 0; i < tmp.length; i++) {
					for (int j = 0; j < errNm.length; j++) {
						plotDataRaw[i][j].add(tmp[i][j]);
					}
				}
			}
		}
		System.out.println();
		System.out.println(succ);
		// compute write results
		String[][] resultsTable = new String[errNm.length][2];
		for (int i = 0; i < errNm.length; i++) {
			resultsTable[i][0] = errNm[i];
			double[] meanstd = Util.MeanAndStD(errData[i]);
			resultsTable[i][1] = Util.round(meanstd[0],3)+" $\\pm$ "+Util.round(meanstd[1],2);
		}
		String[][] plotData = new String[errNm.length][maxEpcs];
		for (int i = 0; i < errNm.length; i++) {
			for (int j = 0; j < plotTics; j++) {
				double[] meanstd = Util.MeanAndStD(plotDataRaw[j][i]);
				plotData[i][j] = "("+(j*plotRes)+","+meanstd[0]+") +- (0,"+meanstd[1]+")";;
			}
		}
		// write log
		TexLog log = new TexLog();
		log.addResultsTable(
				new String[] {"error measure","value"}, 
				resultsTable, 
				"Encoder 424: results extended", 
				"tab:424-results"
		);
		log.addLine("");
		log.addLine("\\begin{figure}[!htbp]");
		log.addLine("\\centering");
		log.addPlot(plotData,errNm,"epoch","error measure");
		log.addLine("\\caption{Encoder 4-2-4: development success rate and weight matrix distance}");
		log.addLine("\\label{fig:424-results}");
		log.addLine("\\end{figure}");
		log.addLine("");
//		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
}
