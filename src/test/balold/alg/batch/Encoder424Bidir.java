package test.balold.alg.batch;

import java.io.IOException;

import base.Conf;
import base.BAL1;
import base.NetUtil;

public class Encoder424Bidir {
	public static final int maxEpcs = 30000;
	public static final int netCount = 50;
	public static final int inps = 4;
	public static final int hids = 2;
	public static final int outs = 4;
	public static final double lRate = 1.2;//Conf.lRate;
	public static final double wmax = 1.0;//Conf.wmax;
	public static final double wmin = 0.0;//Conf.wmin;
	public static final double[][] inputs = Conf.inputs424;
	
	public static void main(String[] args) throws IOException {
		double[] errorsOveral = new double[6];
		int netsSuccesful = 0;
		for (int n = 0; n < netCount; n++) {
			BAL1 net = new BAL1(inps,hids,outs,lRate,wmin,wmax);
			double[] trainErrors = new double[6];
			double[] testErrors = new double[6];
			int epoch = 0;
			while ((NetUtil.errorsUnsatisfied(testErrors) || epoch < 1) && epoch < maxEpcs) {
				trainErrors = net.epoch(inputs,inputs);
				testErrors = net.testEpoch(inputs,inputs);
				epoch++;
			}
			System.out.println("net"+n+"\tepcs:"+epoch+"\tsuccF:"+testErrors[2]+"\tsuccB:"+testErrors[3]);
			for (int i = 0; i < trainErrors.length; i++) {
				errorsOveral[i] += trainErrors[i];
			}
			if (testErrors[2] == 1.0 && testErrors[3] == 1.0)
				netsSuccesful++;
		}
		System.out.println();
		for (int i = 0; i < errorsOveral.length; i++) {
			System.out.println("Average "+Conf.errorLabels[i]+": "+errorsOveral[i]/netCount);
		}
		System.out.println("nets successful: "+netsSuccesful);
	}
}
