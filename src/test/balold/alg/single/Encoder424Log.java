package test.balold.alg.single;

import java.io.IOException;

import util.TexLog;
import util.Util;
import util.visu.Plot;
import base.Conf;
import base.BAL1;
import base.NetUtil;

public class Encoder424Log {
	public static final int maxEpcs = 25000;
	public static final int inps = 4;
	public static final int hids = 2;
	public static final int outs = 4;
	public static final double lRate = 0.2;//Conf.lRate;
	public static final double wmax = 1.0;//Conf.wmax;
	public static final double wmin = 0.0;//Conf.wmin;
	public static final double[][] inputs = Conf.inputs424;
	public static final String figName = "encoder-424";
	public static final String logName = "log-encoder-424.tex";
	
	public static void main(String[] args) throws IOException {
		
		BAL1 net = new BAL1(inps,hids,outs,lRate,wmin,wmax);
		Plot plot = new Plot(Conf.errorLabels);
		double[] trainErrors = new double[6];
		int epoch = 0;
		while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < maxEpcs) {
			if (epoch == 0 || (epoch+1) % 100 == 0) {
//				System.out.println("epoch "+(epoch+1));
				trainErrors = net.epoch(inputs,inputs,true,false);
				for (int i = 0; i < trainErrors.length; i++) {
					plot.addPoint(i, epoch, trainErrors[i]);
				}				
			} else {
				trainErrors = net.epoch(inputs,inputs,true,false);
			}
			epoch++;
		}
		
		System.out.println();
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+trainErrors[i]);
		}
		plot.display();
		double[] testErrors = net.testEpoch(inputs,inputs);
		System.out.println();
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
		// save results
		String[][] resultsTable = new String[7][3];
		for (int i = 0; i < resultsTable.length; i++) {
			resultsTable[i][0] = Conf.testErrors[i];
			if (i == 6) {
				resultsTable[i][1] = ""+epoch;
				resultsTable[i][2] = "1";
			} else if (i > 1) {
				resultsTable[i][1] = ""+Util.round(100.0*trainErrors[i],2)+"\\%";
				resultsTable[i][2] = ""+Util.round(100.0*testErrors[i],2)+"\\%";
			} else {
				resultsTable[i][1] = ""+Util.round(trainErrors[i],3);
				resultsTable[i][2] = ""+Util.round(testErrors[i],3);
			}
		}
		// save plots
		plot.display();
		plot.saveImage(Conf.pathOutput+Conf.pathFigures+figName+"-results"+Conf.figExt);
		// plot results
		double[][][] patterns = net.testEpochPatterns(inputs,inputs);
		for (int i = 0; i < patterns.length; i++) {
			for (int j = 0; j < patterns[i].length; j++) {
				System.out.print(Util.arrayToBinaryString(patterns[i][j])+"\t");
			}
			System.out.println();
		}
		// write log
		String[] setup = {
			"architecture: "+inps+"-"+hids+"-"+outs,
			"weights from: ("+wmin+","+wmax+")",
			"learning rate: "+ lRate,
			"min mse: "+ Conf.minMSE,
			"min pattern succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
		};
		TexLog log = new TexLog();
		log.addLine("\\section{Setup}");
		log.addItemize(setup);
		log.addLine("\\section{Results}");
		log.addResultsTable(
				new String[] {"error","train","test"}, 
				resultsTable, 
				"BALNet Encoder 4-2-4: results",
				"tab:results-encoder-424"
		);
		log.addFigure(
				Conf.pathFigures+figName+"-results"+Conf.figExt, 
				"BALNet Encoder 4-2-4: results",
				"fig:results-encoder-424",
				1.0);
//		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");	
	}
}
