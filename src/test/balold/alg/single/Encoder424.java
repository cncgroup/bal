package test.balold.alg.single;

import java.io.IOException;

import base.BAL1;
import util.visu.Plot;
import base.Conf;
import base.NetUtil;

public class Encoder424 {
	public static final int maxEpcs = 30000;
	public static final int inps = 4;
	public static final int hids = 2;
	public static final int outs = 4;
	public static final double lRate = 1.0;//Conf.lRate;
	public static final double wmax = -.1;//Conf.wmax;
	public static final double wmin = .1;//Conf.wmin;
	public static final double[][] inputs = Conf.inputs424;
	
	public static void main(String[] args) throws IOException {
		BAL1 net = new BAL1(inps,hids,outs,lRate,wmin,wmax);
		Plot plot = new Plot(Conf.errorLabels);
		double[] trainErrors = new double[6];
		int epoch = 0;
		while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < maxEpcs) {
			if (epoch == 0 || (epoch+1) % 100 == 0) {
				trainErrors = net.epoch(inputs,inputs,true,true);
				for (int i = 0; i < trainErrors.length; i++) {
					plot.addPoint(i, epoch, trainErrors[i]);
				}				
			} else {
				trainErrors = net.epoch(inputs,inputs,true,false);
			}
			epoch++;
		}
		System.out.println();
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+trainErrors[i]);
		}
		plot.display();
		double[] testErrors = net.testEpoch(inputs,inputs);
		System.out.println();
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+testErrors[i]);
		}
	}
}
