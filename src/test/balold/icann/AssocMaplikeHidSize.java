package test.balold.icann;

import java.io.IOException;

import test.helpers.BALClassicTest;
import util.TexLog;
import util.Util;
import base.Conf;
import data.DataBinary;

public class AssocMaplikeHidSize {
	
	public static final int k = 3;
	public static final int samples  = 16;
	public static final int samplec = 16;
	public static final String fileIn  = Conf.pathDataMap+"k"+k+"-in-"+samples+"x"+samplec;
	public static final String fileDes = Conf.pathDataMap+"k"+k+"-des-"+samples+"x"+samplec;
	public static final int netc = 50;
	public static final int maxEpcs = 1000;
	public static final String logName = "maplike-"+samples+"x"+samplec+"-hs.tex";
	public static final int[] hiddenSizes = {8,10,12,14,16,18,20,24};
	public static final double lrate = 1.0;
	
	public static void main(String[] args) throws IOException {
				
		BALClassicTest test = new BALClassicTest(netc, maxEpcs);
		DataBinary data = new DataBinary(fileIn,fileDes);
		TexLog log = new TexLog();
		log.addLog(test.logData(test.testHidSize(hiddenSizes,data.dataX(),data.dataY(),lrate),"hidden size",Util.intArrayToDbl(hiddenSizes)));
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
