package test.balold.icann;

import java.io.IOException;

import base.BAL1;
import util.TexLog;
import util.Util;
import base.Conf;
import base.NetUtil;

public class Enc434LRate {
	public static final int maxEpcs = 20000;
	public static final int netc = 100;
	public static final int inps = 4;
	public static final int hids = 3;
	public static final int outs = 4;
	public static final double[][] inputs = Conf.inputs424;
	public static final String logName = "log-icann-434-lrate.tex";
	
	public static void main(String[] args) throws IOException {
		double[] lRates = new double[30];
		for (int i = 0; i < lRates.length; i++) {
			lRates[i] = Util.round(0.1 + 0.1*i,3);
		}
		int size = lRates.length;
		
		double epcs[][] = new double[size][netc];
		double nets[] = new double[size];
		// run tests			
		for (int l = 0; l < lRates.length; l++) {
			System.out.println("testing learning rate: "+lRates[l]);
			for (int n = 0; n < netc; n++) {
				double[] errors = new double[6];
				int epoch = 0;
				BAL1 net = new BAL1(inps,hids,outs,lRates[l]);
				while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs) {
					net.epoch(inputs,inputs);
					errors = net.testEpoch(inputs,inputs);
					epoch++;	
				}
				if (errors[2] == 1.0 && errors[3] == 1.0) {
					nets[l]++;
					epcs[l][n] = epoch*1.0;
				}
				System.out.print(".");
			}
			System.out.println();
		}
		// compute write results
		String[][] resultsTable = new String[2][3];
		
		resultsTable[0][0] = "mean and std";
		double[] mnstd = Util.MeanAndStD(nets);
		resultsTable[0][1] = Util.round(mnstd[0],3)+" $\\pm$ "+Util.round(mnstd[1],3);
		mnstd = Util.MeanAndStD(Util.flatten(epcs));
		resultsTable[0][2] = Util.round(mnstd[0],3)+" $\\pm$ "+Util.round(mnstd[1],3);
		
		resultsTable[1][0] = "maximum";
		int mi = Util.maxIndex(nets);
		resultsTable[1][1] = Math.round(nets[mi])+" lambdaHidden: "+lRates[mi];
		mi = Util.maxIndex(Util.flatten(epcs));
		int mlr = mi/netc;
		int mind = mi%netc;
		resultsTable[1][2] = Math.round(epcs[mlr][mind])+" lambdaHidden: "+lRates[mlr];
		
		String[][] plotDataNets = new String[1][size];
		String[][] plotDataEpochs = new String[1][size];
		for (int l = 0; l < size; l++) {
			plotDataNets[0][l] = "("+lRates[l]+","+nets[l]+")";
			double[] meannstd = Util.MeanAndStD(epcs[l]);
			plotDataEpochs[0][l] = "("+lRates[l]+","+meannstd[0]+") +- (0,"+meannstd[1]+")";
		}
		
		TexLog log = new TexLog();
		log.addResultsTable(
				new String[] {"","nets","epochs"}, 
				resultsTable, 
				"Encoder 4-3-4: comparing learning rates", 
				"tab:434-lrate"
		);
		log.addLine("\\begin{figure}");
		log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
		log.addPlot(plotDataNets,null,"learning rate","nets successful");
		log.addLine("\\end{subfigure}");
		log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
		log.addPlotErrBars(plotDataEpochs,null,"learning rate","training epochs");
		log.addLine("\\end{subfigure}");
		log.addLine("\\caption{Encoder 4-2-4: comparing learning rate}");
		log.addLine("\\label{fig:grbidir-434-lambdaHidden}");
		log.addLine("\\end{figure}");
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
}
