package test.balold.icann;

import java.io.IOException;

import base.BAL1;
import util.visu.Plot;
import base.Conf;
import base.NetUtil;
import data.DataBinary;

public class Assoc {
	
	public static final int[] visParams = {3, 5, 5, 11};
	public static final String[] errlab =  {"bin dist h-","bin dist h+","dist h- h+"};//Conf.errorLabelsExt;
	public static final int k = 10;
	public static final int samplec = 100;
	public static final int samples = 144;
	public static final int maxEpcs = 500;
	public static final String fileIn  = Conf.pathDataKWTA+"k"+k+"-in"+samples+"x"+samplec;
	public static final String fileDes = Conf.pathDataKWTA+"k"+k+"-des"+samples+"x"+samplec;
	public static final int hids = 120; //TODO: best hids
	public static final double lRate = 0.2; //TODO: best lrate
	
	public static void main(String[] args) throws IOException {
		DataBinary data = new DataBinary(fileIn,fileDes);
		BAL1 net = new BAL1(samples,hids,samples,lRate);
//		Plot plot = new Plot(errlab); 
		Plot plot = new Plot(errlab); 
		double[] errors = new double[Conf.errorLabelsExt.length];
		int epoch = 0;
		while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs) {
			errors = net.epochExtInfo(data.dataX(),data.dataY());
			if (epoch % 10 == 0) {
//				for (int i = 0; i < errlab.length; i++) {
//					plot.addPoint(i, epoch, errors[i]);
//				}
				System.out.print(".");
			}
			for (int i = 0; i < errlab.length; i++) {
				plot.addPoint(i, epoch, errors[6+i]);
			}
			epoch++;
		}
		System.out.println();
		for (int i = 0; i < errlab.length; i++) {
			System.out.println(errlab[i]+": "+errors[6+i]);
		}
		System.out.println("epcs: "+epoch);
		plot.display();
		
//		double[][][] patterns = net.testEpochPatternskWTA(data.getInputs(),data.getDesired());
//		DataPairsVisResultsAssym visRes = new DataPairsVisResultsAssym(visParams,patterns);
//		visRes.display();
	}
}
