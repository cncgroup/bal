package test.balold.icann;

import java.io.IOException;

import base.BAL1;
import util.TexLog;
import util.Util;
import util.visu.DataPairsVisResultsAssym;
import base.Conf;
import base.NetUtil;
import data.DataBinary;

public class AssocLog {
	
	public static final int[] visParams = {3, 5, 5, 10};
	public static final int k = 10;
	public static final int samplec = 100;
	public static final int samples = 100;
	public static final String fileIn  = Conf.pathDataKWTA+"k"+k+"-in"+samples+"x"+samplec;
	public static final String fileDes = Conf.pathDataKWTA+"k"+k+"-des"+samples+"x"+samplec;
	public static final String[] errNames = Conf.errorLabels;
	public static final String logName = "log-random-single.tex";
	public static final String dataName = samples+"x"+samples+" k="+k;
	public static final String figResults = Conf.pathFigures+"assoc-robotic-fp-"+dataName+"-results"+Conf.figExt;
	public static final int hids = 80;
	public static final int maxEpcs = 15000;
	public static final int plotRes = 1000;
	
	public static void main(String[] args) throws IOException {
		
		DataBinary data = new DataBinary(fileIn,fileDes);
		BAL1 net = new BAL1(samples,hids,samples,Conf.lRate,Conf.wmax,Conf.wmin);
		String[][][] plotData = new String[errNames.length/2][2][maxEpcs/plotRes+1];
		
		double[] errors = new double[errNames.length];
		int epoch = 0;
		int index = 0;
		while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs) {
			errors = net.epoch(data.dataX(),data.dataY());
			if (epoch % plotRes == 0) {
				for (int i = 0; i < errors.length/2; i++) {
					plotData[i][0][index] = "("+epoch+","+errors[2*i]+")";
					plotData[i][1][index] = "("+epoch+","+errors[2*i+1]+")";
				}	
				index++;
				System.out.print(".");
			}
			epoch++;
		}
		for (int i = 0; i < errors.length; i++) {
			System.out.println(errNames[i]+": "+errors[i]);
		}
		System.out.println("train epochs: "+epoch);
		System.out.println();
//		save results
		String[][] resultsTable = new String[errNames.length+1][2];
		for (int i = 0; i < errNames.length; i++) {
			resultsTable[i][0] = errNames[i];
			resultsTable[i][1] = ""+Util.round(errors[i],3);
		}
		resultsTable[errNames.length][0] = "train epochs";
		resultsTable[errNames.length][1] = ""+epoch;
//		save patterns
		double[][][] patterns = net.testEpochPatterns(data.dataX(),data.dataY());
		System.out.println(patterns[0][0].length);
		System.out.println(patterns[2][0].length);
		DataPairsVisResultsAssym visRes = new DataPairsVisResultsAssym(visParams,patterns);
//		visRes.display();
		visRes.saveImage(Conf.pathOutput+figResults);
//		write log
		String[] setup = {
			"architecture: "+samples+"-"+hids+"-"+samples,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
		};
		TexLog log = new TexLog();
		log.addLine("");
		log.addLine("\\section{BALNet with robotic data: single simulation}");
		log.addLine("\\subsection{Setup parameters}");
		log.addItemize(setup);
		log.addLine("");
		log.addResultsTable(
				new String[] {"error measure","value"}, 
				resultsTable, 
				"Random data "+samples+"x"+samples+": results", 
				""
		);
		log.addLine("");
		log.addLine("\\begin{figure}[!htbp]");
		log.addLine("\\centering");
		for (int i = 0; i < plotData.length; i++) {
			log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
			log.addLine("\\centering");
			log.addPlot(plotData[i],Conf.plotLabels[i],"epoch",Conf.errorNames[i]);
			log.addLine("\\end{subfigure}");
		}
		log.addLine("\\caption{Random data "+samples+"x"+samples+": results}");
		log.addLine("\\end{figure}");
		log.addLine("");
		log.addLine("");
		log.addFigure(figResults, "Random data "+samples+"x"+samples+": pattern match", "", 0.9);	
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
}
