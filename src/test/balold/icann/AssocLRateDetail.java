package test.balold.icann;

import java.io.IOException;

import test.helpers.BALClassicTest;
import util.TexLog;
import util.Util;
import base.Conf;
import data.DataBinary;

public class AssocLRateDetail {
	
	public static final int samplec = 100;
	public static final int samples = 144;
	public static final int netc = 50;
	public static final int maxEpcs = 3000;
	public static final int k = 12;
	public static final int hids = 120;
	public static final String fileIn  = Conf.pathDataKWTA+"k"+k+"-in"+samples+"x"+samplec;
	public static final String fileDes = Conf.pathDataKWTA+"k"+k+"-des"+samples+"x"+samplec;
	public static final String logName = "assoc-"+samples+"x"+samplec+"-k"+k+"-lrate-detail.tex";
	
	public static void main(String[] args) throws IOException {
		
		double[] learningRates = new double[20];
		for (int i = 0; i < learningRates.length; i++) {
			learningRates[i] = Util.round(0.2+0.01*i,3);
		}
		
		BALClassicTest test = new BALClassicTest(netc, maxEpcs);
		DataBinary data = new DataBinary(fileIn,fileDes);
		TexLog log = new TexLog();
		log.addLog(test.logData(test.testLRate(learningRates,data.dataX(),data.dataY(),hids),"learning rate",learningRates));
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
