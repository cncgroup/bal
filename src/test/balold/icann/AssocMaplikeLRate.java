package test.balold.icann;

import java.io.IOException;

import test.helpers.BALClassicTest;
import util.TexLog;
import base.Conf;
import data.DataBinary;

public class AssocMaplikeLRate {
	
	public static final int k = 3;
	public static final int samples  = 16;
	public static final int samplec = 16;
	public static final String fileIn  = Conf.pathDataMap+"k"+k+"-in-"+samples+"x"+samplec;
	public static final String fileDes = Conf.pathDataMap+"k"+k+"-des-"+samples+"x"+samplec;
	public static final int netc = 50;
	public static final int maxEpcs = 1000;
	public static final int hids = 12;
	public static final String logName = "maplike-"+samples+"x"+samplec+"-k10-lambdaHidden.tex";
	public static final double[] learningRates = {0.1, 0.2, 0.3, 0.4, 0.5, 0.8, 1.0, 1.2};
	
	public static void main(String[] args) throws IOException {
				
		BALClassicTest test = new BALClassicTest(netc, maxEpcs);
		DataBinary data = new DataBinary(fileIn,fileDes);
		TexLog log = new TexLog();
		log.addLog(test.logData(test.testLRate(learningRates,data.dataX(),data.dataY(),hids),"learning rate",learningRates));
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
