package test.balold.icann;

import java.io.IOException;

import test.helpers.BALClassicTest;
import util.TexLog;
import util.Util;
import base.Conf;
import data.DataBinary;

public class AssocHidSize {
	
	public static final int samplec = 100;
	public static final int samples = 144;
	public static final int netc = 50;
	public static final int maxEpcs = 3000;
	public static final int k = 12;
	public static final String fileIn  = Conf.pathDataKWTA+"k"+k+"-in"+samples+"x"+samplec;
	public static final String fileDes = Conf.pathDataKWTA+"k"+k+"-des"+samples+"x"+samplec;
	public static final String logName = "assoc-"+samples+"x"+samplec+"-k"+k+"-hidsize.tex";
	public static final int[] hiddenSizes = {80, 100, 120, 140, 160, 180};
	public static final double lrate = 0.2;
	
	public static void main(String[] args) throws IOException {
				
		BALClassicTest test = new BALClassicTest(netc, maxEpcs);
		DataBinary data = new DataBinary(fileIn,fileDes);
		TexLog log = new TexLog();
		log.addLog(test.logData(test.testHidSize(hiddenSizes,data.dataX(),data.dataY(),lrate),"hidden size",Util.intArrayToDbl(hiddenSizes)));
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
