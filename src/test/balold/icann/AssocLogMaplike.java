package test.balold.icann;

import java.io.IOException;

import base.Conf;
import base.BAL1;
import base.NetUtil;
import data.DataBinary;

public class AssocLogMaplike {
	
	public static final int[] visParams = {10, 10, 10, 4, 4};
	public static final int k = 3;
	public static final int sampleSize  = 16;
	public static final int sampleCount = 16;
	public static final String fileIn  = Conf.pathDataMap+"k"+k+"-in-"+sampleSize+"x"+sampleCount;
	public static final String fileDes = Conf.pathDataMap+"k"+k+"-des-"+sampleSize+"x"+sampleCount;
	public static final String figName = "assoc-random-maplike-k"+k+"-"+"s"+sampleCount;
	public static final String logName = "log-single-random-maplike-k"+k+"s"+sampleCount+".tex";
	public static final int hids = 14;
	public static final double lRate = 1.0;
	public static final int maxEpcs = 1000;
	
	public static void main(String[] args) throws IOException {
				
		DataBinary data = new DataBinary(fileIn,fileDes);
		double[][] dataIn = data.dataX();
		double[][] dataDes = data.dataY();
//		DataPairsVis vis = new DataPairsVis(visParams, dataIn, dataDes);
//		vis.display();
//		vis.saveImage(Conf.pathOutput+"data-complex"+Conf.figExt);
		
		BAL1 net = new BAL1(sampleSize,hids,sampleSize,lRate);
		double[] trainErrors = new double[6];
		int epoch = 0;
		
		while ((NetUtil.errorsUnsatisfied(trainErrors) || epoch < 1) && epoch < maxEpcs) {
			trainErrors = net.epoch(dataIn,dataDes);
			epoch++;
			if (epoch % 100 == 0) 
				System.out.print(".");
		}
		System.out.println("training epochs: "+epoch);
		System.out.println();
		for (int i = 0; i < trainErrors.length; i++) {
			System.out.println(Conf.errorLabels[i]+": "+trainErrors[i]);
		}

		double[][][] patterns = net.testEpochPatterns(dataIn,dataDes);
		DataPairsVisResults visRes = new DataPairsVisResults(visParams,patterns);
		visRes.display();
		visRes.saveImage(Conf.pathOutput+"results-assoc-complex"+Conf.figExt);
	}
}
