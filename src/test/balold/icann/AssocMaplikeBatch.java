package test.balold.icann;

import java.io.IOException;

import base.BAL1;
import util.TexLog;
import util.Util;
import base.Conf;
import data.DataBinary;

public class AssocMaplikeBatch {

	public static final String logName = "assoc-maplike-batch.tex";
	public static final int[] visParams = {3, 5, 5, 11};
	public static final String[] errNames = Conf.errorLabels;
	public static final int k = 3;
	public static final int samples  = 16;
	public static final int samplec = 16;
	public static final String fileIn  = Conf.pathDataMap+"k"+k+"-in-"+samples+"x"+samplec;
	public static final String fileDes = Conf.pathDataMap+"k"+k+"-des-"+samples+"x"+samplec;
	public static final String dataName = samples+"x"+samples+" k="+k;
	public static final String figResults = Conf.pathFigures+"assoc-robotic-fp-"+dataName+"-results";
	public static final int maxEpcs = 1000;
	public static final int plotRes = 50;
	public static final int netCount = 50;
	public static final int hids = 14; //TODO: best hids
	public static final double lRate = 1.0; //TODO: best lrate
	
	public static void main(String[] args) throws IOException {
		
		DataBinary data = new DataBinary(fileIn,fileDes);		
		double[][][] trainErr = new double[maxEpcs/plotRes+1][errNames.length][netCount];
		
		for (int n = 0; n < netCount; n++) {
			BAL1 net = new BAL1(samples,hids,samples,lRate);
			double[] trainErrors = new double[errNames.length];
			int epoch = 0;
			int index = 0;
			while (epoch <= maxEpcs) {
				trainErrors = net.epoch(data.dataX(),data.dataY());
				if (epoch % plotRes == 0) {	
					for (int j = 0; j < errNames.length; j++) {
						trainErr[index][j][n] = trainErrors[j];
					}
					index++;
					System.out.print(".");
				}
				epoch++;
			}
			System.out.println();
		}
		
		String[][][] plotData = new String[errNames.length/2][2][maxEpcs/plotRes+1];
		for (int i = 0; i < trainErr.length; i++) {
			for (int j = 0; j < errNames.length; j++) {
				double[] meanstd = Util.MeanAndStD(trainErr[i][j]);
				plotData[j/2][j%2][i] = "("+(plotRes*i)+","+meanstd[0]+") +- (0,"+meanstd[1]+")";
			}
		}
//		save results
		double[][] trainErrTmp = new double[errNames.length][netCount*(maxEpcs/plotRes+1)];
		for (int n = 0; n < trainErr[0][0].length; n++) {
			for (int e = 0; e < errNames.length; e++) {
				for (int i = 0; i < trainErr.length; i++) {
					trainErrTmp[e][n*(maxEpcs/plotRes+1)+i] = trainErr[i][e][n];
				}
			}
		}
		String[][] resultsTable = new String[errNames.length][2];
		for (int i = 0; i < resultsTable.length; i++) {
			resultsTable[i][0] = errNames[i];
			double[] tmp = Util.MeanAndStD(trainErrTmp[i]);
			resultsTable[i][1] = ""+Util.round(tmp[0],3)+" $\\pm$ "+Util.round(tmp[1],3);
		}
//		write log
		String[] setup = {
			"architecture: "+samples+"-"+hids+"-"+samples,
			"weights from: ("+Conf.wmin+","+Conf.wmax+")",
			"learning rate: "+ Conf.lRate,
			"min mse: "+ Conf.minMSE,
			"min succ: "+ Conf.minPatSucc,
			"max epochs: "+ maxEpcs,
		};
		TexLog log = new TexLog();
		log.addLine("");
		log.addLine("\\section{BALNet with robotic data: single simulation}");
		log.addLine("\\subsection{Setup parameters}");
		log.addItemize(setup);
		log.addLine("");
		log.addResultsTable(
				new String[] {"error","train","test"}, 
				resultsTable, 
				"MSOM robotic data first perspective "+dataName, 
				"tab:grbidir-single-"+dataName
		);
		log.addLine("");
		log.addLine("\\begin{figure}[!htbp]");
		log.addLine("\\centering");
		for (int i = 0; i < plotData.length; i++) {
			log.addLine("\\begin{subfigure}[b]{0.45\\textwidth}");
			log.addLine("\\centering");
			log.addPlotErrBars(plotData[i],Conf.plotLabels[i],"epoch",Conf.errorNames[i]);
			log.addLine("\\end{subfigure}");
		}
		log.addLine("\\caption{Results for single instance of BALNet Associator with robotic data}");
		log.addLine("\\label{fig:assoc-single-robotic}");
		log.addLine("\\end{figure}");
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
}
