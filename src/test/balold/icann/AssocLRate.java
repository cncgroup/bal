package test.balold.icann;

import java.io.IOException;

import test.helpers.BALClassicTest;
import util.TexLog;
import base.Conf;
import data.DataBinary;

public class AssocLRate {
	
	public static final int samplec = 100;
	public static final int samples = 144;
	public static final int netc = 50;
	public static final int maxEpcs = 3000;
	public static final int k = 12;
	public static final int hids = 120;
	public static final String logName = "assoc-"+samples+"x"+samplec+"-k"+k+"-lrate.tex";
	public static final double[] learningRates = {0.05, 0.1, 0.2, 0.3, 0.4, 0.5};
	
	public static void main(String[] args) throws IOException {
				
		BALClassicTest test = new BALClassicTest(netc, maxEpcs);
		String pathIn = "input/binary-kWTA/k"+k+"-in"+samples+"x"+samplec;
		String pathDes = "input/binary-kWTA/k"+k+"-des"+samples+"x"+samplec;
		DataBinary data = new DataBinary(pathIn,pathDes);
		TexLog log = new TexLog();
		log.addLog(test.logData(test.testLRate(learningRates,data.dataX(),data.dataY(),hids),"learning rate",learningRates));
		log.echo();
		log.export(Conf.pathOutput+logName);
		System.out.println("Finished writing \""+logName+"\"");
	}
	
}
