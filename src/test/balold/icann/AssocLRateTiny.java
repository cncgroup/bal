package test.balold.icann;

import java.io.IOException;
import java.util.Arrays;

import base.BAL1;
import base.Conf;
import base.NetUtil;
import data.DataBinary;

public class AssocLRateTiny {

	public static final int k = 12;
	public static final int samplec = 100;
	public static final int samples = 144;
	public static final String fileIn  = Conf.pathDataKWTA+"k"+k+"-in"+samples+"x"+samplec;
	public static final String fileDes = Conf.pathDataKWTA+"k"+k+"-des"+samples+"x"+samplec;
	public static final int hids = 100;
	public static final int maxEpcs = 5000;
	public static final int netCount = 5;
	
	public static void main(String[] args) throws IOException {
		
		DataBinary data = new DataBinary(fileIn,fileDes);
		
//		double[] lRates = new double[30];
//		for (int i = 0; i < lRates.length; i++) {
//			lRates[i] = Util.round(0.1 + 0.1*i,1);
//		}
//		double[] lRates = {0.05, 0.1, 0.2, 0.25, 0.3, 0.4, 0.5, 0.8, 1.0, 1.4, 1.8, 2.0};
		double[] lRates = {0.1, 0.2, 0.3};
//		double[] lRates = {0.4, 0.5, 0.8, 1.0, 1.4, 1.8, 2.0};
	
		for (int l = 0; l < lRates.length; l++) {
			int netsSuccesful = 0;
			int minepc = maxEpcs;
			int maxepc = 0;
			System.out.println("lambdaHidden: "+lRates[l]);
			for (int n = 0; n < netCount; n++) {
				BAL1 net = new BAL1(samples,hids,samples,lRates[l]);
				double[] errors = new double[6];
				int epoch = 0;
				while ((NetUtil.errorsUnsatisfied(errors) || epoch < 1) && epoch < maxEpcs) {
					errors = net.epoch(data.dataX(),data.dataY());
					epoch++;
				}
				if (errors[4] >= 0.99 && errors[5] >= 0.99) {
					netsSuccesful++;
					minepc = (epoch < minepc) ? epoch : minepc;
					maxepc = (epoch > maxepc) ? epoch : maxepc;
				}
				System.out.println(Arrays.toString(errors));
			}
			System.out.println("succ:"+netsSuccesful+"/"+netCount);
			System.out.println("max epcs: "+maxepc);
			System.out.println("min epcs: "+minepc);
			System.out.println();
//			System.out.println("lambdaHidden: "+lRates[l]+" nets:"+netsSuccesful);
		}
	}
}
